#include <unistd.h>
#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/method/dca/dca.hpp"

#define HANDLE 1
#define HANDLE_TMP 4
#define HANDLE_IO_IN 2
#define HANDLE_IO_OUT 3
#define HANDLE_IO_EXC 5

#define DATA_ORIG "data_comp"
#define DATA_DCA "data_dca"
#define DATA_EXC "data_dca_exc"
#define TMP_DCA "tmp_dca"
#define TMP_EXC "tmp_exc"
#define TMP_UNDCA "tmp_undca"

class CompDCATestSuite: public CxxTest::TestSuite {
	CompDCA *comp;
public:
	void setUp() {
		comp = new CompDCA(HANDLE);
	}
	void tearDown() {
		delete comp;
	}

	void testParameters() {
		unsigned int v;
		TS_ASSERT_EQUALS(comp->setParameter(-1, NULL), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_MAXLENGTH, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_RAW, NULL), EXCOM_ERR_MEMORY);
		v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_MAXLENGTH, &v), EXCOM_ERR_VALUE);
		v = 256;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_MAXLENGTH, &v), EXCOM_ERR_VALUE);
		v = 20;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_MAXLENGTH, &v), EXCOM_ERR_OK);
		v = 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_RAW, &v), EXCOM_ERR_OK);
		v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_RAW, &v), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(comp->getValue(-1, NULL), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_DCA_MAXLENGTH, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_DCA_RAW, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_DCA_MAXLENGTH, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 20u);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_DCA_RAW, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 0u);

		// a value common to all compression modules
		int u;
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_COMP_RESULT, &u), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(u, EXCOM_ERR_LATER);
	}

	void testIOInputModules() {
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemReader *except = new IOMemReader(HANDLE_IO_EXC);
		// invalid pointer to a module
		TS_ASSERT_EQUALS(comp->connectIOModule(NULL, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->connectIOModule(NULL, EXCOM_CONN_DCA_EXC_INPUT), EXCOM_ERR_MEMORY);
		// invalid connection type
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, -1), EXCOM_ERR_PARAM);
		// input module connected as an output module
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->connectIOModule(except, EXCOM_CONN_DCA_EXC_OUTPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(except, EXCOM_CONN_DCA_EXC_INPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		TS_ASSERT_EQUALS(comp->connectIOModule(except, EXCOM_CONN_DCA_EXC_INPUT), EXCOM_ERR_DUPLICATE);
		delete reader;
		delete except;
	}

	void testIOOutputModules() {
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		IOMemWriter *except = new IOMemWriter(HANDLE_IO_EXC);
		// output module connected as an input module
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->connectIOModule(except, EXCOM_CONN_DCA_EXC_INPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(except, EXCOM_CONN_DCA_EXC_OUTPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_DUPLICATE);
		TS_ASSERT_EQUALS(comp->connectIOModule(except, EXCOM_CONN_DCA_EXC_OUTPUT), EXCOM_ERR_DUPLICATE);
		delete writer;
	}

	void compareFiles(const char *a, const char *b)
	{
		long sizeA, sizeB;
		FILE *A, *B;
		unsigned char *dataA = NULL, *dataB = NULL;
		A = fopen(a, "rb");
		TS_ASSERT_DIFFERS(A, (FILE*)NULL);
		fseek(A, 0, SEEK_END);
		sizeA = ftell(A);
		fseek(A, 0, SEEK_SET);
		dataA = new unsigned char[sizeA];
		TS_ASSERT_EQUALS(fread(dataA, sizeA, 1, A), 1u);
		fclose(A);

		B = fopen(b, "rb");
		TS_ASSERT_DIFFERS(B, (FILE*)NULL);
		fseek(B, 0, SEEK_END);
		sizeB = ftell(B);
		fseek(B, 0, SEEK_SET);
		TS_ASSERT_EQUALS(sizeA, sizeB);
		if (sizeA == sizeB) {
			dataB = new unsigned char[sizeB];
			TS_ASSERT_EQUALS(fread(dataB, sizeB, 1, B), 1u);
			TS_ASSERT_SAME_DATA(dataA, dataB, sizeA);
		}
		fclose(B);

		if (dataA != NULL) delete dataA;
		if (dataB != NULL) delete dataB;
	}

	void testCompression() {
		unsigned int v;

		// input and output modules
		IOFileReader *reader = new IOFileReader(HANDLE_IO_IN);
		IOFileWriter *writer = new IOFileWriter(HANDLE_IO_OUT);
		IOFileWriter *except = new IOFileWriter(HANDLE_IO_EXC);
		TS_ASSERT_EQUALS(reader->openFile(DATA_ORIG), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile(TMP_DCA), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(except->openFile(TMP_EXC), EXCOM_ERR_OK);

		// running shouldn't succeed without an input I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// ...or without output I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// ...or without exception writer
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(except, EXCOM_CONN_DCA_EXC_OUTPUT), EXCOM_ERR_OK);

		// set parameters
		v = 30;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_MAXLENGTH, &v), EXCOM_ERR_OK);
		v = 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_RAW, &v), EXCOM_ERR_OK);
		// run the compression
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether output data matches our specimen
		compareFiles(TMP_DCA, DATA_DCA);
		compareFiles(TMP_EXC, DATA_EXC);

		delete reader;
		delete writer;
		delete except;

		// decompression
		CompDCA *decomp = new CompDCA(HANDLE_TMP);
		reader = new IOFileReader(HANDLE_IO_IN);
		writer = new IOFileWriter(HANDLE_IO_OUT);
		IOFileReader *exc2 = new IOFileReader(HANDLE_IO_EXC);
		TS_ASSERT_EQUALS(reader->openFile(TMP_DCA), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile(TMP_UNDCA), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc2->openFile(TMP_EXC), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(decomp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(exc2, EXCOM_CONN_DCA_EXC_INPUT), EXCOM_ERR_OK);

		// set parameters
		v = 30;
		TS_ASSERT_EQUALS(decomp->setParameter(EXCOM_PARAM_DCA_MAXLENGTH, &v), EXCOM_ERR_OK);
		v = 1;
		TS_ASSERT_EQUALS(decomp->setParameter(EXCOM_PARAM_DCA_RAW, &v), EXCOM_ERR_OK);
		// run the decompression
		TS_ASSERT_EQUALS(decomp->run(), EXCOM_ERR_OK);

		// check, whether output data matches input original
		compareFiles(TMP_UNDCA, DATA_ORIG);

		delete reader;
		delete writer;
		delete exc2;
		delete decomp;

		// delete temporary files
		TS_ASSERT_EQUALS(unlink(TMP_DCA), 0);
		TS_ASSERT_EQUALS(unlink(TMP_EXC), 0);
		TS_ASSERT_EQUALS(unlink(TMP_UNDCA), 0);
	}

	void testHeader() {
		unsigned char bin[1] = {'a'};
		unsigned char x[16];
		unsigned char y[16];
		unsigned char bout[1];
		unsigned int v;
		unsigned int size, size2;
		// set parameters
		v = 3;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_MAXLENGTH, &v), EXCOM_ERR_OK);
		// turn raw off
		v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_DCA_RAW, &v), EXCOM_ERR_OK);

		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		IOMemWriter *exc1 = new IOMemWriter(HANDLE_IO_EXC);
		TS_ASSERT_EQUALS(reader->attachMemory(bin, 1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(x, sizeof(x)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc1->attachMemory(y, sizeof(y)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(exc1, EXCOM_CONN_DCA_EXC_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &size), EXCOM_ERR_OK);
		TS_ASSERT_LESS_THAN_EQUALS(size, sizeof(x));
		TS_ASSERT_EQUALS(exc1->getValue(EXCOM_VALUE_IO_POSITION, &size2), EXCOM_ERR_OK);
		TS_ASSERT_LESS_THAN_EQUALS(size2, sizeof(y));

		delete reader;
		delete writer;
		delete exc1;

		CompDCA *decomp = new CompDCA(HANDLE_TMP);
		reader = new IOMemReader(HANDLE_IO_IN);
		writer = new IOMemWriter(HANDLE_IO_OUT);
		IOMemReader *exc2 = new IOMemReader(HANDLE_IO_EXC);
		TS_ASSERT_EQUALS(reader->attachMemory(x, size), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(bout, 1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(exc2->attachMemory(y, size2), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(exc2, EXCOM_CONN_DCA_EXC_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->run(), EXCOM_ERR_OK);

		// verify parameters obtained from the header
		TS_ASSERT_EQUALS(decomp->getValue(EXCOM_VALUE_DCA_MAXLENGTH, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 3u);
		TS_ASSERT_EQUALS(decomp->getValue(EXCOM_VALUE_DCA_RAW, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 0u);

		delete reader;
		delete writer;
		delete exc2;
		delete decomp;
	}
};


