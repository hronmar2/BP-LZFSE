/*
 * File:       method/integer_compression/heuristic.cpp
 * Purpose:    Implementation of the heuristic functor
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file heuristic.cpp
 *  \brief Implementation of the heuristic functor
 *
 */

#include "method.hpp"
#include <climits>
#include <cstring>

Heuristic::Heuristic(BitBuffer& bb): Method(bb) {
	memset(length_bit, 0, METHODS*sizeof(unsigned long));
	memset(length_giga, 0, METHODS*sizeof(unsigned long));
	memset(length_func, 0, METHODS*sizeof(func_t));
	length_func[FIBONACCI] = Fibonacci::length;
	length_func[GOLOMB] = Golomb::length;
	length_func[RICE] = Rice::length;
	length_func[ALPHA] = Alpha::length;
	length_func[BETA] = Beta::length;
	length_func[BETAPRIME] = BetaPrime::length;
	length_func[GAMMA] = Gamma::length;
	length_func[GAMMAPRIME] = GammaPrime::length;
	length_func[DELTA] = Delta::length;
	length_func[DELTAPRIME] = DeltaPrime::length;
	length_func[OMEGA] = Omega::length;
	length_func[OMEGAPRIME] = OmegaPrime::length;
}

Heuristic::~Heuristic() {
	unsigned long min_giga = INT_MAX;
	unsigned long min_bit = INT_MAX;
	int best_method = 0;

	for (int i = 0; i < METHODS; i++)
		if (length_func[i]) {
			if (!length_giga[i]) printf("Method %d: %lu bits\n", i, length_bit[i]);
			else printf("Method %d: %lu Gb and %lu bits\n", i, length_giga[i], length_bit[i]);
			if (i != BETA && i != BETAPRIME)
				if (length_giga[i] < min_giga) {
					min_giga = length_giga[i];
					min_bit = length_bit[i];
					best_method = i;
				}
				else if (length_giga[i] == min_giga && length_bit[i] < min_bit) {
					min_bit = length_bit[i];
					best_method = i;
				}
		}

	switch (best_method) {
		default:
			printf("Best method is #%d\n", best_method);
			break;
		case FIBONACCI:
			printf("Best method is Fibonacci code (method %d)\n", FIBONACCI);
			break;
		case GAMMA:
			printf("Best method is Gamma code (method %d)\n", GAMMA);
			break;
		case DELTA:
			printf("Best method is Delta code (method %d)\n", DELTA);
			break;
		case OMEGA:
			printf("Best method is Omega code (method %d)\n", OMEGA);
			break;
		case RICE:
			printf("Best method is Rice code (method %d)\n", RICE);
			break;
	}
}

/*
 * Specialni pristup k XML souborum
 * nezapomenout na diferencni kodovani
 * filtry, ktere rozdeli soubor na 'sablonu' a data a po dekompresi zase slepi zpet
 * heuristika, ktera na zaklade typu souboru? par vstupnich dat, vhodne zvoli pouzitou metodu
 * data mohou byt jak z muzikologie, tak napt telemetrie (satelit?)
 * kodovat XML do vice streamu
 */

void Heuristic::operator() (unsigned long data) {
	if (!data) return;
	for (int i = 0; i < METHODS; i++) {
		if (length_func[i]) {
			length_bit[i] += length_func[i](data);
			if (length_bit[i] & 0xC0000000) {
				length_giga[i] += 1;
				length_bit[i] -= 0x40000000;
			}
			printf("%d: %lu\t", i, length_func[i](data));
		}
	}
	printf("\n");
}

unsigned long Heuristic::operator() () {
	throw "This is not decompressing method!";
}
