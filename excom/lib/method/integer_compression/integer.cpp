/*
 * File:       method/integer_compression/integer.cpp
 * Purpose:    Implementation of a wrapper class for the integer compression module
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include "integer.hpp"
#include "coder.hpp"
#include <sstream>
#include <cctype>
#include <cstdlib>
#include <bitset>
#include <cstring>

int CompInteger::connectIOModule(IOModule *module, int connection_type) {
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompInteger::setOperation(enum exc_oper_e _operation) {
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompInteger::setParameter(int parameter, void *value) {
	switch (parameter) {
	// parameter for selecting coding method
	case EXCOM_PARAM_INTEGER_METHOD:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		method = *((unsigned*)value);
		break;
	// parameter for some methods (fibonacci order, rice/golomb coeficient)
	case EXCOM_PARAM_INTEGER_PARAM:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		param = *((unsigned*)value);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompInteger::checkConnection() {
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompInteger::runCompress() {
	int c;
	bool reading = true;
	std::stringbuf buffer;

	try {
		Coder coder(writer, NULL, method, param);
		while (c = reader->readByte(), c!= EOF) {
			if (isdigit(c)) {
				buffer.sputc(c);
				reading = true;
			}
			else if (reading) {
				long value = atol(buffer.str().c_str());
				buffer.str("");
				coder.encode(value);
				reading = false;
			}
		}
		writer->eof();
	}
	catch (const char* c) {
		printf("Error: %s\n\n", c);
		throw;
	}
	return EXCOM_ERR_OK;
}

int CompInteger::runDecompress() {
	bool eof = false;

	try {
		Coder coder(NULL, reader, method, param);
		unsigned long value;
		while (coder.decode(value)) {
			char buffer[256];
			memset(buffer, 0, 256);
			sprintf(buffer, "%lu", value);
			writer->writeBlock((unsigned char*)buffer, strlen(buffer));
			// write delimiter
			writer->writeByte(' ');
		}
		writer->eof();
	}
	catch (const char* c) {
		printf("Error: %s\n\n", c);
		throw;
	}
	return EXCOM_ERR_OK;
}

int CompInteger::run() {
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS)
		res = runCompress();
	else
		res = runDecompress();

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}


