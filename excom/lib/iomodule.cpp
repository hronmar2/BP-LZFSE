/*
 * File:       iomodule.cpp
 * Purpose:    Implmentation of I/O module classes
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             Michal Valach <valacmic@fel.cvut.cz>, 2011
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *               2011 Michal Valach
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "iomodule.hpp"
#include "handles.hpp"

/*!
 * \brief An array of bitmasks. n-th member, when applied using bitwise & operation,
 *        extracts n low-order bits (clears all other bits to 0)
 */
static const unsigned char mask[9] = {
	0x00, 0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff
};

/*!
 * \brief An array of bitmasks. n-th member, when applied using bitwise & operation,
 *        extracts n high-order bits (clears all other bits to 0)
 */
static const unsigned char mask_high[9] = {
	0xFF, 0xFE, 0xFC, 0xF8, 0xF0, 0xE0, 0xC0, 0x80, 0x00
};

IOModule::IOModule(unsigned int handle)
{
	hand_alloc.registerObject(handle, HANDLE_IO, this);
	module_handle = handle;
	buffer_size = 0;
	buffer_occ = 0;
	buffer_pos = 0;
	bit_offset = 0;
	buffer = NULL;
	stream_pos = 0;
	connected = 0;
}

IOModule::~IOModule()
{
	if (buffer != NULL) delete [] buffer;
	hand_alloc.freeHandle(module_handle);
}

void IOModule::_allocateBuffer()
{
	if (buffer != NULL) {
		delete [] buffer;
		buffer = NULL;
	}
	buffer_pos = 0;
	buffer_occ = 0;
	if (buffer_size == 0) return;
	buffer = new unsigned char[buffer_size];
}

int IOModule::setParameter(int parameter, void *value) {
	if (parameter == EXCOM_PARAM_IO_BUFFER_SIZE) {
		if ((buffer != NULL) && // there is already some other buffer
		    (buffer_occ > 0) && // it's not empty
		    (buffer_pos < buffer_occ) // there are still bytes left in the buffer
                    ) {
			// we can't change the size of this buffer
			return EXCOM_ERR_NOT_NOW;
		}
		if (value == NULL) {
			return EXCOM_ERR_MEMORY;
		}
		buffer_size = *(unsigned int*)value;
		_allocateBuffer();
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_PARAM;
	}
}

int IOModule::getValue(int parameter, void *value) {
	switch(parameter) {
	case EXCOM_VALUE_IO_BUFFER_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		*((unsigned int*)value) = buffer_size;
		return EXCOM_ERR_OK;
	case EXCOM_VALUE_IO_POSITION:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		*((unsigned int*)value) = stream_pos;
		return EXCOM_ERR_OK;
	default:
		return EXCOM_ERR_PARAM;
	}
}

//------------------------------------------------------------------

int IOFile::_openFile(const char *file_name, const char *mode)
{
	if (file_name == NULL) return EXCOM_ERR_MEMORY;
	if (file_handle != NULL) return EXCOM_ERR_DUPLICATE;
	file_handle = fopen(file_name, mode);
	if (file_handle == NULL) return EXCOM_ERR_OPEN;
	return EXCOM_ERR_OK;
}

//------------------------------------------------------------------

int IOMemory::attachMemory(void *_base, unsigned int _length)
{
	if (_base == NULL) return EXCOM_ERR_MEMORY;
	if (base_pointer != NULL) return EXCOM_ERR_DUPLICATE;
	base_pointer = (unsigned char*) _base;
	length = _length;
	return EXCOM_ERR_OK;
}

//------------------------------------------------------------------

IOPipe::IOPipe() {
	chain_handle = 0;
	input_attached = 0;
	output_attached = 0;
	at_eof = 0;

	pipe_size = DEFAULT_PIPE_SIZE;
	pipe_prod = 0;
	pipe_cons = 0;
	pipe_poff = 0;
	pipe_coff = 0;
	pipe_ahead = 0;
	pipe = new unsigned char[pipe_size];

	int res;
	res = pthread_mutex_init(&pipe_mutex, NULL);
	if (res != 0) {
		// allocate the mutex statically
		pthread_mutex_t mutex_static = PTHREAD_MUTEX_INITIALIZER;
		pipe_mutex = mutex_static;
	}
	res = pthread_cond_init(&pipe_cond, NULL);
	if (res != 0) {
		// allocate the condition statically
		pthread_cond_t cond_static = PTHREAD_COND_INITIALIZER;
		pipe_cond = cond_static;
	}
}

int IOPipe::_changePipeSize(void *value)
{
	pthread_mutex_lock(&pipe_mutex);
	if ((pipe != NULL) && // there is already a pipe allocated
	    (pipe_cons != pipe_prod) // there are bytes, that haven't been consumed
           ) {
		// we can't reallocate the buffer now
		pthread_mutex_unlock(&pipe_mutex);
		return EXCOM_ERR_NOT_NOW;
	}
	if (value == NULL) {
		// an argument is required
		pthread_mutex_unlock(&pipe_mutex);
		return EXCOM_ERR_MEMORY;
	}
	unsigned int size = *(unsigned int*)value;
	if (size < 8) {
		// the size can't be less than 8 bytes
		pthread_mutex_unlock(&pipe_mutex);
		return EXCOM_ERR_VALUE;
	}
	pipe_size = size;
	_allocatePipe();
	pthread_mutex_unlock(&pipe_mutex);
	return EXCOM_ERR_OK;
}

void IOPipe::_allocatePipe()
{
	if (pipe != NULL) {
		delete [] pipe;
	}
	pipe_prod = 0;
	pipe_cons = 0;
	pipe = new unsigned char[pipe_size];
}

void IOPipe::_eof()
{
	pthread_mutex_lock(&pipe_mutex);
	at_eof = 1;
	// if the consumer is waiting for more data, wake it up
	pthread_cond_signal(&pipe_cond);
	pthread_mutex_unlock(&pipe_mutex);
}

int IOPipe::_readByte()
{
	// this function is called by the consumer
	pthread_mutex_lock(&pipe_mutex);
	while ((pipe_prod == pipe_cons) && !pipe_ahead) {
		// all is consumed
		if (at_eof) {
			pthread_mutex_unlock(&pipe_mutex);
			return EOF;
		}
		// wait for more data
		pthread_cond_wait(&pipe_cond, &pipe_mutex);
	}
	int res = (int) pipe[pipe_cons];
	if (pipe_cons == pipe_prod) {
		// the buffer was full and now it isn't. wake the producer
		pthread_cond_signal(&pipe_cond);
	}
	pipe_cons++;
	if (pipe_cons >= pipe_size) {
		pipe_cons = 0;
		pipe_ahead = 0;
	}
	pthread_mutex_unlock(&pipe_mutex);
	return res;
}

int IOPipe::_writeByte(unsigned char value)
{
	// this function is called by the producer
	pthread_mutex_lock(&pipe_mutex);
	while ((pipe_cons == pipe_prod) && pipe_ahead) {
		// there is no space in the pipe
		// wait for more space
		pthread_cond_wait(&pipe_cond, &pipe_mutex);
	}
	pipe[pipe_prod] = value;
	if (pipe_cons == pipe_prod) {
		// the buffer was empty and now it isn't. wake the consumer
		pthread_cond_signal(&pipe_cond);
	}
	pipe_prod++;
	if (pipe_prod >= pipe_size) {
		pipe_prod = 0;
		pipe_ahead = 1;
	}
	pthread_mutex_unlock(&pipe_mutex);
	return 0;
}

int IOPipe::_readNBits(unsigned int n, unsigned long *data)
{
	// this function is called by the consumer
	pthread_mutex_lock(&pipe_mutex);
	unsigned long res = 0l;
	unsigned int shift = 0, bits_in_byte;
	unsigned long bits_left;
	bits_in_byte = 8 - pipe_coff;
	// FIXME: (?) define bits_left elsewhere and keep as a class member
	//   pros: - we won't have to calculate it every time when
	//           readNBits is called
	//   cons: - we'll have to update this variable in readByte,
	//           which will slow this function down even when
	//           readNBits shall be never called
	if (n < bits_in_byte) {
		// we only need some bits from the current byte
		while ((pipe_prod == pipe_cons) && !pipe_ahead &&\
			(pipe_poff - pipe_coff < n)) {
			// but there's not enough bits in the current byte
			if (at_eof) {
				pthread_mutex_unlock(&pipe_mutex);
				return EOF;
			}
			pthread_cond_wait(&pipe_cond, &pipe_mutex);
		}
		// now there is enough bits already set by
		// the producer in the current byte
		*data = (pipe[pipe_cons] >> (bits_in_byte - n)) & mask[n];
		pipe_coff += n;
		//FIXME: will that be a problem, if we raise the condition
		//  signal even when there's no-one waiting for it?
		pthread_cond_signal(&pipe_cond);
		pthread_mutex_unlock(&pipe_mutex);
		return 0;
	}

	// the buffer is cyclic, either pipe_prod and pipe_cons are in the
	// same cycle or pipe_prod is one cycle ahead
	if (pipe_ahead) {
		bits_left = (pipe_size - pipe_cons + pipe_prod) * 8 - pipe_coff + pipe_poff;
	} else {
		bits_left = (pipe_prod - pipe_cons) * 8 - pipe_coff + pipe_poff;
	}
	while (bits_left < n) {
		if (at_eof) {
			// there will never be enough bits
			pthread_mutex_unlock(&pipe_mutex);
			return EOF;
		}
		// we'll wait till there's enough bits in the pipe
		pthread_cond_wait(&pipe_cond, &pipe_mutex);
		if (pipe_ahead) {
			bits_left = (pipe_size - pipe_cons + pipe_prod) * 8 - pipe_coff + pipe_poff;
		} else {
			bits_left = (pipe_prod - pipe_cons) * 8 - pipe_coff + pipe_poff;
		}
	}
        // now there's enough bits to read
	// use the rest of the current byte
	res = pipe[pipe_cons] & mask[bits_in_byte];
	n -= bits_in_byte;
	pipe_cons++;
	if (pipe_cons >= pipe_size) {
		pipe_cons = 0;
		pipe_ahead = 0;
	}
        // now, the buffer is byte-aligned (even though pipe_coff may
	// not be set to 0, because it will be set to another value in a moment
	// take the whole bytes until n < 8 bits
	while (n >= 8) {
		res = (res << 8) | pipe[pipe_cons];
		pipe_cons++;
		if (pipe_cons >= pipe_size) {
			pipe_cons = 0;
			pipe_ahead = 0;
		}		
		n -= 8;
	}
	// take the remaining n bits from the next byte
	pipe_coff = n;
        if (pipe_coff > 0) {
		res = (res << n) | ((pipe[pipe_cons] >> (8 - n))) & mask[n];
	}
	//FIXME: will that be a problem, if we raise the condition
	//  signal even when there's no-one waiting for it?
	pthread_cond_signal(&pipe_cond);
	*data = res;
	pthread_mutex_unlock(&pipe_mutex);
	return 0;
}

int IOPipe::_writeNBits(unsigned int n, unsigned long value)
{
	// FIXME!!!! the buffer is cyclic ... take wrap-around into account
	// this function is called by the producer
	pthread_mutex_lock(&pipe_mutex);
	unsigned int bits_in_byte;
	unsigned long bits_left;
	bits_in_byte = 8 - pipe_poff;
	// FIXME: (?) define bits_left elsewhere and keep as a class member
	//   pros: - we won't have to calculate it every time when
	//           readNBits is called
	//   cons: - we'll have to update this variable in readByte,
	//           which will slow this function down even when
	//           readNBits shall be never called
	if (n < bits_in_byte) {
		// we only need some bits from the current byte
		while ((pipe_prod == pipe_cons) &&\
			pipe_ahead && (pipe_coff - pipe_poff < n)) {
			// but there's not enough bits in the current byte
			pthread_cond_wait(&pipe_cond, &pipe_mutex);
		}
		// now there is enough bits already set by
		// the producer in the current byte
		pipe[pipe_prod] &= mask_high[bits_in_byte];
		pipe[pipe_prod] |= (unsigned char)(value << (bits_in_byte - n));
		pipe_poff += n;
		//FIXME: will that be a problem, if we raise the condition
		//  signal even when there's no-one waiting for it?
		pthread_cond_signal(&pipe_cond);
		pthread_mutex_unlock(&pipe_mutex);
		return 0;
	}

	// the buffer is cyclic, either pipe_prod and pipe_cons are in the
	// same cycle or pipe_prod is one cycle ahead
	if (pipe_ahead) {
		bits_left = (pipe_cons - pipe_prod) * 8 + pipe_coff - pipe_poff;
	} else {
		bits_left = (pipe_size - pipe_prod + pipe_cons) * 8 + pipe_coff - pipe_poff;
	}
	while (bits_left < n) {
		// we'll wait till there's enough bits in the pipe
		pthread_cond_wait(&pipe_cond, &pipe_mutex);
		if (pipe_ahead) {
			bits_left = (pipe_cons - pipe_prod) * 8 + pipe_coff - pipe_poff;
		} else {
			bits_left = (pipe_size - pipe_prod + pipe_cons) * 8 + pipe_coff - pipe_poff;
		}
	}
	// now there's enough free bits
	// use the rest of the current byte
	n -= bits_in_byte;
	pipe[pipe_prod] &= mask_high[bits_in_byte];
	pipe[pipe_prod] |= (unsigned char)(value >> n);
	pipe_prod++;
	if (pipe_prod >= pipe_size) {
		pipe_prod = 0;
		pipe_ahead = 1;
	}
	// now, the buffer is byte-aligned (even though pipe_poff may
	// not be set to 0, because it will be set to another value in a moment
	// write whole bytes until n < 8 bits
	while (n >= 8) {
		n -= 8;
		pipe[pipe_prod] = (unsigned char) (value >> n);
		pipe_prod++;
		if (pipe_prod >= pipe_size) {
			pipe_prod = 0;
			pipe_ahead = 1;
		}
	}
	// put the remaining n bits to the next byte
	pipe_poff = n;
	if (pipe_poff > 0) {
                value &= mask[n];
		pipe[pipe_prod] = (unsigned char)(value << (8 - n));
	}
	//FIXME: will that be a problem, if we raise the condition
	//  signal even when there's no-one waiting for it?
	pthread_cond_signal(&pipe_cond);
	pthread_mutex_unlock(&pipe_mutex);
	return 0;
}

unsigned int IOPipe::_readBlock(unsigned char *dest, unsigned long len)
{
	// this function is called by the consumer
	unsigned int size;
	unsigned int index = 0;
	pthread_mutex_lock(&pipe_mutex);
	// loop until the request is satisfied
	while (len > 0) {
		// copy bytes that are now in the pipe
		if (pipe_ahead) {
			size = pipe_size - pipe_cons;
			if (size >= len) {
				// we have enough data to complete the request
				memcpy(&dest[index], &pipe[pipe_cons], len);
				pipe_cons += len;
				index += len;
				pthread_cond_signal(&pipe_cond);
				pthread_mutex_unlock(&pipe_mutex);
				return index;
			}
			// copy bytes remaining to pipe_size
			memcpy(&dest[index], &pipe[pipe_cons], size);
			len -= size;
			index += size;
			// this will take the consumer to the same buffer
			// cycle as the producer
			pipe_cons = 0;
			pipe_ahead = 0;
		}
		// now both producer and consumer are in the same cycle,
		// copy bytes that remain between them
		size = pipe_prod - pipe_cons;
		if (size >= len) {
			// we have enough data to complete the request
			memcpy(&dest[index], &pipe[pipe_cons], len);
			pipe_cons += len;
			index += len;
			pthread_cond_signal(&pipe_cond);
			pthread_mutex_unlock(&pipe_mutex);
			return index;
		}
		// copy as many bytes as there is left in the pipe and
		// then wait for more
		memcpy(&dest[index], &pipe[pipe_cons], size);
		len -= size;
		index += size;
		pipe_cons += size;
		if (at_eof) {
			// there will be no more data in the pipe
			pthread_mutex_unlock(&pipe_mutex);
			return index;
		}
		// wake up the producer if it was asleep
		pthread_cond_signal(&pipe_cond);
		// wait for more data to be produced
		pthread_cond_wait(&pipe_cond, &pipe_mutex);
	}
	// this code should be unreachable
	pthread_mutex_unlock(&pipe_mutex);
	return index;
}

unsigned int IOPipe::_writeBlock(const unsigned char *data, unsigned long len)
{
	// this function is called by the producer
	pthread_mutex_lock(&pipe_mutex);
	unsigned int index = 0;
	unsigned int size;
	// loop until the request is satisfied
	while (len > 0) {
		if (!pipe_ahead) {
			size = pipe_size - pipe_prod;
			if (size >= len) {
				// there's enough space in the pipe to
				// complete the request
				memcpy(&pipe[pipe_prod], &data[index], len);
				pipe_prod += len;
				index += len;
				pthread_cond_signal(&pipe_cond);
				pthread_mutex_unlock(&pipe_mutex);
				return index;
			}
			// copy bytes remaining to pipe_size
			memcpy(&pipe[pipe_prod], &data[index], size);
			len -= size;
			index += size;
			// this will bring the producer one cycle ahead
			// of the consumer
			pipe_prod = 0;
			pipe_ahead = 1;
		}
		// now producer is one cycle ahead, we can copy atmost
		// cons - prod bytes
		size = pipe_cons - pipe_prod;
		if (size >= len) {
			// we have enough space to complete the request
			memcpy(&pipe[pipe_prod], &data[index], len);
			index += len;
			pipe_prod += len;
			pthread_cond_signal(&pipe_cond);
			pthread_mutex_unlock(&pipe_mutex);
			return index;
		}
		// copy as many bytes as there is space for in the pipe
		// and wait for more space
		memcpy(&pipe[pipe_prod], &data[index], size);
		len -= size;
		index += size;
		pipe_prod += size;
		// wake up the consumer if it was asleep
		pthread_cond_signal(&pipe_cond);
		// wait for more space
		pthread_cond_wait(&pipe_cond, &pipe_mutex);
	}
	// this code should be unreachable
	pthread_mutex_unlock(&pipe_mutex);
	return index;
}

unsigned long IOPipe::_bitsRead() {
    // FIXME must be able to count how many bits were already read from pipe
    // using the same mechanics as in other readers
    return 0;
}

unsigned long IOPipe::_bitsWritten() {
    // FIXME must be able to count how many bits were already written from pipe
    // using the same mechanics as in other readers
    return 0;
}
//------------------------------------------------------------------

void IOFileReader::_getFileLength()
{
	fseek(file_handle, 0, SEEK_END);
	remaining_len = (size_t) ftell(file_handle);
	fseek(file_handle, 0, SEEK_SET);
}

int IOFileReader::openFile(const char *file_name)
{
	int res;
	res = IOFile::_openFile(file_name, "rb");
	if (res != EXCOM_ERR_OK) return res;
	IOModule::_allocateBuffer();
	_getFileLength();
	stream_pos = 0;
	return EXCOM_ERR_OK;
}

int IOFileReader::readByte()
{
	if (buffer_pos >= buffer_occ) {
		// the buffer is empty or exhausted
		size_t res, size;
		if (remaining_len == 0) return EOF;
		size = (remaining_len > buffer_size)? buffer_size : remaining_len;
		res = fread(buffer, size, 1, file_handle);
		if (res != 1) return EOF;
		remaining_len -= size;
		buffer_pos = 0;
		buffer_occ = size;
		stream_pos += size;
	}
	return (int) buffer[buffer_pos++];
}

int IOFileReader::readNBits(unsigned int n, unsigned long *data)
{
	unsigned long res = 0l;
	unsigned int bits_in_byte;
	unsigned long bits_left;
	bits_in_byte = 8 - bit_offset;
	// FIXME: (?) define bits_left elsewhere and keep as a class member
	//   pros: - we won't have to calculate it every time when
	//           readNBits is called
	//   cons: - we'll have to update this variable in readByte,
	//           which will slow this function down even when
	//           readNBits shall be never called
	bits_left = (buffer_occ - buffer_pos) * 8 - bit_offset;
	if ((n < bits_in_byte) && (bits_left >= n)) {
		// we only need some bits from the current byte
		*data = (buffer[buffer_pos] >> (bits_in_byte - n)) & mask[n];
		bit_offset += n;
		return 0;
	}
	if (bits_left < n) {
		// we'll use all the bits from the current buffer, but
		// it won't be enough
		if (remaining_len == 0) {
			// we know that there's nothing left in the
			// input stream, so we won't even try
			return EOF;
		}
		if ((buffer_occ != buffer_pos) && (bits_in_byte > 0)) {
			// finish current byte
			res = buffer[buffer_pos] & mask[bits_in_byte];
			n -= bits_in_byte;
			buffer_pos++;
			// take all the bytes that remain in the buffer
			while (buffer_pos < buffer_occ) {				
				res = (res << 8) | buffer[buffer_pos];
				buffer_pos++;
				n -= 8;
			}
		}
		// obtain new buffer
		size_t size;
		size_t e;
		size = (remaining_len > buffer_size)? buffer_size : remaining_len;
		e = fread(buffer, size, 1, file_handle);
		if (e != 1) return EOF;
		remaining_len -= size;
		buffer_pos = 0;
		bit_offset = 0;
		buffer_occ = size;
		stream_pos += size;
		bits_left = buffer_occ * 8;
		if (bits_left < n) {
			// even when a new buffer has been read from the
			// input stream, there's not enough bits
			return EOF;
		}
	} else {
		// use the rest of the current byte
                res = buffer[buffer_pos] & mask[bits_in_byte];
                n -= bits_in_byte;
                buffer_pos++;                
	}
	// now it's sure, that there's enough bits in the buffer and
	// that the buffer is byte-aligned (even though bit_offset may
	// not be set to 0, because it will be set to another value in
	// a moment
	// take the whole bytes until n < 8 bits
	while (n >= 8) {                
                res = (res << 8) | buffer[buffer_pos];
                buffer_pos++;
                n -= 8;
	}
	// take the remaining n bits from the next byte
	bit_offset = n;
	if (bit_offset > 0) {
		res = (res << n) | ((buffer[buffer_pos] >> (8 - n)) & mask[n]);
	}
	*data = res;
	return 0;
}

unsigned int IOFileReader::readBlock(unsigned char *dest, unsigned long len)
{
	unsigned int left = buffer_occ - buffer_pos;
	if (left >= len) {
		// there is enough data in the current buffer
		memcpy(dest, &buffer[buffer_pos], len);
		buffer_pos += len;
		return len;
	} else {
		// copy the remains of current buffer
		unsigned int index;
		size_t res, size;
		memcpy(dest, &buffer[buffer_pos], left);
		index = left;
		len -= left;
		buffer_pos += left;
		while (len > 0 && remaining_len > 0) {
			// read new buffer
			size = (remaining_len > buffer_size)?\
				buffer_size : remaining_len;
			res = fread(buffer, size, 1, file_handle);
			if (res != 1) return index;
			remaining_len -= size;
			buffer_occ = size;
			stream_pos += size;
			// copy data to the destination buffer
			if (buffer_occ >= len) size = len;
			else size = buffer_occ;
			memcpy(&dest[index], &buffer[0], size);
			len -= size;
			index += size;
			buffer_pos = size;
		}
		// return the number of bytes that were actually copied
		return index;
	}
}

unsigned long IOFileReader::bitsRead() {
    return ((stream_pos - buffer_occ + buffer_pos) << 3) + bit_offset;
}

int IOFileReader::setParameter(int parameter, void *value)
{
	if (parameter == EXCOM_PARAM_IO_BUFFER_SIZE) {
		if ((value != NULL) && (*(unsigned int*)value < 4)) {
			// this I/O module requires at least 4 bytes for a buffer
			return EXCOM_ERR_VALUE;
		}
	}
	return IOModule::setParameter(parameter, value);
}

//------------------------------------------------------------------

int IOFilePipe::writeByte(unsigned char value)
{
	int res = 0;
	IOPipe::_writeByte(value);

	// also write the byte to the output file
	if (IOFileWriter::file_handle != NULL) {
            IOFileWriter::writeByte(value);
	}
	return res;
}

int IOFilePipe::writeNBits(unsigned int n, unsigned long value)
{
	IOPipe::_writeNBits(n, value);

	if (IOFileWriter::file_handle != NULL) {
                IOFileWriter::writeNBits(n, value);
        }
	return 0;
}

unsigned int IOFilePipe::writeBlock(const unsigned char *data, unsigned long len)
{
	unsigned int res = 0;
	res = IOPipe::_writeBlock(data, len);

	// also write the block to the output file
	if (IOFileWriter::file_handle != NULL) {
                IOFileWriter::writeBlock(data, len);
	}
	return res;
}

void IOFilePipe::flush()
{
	if (IOFileWriter::file_handle != NULL) {
            IOFileWriter::flush();
	}
}

int IOFilePipe::setParameter(int parameter, void *value)
{
	if (parameter == EXCOM_PARAM_IO_BUFFER_SIZE) {
		if ((value != NULL) && (*(unsigned int*)value < 4)) {
			/* this I/O module requires at least 4 bytes as a buffer */
			return EXCOM_ERR_VALUE;
		}
	} else if (parameter == EXCOM_PARAM_IO_PIPE_SIZE) {
		return IOPipe::_changePipeSize(value);
	}
	return IOModule::setParameter(parameter, value);
}

int IOFilePipe::getValue(int parameter, void *value) {
	switch(parameter) {
	case EXCOM_VALUE_IO_PIPE_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		*((unsigned int*)value) = pipe_size;
		return EXCOM_ERR_OK;
	}
	return IOModule::getValue(parameter, value);
}

unsigned long IOFilePipe::bitsRead() {
    return IOPipe::_bitsRead();
}

unsigned long IOFilePipe::bitsWritten() {
    if(IOFileWriter::file_handle != NULL) {
        return IOFileWriter::bitsWritten();
    }
    return IOPipe::_bitsWritten();
}

//-------------------------------------------------------------------

int IOFileWriter::openFile(const char *file_name)
{
	int res;
	res = IOFile::_openFile(file_name, "wb");
	if (res != EXCOM_ERR_OK) return res;
	IOModule::_allocateBuffer();
	stream_pos = 0;
	return EXCOM_ERR_OK;
}

int IOFileWriter::writeByte(unsigned char value)
{
	int res = 0;
	buffer[buffer_occ++] = value;
	if (buffer_occ >= buffer_size) {
		size_t e;
		// the buffer is full, flush it
		e = fwrite(buffer, buffer_size, 1, file_handle);
		if (e != 1) res = EOF;
		stream_pos += buffer_size;
		buffer_occ = 0;
	}
	return res;
}

int IOFileWriter::writeNBits(unsigned int n, unsigned long value)
{
	unsigned int bits_in_byte;
	unsigned long bits_left;
	bits_in_byte = 8 - bit_offset;
	// FIXME: (?) define bits_left elsewhere and keep as a class member
	//   pros: - we won't have to calculate it every time when
	//           readNBits is called
	//   cons: - we'll have to update this variable in readByte,
	//           which will slow this function down even when
	//           readNBits shall be never called
	bits_left = ((buffer_size - buffer_occ) << 3) - bit_offset;
	if ((n <= bits_in_byte) && (bits_left >= n)) {
		// we only need some bits from the current byte
		buffer[buffer_occ] &= mask_high[bits_in_byte];
		// I want MSB writing -> (bit_offset - n) instead of (bit_offset)
		buffer[buffer_occ] |= (unsigned char) (value << (bits_in_byte - n));
		bit_offset += n;
		return 0;
	}
	if (bits_left < n) {
		// we'll use all the bits that remain free in the current
		// buffer, but it won't be enough
		if (buffer_occ < buffer_size) {
			// finish current byte
			n -= bits_in_byte;
			buffer[buffer_occ] &= mask_high[bits_in_byte];			
			// I am writing more than bit_offset
			buffer[buffer_occ] |= (unsigned char)(value >> n);			
			buffer_occ++;
			// fill all remaining bytes of the buffer
			while (buffer_occ < buffer_size) {
				n -= 8;
				buffer[buffer_occ] = (unsigned char) (value >> n) ;
				buffer_occ++;
			}
		}
		// flush current buffer
		size_t e;
		e = fwrite(buffer, buffer_size, 1, file_handle);
		if (e != 1) return EOF;
		stream_pos += buffer_size;
		buffer_occ = 0;
		bit_offset = 0;
	} else {
		// use the rest of the current byte
		// how many bits will stay in buffer after removing MSB bits_in_byte
		n -= bits_in_byte;
		buffer[buffer_occ] &= mask_high[bits_in_byte];
		// assing only MSB bits_in_byte
		buffer[buffer_occ] |= (unsigned char)(value >> n);
		buffer_occ++;
	}
	// Now there's enough space in the buffer for the remaining
	// bits. It's also sure that the buffer is byte-aligned.
	// write whole bytes until n < 8 bits
	while (n >= 8) {
		n -= 8;
		buffer[buffer_occ] = (unsigned char) (value >> n);
		buffer_occ++;
	}	
	// put the remaining n bits to the next byte
	bit_offset = n;
	if (bit_offset > 0) {
		value &= mask[n];
		buffer[buffer_occ] = (unsigned char)(value << (8 - n));
	}
	return 0;
}

unsigned int IOFileWriter::writeBlock(const unsigned char *data, unsigned long len)
{
	unsigned int index = 0;
	unsigned int size, left;
	while (len > 0) {
		// copy as many bytes as possible to the current buffer
		left = buffer_size - buffer_occ;
		size = (len > left)? left : len;
		memcpy(&buffer[buffer_occ], &data[index], size);
		buffer_occ += size;
		index += size;
		len -= size;
		if (buffer_occ >= buffer_size) {
			// flush the buffer
			size_t e;
			e = fwrite(buffer, buffer_size, 1, file_handle);
			if (e != 1) {
				// writing failed
				return index;
			}
			buffer_occ = 0;
			stream_pos += buffer_size;
		}
		// repeat while there's still data to be written
	}
	return index;
}

int IOFileWriter::immWriteBytes(unsigned int offset, unsigned int count, unsigned long value)
{
        unsigned int n = count << 3;
	for (unsigned int i = 0; i < count; i++) {
                n -= 8;
		_immWriteByte(offset + i, (unsigned char)(value >> n));
	}
	return 0;
}

int IOFileWriter::_immWriteByte(unsigned int offset, unsigned char value)
{
	long pos;
	unsigned int o;
	if (offset >= stream_pos) {
		o = offset - stream_pos;
		if (o >= buffer_size) {
			// offset lies beyond the current buffer
			// This is quite strange situation, because the
			// value may be overwritten in the future.
			pos = ftell(file_handle);
			fseek(file_handle, offset, SEEK_SET);
			fwrite(&value, 1, 1, file_handle);
			fseek(file_handle, pos, SEEK_SET);
		} else {
			// offset lies in the current buffer
			buffer[o] = value;
			// Note that if o >= buffer_occ, data may be
			// overwritten in the future and may not be
			// flushed to the output stream in the flush()
			// function. This is not a safe situation
		}
	} else {
		// offset lies in the area, that has been already written to the file
		pos = ftell(file_handle);
		fseek(file_handle, offset, SEEK_SET);
		fwrite(&value, 1, 1, file_handle);
		fseek(file_handle, pos, SEEK_SET);
	}
	return 0;
}

void IOFileWriter::flush()
{
	if (buffer_occ > 0) {
		if ((bit_offset > 0) && (buffer_occ < buffer_size)) {
			// this better shouldn't happen, but it's the
			// caller's responsibility to make the buffer
			// bit-aligned
			buffer_occ++;
		}
		fwrite(buffer, buffer_occ, 1, file_handle);
		stream_pos += buffer_occ;
		buffer_occ = 0;
	}
	fflush(file_handle);
}

unsigned long IOFileWriter::bitsWritten() {
    return ((stream_pos + buffer_occ) << 3) + bit_offset;
}

int IOFileWriter::setParameter(int parameter, void *value)
{
	if (parameter == EXCOM_PARAM_IO_BUFFER_SIZE) {
		if ((value != NULL) && (*(unsigned int*)value < 4)) {
			// this I/O module requires at least 4 bytes as a buffer
			return EXCOM_ERR_VALUE;
		}
	}
	return IOModule::setParameter(parameter, value);
}

//-------------------------------------------------------------------

int IOMemReader::readNBits(unsigned int n, unsigned long *data)
{
	unsigned long res = 0l;
	unsigned int bits_in_byte;
	unsigned long bits_left;
	bits_in_byte = 8 - bit_offset;
	// FIXME: (?) define bits_left elsewhere and keep as a class member
	//   pros: - we won't have to calculate it every time when
	//           readNBits is called
	//   cons: - we'll have to update this variable in readByte,
	//           which will slow this function down even when
	//           readNBits shall be never called
	bits_left = (length - stream_pos) * 8 - bit_offset;
	if (bits_left < n) {
		// there aren't even n bits left
		return EOF;
	}
	if (n < bits_in_byte) {
		// we only need some bits from the current byte
		*data = (base_pointer[stream_pos] >> (bits_in_byte - n)) & mask[n];
		bit_offset += n;
		return 0;
	}              
	// use the rest of the current byte
	res = base_pointer[stream_pos] & mask[bits_in_byte];
	n -= bits_in_byte;
	stream_pos++;
	// now it's sure, that the buffer is byte-aligned (even though
	// bit_offset may not be set to 0, because it will be set to
	// another value in a moment
        // take the whole bytes until n < 8 bits
	while (n >= 8) {
		res = (res << 8) | base_pointer[stream_pos];
		stream_pos++;		
		n -= 8;
	}
	// take the remaining n bits from the next byte
        bit_offset = n;
	if (bit_offset > 0) {
		res = (res << n) | ((base_pointer[stream_pos] >> (8 - n)) & mask[n]);
	}
	*data = res;
	return 0;
}

unsigned int IOMemReader::readBlock(unsigned char *dest, unsigned long len) {
	unsigned int left = length - stream_pos;
	if (left >= len) {
		// there is enough data in the memory
		memcpy(dest, &base_pointer[stream_pos], len);
		stream_pos += len;
		return len;
	} else {
		// copy as many bytes as possible
		memcpy(dest, &base_pointer[stream_pos], left);
		stream_pos += left;
		return left;
	}
}

unsigned long IOMemReader::bitsRead() {
    return (stream_pos << 3) + bit_offset;
}
//-------------------------------------------------------------------

int IOMemPipe::writeByte(unsigned char value)
{
	int res = 0;
	IOPipe::_writeByte(value);

	// also write the byte to the output memory area
	if (IOMemWriter::base_pointer != NULL) {
                IOMemWriter::writeByte(value);
	}
	return res;
}

int IOMemPipe::writeNBits(unsigned int n, unsigned long value) {
	IOPipe::_writeNBits(n, value);
	
	if (IOMemWriter::base_pointer != NULL) {
                IOMemWriter::writeNBits(n, value);
        }
	return 0;
}

unsigned int IOMemPipe::writeBlock(const unsigned char *data, unsigned long len) {
	unsigned int res;
	res = IOPipe::_writeBlock(data, len);

	// also write the block to the output memory area
	if (IOMemWriter::base_pointer != NULL) {
                IOMemWriter::writeBlock(data, len);
	}
	return res;
}

int IOMemPipe::setParameter(int parameter, void *value)
{
	if (parameter == EXCOM_PARAM_IO_PIPE_SIZE) {
		return IOPipe::_changePipeSize(value);
	}
	// no other acceptable parameters now
	return EXCOM_ERR_PARAM;
}

int IOMemPipe::getValue(int parameter, void *value) {
	switch(parameter) {
	case EXCOM_VALUE_IO_PIPE_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		*((unsigned int*)value) = pipe_size;
		return EXCOM_ERR_OK;
	}
	return IOModule::getValue(parameter, value);
}

unsigned long IOMemPipe::bitsRead() {
    return IOPipe::_bitsRead();
}

unsigned long IOMemPipe::bitsWritten() {
    if(IOMemWriter::base_pointer != NULL) {
        return IOMemWriter::bitsWritten();
    }
    return IOPipe::_bitsWritten();
}
//-------------------------------------------------------------------

int IOMemWriter::writeNBits(unsigned int n, unsigned long value)
{
	unsigned int bits_in_byte;
	unsigned long bits_left;
	bits_in_byte = 8 - bit_offset;
	// FIXME: (?) define bits_left elsewhere and keep as a class member
	//   pros: - we won't have to calculate it every time when
	//           readNBits is called
	//   cons: - we'll have to update this variable in readByte,
	//           which will slow this function down even when
	//           readNBits shall be never called
	bits_left = (length - stream_pos) * 8 - bit_offset;
	if (bits_left < n) {
		// there aren't even n bits free in the output memory area
		return EOF;
	}
	if (n < bits_in_byte) {
		// we only need some bits from the current byte
		base_pointer[stream_pos] &= mask_high[bits_in_byte];
		base_pointer[stream_pos] |= (unsigned char)(value << (bits_in_byte - n));
		bit_offset += n;
		return 0;
	}
	// use the rest of the current byte
	n -= bits_in_byte;
	base_pointer[stream_pos] &= mask_high[bits_in_byte];
	base_pointer[stream_pos] |= (unsigned char)(value >> n);
	stream_pos++;
        // now it's sure, that the buffer is byte-aligned (even though
	// bit_offset may not be set to 0, because it will be set to
	// another value in a moment
	// take the whole bytes until n < 8 bits
	while (n >= 8) {
            	n -= 8;
		base_pointer[stream_pos] = (unsigned char)(value >> n);
		stream_pos++;		
	}
	// put the remaining n bits to the next byte
	bit_offset = n;
	if (bit_offset > 0) {
                value &= mask[n];
		base_pointer[stream_pos] = (unsigned char)(value << (8 - n));
	}
	return 0;
}

unsigned int IOMemWriter::writeBlock(const unsigned char *data, unsigned long len)
{
	unsigned int left = length - stream_pos;
	if (left >= len) {
		// there is enough space in the memory
		memcpy(&base_pointer[stream_pos], data, len);
		stream_pos += len;
		return len;
	} else {
		// copy as many bytes as possible
		memcpy(&base_pointer[stream_pos], data, left);
		stream_pos += left;
		return left;
	}
}

int IOMemWriter::immWriteBytes(unsigned int offset, unsigned int count, unsigned long value)
{
	if (offset+count > length) return EOF;
        unsigned int n = count << 3;
	for (unsigned int i = 0; i < count; i++) {
                n -= 8;
		base_pointer[offset+i] = (unsigned char)(value >> n);
	}
	return 0;
}

unsigned long IOMemWriter::bitsWritten() {
    return (stream_pos << 3) + bit_offset;
}
//-------------------------------------

void* ioGeneralToParticular(IOModule* io, int output)
{
	IOFileReader *fr;
	IOFileWriter *fw;
	IOFilePipe   *fp;
	IOMemReader  *mr;
	IOMemWriter  *mw;
	IOMemPipe    *mp;
	switch(io->variant) {
	case IO_VAR_FILE_READER:
		fr = (IOFileReader*) io;
		if (output == IO_VAR_FILE)
			return (IOFile*) fr;
		else if (output == IO_VAR_READER)
			return (IOReader*) fr;
		else if (output == IO_VAR_FILE_READER)
			return fr;
		return NULL;
	case IO_VAR_FILE_WRITER:
		fw = (IOFileWriter*) io;
		if (output == IO_VAR_FILE)
			return (IOFile*) fw;
		else if (output == IO_VAR_WRITER)
			return (IOWriter*) fw;
		else if (output == IO_VAR_FILE_WRITER)
			return fw;
		return NULL;
	case IO_VAR_FILE_PIPE:
		fp = (IOFilePipe*) io;
		if (output == IO_VAR_FILE)
			return (IOFile*) fp;
		else if (output == IO_VAR_PIPE)
			return (IOPipe*) fp;
		else if (output == IO_VAR_READER)
			return (IOReader*) fp;
		else if (output == IO_VAR_WRITER)
			return (IOWriter*) ((IOPipe*) fp);
		else if (output == IO_VAR_FILE_PIPE)
			return fp;
		return NULL;
	case IO_VAR_MEM_READER:
		mr = (IOMemReader*) io;
		if (output == IO_VAR_MEMORY)
			return (IOMemory*) mr;
		else if (output == IO_VAR_READER)
			return (IOReader*) mr;
		else if (output == IO_VAR_MEM_READER)
			return mr;
		return NULL;
	case IO_VAR_MEM_WRITER:
		mw = (IOMemWriter*) io;
		if (output == IO_VAR_MEMORY)
			return (IOMemory*) mw;
		else if (output == IO_VAR_WRITER)
			return (IOWriter*) mw;
		else if (output == IO_VAR_MEM_WRITER)
			return mw;
		return NULL;
	case IO_VAR_MEM_PIPE:
		mp = (IOMemPipe*) io;
		if (output == IO_VAR_MEMORY)
			return (IOMemory*) mp;
		else if (output == IO_VAR_PIPE)
			return (IOPipe*) mp;
		else if (output == IO_VAR_READER)
			return (IOReader*) mp;
		else if (output == IO_VAR_WRITER)
			return (IOWriter*) ((IOPipe*) mp);
		else if (output == IO_VAR_MEM_PIPE)
			return mp;
		return NULL;
	}
	return NULL;
}

int ioCheckConnection(IOModule* io, int direction)
{
	if (io == NULL) return EXCOM_ERR_MEMORY;
	if (direction == __EXC_CONN_INPUT) {
		// the I/O module must be either reader or pipe
		if ((io->variant & (IO_VAR_READER | IO_VAR_PIPE)) == 0) {
			return EXCOM_ERR_PARAM;
		}
		// if it's a pipe, it mustn't have its output already attached
		if (io->variant & IO_VAR_PIPE) {
			IOPipe *pipe = (IOPipe*) ioGeneralToParticular(io, IO_VAR_PIPE);
			if (pipe->output_attached) {
				return EXCOM_ERR_DUPLICATE;
			}
		}
	} else if (direction == __EXC_CONN_OUTPUT) {
		if ((io->variant & (IO_VAR_WRITER | IO_VAR_PIPE)) == 0)
			return EXCOM_ERR_PARAM;
		if (io->variant & IO_VAR_PIPE) {
			IOPipe *pipe = (IOPipe*) ioGeneralToParticular(io, IO_VAR_PIPE);
		if (pipe->input_attached) {
				return EXCOM_ERR_DUPLICATE;
			}
		}
	}
	
	return EXCOM_ERR_OK;
}

