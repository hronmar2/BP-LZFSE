#!/bin/sh

resdir=`date +"%d-%m_%H-%M-%S"`_hist

mkdir $resdir

for f in korpus/*; do
	g=`basename $f`
	./app -m acb -r 1 -q -i $f -o $resdir/$g -p s=12,h >/dev/null 2>&1
	mv hist.ctx $resdir/$g.ctx
	mv hist.conf $resdir/$g.conf
done

