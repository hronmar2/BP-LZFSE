#ifndef __IOMODULE_HPP__
#define __IOMODULE_HPP__

/*
 * File:       iomodule.hpp
 * Purpose:    Declaration of I/O module classes
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file iomodule.hpp
 *  \brief Declarations of I/O modules' classes
 */

#include <stdio.h>
#include <pthread.h>

#include <excom.h>

//declarations from compmodule.hpp
class CompModule;

#define IO_VAR_FILE 1
#define IO_VAR_MEMORY 2
#define IO_VAR_READER 4
#define IO_VAR_WRITER 8
#define IO_VAR_PIPE 16

//! \brief Distinct types of instantionable descendants of the IOModule class
enum io_variant_e {
	IO_VAR_FILE_READER = IO_VAR_FILE + IO_VAR_READER,
	IO_VAR_FILE_WRITER = IO_VAR_FILE + IO_VAR_WRITER,
	IO_VAR_FILE_PIPE = IO_VAR_FILE + IO_VAR_PIPE,
	IO_VAR_MEM_READER = IO_VAR_MEMORY + IO_VAR_READER,
	IO_VAR_MEM_WRITER = IO_VAR_MEMORY + IO_VAR_WRITER,
	IO_VAR_MEM_PIPE = IO_VAR_MEMORY + IO_VAR_PIPE
};

#ifndef DEFAULT_BUFFER_SIZE
#define DEFAULT_BUFFER_SIZE 4096
#endif  //ifndef DEFAULT_BUFFER_SIZE

#ifndef DEFAULT_PIPE_SIZE
#define DEFAULT_PIPE_SIZE DEFAULT_BUFFER_SIZE
#endif

//! \brief Ancestor of all the I/O modules
class IOModule {
protected:
	unsigned int module_handle;  //!< unique identifier of the I/O module
	unsigned char *buffer;       //!< pointer to the buffer that the module uses for better performance of I/O operations
	unsigned int buffer_size;    //!< number of bytes allocated for the buffer
	unsigned int buffer_occ;     //!< number of bytes, that are actually in the buffer (may be <= buffer_size)
	unsigned int buffer_pos;     //!< current position in the input or output buffer (i.e. index of the next byte to be read or written)
	unsigned int bit_offset;     /*!< number of bits from the byte with
		index \c buffer_pos, that have already been used. This
		variable only changes when bitwise functions (readNBits,
		writeNBits) are used. */
	unsigned int stream_pos;     //!< current position in the input or output stream (the whole file or memory, etc.)
public:
	int connected;               //!< number of compression modules that this I/O module is connected to
	enum io_variant_e variant;   //!< particular type of the I/O module
protected:
	/*! \brief Allocates the array pointed to by \c buffer to \c buffer_size bytes and resets entangled variables. */
	void _allocateBuffer();
public:
	/*! \brief Constructor, registers the given handle
	 *  \param handle Unique handle to this newly created I/O module
	 */
	IOModule(unsigned int handle);
	//! \brief Destructor, destroys the I/O module and unregisters its handle
	virtual ~IOModule();

	/*! \brief Sets a parameter of the I/O module
	 *  \param parameter Symbolic constant of the parameter to be set
	 *  \param value Pointer to the actual value of the parameter.
	 *
	 *  This is the implementation of the #exc_setParameter API call
	 *  within a particular I/O module.
	 */
	virtual int setParameter(int parameter, void *value);

	/*! \brief Requests a value from the I/O module
	 *  \param parameter Symbolic constant of the parameter to be retrieved
	 *  \param value Pointer to the memory area to store the value into
	 *
	 *  This is the implementation of the #exc_getValue API call
	 *  within a particular I/O module.
	 */
	virtual int getValue(int parameter, void *value);
};

//! \brief Ancestor of the classes that operate on files
class IOFile {
protected:
	FILE* file_handle;      //!< handle to the open file
protected:
	/*! \brief Opens a file in given mode
	 *  \param file_name Name of the file
	 *  \param mode Opening mode (the same as the second parameter of fopen)
	 *  
	 *  This is a backend function for openFile which actually
	 *  opens the file in the given mode.
	 */
	int _openFile(const char *file_name, const char *mode);
public:
	IOFile(): file_handle(NULL) {}
	virtual ~IOFile() {
		// the particular child class has to flush cache buffers if necessary
		if (file_handle != NULL) fclose(file_handle);
	}
	/*! \brief Opens a file to read from or to write to
	 *  \param file_name Path to the file to open
	 *  \return
	 *         - \c EXCOM_ERR_OK - success
	 *         - \c EXCOM_ERR_OPEN - unable to open the file
	 *         - \c EXCOM_ERR_MEMORY - \c file_name is an invalid pointer
	 *         - \c EXCOM_ERR_DUPLICATE - the instance has already opened another file
	 */
	virtual int openFile(const char *file_name) = 0;
};

//! \brief Ancestor of the classes that operate on memory
class IOMemory {
protected:
	unsigned char *base_pointer;   //!< pointer to the beginning of the memory area
	unsigned int length;           //!< length of the operable memory area
public:
	IOMemory(): base_pointer(NULL), length(0) {}
	virtual ~IOMemory() {}
	/*! \brief Attaches the module to a block of memory
	 *  \param base Pointer to the start of the memory area
	 *  \param length Length of the memory area
	 *  \return
	 *         - \c EXCOM_ERR_OK - success
	 *         - \c EXCOM_ERR_MEMORY - \c base is an invalid pointer
	 *         - \c EXCOM_ERR_DUPLICATE - the instance is already attached to another block of memory
	 */
	int attachMemory(void *base, unsigned int length);
};

//! \brief Ancestor of the classes that provide input data to a compression module
class IOReader {
public:
	virtual ~IOReader() {}
	/*! \brief Reads a byte from the input stream
	 *  \return value of the byte casted to int, or \c EOF if there's
	 *  no data left in the stream.
	 */
	virtual int readByte() = 0;
	/*! \brief Reads N bits from the input stream
	 *  \param n Number of bits to read (1 to 32)
	 *  \param data Pointer to a variable to store the N bits in. The
	 *    pointer must be valid, though the function doesn't verify it
	 *    for performance reasons (but if it's invalid, the program
	 *    will most likely segfault).
	 *  \return zero on success, EOF when there's not enough bits left
	 *  in the input stream
	 *
	 *  Please note, that calls to \c readByte and \c readNBits can
	 *  be combined, but it's the caller's responsibility to make sure,
	 *  that the input stream is byte-aligned when calling \c readByte
	 *  (otherwise the result is undefined).
	 */
	virtual int readNBits(unsigned int n, unsigned long *data) = 0;
	/*! \brief Reads a block of bytes from the input stream to a given buffer
	 *  \param dest Pointer to the destination buffer
	 *  \param len Number of bytes to be read
	 *  \return the number of bytes, that were actually transferred.
	 *  This can be less or equal to \c len. If the value is less than
	 *  \c len, the reader reached end of the input stream.
	 */
	virtual unsigned int readBlock(unsigned char *dest, unsigned long len) = 0;
	//! \brief Rewinds the input object (file or memory) back to start
	virtual void rewind() = 0;

        /*!
         * \brief Returns count of bits processed by read* methods using this module
         *
         * \return count of bits read
         */
        virtual unsigned long bitsRead() = 0;
};

//! \brief Ancestor of the classes that take output data from a compression module
class IOWriter {
public:
	int seekable;                //!< set to 1 if the module is seekable (i.e. supports random acces, fseek, etc.), set to 0 otherwise
public:
	IOWriter() {seekable = 0;}
	virtual ~IOWriter() {}
	/*! \brief Writes one byte to the output stream
	 *  \param value Value of the byte to be written
	 *  \return zero on success, EOF when there was an error
	 *
	 *  Please note, that output may be buffered. This means, that
	 *  a call to this function may succeed, but data still may not be
	 *  stored and if the next flush of the buffer fails, data can
	 *  be lost. Therefore, you can only get the information, that
	 *  not all data was written, but you may not be able to know,
	 *  how many bytes were really successfully written.
	 */
	virtual int writeByte(unsigned char value) = 0;
	/*! \brief Writes N bits to the output stream
	 *  \param n Number of bits to write
	 *  \param value The variable of which the N lowest-order bits
	 *         will be written
	 *  \return zero on succes, EOF when there was an error
	 *
         *  If \c value contains more bits than \c bits, it's the caller's
         *  responsibility to assign all those bits to 0.
         *
	 *  When combining \c writeByte and \c writeNBits calls, it's
	 *  the caller's responsibility to make sure that the output stream
	 *  is byte-aligned prior to \c writeByte call (otherwise the
	 *  result is undefined).
	 *
	 *  The same note as for #IOWriter::writeByte, regarding error
	 *  checking, applies to \c writeNBits as well.
	 */
	virtual int writeNBits(unsigned int n, unsigned long value) = 0;
	/*! \brief Writes a block of bytes to the output stream in one go
	 *  \param data Pointer to the source buffer
	 *  \param len Number of bytes to be written
	 *  \return the number of bytes, that were actually transferred.
	 *  This can be less or equal to \c len. If the value is less than
	 *  \c len, the writer was unable to write all data.
	 */
	virtual unsigned int writeBlock(const unsigned char *data, unsigned long len) = 0;
	/*! \brief Immediately writes a given number of bytes to given offset
	 *  of the output stream by-passing the output buffer.
	 *  \param offset Byte-offset in the output stream to write data to
	 *  \param count Number of bytes to write
	 *  \param value Value to be written
	 *  \return zero on success, EOF if there was an error
	 *
	 *  This function by-passes the output buffer and writes data to
	 *  a given offset of the output stream. This may not be acceptable
	 *  for all types of output I/O modules, but should be implemented
	 *  in I/O modules that are \c seekable. Also, if \c offset is
	 *  bigger than the current position in the stream, data might
	 *  be overwritten in the future or may not be written to the output
	 *  stream at all.
	 *
	 *  The function stores \c count lowest-order bytes of \c value
	 *  in a fashion that is compatible with
	 *  \c writeNBits(8*count,value) to eliminate endianess problems.
	 *
	 *  Please note, that this function might be quite ineffective
	 *  and should be used only where necessary.
	 */
	virtual int immWriteBytes(unsigned int offset, unsigned int count, unsigned long value) = 0;

        /*!
         * \brief Returns count of bits processed by write* methods using this module
         *
         * \return count of bits written
         */
        virtual unsigned long bitsWritten() = 0;
        
	//! \brief Flushes all remaining (buffered) data to the output object (file or memory)
	virtual void flush() = 0;
	/*! \brief This tells the I/O module, that there will
	 *  be no more data to write. As a by-product, this function will
	 *  also call \c flush(). */
	virtual void eof() {
		flush();
	}
};

//! \brief Ancestor of the classes that pipe output of one compression module to the input of another one
class IOPipe: public IOReader, public IOWriter {
public:
	/*! Handle to the compression chain of which the pipe is a member. This variable is needed to
	 *  check whether both ends of the pipe are in the same compression chain
	 */
	unsigned int chain_handle;
	/*! This variable is nonzero if the input end of the pipe (the one that data is
	 *  flowing through into the pipe) is already attached to a compression module */
	int input_attached;
	/*! This variable is nonzero if the output end of the pipe (the one that data is
	 *  flowing through out of the pipe) is already attached to a compression module */
	int output_attached;

	/*! This variable is nonzero if there won't be any more data written
	 *  into the pipe. After the consumer reads all what is left in the
	 *  pipe now, it shall return EOF. */
	int at_eof;

protected:
	unsigned char *pipe;         //!< pointer to a buffer for piping data
	unsigned int pipe_size;      //!< size of the \c pipe buffer in bytes
	unsigned int pipe_prod;      //!< index of the next item, which the producer shall fill
	unsigned int pipe_cons;      //!< index of the next item, that the consumer will consume 
	int pipe_ahead;              //!< 0 if pipe_prod >= pipe_cons, 1 if pipe_prod <= pipe_cons. This is necessary for determining when there's no room or no data in the pipe if pipe_prod == pipe_cons.
	unsigned int pipe_poff;      //!< index of the next bit in the current byte for the producer (\c pipe_prod). Has the same purpose as #IOModule::bit_offset.
	unsigned int pipe_coff;      //!< index of the next bit in the current byte for the consumer (\c pipe_cons). Has the same purpose as #IOModule::bit_offset.

	/* Piping data through a buffer always raises a producer-consumer
	 * problem. To solve it without race conditions and possible
	 * dead-locks, we need a mutex and a condition variable. There
	 * is always only one producer and one consumer involved. */
	//! Mutex for operations on the buffer (production or consumption
	pthread_mutex_t pipe_mutex;
	//! Condition variable to block a thread which can't work
	pthread_cond_t pipe_cond;
protected:
	/*! \brief Changes size of the buffer used for piping. This is a backend function for \c setParameter. */
	int _changePipeSize(void *value);
	/*! \brief Allocates a new buffer for the pipe according to the \c pipe_size variable. */
	void _allocatePipe();
	/*! \brief Response to a signalized end of input data. */
	void _eof();

	/* These functions read/write data from/to the pipe. They are
	 * common to all types of pipes. */
	int _readByte();
	int _writeByte(unsigned char value);
	int _readNBits(unsigned int n, unsigned long *data);
	int _writeNBits(unsigned int n, unsigned long data);
	unsigned int _readBlock(unsigned char *data, unsigned long len);
	unsigned int _writeBlock(const unsigned char *data, unsigned long len);
        unsigned long _bitsWritten();
        unsigned long _bitsRead();
public:
	IOPipe();
	virtual ~IOPipe() {
		if (pipe != NULL) delete [] pipe;
		pthread_mutex_destroy(&pipe_mutex);
		pthread_cond_destroy(&pipe_cond);
	}

	virtual void rewind() {
		// reset all indices
		pthread_mutex_lock(&pipe_mutex);
		pipe_prod = 0;
		pipe_cons = 0;
		pipe_poff = 0;
		pipe_coff = 0;
		pipe_ahead = 0;
		at_eof = 0;
		pthread_mutex_unlock(&pipe_mutex);
	}
};

//! \brief Class that reads data from a file
class IOFileReader: public IOModule, public IOReader, public IOFile {
private:
	size_t remaining_len;  //!< number of bytes, that haven't been read from the file
protected:
	/*! \brief Determines length of the opened file */
	void _getFileLength();
public:
	IOFileReader(unsigned int handle): IOModule(handle) {
		variant = IO_VAR_FILE_READER;
		buffer_size = DEFAULT_BUFFER_SIZE;
	}
	//! \brief Opens a file for reading. See #IOFile::openFile
	int openFile(const char *file_name);
	int readByte();
	int readNBits(unsigned int n, unsigned long *data);
	unsigned int readBlock(unsigned char *dest, unsigned long len);
        unsigned long bitsRead();
	void rewind() {
		fseek(file_handle, 0, SEEK_SET);
		_getFileLength();
		stream_pos = 0;
		buffer_occ = 0;
		buffer_pos = 0;
		bit_offset = 0;
	}

	int setParameter(int parameter, void *value);
};

//! \brief Class that writes data to a file
class IOFileWriter: public IOModule, public IOWriter, public IOFile {
public:
	IOFileWriter(unsigned int handle): IOModule(handle) {
		variant = IO_VAR_FILE_WRITER;
		buffer_size = DEFAULT_BUFFER_SIZE;
		seekable = 1;
	}
	~IOFileWriter() {
		flush();
	}
	//! \brief Opens a file for writing. See #IOFile::openFile
	int openFile(const char *file_name);
	int writeByte(unsigned char value);
	int writeNBits(unsigned int n, unsigned long value);
	unsigned int writeBlock(const unsigned char *data, unsigned long len);
	int immWriteBytes(unsigned int offset, unsigned int count, unsigned long value);
        unsigned long bitsWritten();
	void flush();

	int setParameter(int parameter, void *value);
private:
	//! Backend function for #immWriteBytes which is called from
	//  that function as many times as needed. This approach is
	//  less effective but much easier to code.
	int _immWriteByte(unsigned int offset, unsigned char value);
};

//! \brief Class that reads data from a memory area
class IOMemReader: public IOModule, public IOReader, public IOMemory {
public:
	IOMemReader(unsigned int handle): IOModule(handle) {
		variant = IO_VAR_MEM_READER;
		buffer_size = 0;
	}
	int readByte() {
		if (stream_pos >= length) return EOF;
		return base_pointer[stream_pos++];
	}
	int readNBits(unsigned int n, unsigned long *data);
	unsigned int readBlock(unsigned char *dest, unsigned long len);
        unsigned long bitsRead();
	void rewind() {
		stream_pos = 0;
		bit_offset = 0;
	}

	int setParameter(int parameter, void *value) {
		// no acceptable parameters now
		return EXCOM_ERR_PARAM;
	}
};

//! \brief Class that pipes data from one compression module to another and optionally stores the data, that flows through, to a file
class IOFilePipe: public IOFileWriter, public IOPipe {
public:
	IOFilePipe(unsigned int handle): IOFileWriter(handle){
		variant = IO_VAR_FILE_PIPE;
		buffer_size = DEFAULT_BUFFER_SIZE;
		IOPipe::seekable = 0;
                IOFileWriter::seekable = 0;
	}
	~IOFilePipe() {
		flush();
	}

        int readByte() {
		return IOPipe::_readByte();
	}
	int readNBits(unsigned int n, unsigned long *data) {
		return IOPipe::_readNBits(n, data);
	}
	unsigned int readBlock(unsigned char *dest, unsigned long len) {
		return IOPipe::_readBlock(dest, len);
	}
	void rewind() {
		IOPipe::rewind();
		if (IOFileWriter::file_handle != NULL) {
			::rewind(file_handle);
		}
		stream_pos = 0;
		buffer_occ = 0;
		buffer_pos = 0;
		bit_offset = 0;
	}

	int writeByte(unsigned char value);
	int writeNBits(unsigned int n, unsigned long value);
	unsigned int writeBlock(const unsigned char *data, unsigned long len);
	int immWriteBytes(unsigned int offset, unsigned int count, unsigned long value) {
            return EOF;
        }
        unsigned long bitsWritten();
        unsigned long bitsRead();
	void flush();
	void eof() {
		IOPipe::_eof();
		flush();
	}

	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
};

//! \brief Class that writes data to a memory area
class IOMemWriter: public IOModule, public IOWriter, public IOMemory {
public:
	IOMemWriter(unsigned int handle): IOModule(handle) {
		variant = IO_VAR_MEM_WRITER;
		buffer_size = 0;
		seekable = 1;
	}
	int writeByte(unsigned char value) {
		if (stream_pos >= length) return EOF;
		base_pointer[stream_pos++] = value;
		return 0;
	}
	int writeNBits(unsigned int n, unsigned long value);
	unsigned int writeBlock(const unsigned char *data, unsigned long len);
	int immWriteBytes(unsigned int offset, unsigned int count, unsigned long value);
        unsigned long bitsWritten();
	void flush() {}

	int setParameter(int parameter, void *value) {
		// no acceptable parameters now
		return EXCOM_ERR_PARAM;
	}
};

//! \brief Class that pipes data from one compression module to another and optionally stores the data, that flows through, to a memory area
class IOMemPipe: public IOMemWriter, public IOPipe {
public:
	IOMemPipe(unsigned int handle): IOMemWriter(handle) {
		variant = IO_VAR_MEM_PIPE;
		buffer_size = 0;
		IOPipe::seekable = 0;
                IOMemWriter::seekable = 0;
	}
	int readByte() {
		return IOPipe::_readByte();
	}
	int readNBits(unsigned int n, unsigned long *data) {
		return IOPipe::_readNBits(n, data);
	}
	unsigned int readBlock(unsigned char *dest, unsigned long len) {
		return IOPipe::_readBlock(dest, len);
	}
	void rewind() {
		IOPipe::rewind();
		stream_pos = 0;
		bit_offset = 0;
	}

	int writeByte(unsigned char value);
	int writeNBits(unsigned int n, unsigned long value);
	unsigned int writeBlock(const unsigned char *data, unsigned long len);
	int immWriteBytes(unsigned int offset, unsigned int count, unsigned long value) {
            return EOF;
        }
        unsigned long bitsWritten();
        unsigned long bitsRead();
	void flush() {}
	void eof() {
		IOPipe::_eof();
		flush();
	}

	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
};

/*! \brief Converts pointer to a general IOModule to a pointer to
 *  the particular type of I/O module (the child class)
 *  \param io Pointer to an I/O module
 *  \param output Class of the output pointer (#IO_VAR_FILE for \c IOFile*,
 *         #IO_VAR_MEMORY for \c IOMemory*, #IO_VAR_READER for \c IOReader*,
 *         #IO_VAR_WRITER for \c IOWriter*, #IO_VAR_PIPE for \c IOPipe*
 *         and possibly more - that depends of future modifications).
 *  \return pointer to the same module, but to another class. This
 *  function returns NULL if the conversion is not possible.
 *
 *  Due to multiple inheritance used in the hierarchy of I/O modules,
 *  it's not possible to cast \c IOModule* to \c IOPipe* directly (these
 *  are two base clases of p.e. \c IOMemPipe, but they are on the same
 *  level). To obtain pipe-specific data from a general \c IOModule*,
 *  you need to convert the pointer through this function. It checks
 *  the variant member variable of the I/O module a casts the object
 *  to the particular class pointer.
 *
 *  For example, if you want to retrieve pipe-related data from an
 *  object, that is \c IOMemPipe*, but you have an \c IOModule* pointer,
 *  this function will return an \c IOMemPipe* pointer, which can
 *  be implicitely casted to IOPipe*.
 */
void* ioGeneralToParticular(IOModule* io, int output);

/*! \brief Checks, whether an I/O module may be connected to a compression
 *  module for transferring data in the given direction.
 *  \param io Pointer to an I/O module
 *  \param direction Desired direction of data transfers (may be either
 *         __EXC_CONN_INPUT or __EXC_CONN_OUTPUT)
 *  \return \c EXCOM_ERR_OK on succes or an error code compatible with
 *  #exc_connectModules API function.
 */
int ioCheckConnection(IOModule* io, int direction);

#endif // ifndef __IOMODULE_HPP__

