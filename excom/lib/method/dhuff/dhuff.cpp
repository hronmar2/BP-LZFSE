/*
 * File:       method/dhuff/dhuff.hpp
 * Purpose:    Declaration of the Dynamic Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Dynamic Huffman coding module linked to the ExCom library.
 *
 * Dynamic Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dynamic Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Dynamic Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "dhuff.hpp"
#include "DHuffTree.h"

using namespace std;

int CompDHuff::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompDHuff::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompDHuff::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompDHuff::runCompress() {
	unsigned long fileSize;

	int c;

	unsigned int x;

	DHuffTree* dht = new DHuffTree();	

	// Prepare space for future writing of the file size
	writer->writeNBits(32,0);

	fileSize = 0;
	while (c = reader->readByte(), c != EOF) {
		unsigned int length = 0;
		x = dht->insert(c, length);
                writer->writeNBits(length, x);
		fileSize++;
	}

	// The file size written to the first four bytes 
	writer->immWriteBytes(0,4,fileSize);

	delete dht;
	dht = NULL;

	writer->eof();

	return EXCOM_ERR_OK;
}

unsigned char CompDHuff::getSymbol() {
	unsigned long bits = 0;
        eofIndicator = reader->readNBits(ALPHABET_SIZE_BITS, &bits);
        return (unsigned char) bits;
}

int CompDHuff::runDecompress() {
	unsigned long fileSize;
	unsigned long c;

	unsigned char x;

	int i;

	DHuffTree* dht = new DHuffTree();

	i = reader->readNBits(32, &fileSize);

	if (i != EOF) {

		// Get the first symbol
		x = getSymbol();

		if (eofIndicator !=  EOF) {
			writer->writeByte(x);
			dht->insert(x);

			fileSize--;

			do {
				do {
					i = reader->readNBits(1, &c);
					// check EOF
				} while ((!dht->isLeaf(c)) && (i != EOF));

				fileSize--;

				if (i == EOF) {
					break;
				}
				// Symbol not previously encountered
				if (dht->isEsc()) {
					x = getSymbol();	
					writer->writeByte(x);
				// Symbol already seen
				} else if (dht->isLeaf()) {
					x = dht->getCurrentSymbol();
					writer->writeByte(x);
				} else {
					break;
				}

				if (fileSize == 0) break;

				dht->insert(x);
				dht->traverseUp();

			} while (true);

			if (fileSize > 0) {
				delete dht;
				dht = NULL;

				return EXCOM_ERR_INCOMPLETE;
			}
		}
	
		writer->eof();

		delete dht;
		dht = NULL;

		return EXCOM_ERR_OK;
	} else {
		delete dht;
		dht = NULL;

		return EXCOM_ERR_INCOMPLETE;
	}
}

int CompDHuff::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}


