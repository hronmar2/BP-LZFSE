/*
 * File:       libexcom/main.cpp
 * Purpose:    Implementation of the library's outer API (entry points)
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include <excom.h>
#include <pthread.h>
#include <new>
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "handles.hpp"
#include "chain.hpp"
#include "iomodule.hpp"
#include "compmodule.hpp"
#include "method/copy/copy.hpp"
#include "method/histogram/histogram.hpp"
//FIXME: only include these headers if the respective methods are enabled
#include "method/dhuff/dhuff.hpp"
#include "method/shuff/shuff.hpp"
#include "method/lz77/lz77.hpp"
#include "method/lz78/lz78.hpp"
#include "method/lzss/lzss.hpp"
#include "method/lzw/lzw.hpp"
#include "method/lzmw/lzmw.hpp"
#include "method/lzap/lzap.hpp"
#include "method/lzy/lzy.hpp"
#include "method/sfano/sfano.hpp"
#include "method/acb/acb.hpp"
#include "method/dca/dca.hpp"
#include "method/ppm/ppm.hpp"
#include "method/arith/arith.hpp"
#include "method/integer_compression/integer.hpp"
#include "method/bwt/bwt.hpp"
#include "method/mtf/mtf.hpp"
#include "method/rle-n/rle-n.hpp"
#include "method/lzfse/lzfse.hpp"

/* mutex that will only allow executing one API function (other than exc_wait and
 * exc_tryWait) at a time */
static pthread_mutex_t mutex_api;

// Library's init and cleanup functions
// FIXME: how to make this portable?
void __attribute__ ((constructor)) excom_init(void)
{
	int res;
	res = pthread_mutex_init(&mutex_api, NULL);
	if (res != 0) {
		// initialize the mutex statically
		pthread_mutex_t mutex_static = PTHREAD_MUTEX_INITIALIZER;
		mutex_api = mutex_static;
	}
}

void __attribute__ ((destructor)) excom_fini(void)
{
	pthread_mutex_destroy(&mutex_api);
}

//-----------------------------------------------

int exc_setParameter(unsigned int mod_handle, int param, void *value)
{
	void *object;
	enum handle_type_e type;
	int res;

	pthread_mutex_lock(&mutex_api);
	res = hand_alloc.objectFromHandle(mod_handle, &type, &object);
	if (res != EXCOM_ERR_OK) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}
	try {
		switch (type) {
		case HANDLE_IO:
			res = ((IOModule*)object)->setParameter(param, value);
			break;
		case HANDLE_COMPRESSION:
			res = ((CompModule*)object)->setParameter(param, value);
			break;
		default:
			res = EXCOM_ERR_HANDLE;
		}
	} catch (std::bad_alloc &ex) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_ALLOC;
	}
	pthread_mutex_unlock(&mutex_api);
	return res;
}

int exc_getValue(unsigned int mod_handle, int param, void *value)
{
	void *object;
	enum handle_type_e type;
	int res;

	pthread_mutex_lock(&mutex_api);
	res = hand_alloc.objectFromHandle(mod_handle, &type, &object);
	if (res != EXCOM_ERR_OK) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}
	switch (type) {
	case HANDLE_IO:
		res = ((IOModule*)object)->getValue(param, value);
		break;
	case HANDLE_COMPRESSION:
		res = ((CompModule*)object)->getValue(param, value);
		break;
	default:
		res = EXCOM_ERR_HANDLE;
	}
	pthread_mutex_unlock(&mutex_api);
	return res;
}

//-----------------------------------------------

int exc_fileIOModule(const char *filename, enum exc_iotype_e type, unsigned int *mod_handle)
{
	unsigned int handle;
	int res;
	
	// allocate a handle for the new module
	if (mod_handle == NULL) return EXCOM_ERR_HANDLE;
	pthread_mutex_lock(&mutex_api);
	res = hand_alloc.getFreeHandle(&handle);
	if (res != EXCOM_ERR_OK) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}

	// create the particular object
	try {
		if (type == EXCOM_IO_READ) {
			if (filename == NULL) {
				hand_alloc.freeHandle(handle);
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_MEMORY;
			}
			IOFileReader *io = new IOFileReader(handle);
			res = io->openFile(filename);
			if (res != EXCOM_ERR_OK) {
				// deleting the object also frees its handle
				delete io;
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_OPEN;
			}
		} else if (type == EXCOM_IO_WRITE) {
			if (filename == NULL) {
				hand_alloc.freeHandle(handle);
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_MEMORY;
			}
			IOFileWriter *io = new IOFileWriter(handle);
			res = io->openFile(filename);
			if (res != EXCOM_ERR_OK) {
				// deleting the object also frees its handle
				delete io;
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_OPEN;
			}
		} else if (type == EXCOM_IO_PIPE) {
			IOFilePipe *io = new IOFilePipe(handle);
			// the pipe I/O module doesn't require a file name
			if (filename != NULL) {
				res = io->openFile(filename);
				if (res != EXCOM_ERR_OK) {
					// deleting the object also frees its handle
					delete io;
					pthread_mutex_unlock(&mutex_api);
					return EXCOM_ERR_OPEN;
				}
			}
		} else {
			hand_alloc.freeHandle(handle);
			pthread_mutex_unlock(&mutex_api);
			return EXCOM_ERR_PARAM;
		}
	} catch (std::bad_alloc &ex) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_ALLOC;
	}

	pthread_mutex_unlock(&mutex_api);
	*mod_handle = handle;
	return EXCOM_ERR_OK;
}

int exc_memIOModule(void *start, unsigned int length, enum exc_iotype_e type, unsigned int *mod_handle)
{
	unsigned int handle;
	int res;
	
	// allocate a handle for the new module
	if (mod_handle == NULL) return EXCOM_ERR_HANDLE;
	pthread_mutex_lock(&mutex_api);
	res = hand_alloc.getFreeHandle(&handle);
	if (res != EXCOM_ERR_OK) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}

	try {
		// create the particular object
		if (type == EXCOM_IO_READ) {
			if (start == NULL) {
				hand_alloc.freeHandle(handle);
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_MEMORY;
			}
			IOMemReader *io = new IOMemReader(handle);
			res = io->attachMemory(start, length);
			if (res != EXCOM_ERR_OK) {
				// deleting the object also frees its handle
				delete io;
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_MEMORY;
			}
		} else if (type == EXCOM_IO_WRITE) {
			if (start == NULL) {
				hand_alloc.freeHandle(handle);
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_MEMORY;
			}
			IOMemWriter *io = new IOMemWriter(handle);
			res = io->attachMemory(start, length);
			if (res != EXCOM_ERR_OK) {
				// deleting the object also frees its handle
				delete io;
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_MEMORY;
			}
		} else if (type == EXCOM_IO_PIPE) {
			IOMemPipe *io = new IOMemPipe(handle);
			// the pipe I/O module doesn't require a valid memory block
			if (start != NULL) {
				res = io->attachMemory(start, length);
				if (res != EXCOM_ERR_OK) {
					// deleting the object also frees its handle
					delete io;
					pthread_mutex_unlock(&mutex_api);
					return EXCOM_ERR_MEMORY;
				}
			}
		} else {
			hand_alloc.freeHandle(handle);
			pthread_mutex_unlock(&mutex_api);
			return EXCOM_ERR_PARAM;
		}
	} catch (std::bad_alloc &ex) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_ALLOC;
	}

	pthread_mutex_unlock(&mutex_api);
	*mod_handle = handle;
	return EXCOM_ERR_OK;
}

int exc_destroyIOModule(unsigned int mod_handle)
{
	int res;
	IOModule *io;
	enum handle_type_e type;

	pthread_mutex_lock(&mutex_api);
	// translate I/O module handle
	res = hand_alloc.objectFromHandle(mod_handle, &type, (void**)&io);
	if ((res != EXCOM_ERR_OK) || (type != HANDLE_IO)) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}

	delete io;

	pthread_mutex_unlock(&mutex_api);
	return EXCOM_ERR_OK;
}


//-----------------------------------------------

int exc_compModule(enum exc_method_e method, enum exc_oper_e operation, unsigned int chain_handle, unsigned int *mod_handle)
{
	unsigned int handle;
	int res;
	CompModule *comp = NULL;
	enum handle_type_e type;
	CompressionChain *chain;
	
	if (mod_handle == NULL) return EXCOM_ERR_HANDLE;
	pthread_mutex_lock(&mutex_api);
	// translate chain's handle to a chain object
	res = hand_alloc.objectFromHandle(chain_handle, &type, (void**)&chain);
	if ((res != EXCOM_ERR_OK) || (type != HANDLE_CHAIN)) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_CHAIN;
	}

	// allocate a handle for the new module
	res = hand_alloc.getFreeHandle(&handle);
	if (res != EXCOM_ERR_OK) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}

	try {
		// create the particular object depending on selected method
		if (method == EXCOM_MET_COPY) {
			comp = new CompCopy(handle);
		}
		else if (method == EXCOM_MET_HISTOGRAM) {
			comp = new CompHistogram(handle);
		}
		else if (method == EXCOM_MET_DHUFF) {
			comp = new CompDHuff(handle);
		}
		else if (method == EXCOM_MET_SHUFF) {
			comp = new CompSHuff(handle);
		}
		else if (method == EXCOM_MET_LZ77) {
			comp = new CompLZ77(handle);
		}
		else if (method == EXCOM_MET_LZ78) {
			comp = new CompLZ78(handle);
		}
		else if (method == EXCOM_MET_LZSS) {
			comp = new CompLZSS(handle);
		}
		else if (method == EXCOM_MET_LZW) {
			comp = new CompLZW(handle);
		}
//#ifdef METHOD_LZMW
		else if (method == EXCOM_MET_LZMW) {
			comp = new CompLZMW(handle);
		}
//#endif
//#ifdef METHOD_LZAP
		else if (method == EXCOM_MET_LZAP) {
			comp = new CompLZAP(handle);
		}
//#endif
//#ifdef METHOD_LZY
		else if (method == EXCOM_MET_LZY) {
			comp = new CompLZY(handle);
		}
//#endif
		else if (method == EXCOM_MET_SFANO) {
			comp = new CompSFano(handle);
		}
#ifdef METHOD_DCA
		else if (method == EXCOM_MET_DCA) {
			comp = new CompDCA(handle);
		}
#endif
#ifdef METHOD_ACB
		else if (method == EXCOM_MET_ACB) {
			comp = new CompACB(handle);
		}
#endif
#ifdef METHOD_PPM
		else if (method == EXCOM_MET_PPM) {
			comp = new CompPPM(handle);
		}
#endif
//#ifdef METHOD_ARITH
		else if (method == EXCOM_MET_ARITH) {
			comp = new CompArith(handle);
		}
//#endif
//#ifdef METHOD_INTEGER
		else if (method == EXCOM_MET_INTEGER) {
			comp = new CompInteger(handle);
		}
//#endif	
//#ifdef METHOD_BWT
		else if (method == EXCOM_MET_BWT) {
			comp = new CompBWT(handle);
		}
//#endif
//#ifdef METHOD_MTF
		else if (method == EXCOM_MET_MTF) {
			comp = new CompMTF(handle);
		}
//#endif
//#ifdef METHOD_RLE_N
		else if (method == EXCOM_MET_RLE_N) {
			comp = new CompRLE_N(handle);
		}
//#endif
//#ifdef METHOD_LZFSE
		else if (method == EXCOM_MET_LZFSE) {
			comp = new CompLZFSE(handle);
		}
//#endif
		else {
			hand_alloc.freeHandle(handle);
			pthread_mutex_unlock(&mutex_api);
			return EXCOM_ERR_METHOD;
		}

		// set operation that the module will be performing
		res = comp->setOperation(operation);
		if (res != EXCOM_ERR_OK) {
			// deleting the object also frees its handle
			delete comp;
			pthread_mutex_unlock(&mutex_api);
			return EXCOM_ERR_OPERATION;
		}

		// add module to the compression chain
		chain->addCompModule(comp);

	} catch (std::bad_alloc &ex) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_ALLOC;
	}

	pthread_mutex_unlock(&mutex_api);
	*mod_handle = handle;
	return EXCOM_ERR_OK;
}

//-----------------------------------------------

int exc_compressionChain(unsigned int *chain_handle)
{
	unsigned int handle;
	int res;
	CompressionChain *chain;

	if (chain_handle == NULL) return EXCOM_ERR_HANDLE;
	pthread_mutex_lock(&mutex_api);
	// allocate a handle for the new chain
	res = hand_alloc.getFreeHandle(&handle);
	if (res != EXCOM_ERR_OK) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}

	// create the new chain
	try {
		chain = new CompressionChain(handle);
	} catch (std::bad_alloc &ex) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_ALLOC;
	}

	pthread_mutex_unlock(&mutex_api);
	*chain_handle = handle;
	return EXCOM_ERR_OK;
}

int exc_destroyChain(unsigned int chain_handle)
{
	enum handle_type_e type;
	CompressionChain *chain;
	int res;

	pthread_mutex_lock(&mutex_api);
	// translate chain's handle to a chain object
	res = hand_alloc.objectFromHandle(chain_handle, &type, (void**)&chain);
	if ((res != EXCOM_ERR_OK) || (type != HANDLE_CHAIN)) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}
	if (chain->running) {
		// a running chain cannot be destroyed
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_BUSY;
	}

	delete chain;

	pthread_mutex_unlock(&mutex_api);
	return EXCOM_ERR_OK;
}

int exc_connectModules(unsigned int iomod_handle, unsigned int compmod_handle, int connection_type)
{
	enum handle_type_e type;
	int res;
	unsigned int chain_handle;
	CompressionChain *chain;
	IOModule *io;
	CompModule *comp;

	pthread_mutex_lock(&mutex_api);
	// translate I/O module handle
	res = hand_alloc.objectFromHandle(iomod_handle, &type, (void**)&io);
	if ((res != EXCOM_ERR_OK) || (type != HANDLE_IO)) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}

	// translate compression module handle
	res = hand_alloc.objectFromHandle(compmod_handle, &type, (void**)&comp);
	if ((res != EXCOM_ERR_OK) || (type != HANDLE_COMPRESSION)) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}
	chain = comp->chain;
	chain_handle = chain->chain_handle;

	if (io->variant & IO_VAR_PIPE) {
		if (io->connected > 1) {
			// the I/O module has already all its ends connected
			pthread_mutex_unlock(&mutex_api);
			return EXCOM_ERR_DUPLICATE;
		} else if (io->connected == 1) {
			// check whether both ends of the pipe are in the same chain
			IOPipe *pipe = (IOPipe*) ioGeneralToParticular(io, IO_VAR_PIPE);
			if (pipe->chain_handle != chain_handle) {
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_CHAIN;
			}
			// check whether the same end of the pipe hasn't been already connected
			if (\
				((connection_type & __EXC_CONN_INPUT) && (pipe->output_attached)) ||\
				((connection_type & __EXC_CONN_OUTPUT) && (pipe->input_attached))) {
				pthread_mutex_unlock(&mutex_api);
				return EXCOM_ERR_DUPLICATE;
			}
		}
	} else {
		if (io->connected > 0) {
			// the I/O module has already all its ends connected
			pthread_mutex_unlock(&mutex_api);
			return EXCOM_ERR_DUPLICATE;
		}
		// check whether the I/O module provides transferring data in the requested direction
		// (either input or output)
		if (\
			((connection_type & __EXC_CONN_INPUT)  && !(io->variant & IO_VAR_READER)) ||\
			((connection_type & __EXC_CONN_OUTPUT) && !(io->variant & IO_VAR_WRITER))) {
			pthread_mutex_unlock(&mutex_api);
			return EXCOM_ERR_PARAM;
		}
	}

	// connect I/O module to the compression module
	res = comp->connectIOModule(io, connection_type);
	if (res != EXCOM_ERR_OK) {
		pthread_mutex_unlock(&mutex_api);
		return res;
	}

	// update all necessary member variables of the I/O module
	if (io->variant & IO_VAR_PIPE) {
		IOPipe *pipe = (IOPipe*) ioGeneralToParticular(io, IO_VAR_PIPE);
		pipe->chain_handle = chain_handle;
		if (connection_type & __EXC_CONN_INPUT) pipe->output_attached = 1;
		else if (connection_type & __EXC_CONN_OUTPUT) pipe->input_attached = 1;
	}
	if (io->connected == 0) {
		/* important: only add the I/O module, if it's the first time it's being connected */
		// let the compression chain know about the connection
		chain->addIOModule(io);
	}
	io->connected++;
	
	pthread_mutex_unlock(&mutex_api);
	return EXCOM_ERR_OK;
}

int exc_run(unsigned int chain_handle)
{
	enum handle_type_e type;
	CompressionChain *chain;
	int res;

	pthread_mutex_lock(&mutex_api);
	// translate chain's handle to a chain object
	res = hand_alloc.objectFromHandle(chain_handle, &type, (void**)&chain);
	if ((res != EXCOM_ERR_OK) || (type != HANDLE_CHAIN)) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_CHAIN;
	}

	try {
		res = chain->run();
	} catch (std::bad_alloc &ex) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_ALLOC;
	}
	pthread_mutex_unlock(&mutex_api);
	return res;
}

int exc_wait(unsigned int chain_handle)
{
	enum handle_type_e type;
	CompressionChain *chain;
	int res;

	// translate chain's handle to a chain object
	pthread_mutex_lock(&mutex_api);
	res = hand_alloc.objectFromHandle(chain_handle, &type, (void**)&chain);
	if ((res != EXCOM_ERR_OK) || (type != HANDLE_CHAIN)) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_CHAIN;
	}
	if (chain->running == 0) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_NOT_NOW;
	}
	pthread_mutex_unlock(&mutex_api);

	/* Note: The exc_wait function doesn't lock mutex_api because waiting for
	 * all threads may take long. Anyway, waiting will not cause race conditions.
	 * We only have to make sure, that chain->wait sets this->running to 0. */

	return chain->wait();	
}

int exc_tryWait(unsigned int chain_handle)
{
	enum handle_type_e type;
	CompressionChain *chain;
	int res;

	// translate chain's handle to a chain object
	pthread_mutex_lock(&mutex_api);
	res = hand_alloc.objectFromHandle(chain_handle, &type, (void**)&chain);
	if ((res != EXCOM_ERR_OK) || (type != HANDLE_CHAIN)) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_CHAIN;
	}
	if (chain->running == 0) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_NOT_NOW;
	}
	pthread_mutex_unlock(&mutex_api);

	/* note: The exc_tryWait function doesn't lock mutex_api, even though execution
	 * of this function shouldn't take long. Anyway, waiting will not cause race conditions.
	 * We only have to make sure, that chain->wait sets this->running to 0. */

	return chain->tryWait();	
}

//-----------------------------------------------

int exc_getPerformanceData(unsigned int mod_handle, unsigned long *time)
{
#ifdef PERF_MON
	enum handle_type_e type;
	void *object;
	int res;

	pthread_mutex_lock(&mutex_api);
	// translate handle to a pointer to the respective object
	res = hand_alloc.objectFromHandle(mod_handle, &type, &object);
	if (res != EXCOM_ERR_OK) {
		pthread_mutex_unlock(&mutex_api);
		return EXCOM_ERR_HANDLE;
	}

	CompModule *comp;
	CompressionChain *chain;
	switch (type) {
	case HANDLE_COMPRESSION:
		comp = (CompModule*) object;
		res = comp->getPerformanceData(time);
		break;
	case HANDLE_CHAIN:
		chain = (CompressionChain*) object;
		res = chain->getPerformanceData(time);
		break;
	default:
		// other modules don't provide performance info
		res = EXCOM_ERR_HANDLE;
	}
	pthread_mutex_unlock(&mutex_api);
	return res;

#else   //ifdef PERF_MON
	// performance measurement wasn't compiled into the library
	return EXCOM_ERR_MEASURE;
#endif  //ifdef PERF_MON
}
