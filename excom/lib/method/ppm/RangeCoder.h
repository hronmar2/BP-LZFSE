#ifndef __RANGE_CODER_H__
#define __RANGE_CODER_H__

/*
 * File:       method/ppm/PPMDcoder.cpp
 * Purpose:    Implementation of range coder (arithmetic coding)
 *
 * Based on implementation from p7zip
 *
 * Copyright (C) 1999-2007 Igor Pavlov
 * Copyright (C) 2009 Filip Simek <simekf1@fel.cvut.cz>
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * Changes:
 *   090409 by Filip Simek <simekf1@fel.cvut.cz>
 *          numeric types changed to those defined in stdint.h (to further typedefs applied)
 *          p7zip's streams replaced by ExCom's I/O modules
 */

#include <stdint.h>
#include <excom.h>
#include "../../iomodule.hpp"

namespace rangecoder {

const int kNumTopBits = 24;
const uint32_t kTopValue = (1 << kNumTopBits);

class CEncoder
{
	uint32_t _cacheSize;
	uint8_t _cache;
public:
	uint64_t Low;
	uint32_t Range;
	::IOWriter *Stream;

	void SetStream(::IOWriter *stream) {
		Stream = stream;
	}

	void Init() {
		Low = 0;
		Range = 0xFFFFFFFF;
		_cacheSize = 1;
		_cache = 0;
	}

	void FlushData() {
		// Low += 1;
		for (int i = 0; i < 5; i++)
			ShiftLow();
	}

	void Encode(uint32_t start, uint32_t size, uint32_t total) {
		Low += start * (Range /= total);
		Range *= size;
		while (Range < kTopValue) {
			Range <<= 8;
			ShiftLow();
		}
	}

	void ShiftLow() {
		if ((uint32_t)Low < (uint32_t)0xFF000000 || (int)(Low >> 32) != 0) {
			uint8_t temp = _cache;
			do {
				Stream->writeByte((unsigned char)(temp + (unsigned char)(Low >> 32)));
				temp = 0xFF;
			} while (--_cacheSize != 0);
			_cache = (uint8_t)((uint32_t)Low >> 24);
		}
		_cacheSize++;
		Low = (uint32_t)Low << 8;
	}

	void EncodeDirectBits(uint32_t value, int numBits) {
		for (numBits--; numBits >= 0; numBits--) {
			Range >>= 1;
			Low += Range & (0 - ((value >> numBits) & 1));
			if (Range < kTopValue) {
				Range <<= 8;
				ShiftLow();
			}
		}
	}

	void EncodeBit(uint32_t size0, uint32_t numTotalBits, uint32_t symbol) {
		uint32_t newBound = (Range >> numTotalBits) * size0;
		if (symbol == 0)
			Range = newBound;
		else {
			Low += newBound;
			Range -= newBound;
		}
		while (Range < kTopValue) {
			Range <<= 8;
			ShiftLow();
		}
	}
};

class CDecoder
{
public:
	::IOReader *Stream;
	uint32_t Range;
	uint32_t Code;

	void Normalize() {
		int x;
		while (Range < kTopValue) {
			x = Stream->readByte();
			if (x == EOF) throw EOF;
			Code = (Code << 8) | x;
			Range <<= 8;
		}
	}

	void SetStream(::IOReader *stream) {
		Stream = stream;
	}

	void Init() {
		int x;
		Code = 0;
		Range = 0xFFFFFFFF;
		for (int i = 0; i < 5; i++) {
			x = Stream->readByte();
			if (x == EOF) throw EOF;
			Code = (Code << 8) | x;
		}
	}

	uint32_t GetThreshold(uint32_t total) {
		return (Code) / (Range /= total);
	}

	void Decode(uint32_t start, uint32_t size) {
		Code -= start * Range;
		Range *= size;
		Normalize();
	}

	uint32_t DecodeDirectBits(int numTotalBits) {
		uint32_t range = Range;
		uint32_t code = Code;
		uint32_t result = 0;
		for (int i = numTotalBits; i != 0; i--) {
			range >>= 1;
			/*
			result <<= 1;
			if (code >= range)
			{
			  code -= range;
			  result |= 1;
			}
			*/
			uint32_t t = (code - range) >> 31;
			code -= range & (t - 1);
			result = (result << 1) | (1 - t);

			if (range < kTopValue) {
				int x = Stream->readByte();
				if (x == EOF) throw EOF;
				code = (code << 8) | x;
				range <<= 8;
			}
		}
		Range = range;
		Code = code;
		return result;
	}

	uint32_t DecodeBit(uint32_t size0, uint32_t numTotalBits) {
		uint32_t newBound = (Range >> numTotalBits) * size0;
		uint32_t symbol;
		if (Code < newBound) {
			symbol = 0;
			Range = newBound;
		} else {
			symbol = 1;
			Code -= newBound;
			Range -= newBound;
		}
		Normalize();
		return symbol;
	}
};

}

#endif // ifndef __RANGE_CODER_H__
