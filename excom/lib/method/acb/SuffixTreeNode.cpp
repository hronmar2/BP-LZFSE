/*
 * File:       method/acb/SuffixTreeNode.cpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "SuffixTreeNode.hpp"

SuffixTreeNode::SuffixTreeNode() {
	init(0l, 0l, 0l);
	SuffixTreeNode* node = new SuffixTreeNode(-1, -1, -1);
	children = new children_map_t();
	children->insert(child_entry_t('\0', node));
}

SuffixTreeNode::SuffixTreeNode(long position, long nodeStartPosition, long nodeEndPosition){
        init(position, nodeStartPosition, nodeEndPosition);
};

void SuffixTreeNode::init(long position, long nodeStartPosition, long nodeEndPosition) {
	this->position = position;
	this->nodeStartPosition = nodeStartPosition;
	this->nodeEndPosition = nodeEndPosition;
	this->leftSibling = NULL;
	this->rightSibling = NULL;
	this->leaf = true;
        children = NULL;
}

SuffixTreeNode::~SuffixTreeNode(){
	if(children != NULL) {
                for(children_iterator_t it = children->begin(); it != children->end(); it++) {
                    delete it->second;
                }
		children->clear();
		delete children;
		children = NULL;
	}
};

void SuffixTreeNode::addNode(const uint16_t* data, long position, long nodeStartPosition) {
	unsigned char character = (unsigned char) data[nodeStartPosition];
	children_iterator_t it = children->find(character);
	SuffixTreeNode* node = NULL;
	if(it != children->end()) {
		node = it->second;
	}
	if(node == NULL) {
		node = new SuffixTreeNode(position, nodeStartPosition, 0);
		this->leaf = false;
		children->insert(child_entry_t(character, node));
		it = children->upper_bound(character);
		// node was added somewhere in the middle/first of children list
		if(it != children->end()) {
			SuffixTreeNode* rightSibling = it->second;
			while(!rightSibling->isLeaf()) {
				rightSibling = rightSibling->children->begin()->second;
			}
			node->rightSibling = rightSibling;
			node->leftSibling = rightSibling->leftSibling;
			node->rightSibling->leftSibling = node;
			if(node->leftSibling !=  NULL) {
				node->leftSibling->rightSibling = node;
			}
		} else {
			// Juast added item is last element, so -1 to get just added element
			it--;
			SuffixTreeNode* leftSibling;
			// If current element is not only one, take left element
			// This is only posibility, since if I am adding, it means there already exists some sibling
			if(it != children->begin()) {
				it--;
				leftSibling = it->second;
				while(!leftSibling->isLeaf()) {
					leftSibling = leftSibling->children->rbegin()->second;
				}
			} else {
				leftSibling = this->leftSibling;
			}
			node->leftSibling = leftSibling;
			node->rightSibling = leftSibling->rightSibling;
			node->leftSibling->rightSibling = node;
			if(node->rightSibling != NULL) {
				node->rightSibling->leftSibling = node;
			}
		}
	} else {
		int branch = 0;
		// Do not compare first character
		long same = 1;
		nodeStartPosition--;
		long currentNodeStartPosition = node->nodeStartPosition - same;
		while(currentNodeStartPosition >= node->nodeEndPosition) {
			if(data[nodeStartPosition] != data[currentNodeStartPosition]) {
				node->branchNode(data, position, nodeStartPosition, same);
				branch = 1;
				break;
			}
			same++;
			currentNodeStartPosition--;
			nodeStartPosition--;
		}
		if(!branch) {
			if(!node->isLeaf()) {
				// Current node has children so ill just pass the creation on them
				node->addNode(data, position, nodeStartPosition);
			} else {
				// Current node has no children, i have to branch, adding empty node and this new node
				node->branchNode(data, position, nodeStartPosition, same);
			}
		}
	}
}

void SuffixTreeNode::branchNode(const uint16_t* data, long position, long nodeStartPosition, long same) {
	
	long branchedStartPosition = this->nodeStartPosition - same;
	unsigned char startingCharacterA = '\0';
	if(branchedStartPosition >= 0) {
		startingCharacterA = (unsigned char) data[branchedStartPosition];
	}
	SuffixTreeNode* nodeA = new SuffixTreeNode(this->position, branchedStartPosition , this->nodeEndPosition);

	unsigned char startingCharacterB = (unsigned char) data[nodeStartPosition];
	SuffixTreeNode* nodeB = new SuffixTreeNode(position, nodeStartPosition, 0);

	if(children != NULL) {
		nodeA->children = children;
		if(nodeA->children->size() > 0) {
			nodeA->leaf = false;
		}
	}
	
	children = new children_map_t();

	children->insert(child_entry_t(startingCharacterA, nodeA));
	children->insert(child_entry_t(startingCharacterB, nodeB));

	if(!nodeA->isLeaf()) {
		// Node A is not a leaf - has children
		if(startingCharacterA < startingCharacterB) {
			// If branched node is lower than new node, leftSibling is rightmost child
			// rightSibling is rightmosts child rightSibling
			while(!nodeA->isLeaf()) {
				nodeA = nodeA->children->rbegin()->second;
			}
			
			nodeB->rightSibling = nodeA->rightSibling;
			if(nodeB->rightSibling != NULL) {
				nodeB->rightSibling->leftSibling = nodeB;
			}
			nodeA->rightSibling = nodeB;
			nodeB->leftSibling = nodeA;
		} else {
			// If branched node is greater than new node, rightSibling is lefttmost child
			// leftSibling is lefttmosts child leftSibling
			while(!nodeA->isLeaf()) {
				nodeA = nodeA->children->begin()->second;
			}
			nodeB->leftSibling = nodeA->leftSibling;
			if(nodeB->leftSibling != NULL) {
				nodeB->leftSibling->rightSibling = nodeB;
			}
			nodeA->leftSibling = nodeB;
			nodeB->rightSibling = nodeA;
		}
	} else {
		// Update siblings
		if(startingCharacterA > startingCharacterB) {
			// Swith nodes
			SuffixTreeNode* temp = nodeA;
			nodeA = nodeB;
			nodeB = temp;
		}

		// Branching from leaf - leftSibling is parents leftSibling
		// and rightSibling is parents rightSibling
		// nodes show on each other
		nodeA->leftSibling = this->leftSibling;
		if(nodeA->leftSibling != NULL ) {
			nodeA->leftSibling->rightSibling = nodeA;
		}
		nodeA->rightSibling = nodeB;

		nodeB->leftSibling = nodeA;
		nodeB->rightSibling = this->rightSibling;
		if(nodeB->rightSibling != NULL ) {
			nodeB->rightSibling->leftSibling = nodeB;
		}
	}

	this->nodeEndPosition = this->nodeStartPosition - same + 1;
	this->leaf = false;
}

bool SuffixTreeNode::isLeaf() {
	return leaf;
}
