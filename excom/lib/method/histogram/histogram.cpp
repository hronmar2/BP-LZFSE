/*
 * File:       method/histogram/histogram.cpp
 * Purpose:    Implementation of pseudo-compression method which extracts statistical data from input
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "histogram.hpp"

int CompHistogram::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompHistogram::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_HISTOGRAM) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompHistogram::setParameter(int parameter, void *value)
{
	unsigned int u;
	int v;
	switch (parameter) {
	case EXCOM_PARAM_HIST_BYTES_PER_CHAR:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u < 1 || u > 4) return EXCOM_ERR_VALUE;
		bpc = u;
		break;
	case EXCOM_PARAM_HIST_INTERVALS:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u < 1) return EXCOM_ERR_VALUE;
		intervals = u;
		break;
	case EXCOM_PARAM_HIST_OUTPUT:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		v = *((int*)value);
		if (v < EXCOM_HIST_OUTPUT_BINARY ||\
			v > EXCOM_HIST_OUTPUT_HUMAN) return EXCOM_ERR_VALUE;
		output = v;
		break;
	case EXCOM_PARAM_HIST_RANGE_MIN:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		v = *((int*)value);
		range_min = v;
		break;
	case EXCOM_PARAM_HIST_RANGE_MAX:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		v = *((int*)value);
		range_max = v;
		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompHistogram::getValue(int parameter, void *value)
{
	//TODO: later
	return CompModule::getValue(parameter, value);
}


int CompHistogram::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	if (data != NULL) delete [] data;
	data = NULL;
	unsigned int x;
	x = 1 << (8*bpc);
	// sanitize ranges
	if (range_min < 0) real_rmin = 0;
	else real_rmin = range_min;
	if (range_max < 0) real_rmax = x-1;
	else real_rmax = range_max;
	if (real_rmax < real_rmin) {
		unsigned int a = real_rmax;
		real_rmax = real_rmin;
		real_rmin = a;
	}
	x = real_rmax - real_rmin + 1;
	if (x < intervals) intervals = x;
	data = new unsigned int[intervals+1];
	memset(data, 0, (intervals+1) * sizeof(unsigned int));
	int_size = x / intervals;
	return EXCOM_ERR_OK;
}

static void _printNumber(IOWriter *writer, unsigned int i)
{
	if (i == 0) {
		writer->writeByte('0');
		return;
	}
	unsigned int div = 1;
	unsigned int j;
	while (10*div <= i) {
		div *= 10;
	}
	while (div > 0) {
		j = i / div;
		writer->writeByte((unsigned char)j+'0');
		i %= div;
		div /= 10;
	}
}

#define FOURBYTE(x) \
	writer->writeByte((unsigned char)(x & 0xff));\
	writer->writeByte((unsigned char)((x>>8) & 0xff));\
	writer->writeByte((unsigned char)((x>>16) & 0xff));\
	writer->writeByte((unsigned char)((x>>24) & 0xff))
int CompHistogram::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif
	unsigned long val;
	int x;
	do {
		val = 0;
		for (unsigned int i = 0; i < bpc; i++) {
			x = reader->readByte();
			if (x == EOF) goto data_end;
			val |= ((unsigned int)x) << (8*i);
		}
		if (val < real_rmin || val > real_rmax) {
			// out of range
			data[intervals]++;
		} else {
			val = (val - real_rmin) / int_size;
			data[val]++;
		}
	} while (true);

data_end:
	unsigned int acc;
	switch (output) {
	case EXCOM_HIST_OUTPUT_BINARY:
		writer->writeByte((unsigned char)bpc);
		FOURBYTE(intervals);
		FOURBYTE(real_rmin);
		for (unsigned int i = 0; i <= intervals; i++) {
			FOURBYTE(data[i]);
		}
		break;
	case EXCOM_HIST_OUTPUT_HUMAN:
		acc = real_rmin;
		for (unsigned int i = 0; i < intervals; i++) {
			if (int_size > 1) {
				writer->writeByte('[');
				_printNumber(writer, acc);
				writer->writeByte('.');
				writer->writeByte('.');
				_printNumber(writer, acc + int_size);
				writer->writeByte(')');
			} else {
				_printNumber(writer, acc);
			}
			writer->writeByte(':');
			writer->writeByte(' ');
			_printNumber(writer, data[i]);
			writer->writeByte(10);
			acc += int_size;
		}
		writer->writeByte('o');
		writer->writeByte('u');
		writer->writeByte('t');
		writer->writeByte(':');
		writer->writeByte(' ');
		_printNumber(writer, data[intervals]);
		writer->writeByte(10);
		break;
	}
	writer->eof();

#ifdef PERF_MON
	PERF_STOP
#endif

	return EXCOM_ERR_OK;
}


