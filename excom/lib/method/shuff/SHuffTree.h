#ifndef _STATIC_HUFF_TREE_H
#define	_STATIC_HUFF_TREE_H

/*
 * File:       method/shuff/SHuffTree.h
 * Purpose:    Declaration of the Static Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Static Huffman coding module linked to the ExCom library.
 *
 * Static Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Static Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Static Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file SHuffTree.h
 *  \brief Declaration of Static Huffman tree representation.
 *
 *  This class represents a static Huffman tree and its manipulation 
 */

#include "SHuffNode.h"
#include "../universal_codes/UniversalCodes.h"

#include <queue>
#include <vector>
#include <map>

#define ALPHABET_SIZE 256
#define INTERNAL_NODE_CODE "0"
#define LEAF_NODE_CODE "1"
#define BITS_PER_SYMBOL 8

using namespace std;

class SHuffTree {
private:
	int leavesCount;

	vector<SHuffNode*> nodes;
	vector<SHuffNode*>::iterator nodeIter;	

	SHuffNode** leaves;

	priority_queue<SHuffNode*, vector<SHuffNode*>, SHuffNode::compare_nodes > nodeQueue;
	unsigned long* symbolFrequency;

	bool err;

	SHuffNode* currentNode;
	SHuffNode* root;

	UniversalCodes* universalCodes;

	/*! \brief Inserts an internal node to the tree
	 *  \param node Pointer to a Huffman node
	 */
	void addNode(SHuffNode* node);
	/*! \brief Assigns the codes to all input symbols
	 * 
	 *  The codes are obtained by a tree traversal
	 */
	void setSymbolCodes();

	/*! \brief Returns a binary code for a particular node
	 *  \param node Pointer to a node
	 *  \param length Codeword length in bits
	 *  \return The binary codeword as intiger
	 */
	unsigned int getCode(SHuffNode* node, unsigned int &length);
	/*! \brief Traverses the whole Huffman tree
	 *  \param node Pointer to node where to split the tree
	 *  \return The Static Huffman tree as a string
	 *
	 *  This function traverses the tree in preorder, i.e., starts at the root
	 *  and continues to visit all nodes and thus to save it in a binary form,
	 *  which would be easy to reconstruct it again
	 */
	string traversePreorder(SHuffNode* node); 
public:
	// Constructor
	SHuffTree();

	// Destructor
	~SHuffTree();

	/*! \brief Adds a symbol to the tree
	 *  \param s A symbol to be added
	 *
	 *  This function checks whether the symbol was previously encountered
	 *  if yes, its frequency is increased, otherwise the symbol is added	 
	 */
	void addSymbol(unsigned char s);
	/*! \brief Reconstructs the static Huffman tree
	 *  \param s A tree in binary form as a string
	 *
	 *  This function reconstructs a tree to obtain the symbol codes
	 *  however the frequencies of occurrence are not retrieved due
	 *  to saving more space	 
	 */
	void reconstructTree(string s);
	/*! \brief Sets the root as a current node
	 *
	 *  This function is called when a leaf during the traversal is reached
	 *  when the decoder reads a bit by bit with a view to find the match
	 *  for a codeword. In other words, it is some kind of reset 
	 */
	void traverseUp();

	/*! \brief Generates a tree from the input symbols
	 */
	bool buildTree();
	/*! \brief Checks if the current node is a leaf
	 *
	 *  This function is called when a leaf during the traversal is reached
	 *  when the decoder reads a bit by bit with a view to find the match
	 *  for a codeword. In other words, it is some kind of reset 
	 */
	bool isLeaf(int);
	/*! \brief Returns true if some error occurs
	 */
	bool isErr();

	/*! \brief Returns a code for the current node in a tree
	 */
	unsigned char getActualNodeSymbol();

	/*! \brief Returns a code of a node for a given symbol
	 *  \param s A symbol
	 */
	unsigned int getSymbolCode(unsigned char s, unsigned int &length);
	/*! \brief A wrapper function for a tree traversal
	 *
	 *  Preorder traversal, 0=internal node, 1=leaf + its code
	 */
	string getTreeAsString();
};

#endif	/* _STATIC_HUFF_TREE_H */

