#include <unistd.h>
#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/iomodule.hpp"

#define HANDLE 1
#define FILEREADER "data_reader"
#define FILEWRITER "data_writer"

class IOMemReaderTestSuite: public CxxTest::TestSuite {
	IOMemReader *reader;
public:
	void setUp() {
		reader = new IOMemReader(HANDLE);
	}
	void tearDown() {
		delete reader;
	}

	void testParameters() {
		/* test setting parameters and retrieving values */
		unsigned int val;

		// invalid memory pointer
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_POSITION, NULL), EXCOM_ERR_MEMORY);
		// invalid parameter constant
		TS_ASSERT_EQUALS(reader->getValue(-1, &val), EXCOM_ERR_PARAM);
		// position in the stream must be 0
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// buffer size must be 0 for MemReader
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_BUFFER_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// MemReader shouldn't allow setting buffer size
		TS_ASSERT_EQUALS(reader->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_PARAM);
	}

	void testReadBytes() {
		char bin[10];
		char bout[10];
		unsigned int val;

		for (int i = 0; i < 10; i++) {
			bin[i] = 'A' + i;
			bout[i] = 0;
		}

		// test all possible return values
		TS_ASSERT_EQUALS(reader->attachMemory(NULL, 10), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(reader->attachMemory(bin, 10), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(reader->attachMemory(bin, 10), EXCOM_ERR_DUPLICATE);

		// try to read data, no EOF should occur
		for (int i = 0; i < 10; i++) {
			int c;
			c = reader->readByte();
			TS_ASSERT_DIFFERS(EOF, c);
			bout[i] = (char) c;
		}
		// all data has been read, we should get an EOF
		TS_ASSERT_EQUALS(reader->readByte(), EOF);
		// read data should be the same as the original data
		TS_ASSERT_SAME_DATA(bin, bout, 10);

		// current stream position should be beyond the end of the buffer
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_LESS_THAN(9u, val);

		// rewinding the stream should move the pointer to the beginning
		reader->rewind();
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		TS_ASSERT_EQUALS(reader->readByte(), 'A');
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 1u);
	}

	void testReadBits() {
		unsigned char bin[8] = {0xde, 0xad, 0xbe, 0xef, 0x12, 0x00, 0x97, 0xff};
		unsigned long val;
		TS_ASSERT_EQUALS(reader->attachMemory(bin, 8), EXCOM_ERR_OK);
		// read more than one byte
		TS_ASSERT_EQUALS(reader->readNBits(12, &val), 0);
		TS_ASSERT_EQUALS(val, 0xdeau);
		// round-up to whole bytes
		TS_ASSERT_EQUALS(reader->readNBits(4, &val), 0);
		TS_ASSERT_EQUALS(val, 0xdu);
		// combine with the readByte function
		val = (unsigned long) reader->readByte();
		TS_ASSERT_EQUALS(val, 0xbeu);
		// read one byte = 8 bits
		TS_ASSERT_EQUALS(reader->readNBits(8, &val), 0);
		TS_ASSERT_EQUALS(val, 0xefu);
		// read 0 bits
		TS_ASSERT_EQUALS(reader->readNBits(0, &val), 0);
		// odd number of bits
		TS_ASSERT_EQUALS(reader->readNBits(3, &val), 0);
		TS_ASSERT_EQUALS(val, 0x0u);
		TS_ASSERT_EQUALS(reader->readNBits(15, &val), 0);
		TS_ASSERT_EQUALS(val, 0x4802u);
		// attempt to read more bits than is left
		TS_ASSERT_EQUALS(reader->readNBits(16, &val), EOF);
		// read the remaining 14 bits
		TS_ASSERT_EQUALS(reader->readNBits(14, &val), 0);
		TS_ASSERT_EQUALS(val, 0x17ffu);
		// nothing left
		TS_ASSERT_EQUALS(reader->readNBits(1, &val), EOF);

		reader->rewind();
		TS_ASSERT_EQUALS(reader->readNBits(32, &val), 0);
		TS_ASSERT_EQUALS(val, 0xdeadbeefu);
	}

	void testReadBlock() {
		unsigned char bin[8] = {0xde, 0xad, 0xbe, 0xef, 0x12, 0x00, 0x97, 0xff};
		unsigned char bout[4];
		TS_ASSERT_EQUALS(reader->attachMemory(bin, 8), EXCOM_ERR_OK);
		// read a small block
		TS_ASSERT_EQUALS(reader->readBlock(bout, 3), 3u);
		TS_ASSERT_SAME_DATA(bin, bout, 3);
		// read a byte
		TS_ASSERT_EQUALS(reader->readByte(), 0xef);
		// attempt to read more than what is left
		TS_ASSERT_EQUALS(reader->readBlock(bout, 100), 4u);
		TS_ASSERT_SAME_DATA(&bin[4], bout, 4);
		// there should be nothing left
		TS_ASSERT_EQUALS(reader->readBlock(bout, 4), 0u);
		reader->rewind();
		TS_ASSERT_EQUALS(reader->readBlock(bout, 4), 4u);
		TS_ASSERT_SAME_DATA(&bin[0], bout, 4);
	}
};

class IOMemWriterTestSuite: public CxxTest::TestSuite {
	IOMemWriter *writer;
public:
	void setUp() {
		writer = new IOMemWriter(HANDLE);
	}
	void tearDown() {
		delete writer;
	}

	void testParameters() {
		/* test setting parameters and retrieving values */
		unsigned int val;

		// invalid memory pointer
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, NULL), EXCOM_ERR_MEMORY);
		// invalid parameter constant
		TS_ASSERT_EQUALS(writer->getValue(-1, &val), EXCOM_ERR_PARAM);
		// position in the stream must be 0
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// buffer size must be 0 for MemWriter
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_BUFFER_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// MemWriter shouldn't allow setting buffer size
		TS_ASSERT_EQUALS(writer->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_PARAM);
	}

	void testWriteBytes() {
		char bin[10];
		char bout[10];
		unsigned int val;

		for (int i = 0; i < 10; i++) {
			bin[i] = 'A' + i;
			bout[i] = 0;
		}

		// test all possible return values
		TS_ASSERT_EQUALS(writer->attachMemory(NULL, 10), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(writer->attachMemory(bout, 10), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(bout, 10), EXCOM_ERR_DUPLICATE);

		// write data
		for (int i = 0; i < 10; i++) {
			TS_ASSERT_EQUALS(writer->writeByte((unsigned char) bin[i]), 0);
		}
		// this is meaningless for MemWriter but may be necessary for other Writers
		writer->flush();
		// written data should be the same as the original data
		TS_ASSERT_SAME_DATA(bin, bout, 10);

		// current stream position should be beyond the end of the buffer
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_LESS_THAN(9u, val);
	}

	void testWriteBits() {
		unsigned char bin[8] = {0x13, 0x37, 0x42, 0xff, 0xaa, 0x05, 0x82, 0x73};
		unsigned char bout[8];
		TS_ASSERT_EQUALS(writer->attachMemory(bout, 8), EXCOM_ERR_OK);
		// write more than one byte
		TS_ASSERT_EQUALS(writer->writeNBits(12, 0x133), 0);
		// round-up to whole bytes
		TS_ASSERT_EQUALS(writer->writeNBits(4, 0x7), 0);
		// combine with the writeByte function
		writer->writeByte(0x42);
		// write one byte = 8 bits
		TS_ASSERT_EQUALS(writer->writeNBits(8, 0xff), 0);
		// write 0 bits
		TS_ASSERT_EQUALS(writer->writeNBits(0, 0), 0);
		// odd number of bits
		TS_ASSERT_EQUALS(writer->writeNBits(3, 0x5), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(15, 0x2816), 0);
		// attempt to write more bits than there's space for
		TS_ASSERT_EQUALS(writer->writeNBits(16, 0xffff), EOF);
		// write the remaining 14 bits
		TS_ASSERT_EQUALS(writer->writeNBits(14, 0x0273), 0);
		// nothing left
		TS_ASSERT_EQUALS(writer->writeNBits(1, 0), EOF);
		// this is meaningless for MemWriter but may be necessary for other Writers
		writer->flush();
		// written data should be the same as the original data
		TS_ASSERT_SAME_DATA(bin, bout, 8);
	}

	void testWriteBlock() {
		unsigned char bin[10] = {0xde, 0xad, 0xbe, 0xef, 0x12, 0x00, 0x97, 0xff, 0x81, 0xfe};
		unsigned char bout[8];
		TS_ASSERT_EQUALS(writer->attachMemory(bout, 8), EXCOM_ERR_OK);
		// write a small block
		TS_ASSERT_EQUALS(writer->writeBlock(bin, 3), 3u);
		// write a byte
		TS_ASSERT_EQUALS(writer->writeByte(bin[3]), 0);
		// attempt to write more than what is left
		TS_ASSERT_EQUALS(writer->writeBlock(&bin[4], 6), 4u);
		writer->flush();
		TS_ASSERT_SAME_DATA(bin, bout, 8);
	}

	void testImmWriteBytes() {
		unsigned char bin[8] = {0x13, 0x37, 0x42, 0xff, 0xaa, 0x05, 0x82, 0x73};
		unsigned char junk[6] = {0x55, 0x21, 0xa4, 0x10, 0xcd, 0x5f};
		unsigned char bout[8];
		TS_ASSERT_EQUALS(writer->attachMemory(bout, 8), EXCOM_ERR_OK);
		// write some random data, not the same data that is stored in bin[]
		TS_ASSERT_EQUALS(writer->writeBlock(junk, 6), 6u);
		// current offset is 6
		// write 4 bytes of bin from offset 1
		// data is written in little endian
		TS_ASSERT_EQUALS(writer->immWriteBytes(1, 4, 0x3742ffaa), 0);
		// correct the first byte
		TS_ASSERT_EQUALS(writer->immWriteBytes(0, 1, 0x13), 0);
		// write 3 bytes from offset 5 (note that current offset is still 6)
		// the middle byte (which will be stored at offset 6) is incorrect
		TS_ASSERT_EQUALS(writer->immWriteBytes(5, 3, 0x05aa73), 0);
		// correct the byte at offset 6 (which is still the current offset) using
		// the basic writeByte method
		TS_ASSERT_EQUALS(writer->writeByte(0x82), 0);
		// writing past buffer end should fail
		TS_ASSERT_EQUALS(writer->immWriteBytes(8, 4, 0xdeadbabe), EOF);
		writer->flush();
		// after all those crazy operations, bout should contain the same data as bin
		TS_ASSERT_SAME_DATA(bin, bout, 8);
	}
};

class IOMemPipeTestSuite: public CxxTest::TestSuite {
	IOMemPipe *pipe;
public:
	void setUp() {
		pipe = new IOMemPipe(HANDLE);
	}
	void tearDown() {
		delete pipe;
	}

	void testParameters() {
		/* test setting parameters and retrieving values */
		unsigned int val;

		// invalid memory pointer
		TS_ASSERT_EQUALS(pipe->getValue(EXCOM_VALUE_IO_POSITION, NULL), EXCOM_ERR_MEMORY);
		// invalid parameter constant
		TS_ASSERT_EQUALS(pipe->getValue(-1, &val), EXCOM_ERR_PARAM);
		// position in the stream must be 0
		TS_ASSERT_EQUALS(pipe->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// buffer size must be 0 for MemPipe
		TS_ASSERT_EQUALS(pipe->getValue(EXCOM_VALUE_IO_BUFFER_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// pipe size mustn't be 0 for MemPipe
		TS_ASSERT_EQUALS(pipe->getValue(EXCOM_VALUE_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_DIFFERS(val, 0u);
		// MemPipe shouldn't allow setting buffer size ...
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE, &val), EXCOM_ERR_PARAM);
		// ... but should allow setting pipe size
		val = 64;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
	}

	void testAccessBytes() {
		char bin[10];
		char bout[10];
		unsigned int val;

		for (int i = 0; i < 10; i++) {
			bin[i] = 'A' + i;
			bout[i] = 0;
		}

		// without memory output
		val = 8;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		for (int i = 0; i < 8; i++) {
			TS_ASSERT_EQUALS(pipe->writeByte((unsigned char) bin[i]), 0);
		}
		TS_ASSERT_EQUALS(pipe->readByte(), 'A');
		TS_ASSERT_EQUALS(pipe->readByte(), 'B');                
		TS_ASSERT_EQUALS(pipe->writeByte((unsigned char) bin[8]), 0);
		TS_ASSERT_EQUALS(pipe->writeByte((unsigned char) bin[9]), 0);
		pipe->eof();
		for (int i = 2; i < 10; i++) {
			TS_ASSERT_EQUALS(pipe->readByte(), 'A' + i);
		}
		TS_ASSERT_EQUALS(pipe->readByte(), EOF);

		pipe->rewind();

		// with memory output
		TS_ASSERT_EQUALS(pipe->attachMemory(bout, 10), EXCOM_ERR_OK);                
		for (int i = 0; i < 10; i++) {
			TS_ASSERT_EQUALS(pipe->writeByte((unsigned char) bin[i]), 0);                        
			TS_ASSERT_EQUALS(pipe->readByte(), bin[i]);
		}
		// eof also flushes the output
		pipe->eof();
		TS_ASSERT_EQUALS(pipe->readByte(), EOF);

		TS_ASSERT_SAME_DATA(bin, bout, 10);                
	}

	void testAccessBits() {
		unsigned char bin[10] = {0x13, 0x37, 0x2a, 0xff, 0xaa, 0x05, 0x82, 0x73, 0xab, 0x65};
		unsigned char bout[10];
		unsigned long val;

		// without memory output
		val = 8;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(pipe->writeNBits(1, 0x0), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(2, 0x0), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(3, 0x4), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(4, 0xc), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(5, 0x1b), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(6, 0x25), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(7, 0x2f), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(8, 0xfa), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(9, 0x140), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(10, 0x2c1), 0);

		TS_ASSERT_EQUALS(pipe->readNBits(10, &val), 0);
		TS_ASSERT_EQUALS(val, 0x04cu);
		TS_ASSERT_EQUALS(pipe->readNBits(9, &val), 0);
		TS_ASSERT_EQUALS(val, 0x1b9u);
		TS_ASSERT_EQUALS(pipe->readNBits(8, &val), 0);
		TS_ASSERT_EQUALS(val, 0x57u);
		TS_ASSERT_EQUALS(pipe->readNBits(7, &val), 0);
		TS_ASSERT_EQUALS(val, 0x7eu);
		TS_ASSERT_EQUALS(pipe->readNBits(6, &val), 0);
		TS_ASSERT_EQUALS(val, 0x2au);
		TS_ASSERT_EQUALS(pipe->readNBits(5, &val), 0);
		TS_ASSERT_EQUALS(val, 0x00u);
		TS_ASSERT_EQUALS(pipe->readNBits(4, &val), 0);
		TS_ASSERT_EQUALS(val, 0xbu);
		TS_ASSERT_EQUALS(pipe->readNBits(3, &val), 0);
		TS_ASSERT_EQUALS(val, 0x0u);
		TS_ASSERT_EQUALS(pipe->readNBits(2, &val), 0);
		TS_ASSERT_EQUALS(val, 0x0u);
		TS_ASSERT_EQUALS(pipe->readNBits(1, &val), 0);
		TS_ASSERT_EQUALS(val, 0x1u);
		TS_ASSERT_EQUALS(pipe->readNBits(0, &val), 0);

		TS_ASSERT_EQUALS(pipe->writeNBits(9, 0x073), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(16, 0xab65), 0);
		pipe->eof();

		TS_ASSERT_EQUALS(pipe->readNBits(5, &val), 0);
		TS_ASSERT_EQUALS(val, 0x07u);
		TS_ASSERT_EQUALS(pipe->readNBits(20, &val), 0);
		TS_ASSERT_EQUALS(val, 0x3ab65u);

		TS_ASSERT_EQUALS(pipe->readNBits(1, &val), EOF);

		// with memory output
		pipe->rewind();
		TS_ASSERT_EQUALS(pipe->attachMemory(bout, 10), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(pipe->writeNBits(1, 0x0), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(2, 0x0), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(3, 0x4), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(4, 0xc), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(5, 0x1b), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(6, 0x25), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(7, 0x2f), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(8, 0xfa), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(9, 0x140), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(10, 0x2c1), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(9, 0x073), 0);

		TS_ASSERT_EQUALS(pipe->readNBits(32, &val), 0);
		TS_ASSERT_EQUALS(val, 0x13372affu);
		TS_ASSERT_EQUALS(pipe->writeNBits(16, 0xab65), 0);
		pipe->eof();
		TS_ASSERT_EQUALS(pipe->readNBits(16, &val), 0);
		TS_ASSERT_EQUALS(val, 0xaa05u);
		TS_ASSERT_EQUALS(pipe->readNBits(32, &val), 0);
		TS_ASSERT_EQUALS(val, 0x8273ab65u);
		TS_ASSERT_EQUALS(pipe->readNBits(1, &val), EOF);

		pipe->flush();
		// written data should be the same as the original data
		TS_ASSERT_SAME_DATA(bin, bout, 10);
	}

	void testAccessBlocks() {
		unsigned char bin[10] = {0xde, 0xad, 0xbe, 0xef, 0x12, 0x00, 0x97, 0xff, 0x13, 0xdc};
		unsigned char bout[10];
		unsigned int val;
		// without memory output
		val = 8;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		// write a block of 6 bytes
		TS_ASSERT_EQUALS(pipe->writeBlock(bin, 6), 6u);
		// read 4 bytes
		TS_ASSERT_EQUALS(pipe->readBlock(bout, 4), 4u);
		TS_ASSERT_SAME_DATA(bin, bout, 4);
		// write 4 more bytes
		TS_ASSERT_EQUALS(pipe->writeBlock(&bin[6], 4), 4u);
		// read the 6 remaining bytes
		TS_ASSERT_EQUALS(pipe->readBlock(bout, 6), 6u);
		TS_ASSERT_SAME_DATA(&bin[4], bout, 6);

		pipe->rewind();
		// with memory output
		TS_ASSERT_EQUALS(pipe->attachMemory(bout, 10), EXCOM_ERR_OK);
		int j = 0;
		for (unsigned int i = 1; i <= 4; i++) {
			TS_ASSERT_EQUALS(pipe->writeBlock(&bin[j], i), i);
			j += i;
			for (unsigned int k = 0; k < i; k++) {
				pipe->readByte();
			}
		}
		pipe->flush();
		TS_ASSERT_SAME_DATA(bin, bout, 10);
	}

	void testImmWriteBytes() {
		// this is simple; since pipes are not seekable, this method shall fail every time
		TS_ASSERT_EQUALS(pipe->immWriteBytes(0, 4, 0x11223344), EOF);
	}
};

class IOFileReaderTestSuite: public CxxTest::TestSuite {
	IOFileReader *reader;
public:
	void setUp() {
		reader = new IOFileReader(HANDLE);
	}
	void tearDown() {
		delete reader;
	}

	void testOpening() {
		TS_ASSERT_EQUALS(reader->openFile(NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(reader->openFile("nonexist"), EXCOM_ERR_OPEN);
		TS_ASSERT_EQUALS(reader->openFile(FILEREADER), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(reader->openFile(FILEREADER), EXCOM_ERR_DUPLICATE);
	}

	void testParameters() {
		/* test setting parameters and retrieving values */
		unsigned int val;

		// invalid memory pointer
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_POSITION, NULL), EXCOM_ERR_MEMORY);
		// invalid parameter constant
		TS_ASSERT_EQUALS(reader->getValue(-1, &val), EXCOM_ERR_PARAM);
		// position in the stream must be 0
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// buffer size mustn't be 0 for FileReader
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_BUFFER_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_DIFFERS(val, 0u);
		// try to change buffer size
		val = 64;
		TS_ASSERT_EQUALS(reader->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);
	}

	void testReadBytes() {
		unsigned int val;

		// open a file with known contents
		TS_ASSERT_EQUALS(reader->openFile(FILEREADER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(reader->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);
		// read all data
		TS_ASSERT_EQUALS(reader->readByte(), 0x13);
		TS_ASSERT_EQUALS(reader->readByte(), 0x37);
		TS_ASSERT_EQUALS(reader->readByte(), 0x2a);
		TS_ASSERT_EQUALS(reader->readByte(), 0xff);
		TS_ASSERT_EQUALS(reader->readByte(), 0xaa);
		TS_ASSERT_EQUALS(reader->readByte(), 0x05);
		TS_ASSERT_EQUALS(reader->readByte(), 0x82);
		TS_ASSERT_EQUALS(reader->readByte(), 0x73);
		TS_ASSERT_EQUALS(reader->readByte(), 0xab);
		TS_ASSERT_EQUALS(reader->readByte(), 0x65);
		TS_ASSERT_EQUALS(reader->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 10u);
		TS_ASSERT_EQUALS(reader->readByte(), EOF);
		reader->rewind();
		TS_ASSERT_EQUALS(reader->readByte(), 0x13);
		TS_ASSERT_EQUALS(reader->readByte(), 0x37);
	}

	void testReadBits() {
                unsigned long val;
		TS_ASSERT_EQUALS(reader->openFile(FILEREADER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(reader->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);
		// read all data
		TS_ASSERT_EQUALS(reader->readNBits(1, &val), 0);
		TS_ASSERT_EQUALS(val, 0x0u);
		TS_ASSERT_EQUALS(reader->readNBits(2, &val), 0);
		TS_ASSERT_EQUALS(val, 0x0u);
		TS_ASSERT_EQUALS(reader->readNBits(3, &val), 0);
		TS_ASSERT_EQUALS(val, 0x4u);
		TS_ASSERT_EQUALS(reader->readNBits(4, &val), 0);
		TS_ASSERT_EQUALS(val, 0xcu);
		TS_ASSERT_EQUALS(reader->readNBits(5, &val), 0);
		TS_ASSERT_EQUALS(val, 0x1bu);
		TS_ASSERT_EQUALS(reader->readNBits(6, &val), 0);
		TS_ASSERT_EQUALS(val, 0x25u);
		TS_ASSERT_EQUALS(reader->readNBits(7, &val), 0);
		TS_ASSERT_EQUALS(val, 0x2fu);
		TS_ASSERT_EQUALS(reader->readNBits(4, &val), 0);
		TS_ASSERT_EQUALS(val, 0xfu);
		// first buffer is exhausted
		TS_ASSERT_EQUALS(reader->readNBits(16, &val), 0);
		TS_ASSERT_EQUALS(val, 0xaa05u);
		TS_ASSERT_EQUALS(reader->readNBits(32, &val), 0);
		TS_ASSERT_EQUALS(val, 0x8273ab65u);
		TS_ASSERT_EQUALS(reader->readNBits(1, &val), EOF);
		reader->rewind();
		TS_ASSERT_EQUALS(reader->readNBits(14, &val), 0);
		TS_ASSERT_EQUALS(val, 0x4cdu);
	}
	
	void testReadBlock() {
		// this data should be the same as in the FILEREADER file
		unsigned char bin[10] = {0x13, 0x37, 0x2a, 0xff, 0xaa, 0x05, 0x82, 0x73, 0xab, 0x65};
		unsigned char bout[10];
		unsigned long val;
		TS_ASSERT_EQUALS(reader->openFile(FILEREADER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(reader->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);
		// read a small block
		TS_ASSERT_EQUALS(reader->readBlock(bout, 3), 3u);
		// read a byte
		int x = reader->readByte();
		TS_ASSERT_DIFFERS(x, EOF);
		bout[3] = (unsigned char)x;
		// attempt to read more than what is left
		TS_ASSERT_EQUALS(reader->readBlock(&bout[4], 100), 6u);
		// there should be nothing left
		TS_ASSERT_EQUALS(reader->readBlock(bout, 4), 0u);
		// verify data read so far
		TS_ASSERT_SAME_DATA(bin, bout, 10);

		reader->rewind();
		TS_ASSERT_EQUALS(reader->readBlock(bout, 4), 4u);
		TS_ASSERT_SAME_DATA(&bin[0], bout, 4);
	}
};

class IOFileWriterTestSuite: public CxxTest::TestSuite {
	IOFileWriter *writer;
public:
	void testParameters() {
		/* test setting parameters and retrieving values */
		unsigned int val;
		writer = new IOFileWriter(HANDLE);

		// invalid memory pointer
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, NULL), EXCOM_ERR_MEMORY);
		// invalid parameter constant
		TS_ASSERT_EQUALS(writer->getValue(-1, &val), EXCOM_ERR_PARAM);
		// position in the stream must be 0
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// buffer size mustn't be 0 for FileWriter
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_BUFFER_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_DIFFERS(val, 0u);
		// FileWriter should allow setting buffer size
		val = 64;
		TS_ASSERT_EQUALS(writer->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);
		delete writer;
	}

	void testOpening() {
		writer = new IOFileWriter(HANDLE);
		TS_ASSERT_EQUALS(writer->openFile(NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(writer->openFile("create_me"), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile("create_me"), EXCOM_ERR_DUPLICATE);
		delete writer;
		TS_ASSERT_EQUALS(unlink("create_me"), 0);
	}

	void testWriteBytes() {
		char bin[10];
		char bout[10];
		unsigned int val;
		writer = new IOFileWriter(HANDLE);

		for (int i = 0; i<10; i++) {
			bin[i] = 'A' + i;
			bout[i] = 0;
		}
		TS_ASSERT_EQUALS(writer->openFile(FILEWRITER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(writer->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);
		
		// write data to the file
		for (int i = 0; i<10; i++) {
			TS_ASSERT_EQUALS(writer->writeByte(bin[i]), 0);
		}
		writer->flush();
		delete writer;

		// re-read output file and verify its contents
		FILE *f = fopen(FILEWRITER, "rb");
		TS_ASSERT_EQUALS(fread(bout, 10, 1, f), 1u);
		TS_ASSERT_SAME_DATA(bin, bout, 10);
		fclose(f);

		// delete the file
		TS_ASSERT_EQUALS(unlink(FILEWRITER), 0);
	}

	void testWriteBits() {
		unsigned char bin[10] = {0x13, 0x37, 0x2a, 0xff, 0xaa, 0x05, 0x82, 0x73, 0xab, 0x65};
		unsigned char bout[10];
		unsigned int val;
		writer = new IOFileWriter(HANDLE);
		TS_ASSERT_EQUALS(writer->openFile(FILEWRITER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(writer->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);

		// write data
		TS_ASSERT_EQUALS(writer->writeNBits(1, 0x0), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(2, 0x0), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(3, 0x4), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(4, 0xc), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(5, 0x1b), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(6, 0x25), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(7, 0x2f), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(8, 0xfa), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(9, 0x140), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(10, 0x2c1), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(9, 0x073), 0);
		TS_ASSERT_EQUALS(writer->writeNBits(16, 0xab65), 0);

		writer->flush();
		delete writer;

		// re-read output file and verify its contents
		FILE *f = fopen(FILEWRITER, "rb");
		TS_ASSERT_EQUALS(fread(bout, 10, 1, f), 1u);
		TS_ASSERT_SAME_DATA(bin, bout, 10);
		fclose(f);

		// delete the file
		TS_ASSERT_EQUALS(unlink(FILEWRITER), 0);
	}
	
	void testWriteBlock() {
		unsigned char bin[10] = {0xde, 0xad, 0xbe, 0xef, 0x12, 0x00, 0x97, 0xff, 0x81, 0xfe};
		unsigned char bout[10];
		unsigned int val;
		writer = new IOFileWriter(HANDLE);

		TS_ASSERT_EQUALS(writer->openFile(FILEWRITER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(writer->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);

		// write data to the file
		int j = 0;
		for (unsigned int i = 1; i <= 4; i++) {
			TS_ASSERT_EQUALS(writer->writeBlock(&bin[j], i), i);
			j += i;
		}
		writer->flush();
		delete writer;

		// re-read output file and verify its contents
		FILE *f = fopen(FILEWRITER, "rb");
		TS_ASSERT_EQUALS(fread(bout, 10, 1, f), 1u);
		TS_ASSERT_SAME_DATA(bin, bout, 10);
		fclose(f);

		// delete the file
		TS_ASSERT_EQUALS(unlink(FILEWRITER), 0);
	}

	void testImmWriteBytes() {
		unsigned char bin[8] = {0x13, 0x37, 0x42, 0xff, 0xaa, 0x05, 0x82, 0x73};
		unsigned char junk[6] = {0x55, 0x21, 0xa4, 0x10, 0xcd, 0x5f};
		unsigned char bout[8];
		unsigned int val;
		writer = new IOFileWriter(HANDLE);

		TS_ASSERT_EQUALS(writer->openFile(FILEWRITER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(writer->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);

		// write some random data, not the same data that is stored in bin[]
		TS_ASSERT_EQUALS(writer->writeBlock(junk, 6), 6u);
		// current offset is 6
		// write 4 bytes of bin from offset 1
		// data is written in little endian
		TS_ASSERT_EQUALS(writer->immWriteBytes(1, 4, 0x3742ffaa), 0);
		// correct the first byte
		TS_ASSERT_EQUALS(writer->immWriteBytes(0, 1, 0x13), 0);
		// correct the sixth byte
		TS_ASSERT_EQUALS(writer->immWriteBytes(5, 1, 0x05), 0);
		// write the two remaining bytes using the basic writeByte method
		TS_ASSERT_EQUALS(writer->writeByte(0x82), 0);
		TS_ASSERT_EQUALS(writer->writeByte(0x73), 0);
		writer->flush();
		delete writer;
		// After all those crazy operations, output file should
		// containthe same data as bin. Re-read the file and verify
		FILE *f = fopen(FILEWRITER, "rb");
		TS_ASSERT_EQUALS(fread(bout, 8, 1, f), 1u);
		TS_ASSERT_SAME_DATA(bin, bout, 8);
		fclose(f);

		// delete the file
		TS_ASSERT_EQUALS(unlink(FILEWRITER), 0);
	}
};

class IOFilePipeTestSuite: public CxxTest::TestSuite {
	IOFilePipe *pipe;
public:
	void testParameters() {
		/* test setting parameters and retrieving values */
		unsigned int val;
		pipe = new IOFilePipe(HANDLE);

		// invalid memory pointer
		TS_ASSERT_EQUALS(pipe->getValue(EXCOM_VALUE_IO_POSITION, NULL), EXCOM_ERR_MEMORY);
		// invalid parameter constant
		TS_ASSERT_EQUALS(pipe->getValue(-1, &val), EXCOM_ERR_PARAM);
		// position in the stream must be 0
		TS_ASSERT_EQUALS(pipe->getValue(EXCOM_VALUE_IO_POSITION, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(val, 0u);
		// buffer size mustn't be 0 for FilePipe
		TS_ASSERT_EQUALS(pipe->getValue(EXCOM_VALUE_IO_BUFFER_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_DIFFERS(val, 0u);
		// pipe size mustn't be 0 for FilePipe
		TS_ASSERT_EQUALS(pipe->getValue(EXCOM_VALUE_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_DIFFERS(val, 0u);
		// FilePipe should allow setting buffer size ...
		val = 64;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);
		// ... and also setting pipe size
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		delete pipe;
	}

	void testOpening() {
		pipe = new IOFilePipe(HANDLE);
		TS_ASSERT_EQUALS(pipe->openFile(NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(pipe->openFile("create_me"), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(pipe->openFile("create_me"), EXCOM_ERR_DUPLICATE);
		delete pipe;
		TS_ASSERT_EQUALS(unlink("create_me"), 0);
	}

	void testAccessBytes() {
		char bin[10];
		char bout[10];
		unsigned int val;
		pipe = new IOFilePipe(HANDLE);

		for (int i = 0; i<10; i++) {
			bin[i] = 'A' + i;
			bout[i] = 0;
		}

		// without file output
		val = 8;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		for (int i = 0; i < 8; i++) {
			TS_ASSERT_EQUALS(pipe->writeByte((unsigned char) bin[i]), 0);
		}
		TS_ASSERT_EQUALS(pipe->readByte(), 'A');
		TS_ASSERT_EQUALS(pipe->readByte(), 'B');
		TS_ASSERT_EQUALS(pipe->writeByte((unsigned char)bin[8]), 0);
		TS_ASSERT_EQUALS(pipe->writeByte((unsigned char)bin[9]), 0);
		pipe->eof();
		for (int i = 2; i < 10; i++) {
			TS_ASSERT_EQUALS(pipe->readByte(), 'A' + i);
		}
		TS_ASSERT_EQUALS(pipe->readByte(), EOF);
		pipe->rewind();

		// with file output
		TS_ASSERT_EQUALS(pipe->openFile(FILEWRITER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);
		
		// write data to the file
		for (int i = 0; i<10; i++) {
			TS_ASSERT_EQUALS(pipe->writeByte(bin[i]), 0);
			TS_ASSERT_EQUALS(pipe->readByte(), bin[i]);
		}
		// eof also flushes the output
		pipe->eof();
		TS_ASSERT_EQUALS(pipe->readByte(), EOF);
		delete pipe;

		// re-read output file and verify its contents
		FILE *f = fopen(FILEWRITER, "rb");
		TS_ASSERT_EQUALS(fread(bout, 10, 1, f), 1u);
		TS_ASSERT_SAME_DATA(bin, bout, 10);
		fclose(f);

		// delete the file
		TS_ASSERT_EQUALS(unlink(FILEWRITER), 0);
	}

	void testAccessBits() {
		unsigned char bin[10] = {0x13, 0x37, 0x2a, 0xff, 0xaa, 0x05, 0x82, 0x73, 0xab, 0x65};
		unsigned char bout[10];
		unsigned long val;
		pipe = new IOFilePipe(HANDLE);

		// without file output
		val = 8;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(pipe->writeNBits(1, 0x0), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(2, 0x0), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(3, 0x4), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(4, 0xc), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(5, 0x1b), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(6, 0x25), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(7, 0x2f), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(8, 0xfa), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(9, 0x140), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(10, 0x2c1), 0);

				TS_ASSERT_EQUALS(pipe->readNBits(10, &val), 0);
		TS_ASSERT_EQUALS(val, 0x04cu);
		TS_ASSERT_EQUALS(pipe->readNBits(9, &val), 0);
		TS_ASSERT_EQUALS(val, 0x1b9u);
		TS_ASSERT_EQUALS(pipe->readNBits(8, &val), 0);
		TS_ASSERT_EQUALS(val, 0x57u);
		TS_ASSERT_EQUALS(pipe->readNBits(7, &val), 0);
		TS_ASSERT_EQUALS(val, 0x7eu);
		TS_ASSERT_EQUALS(pipe->readNBits(6, &val), 0);
		TS_ASSERT_EQUALS(val, 0x2au);
		TS_ASSERT_EQUALS(pipe->readNBits(5, &val), 0);
		TS_ASSERT_EQUALS(val, 0x00u);
		TS_ASSERT_EQUALS(pipe->readNBits(4, &val), 0);
		TS_ASSERT_EQUALS(val, 0xbu);
		TS_ASSERT_EQUALS(pipe->readNBits(3, &val), 0);
		TS_ASSERT_EQUALS(val, 0x0u);
		TS_ASSERT_EQUALS(pipe->readNBits(2, &val), 0);
		TS_ASSERT_EQUALS(val, 0x0u);
		TS_ASSERT_EQUALS(pipe->readNBits(1, &val), 0);
		TS_ASSERT_EQUALS(val, 0x1u);
		TS_ASSERT_EQUALS(pipe->readNBits(0, &val), 0);

		TS_ASSERT_EQUALS(pipe->writeNBits(9, 0x073), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(16, 0xab65), 0);
		pipe->eof();

		TS_ASSERT_EQUALS(pipe->readNBits(5, &val), 0);
		TS_ASSERT_EQUALS(val, 0x07u);
		TS_ASSERT_EQUALS(pipe->readNBits(20, &val), 0);
		TS_ASSERT_EQUALS(val, 0x3ab65u);

		TS_ASSERT_EQUALS(pipe->readNBits(1, &val), EOF);

		// with file output
		pipe->rewind();

		TS_ASSERT_EQUALS(pipe->openFile(FILEWRITER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);

		// write data
                TS_ASSERT_EQUALS(pipe->writeNBits(1, 0x0), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(2, 0x0), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(3, 0x4), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(4, 0xc), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(5, 0x1b), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(6, 0x25), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(7, 0x2f), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(8, 0xfa), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(9, 0x140), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(10, 0x2c1), 0);
		TS_ASSERT_EQUALS(pipe->writeNBits(9, 0x073), 0);

		TS_ASSERT_EQUALS(pipe->readNBits(32, &val), 0);
		TS_ASSERT_EQUALS(val, 0x13372affu);
		TS_ASSERT_EQUALS(pipe->writeNBits(16, 0xab65), 0);
		pipe->eof();
		TS_ASSERT_EQUALS(pipe->readNBits(16, &val), 0);
		TS_ASSERT_EQUALS(val, 0xaa05u);
		TS_ASSERT_EQUALS(pipe->readNBits(32, &val), 0);
		TS_ASSERT_EQUALS(val, 0x8273ab65u);
		TS_ASSERT_EQUALS(pipe->readNBits(1, &val), EOF);

		pipe->flush();
		delete pipe;

		// re-read output file and verify its contents
		FILE *f = fopen(FILEWRITER, "rb");
		TS_ASSERT_EQUALS(fread(bout, 10, 1, f), 1u);
		TS_ASSERT_SAME_DATA(bin, bout, 10);
		fclose(f);

		// delete the file
		TS_ASSERT_EQUALS(unlink(FILEWRITER), 0);
	}

	void testAccessBlocks() {
		unsigned char bin[10] = {0xde, 0xad, 0xbe, 0xef, 0x12, 0x00, 0x97, 0xff, 0x13, 0xdc};
		unsigned char bout[10];
		unsigned int val;
		pipe = new IOFilePipe(HANDLE);
		// without file output
		val = 8;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_PIPE_SIZE, &val), EXCOM_ERR_OK);
		// write a block of 6 bytes
		TS_ASSERT_EQUALS(pipe->writeBlock(bin, 6), 6u);
		// read 4 bytes
		TS_ASSERT_EQUALS(pipe->readBlock(bout, 4), 4u);
		TS_ASSERT_SAME_DATA(bin, bout, 4);
		// write 4 more bytes
		TS_ASSERT_EQUALS(pipe->writeBlock(&bin[6], 4), 4u);
		// read the 6 remaining bytes
		TS_ASSERT_EQUALS(pipe->readBlock(bout, 6), 6u);
		TS_ASSERT_SAME_DATA(&bin[4], bout, 6);

		pipe->rewind();
		// with file output
		TS_ASSERT_EQUALS(pipe->openFile(FILEWRITER), EXCOM_ERR_OK);
		// set buffer size to 4 bytes
		// this is very ineffective, but will check, that buffer wrap-around works
		val = 4;
		TS_ASSERT_EQUALS(pipe->setParameter(EXCOM_PARAM_IO_BUFFER_SIZE,&val), EXCOM_ERR_OK);

		int j = 0;
		for (unsigned int i = 1; i <= 4; i++) {
			TS_ASSERT_EQUALS(pipe->writeBlock(&bin[j], i), i);
			j += i;
			for (unsigned int k = 0; k < i; k++) {
				pipe->readByte();
			}
		}
		// eof also flushes the output
		pipe->eof();
		delete pipe;

		// re-read output file and verify its contents
		FILE *f = fopen(FILEWRITER, "rb");
		TS_ASSERT_EQUALS(fread(bout, 10, 1, f), 1u);
		TS_ASSERT_SAME_DATA(bin, bout, 10);
		fclose(f);

		// delete the file
		TS_ASSERT_EQUALS(unlink(FILEWRITER), 0);
	}

	void testImmWriteBytes() {
		// this is simple; since pipes are not seekable, this method shall fail every time
		pipe = new IOFilePipe(HANDLE);
		TS_ASSERT_EQUALS(pipe->immWriteBytes(0, 4, 0x11223344), EOF);
		delete pipe;
	}
};


