/*
 * File:       method/lzmw/lzmw.cpp
 * Purpose:    Implementation of the LZMW compression methods
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of LZMW compression module linked to the ExCom library.
 *
 * LZMW compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZMW compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZMW module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzmw.cpp
 *  \brief Implementation of the LZMW compression method
 */

#include "lzmw.hpp"
#include <list>

CompLZMW::CompLZMW(unsigned int handle) : 
	CompModule(handle), 
	reader(NULL), 
	writer(NULL), 
	dict_capacity(DEFAULT_DICT_SIZE)
{
	char_bitsize = DEFAULT_CHAR_BITSIZE;
	readSymbol = &CompLZMW::readByte;
	writeSymbol = &CompLZMW::writeByte;
	root_size = 1 << DEFAULT_CHAR_BITSIZE;
	indices = new unsigned[dict_capacity];
	decodedString.reserve(512);
}

int CompLZMW::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZMW::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompLZMW::setParameter(int parameter, void *value)
{
	if (value == NULL) 
		return EXCOM_ERR_MEMORY;

	unsigned int u = *((unsigned int*)value);

	switch (parameter) {
	case EXCOM_PARAM_LZMW_DICTIONARY_SIZE:
		if (u < (1 << char_bitsize) || u == ~0u) 
			return EXCOM_ERR_VALUE;
		if (u > dict_capacity) {
			delete[] indices;
			indices = new unsigned[u];
		}
		dict_capacity = u;
		break;
	case EXCOM_PARAM_LZMW_CHAR_BITSIZE:
		if (u == 0 || u > 31 || dict_capacity < (1 << u))
			return EXCOM_ERR_VALUE;
		if ((char_bitsize = (unsigned char)u) == 8) {
			readSymbol = &CompLZMW::readByte;
			writeSymbol = &CompLZMW::writeByte;
		}	
		else {
			readSymbol = &CompLZMW::readNBits;
			writeSymbol = &CompLZMW::writeNBits;
		}
		root_size = 1 << char_bitsize;
		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZMW::getValue(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_LZMW_DICTIONARY_SIZE:
		u = dict_capacity;
		break;
	case EXCOM_VALUE_LZMW_CHAR_BITSIZE:
		u = char_bitsize;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

void CompLZMW::initializeDictionary() {
	encDict.clear();
	encDict.reserve(dict_capacity);

	lzw_node_enc_t lzwEncNode;
	lzwEncNode.first = ~0u;
	lzwEncNode.left = ~0u;
	lzwEncNode.right = ~0u;
	lzwEncNode.prefixIndex = ~0u;

	while (encDict.size() < root_size) {
		lzwEncNode.symbol = lzwEncNode.ID = encDict.size();
		encDict.push_back(lzwEncNode);
	}

	bitsize = char_bitsize;
	threshold = 1 << char_bitsize;
}

// The code is based on an idea of Juha Nieminen
// http://warp.povusers.org/EfficientLZW/ 
unsigned CompLZMW::add(lzw_node_enc_t &node) {
	unsigned index = encDict[node.prefixIndex].first;

	if (index == ~0u) {
		encDict[node.prefixIndex].first = encDict.size();
	} else {
		while (true) {
			if (encDict[index].symbol == node.symbol) 
				return index;

			if (encDict[index].symbol < node.symbol) {
				if (encDict[index].right == ~0u) {
					encDict[index].right = encDict.size();
					break; 
				}
				index = encDict[index].right;
			} else {
				if (encDict[index].left == ~0u) {
					encDict[index].left = encDict.size();
					break; 
				}        
				index = encDict[index].left;		
			}
		}
	}

	encDict.push_back(node);

	return encDict.size() - 1;
}

unsigned CompLZMW::find(lzw_node_enc_t &node) const {
	unsigned index = encDict[node.prefixIndex].first;

	if (index != ~0u) {
		while (true) {
			if (encDict[index].symbol == node.symbol)
				return index;

			if (encDict[index].symbol < node.symbol) {
				if (encDict[index].right == ~0u) {
					break; 
				}
				index = encDict[index].right;
			} else {
				if (encDict[index].left == ~0u) {
					break; 
				}        
				index = encDict[index].left;		
			}
		}
	}

	return ~0u;
}

unsigned CompLZMW::searchInsertNode(lzw_node_enc_t& node) {
	if (node.prefixIndex == ~0u)
		return node.symbol;

	return add(node);
}
unsigned CompLZMW::searchNode(lzw_node_enc_t &node) const {
	if (node.prefixIndex == ~0u)
		return node.symbol;

	return find(node);
}

int CompLZMW::runCompress() {
	lzw_node_enc_t lzwNode;
	lzwNode.prefixIndex = ~0u;
	lzwNode.first = ~0u;
	lzwNode.right = ~0u;
	lzwNode.left = ~0u;
	lzwNode.ID = INVALID_ID;

	initializeDictionary();

	unsigned long c;
	unsigned idx;
	unsigned lastID = INVALID_ID;

	unsigned phrase_count = root_size;

	unsigned index_of_phrase_G, index_of_phrase_F = ~0u;

	std::list<unsigned> buffer; //input buffer, needed for backtracking
	std::list<unsigned>::iterator buffer_it = buffer.end(); //iterator for this buffer
	std::list<unsigned>::iterator phrase_G_end_pos; //iterator marking end position of phrase G in buffer
	
	while (true) {
		if (buffer_it == buffer.end()) { //no available chars in buffer, read from input
			if ((this->*readSymbol)(&c) == EOF)
				break;
			buffer.push_back((unsigned)c); //put read character into buffer
			buffer_it = buffer.end();
		}
		else { //read character from buffer
			c = *buffer_it;
			++buffer_it;
		}

		lzwNode.symbol = (unsigned)c;

		if ((idx = searchNode(lzwNode)) != ~0u) { //if node is in dict...
			if (encDict[idx].ID != INVALID_ID) { //...check if it's a valid phrase
				lastID = encDict[idx].ID; //remember ID of this phrase...
				index_of_phrase_G = idx; //...it's index in dict...
				phrase_G_end_pos = buffer_it; //...and it's end position in buffer
				--phrase_G_end_pos; //correction so it doesn't point to the soon-invalid buffer.end() position			
			}
			lzwNode.prefixIndex = idx;
			continue;
		}

		writer->writeNBits(bitsize, lastID); //output ID of last valid phrase

		if (phrase_count == dict_capacity) {
			initializeDictionary();
			index_of_phrase_F = lzwNode.prefixIndex = ~0u;

			++phrase_G_end_pos;
			while (buffer.begin() != phrase_G_end_pos)
				buffer.pop_front();
			buffer_it = phrase_G_end_pos;

			phrase_count = root_size;
			continue;
		}

		//insert F + G into dictionary
		lzwNode.prefixIndex = index_of_phrase_F; //start on position of phrase F, we know this one

		++phrase_G_end_pos; //recorrection
		for (buffer_it = buffer.begin(); buffer_it != phrase_G_end_pos;) { //and add new entry for every character read from buffer - it is our phrase G
			lzwNode.symbol = *buffer_it;
			idx = searchInsertNode(lzwNode); //find this new entry in dict and if it's not there, add it but with invalid ID, it does not create phrase yet			
			lzwNode.prefixIndex = idx;

			++buffer_it;
			buffer.pop_front(); //remove used character from buffer
		}			
		if (encDict[idx].ID == INVALID_ID) { //now, if last entry have invalid ID...
			encDict[idx].ID = phrase_count++; //validate it -> create new phrase F + G

			//modify bitsize of next output if neccessary
			if (phrase_count != dict_capacity && phrase_count >= threshold) {
				threshold <<= 1;
				++bitsize;
			}
		}

		index_of_phrase_F = index_of_phrase_G; //basically F = G

		lzwNode.prefixIndex = ~0u;	
	}

	if (lastID != INVALID_ID) { //if there was some in the input
		//we have to match and output what is left in buffer

		++phrase_G_end_pos; //recorrection from previous loop
		while (true) { 
			writer->writeNBits(bitsize, lastID); //output last valid phrase
		
			buffer.erase(buffer.begin(), phrase_G_end_pos); //remove this phrase from buffer
			if (buffer.empty()) //if buffer is empty after this, our work here is done
				break;

			lzwNode.prefixIndex = ~0u;
			buffer_it = buffer.begin();

			while (buffer_it != buffer.end()) {
				lzwNode.symbol = *buffer_it;
				++buffer_it;

				if ((idx = searchNode(lzwNode)) == ~0u) //it's not there, jump and output lastID
					break;			
				if (encDict[idx].ID != INVALID_ID) { //it's there and it's valid
					lastID = encDict[idx].ID; //remember ID of this phrase...
					phrase_G_end_pos = buffer_it; //...and it's end position in buffer (no correction this time, end of buffer won't move anymore)
				}

				lzwNode.prefixIndex = idx;
			}		
		}
	}

	writer->eof();

	return EXCOM_ERR_OK;
}

int CompLZMW::runDecompress() {
	unsigned long input;

	std::vector<unsigned>::const_reverse_iterator it;
	unsigned index;
	unsigned index_of_phrase_G, index_of_phrase_F;

	initializeDictionary();

	for (unsigned i = 0; i < root_size; ++i)
		indices[i] = i;

	unsigned phrase_count = root_size;

	lzw_node_enc_t lzwNode;
	lzwNode.first = ~0u;
	lzwNode.right = ~0u;
	lzwNode.left = ~0u;
	lzwNode.ID = INVALID_ID;

	if (reader->readNBits(bitsize, &input) != EOF) {
		index_of_phrase_F = (unsigned)input;		
		(this->*writeSymbol)(input);

		while (reader->readNBits(bitsize, &input) != EOF) {
			if (input >= phrase_count)
				return EXCOM_ERR_FORMAT;

			decodedString.clear();
			index_of_phrase_G = index = indices[input];
			do {
				lzw_node_enc_t &node = encDict[index];
				decodedString.push_back(node.symbol);
				index = node.prefixIndex;
			} while (index != ~0u);

			for (it = decodedString.rbegin(); it != decodedString.rend(); ++it)
				(this->*writeSymbol)(*it);

			if (phrase_count == dict_capacity) {
				initializeDictionary();
				index_of_phrase_F = ~0u;
				phrase_count = root_size;	
				continue;
			}

			//insert F + G into dictionary
			lzwNode.prefixIndex = index_of_phrase_F; //start on position of phrase F

			for (it = decodedString.rbegin(); it != decodedString.rend(); ++it) { //and add new entry for every character read from phrase G
				lzwNode.symbol = *it;
				index = searchInsertNode(lzwNode);	
				lzwNode.prefixIndex = index;
			}		
		
			if (encDict[index].ID == INVALID_ID) {
				encDict[index].ID = phrase_count;
				indices[phrase_count++] = index;
				
				//modify bitsize of next output if neccessary
				if (phrase_count == dict_capacity) {
					//bitsize = char_bitsize;
				}
				else if (phrase_count >= threshold) {
					threshold <<= 1;
					++bitsize;
				}
			}

			index_of_phrase_F = index_of_phrase_G; //basically F = G	
		}
	}
	writer->eof();

	return EXCOM_ERR_OK;
}


int CompLZMW::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}


#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}
