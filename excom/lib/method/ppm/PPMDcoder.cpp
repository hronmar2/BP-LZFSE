/*
 * File:       method/ppm/PPMDcoder.cpp
 * Purpose:    Implementation of PPMD class
 *
 * Based on PPMD implementation from p7zip
 *
 * Copyright (C) 1999-2007 Igor Pavlov
 * Copyright (C) 2009 Filip Simek <simekf1@fel.cvut.cz>
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "ppm.hpp"

namespace ppmd {

//===== encoder implementation =====

const uint32_t kMinMemSize = (1 << 11);
const uint32_t kMinOrder = 2;

void PPMDcoder::WriteCoderProperties(IOWriter *outStream)
{
	outStream->writeByte(_order);
	for (int i = 0; i < 4; i++)
		outStream->writeByte(_usedMemorySize >> (8 * i));
}

PPMDcoder::PPMDcoder() {
	// set parameters to default values
	_usedMemorySize = PPM_MEMORY_SIZE;
	_order = PPM_ORDER;
	_raw = PPM_RAW;
}

int PPMDcoder::compress(IOReader *inStream, IOWriter *outStream)
{
	if (!_einfo.SubAllocator.StartSubAllocator(_usedMemorySize))
		return EXCOM_ERR_MEMORY;

	if (!_raw) {
		// write header
		outStream->writeByte(PPM_VERSION);
		outStream->writeByte((unsigned char)_order);
		outStream->writeByte((unsigned char)(_usedMemorySize & 0xff));
		outStream->writeByte((unsigned char)((_usedMemorySize >> 8) & 0xff));
		outStream->writeByte((unsigned char)((_usedMemorySize >> 16) & 0xff));
		outStream->writeByte((unsigned char)((_usedMemorySize >> 24) & 0xff));
	}

	_rangeEncoder.SetStream(outStream);
	_rangeEncoder.Init();

	// automatic flusher object
	PPMDcoderFlusher flusher(outStream, &_rangeEncoder);

	_einfo.MaxOrder = 0;
	_einfo.StartModelRare(_order);

	for (;;) {
		unsigned char symbol;
		int res;
		res = inStream->readByte();
		if (res == EOF) {
			// here we'll write the End Mark
			_einfo.EncodeSymbol(-1, &_rangeEncoder);
			return EXCOM_ERR_OK;
		}
		symbol = (unsigned char) res;
		_einfo.EncodeSymbol(symbol, &_rangeEncoder);
	}
}

//===== decoder implementation =====
const int kLenIdFinished = -1;
const int kLenIdNeedInit = -2;

int PPMDcoder::CodeSpec(uint32_t size, IOWriter *outStream)
{
	if (_remainLen == kLenIdFinished)
		return EXCOM_ERR_OK;
	if (_remainLen == kLenIdNeedInit) {
		_rangeDecoder.Init();
		_remainLen = 0;
		_dinfo.MaxOrder = 0;
		_dinfo.StartModelRare(_order);
	}
	while (size != 0) {
		int symbol = _dinfo.DecodeSymbol(&_rangeDecoder);
		if (symbol < 0) {
			// this is the End Mark
			_remainLen = kLenIdFinished;
			break;
		}
		outStream->writeByte((uint8_t)symbol);
		size--;
	}
	return EXCOM_ERR_OK;
}

int PPMDcoder::decompress(IOReader *inStream, IOWriter *outStream)
{
	if (!_raw) {
		int x;
		uint32_t m;
		// read header
		x = inStream->readByte();
		if (x == EOF) return EXCOM_ERR_INCOMPLETE;
		if (x > PPM_VERSION) return EXCOM_ERR_FORMAT;
		x = inStream->readByte();
		if (x == EOF) return EXCOM_ERR_INCOMPLETE;
		if (x < PPM_MIN_ORDER || x > PPM_MAX_ORDER)
			return EXCOM_ERR_FORMAT;
		_order = (uint8_t) x;
		x = inStream->readByte();
		if (x == EOF) return EXCOM_ERR_INCOMPLETE;
		m = (uint32_t) x;
		x = inStream->readByte();
		if (x == EOF) return EXCOM_ERR_INCOMPLETE;
		m |= ((uint32_t) x) << 8;
		x = inStream->readByte();
		if (x == EOF) return EXCOM_ERR_INCOMPLETE;
		m |= ((uint32_t) x) << 16;
		x = inStream->readByte();
		if (x == EOF) return EXCOM_ERR_INCOMPLETE;
		m |= ((uint32_t) x) << 24;
		_usedMemorySize = m;
	}

	if (!_dinfo.SubAllocator.StartSubAllocator(_usedMemorySize))
		return EXCOM_ERR_MEMORY;
	
	_rangeDecoder.SetStream(inStream);
	_remainLen = kLenIdNeedInit;
	// automatic flusher object
	PPMDcoderFlusher flusher(outStream);

	for (;;) {
		uint32_t curSize = (1 << 18);
		int res;
		// decompress up to curSize bytes
		res = CodeSpec(curSize, outStream);
		if (res != EXCOM_ERR_OK) return res;

		if (_remainLen == kLenIdFinished)
			break;
	}
	return EXCOM_ERR_OK;
}

} // namespace ppmd

