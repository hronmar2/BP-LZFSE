/*
 * File:       method/ppm/ppm.cpp
 * Purpose:    Implementation of a wrapper class for PPM compression method
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "ppm.hpp"

int CompPPM::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		// the comp. module mustn't have a reader module
		// attached already
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompPPM::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompPPM::setParameter(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_PARAM_PPM_MEMORY_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u < PPM_MIN_MEM_SIZE) return EXCOM_ERR_VALUE;
		comp->_usedMemorySize = (uint32_t)u;
		break;
	case EXCOM_PARAM_PPM_ORDER:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u < PPM_MIN_ORDER || u > PPM_MAX_ORDER) return EXCOM_ERR_VALUE;
		comp->_order = (uint8_t)u;
		break;
	case EXCOM_PARAM_PPM_RAW:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u == 0)
			comp->_raw = false;
		else
			comp->_raw = true;
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompPPM::getValue(int parameter, void *value)
{
	switch (parameter) {
	case EXCOM_VALUE_PPM_MEMORY_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		*(unsigned int*)value = comp->_usedMemorySize;
		break;
	case EXCOM_VALUE_PPM_ORDER:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		*(unsigned int*)value = comp->_order;
		break;
	case EXCOM_VALUE_PPM_RAW:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		*(unsigned int*)value = comp->_raw;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	return EXCOM_ERR_OK;
}

int CompPPM::checkConnection()
{
	// reader and writer are required for both operations
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompPPM::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	try {
		if (operation == EXCOM_OP_COMPRESS) {
			res = comp->compress(reader, writer);
		} else if (operation == EXCOM_OP_DECOMPRESS) {
			res = comp->decompress(reader, writer);
		}
	} catch (...) {
		// if there was an exception, the operation wasn't completed
		res = EXCOM_ERR_INCOMPLETE;
	}

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}


