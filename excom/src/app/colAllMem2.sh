#!/bin/sh

source "common.sh"

#putline <method> <tex_file> <tab_file> <met_index>
putline() {
	printf '%8s' "${names[$4]}" >>"$2"
	printf '%8s' "${names[$4]}" >>"$3"
	for f in corpus/*; do
		g=`basename $f`
		m=`getmem "$d"/_2$g.$1.txt`
		xm=`separators $m`
		printf '&%13s' "$xm" >>"$2"
		printf '%13s' "$m" >>"$3"
	done
	printf '\\\\\n' >>"$2"
	printf '\n' >>"$3"
}

#putunline <method> <tex_file> <tab_file> <met_index>
putunline() {
	printf '%8s' "de-${names[$4]}" >>"$2"
	printf '%8s' "de-${names[$4]}" >>"$3"
	for f in corpus/*; do
		g=`basename $f`
		m=`getmem "$d"/_2$g.$1.un.txt`
		xm=`separators $m`
		printf '&%13s' "$xm" >>"$2"
		printf '%13s' "$m" >>"$3"
	done
	printf '\\\\\n' >>"$2"
	printf '\n' >>"$3"
}

out=mem_all.txt
tex=mem_all.tex
# write header
echo '\hline' >$tex
printf 'Method ' >>$tex
printf 'Method  ' >$out
for f in corpus/*; do
	g=`basename $f`
	printf '&%13s' "$g" >>$tex
	printf '%13s' "$g" >>$out
done
printf '\\\\\n\\hline\n' >>$tex
printf '\n\n\n' >>$out

j=0
for met in acb dca ppm; do
	putline $met "$tex" "$out" $j
	j=$(($j+1))
done

printf '\\hline\n' >>$tex
printf '\n\n' >>$out
	
j=0
for met in acb dca ppm; do
	putunline $met "$tex" "$out" $j
	j=$(($j+1))
done
printf '\\hline\n' >>$tex


