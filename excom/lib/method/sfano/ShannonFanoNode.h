#ifndef _SHANON_FANO_NODE_H
#define	_SHANON_FANO_NODE_H

/*
 * File:       method/sfano/ShannonFanoNode.hpp
 * Purpose:    Declaration of Shannon-Fano statistical compression method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Shannon-Fano module linked to the ExCom library.
 *
 * Shannon-Fano module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Shannon-Fano module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Shannon-Fano module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file ShannonFanoNode.h
 *  \brief Declaration of Shannon-Fano node representation.
 *
 *  This class serves for a Shannon-Fano node representation
 */

#include <string>

using namespace std;

class ShannonFanoNode {
private:
	// Because of -1 value which indicates an internal node
	signed int symbol;
	unsigned long freq;
	ShannonFanoNode* leftChild;
	ShannonFanoNode* rightChild;
	ShannonFanoNode* parent;
	string code;
public:

	// Constructors
	ShannonFanoNode();
	ShannonFanoNode(unsigned char, unsigned long);

	// Destructor
	~ShannonFanoNode();

	// Getters
	signed int getSymbol();
	unsigned long getFreq();
	string getCode();

	ShannonFanoNode* getLeftChild();
	ShannonFanoNode* getRightChild();
	ShannonFanoNode* getParent();

	// Setters
	void setSymbol(signed int);
	void setFreq(unsigned long);
	void setLeftChild(ShannonFanoNode *);
	void setRightChild(ShannonFanoNode *);
	void setParent(ShannonFanoNode *);
	void setCode(string);
};

#endif	/* _SHANON_FANO_NODE_H */

