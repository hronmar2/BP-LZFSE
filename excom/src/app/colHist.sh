#!/bin/sh

source 'common.sh'

add_freq_ctx() {
	i=0
	while read l; do
		if [ $i -gt 32 ]; then break; fi
		v=`echo $l | sed 's/^.*: \([0-9][0-9]*\)$/\1/'`
		ctx[$i]=$((${ctx[$i]}+$v))
		i=$(($i+1))
	done <"$d/$1.ctx"
}

add_freq_conf() {
	i=0
	while read l; do
		if [ $i -gt 32 ]; then break; fi
		v=`echo $l | sed 's/^.*: \([0-9][0-9]*\)$/\1/'`
		conf[$i]=$((${conf[$i]}+$v))
		i=$(($i+1))
	done <"$d/$1.conf"
}


for i in `seq 0 32`; do
	ctx[$i]=0
	conf[$i]=0
done
export ctx
export conf

for f in corpus/*; do
	g=`basename "$f"`

	add_freq_ctx "$g"
	add_freq_conf "$g"
done

out=acb_hist_ctx.txt
tex=acb_hist_ctx.tex
# write header
echo '\hline' >$tex
printf '%16s%16s\\\\\n' "Interval &" "Frequency" >>$tex
echo '\hline' >>$tex
printf '%16s%16s\n' "Interval"  "Frequency" >$out
echo -e "\n" >>$out

for i in `seq 0 31`; do
	a=$(($i * 128))
	b=$(($a + 128))
	c=`separators "${ctx[$i]}"`
	printf '%s& %16s\\\\\n' "\$\\langle$a; $b)\$" "$c" >>$tex
	printf '%16s%16s\n' "$a" "${ctx[$i]}" >>$out
done
echo '\hline' >>$tex

out=acb_hist_conf.txt
tex=acb_hist_conf.tex
# write header
echo '\hline' >$tex
printf '%16s%16s\\\\\n' "Interval &" "Frequency" >>$tex
echo '\hline' >>$tex
printf '%16s%16s\n' "Interval"  "Frequency" >$out
echo -e "\n" >>$out

for i in `seq 0 32`; do
	a=$(($i * 4))
	b=$(($a + 4))
	c=`separators "${conf[$i]}"`
	printf '%s& %16s\\\\\n' "\$\\langle$a; $b)\$" "$c" >>$tex
	printf '%16s%16s\n' "$a" "${conf[$i]}" >>$out
done
echo '\hline' >>$tex


