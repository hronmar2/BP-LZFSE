/*
 * File:       method/acb/TripletCoder.cpp
 * Purpose:    Declaration of ACB compression method (Associative Coder of Buyanovsky)
 *
 * Copyright (C) 2011 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "TripletCoder.hpp"

namespace tripletcoder {
 
Encoder::Encoder(::IOWriter* writer) {
    this->writer = writer;
}

Encoder::~Encoder() {}


ArithmeticEncoder::ArithmeticEncoder(
    ::IOWriter* writer, unsigned int length,
        unsigned int distance, unsigned int alphabet) : Encoder(writer) {
    
    this->encoder = new arithcoder::AEncoder(writer);

    this->lengthAlphabet = new arithcoder::AlphabetInfo(1 << length);
    this->distanceAlphabet = new arithcoder::AlphabetInfo(1 << distance);
    this->characterAlphabet = new arithcoder::AlphabetInfo(1 << alphabet);

    this->isLowerAlphabet = new arithcoder::AlphabetInfo(2);
}

ArithmeticEncoder::~ArithmeticEncoder() {
    delete encoder;
    delete lengthAlphabet;
    delete distanceAlphabet;
    delete characterAlphabet;
    delete isLowerAlphabet;
}

void ArithmeticEncoder::encodeCharacter(unsigned int character) {
    encoder->add(characterAlphabet, character);
}

void ArithmeticEncoder::encodeLength(unsigned int length) {
    encoder->add(lengthAlphabet, length);
}

void ArithmeticEncoder::encodeDistance(unsigned int distance) {
    encoder->add(distanceAlphabet, distance);
}

void ArithmeticEncoder::encodeIsLower(unsigned int isLower) {
    encoder->add(isLowerAlphabet, isLower);
}

void ArithmeticEncoder::flush() {
    encoder->flush(lengthAlphabet);
}

HuffmanEncoder::HuffmanEncoder(::IOWriter* writer, unsigned int length,
        unsigned int distance, unsigned int alphabet,
        unsigned char character) : Encoder(writer) {
    lengthTree = new DHuffTree(length);
    distanceTree = new DHuffTree(distance);
    characterTree = new DHuffTree(alphabet);

    lengthTree->insert(0);
    lengthTree->traverseUp();
    distanceTree->insert(1);
    distanceTree->traverseUp();
    characterTree->insert(character);
    characterTree->traverseUp();

    codeLength = 0;
}

HuffmanEncoder::~HuffmanEncoder() {
    delete lengthTree;
    delete distanceTree;
    delete characterTree;

    codeLength = 0;
}


void HuffmanEncoder::encodeLength(unsigned int length) {
    unsigned int code = lengthTree->insert(length, codeLength);
    writer->writeNBits(codeLength, code);
}

void HuffmanEncoder::encodeDistance(unsigned int distance) {
    unsigned int code = distanceTree->insert(distance, codeLength);
    writer->writeNBits(codeLength, code);
}

void HuffmanEncoder::encodeCharacter(unsigned int character) {
    unsigned int code = characterTree->insert(character, codeLength);
    writer->writeNBits(codeLength, code);
}

void HuffmanEncoder::encodeIsLower(unsigned int isLower) {
    writer->writeNBits(1, isLower);
}

void HuffmanEncoder::flush() {
    unsigned int bitsToByte = (8 - writer->bitsWritten() & 7) & 7;
    // Insert msb bits of data esc node into stream if it is not byte alligned
    if(bitsToByte > 0) {
        unsigned int escSymbol = lengthTree->getEsc(codeLength);
        if(codeLength > bitsToByte) {
            // shift only when esc code is longer than bits left
            // otherwise write the whole esc code
            //
            // If bitsToByte is greater than esc code length
            // virtual symbol of max 8 bits will take the rest of bits
            // and thus making end of stream
            escSymbol = escSymbol >> (codeLength - bitsToByte);
            codeLength = bitsToByte;
        }
        writer->writeNBits(codeLength, escSymbol);
        // check if esc symbol was enough
        // if not write zero for length and esc symbol for
        // data character
        bitsToByte = (8 - writer->bitsWritten() & 7) & 7;
        if(bitsToByte > lengthTree->getCodeLength()) {
            writer->writeNBits(lengthTree->getCodeLength(), 0);
            bitsToByte -= lengthTree->getCodeLength();
            escSymbol = characterTree->getEsc(codeLength);
            if(codeLength > bitsToByte) {
                escSymbol = escSymbol >> (codeLength - bitsToByte);
                codeLength = bitsToByte;
            }
            writer->writeNBits(bitsToByte, escSymbol);
        }
    }
}

Decoder::Decoder(::IOReader* reader) {
    this->reader = reader;
}

Decoder::~Decoder() {}

ArithmeticDecoder::ArithmeticDecoder(
        ::IOReader* reader, unsigned int length,
        unsigned int distance, unsigned int alphabet) : Decoder(reader) {
    this->decoder = new arithcoder::ADecoder(reader);

    this->lengthAlphabet = new arithcoder::AlphabetInfo(1 << length);
    this->distanceAlphabet = new arithcoder::AlphabetInfo(1 << distance);
    this->characterAlphabet = new arithcoder::AlphabetInfo(1 << alphabet);

    this->isLowerAlphabet = new arithcoder::AlphabetInfo(2);
}

ArithmeticDecoder::~ArithmeticDecoder() {
    delete decoder;
    delete lengthAlphabet;
    delete distanceAlphabet;
    delete characterAlphabet;
    delete isLowerAlphabet;
}

unsigned int ArithmeticDecoder::decodeLength() {
    return this->lastLength = decoder->read(lengthAlphabet);
}
unsigned int ArithmeticDecoder::decodeDistance() {
    return decoder->read(distanceAlphabet);
}
unsigned int ArithmeticDecoder::decodeCharacter() {
    return decoder->read(characterAlphabet);
}
unsigned int ArithmeticDecoder::decodeIsLower() {
    return decoder->read(isLowerAlphabet);
}

bool ArithmeticDecoder::isEof() {
   return this->lastLength == lengthAlphabet->size;
}

HuffmanDecoder::HuffmanDecoder(::IOReader* reader, unsigned int length,
        unsigned int distance, unsigned int alphabet,
        unsigned char character) : Decoder(reader) {
    lengthTree = new DHuffTree(length);
    distanceTree = new DHuffTree(distance);
    characterTree = new DHuffTree(alphabet);

    lengthTree->insert(0);
    lengthTree->traverseUp();
    distanceTree->insert(1);
    distanceTree->traverseUp();
    characterTree->insert(character);
    characterTree->traverseUp();

    eof = 0;
}

HuffmanDecoder::~HuffmanDecoder() {
    delete lengthTree;
    delete distanceTree;
    delete characterTree;

    eof = 0;
}

unsigned int HuffmanDecoder::decodeLength() {
    return getNextCode(lengthTree);
}
unsigned int HuffmanDecoder::decodeDistance() {
    return getNextCode(distanceTree);
}
unsigned int HuffmanDecoder::decodeCharacter() {
    return getNextCode(characterTree);
}
unsigned int HuffmanDecoder::decodeIsLower() {
    unsigned long bit = 0;
    eof = reader->readNBits(1, &bit);
    return bit;
}

bool HuffmanDecoder::isEof() {
    return eof == EOF;
}

inline unsigned int HuffmanDecoder::getNextCode(DHuffTree* tree) {
    unsigned long bit = 0;
    unsigned long code = 0;
    do {
        eof = reader->readNBits(1, &bit);
    } while (!tree->isLeaf(bit) && eof != EOF);

    if (tree->isEsc()) {
        // Symbol not previously encountered
        reader->readNBits(tree->getCodeLength(), &code);
    } else {
        // Symbol already seen
        code = tree->getCurrentSymbol();
    }
    tree->insert(code);
    tree->traverseUp();
    return code;
}

}
