#ifndef BPLUSTREE_H_
#define BPLUSTREE_H_

/*
 * File:       method/acb/BPlusTree.hpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "BPlusTreeNode.hpp"
#include "SortedContent.hpp"

#include <stdlib.h>
#include <string.h>

/*!
 * \brief Class representing B+ Tree
 */
class BPlusTree {
private:
        /*!
         * \brief Root of this tree
         */
	BPlusTreeNode* root;

        /*!
         * \brief Path for last search so I can reverse back up without
         *        the need of remembering parents in the nodes.
         */
	BPlusTreeInnerNode** path;

        /*!
         * \brief Positions in path used to traverse down in last search.
         */
	unsigned int* slots;

        /*!
         * \brief Depth of last search.
         */
	unsigned int depth;

        /*!
         * \brief Position in \c data used for last search
         */
	long lastSearchPosition;
        /*!
         * \brief Position in keys which was best context match with \c lastSearchPosition
         */
	unsigned int lastSearchSlot;
        /*!
         * \brief Node in which best context is
         */
	BPlusTreeNode* lastSearchNode;

        /*!
         * \brief Find best matching suffix (context) to current suffix in \c data
         *        starting at \c position.
         * \param data - Data in which is the suffix starting at \c position
         * \param position - Position at which suffix starts
         * \param contextIndex - Position of best matching context
         * \param contextLength - Common length of best matching context and new context
         * \return node in which is best matching context
         *
         * This method should be implemented as inline so it can be reused without
         * method call performance issues.
         */
	BPlusTreeLeafNode* searchInline(uint16_t* data, long position, unsigned int &contextIndex, uint16_t &contextLength);

        /*!
         * \brief Inserts new context starting at \c position to its position
         *        represented by \c slot in \c leaf.
         * \param leaf - Node to add context to
         * \param slot - Position in \c leaf to add context to
         * \param position - Starting position of context to add
         * \return New leaf node if this node was full before this insertion.
         *
         * If this node is full new one is created and half the of its \c keys and
         * \c children are moved to the new node.
         */
	BPlusTreeLeafNode* insertKey(BPlusTreeLeafNode* leaf, unsigned int slot, long position);
public:

	BPlusTree();
	virtual ~BPlusTree();

        /*!
         * \brief Find best matching suffix (context) to current suffix in \c data
         *        starting at \c position.
         * \param data - Data in which is the suffix starting at \c position
         * \param position - Position at which suffix starts
         * \param contextIndex - Position of best matching context
         * \param contextLength - Common length of best matching context and new context
         * \return node in which is best matching context
         */
	BPlusTreeLeafNode* search(uint16_t* data, long position, unsigned int &contextIndex, uint16_t &contextLength);

        /*!
         * \brief Insert new suffix to its position in this tree
         * \param data - Data in which is the suffix starting at \c position
         * \param position - Position at which suffix starts
         * \param contentsTree - Tree of sorted contents for this context
         */
	void insert(uint16_t* data, long position, SortedContentsTree* contentsTree);

        /*!
         * \brief Initialize this tree
         * \param position - Position in \c data to which initialize this tree
         */
        void init(long position);

        /*!
         * \brief Returns left most leaf node in this tree
         * \return left most leaf node in this tree
         */
	BPlusTreeLeafNode* getMostLeftLeafNode();
};

#endif /* BPLUSTREE_H_ */
