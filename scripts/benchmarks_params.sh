#!/bin/bash

# This script is similar to benchmarks.sh but is intended for measuring
# impacts of different method parameters settings on compression ratio
# and speed. Arguments must be passed to this script to choose method, 
# parameter and its value range. By default, the parameter is only
# applied to compression operation, use -a option to apply it to 
# decompression as well.
#
# This script will produce three CSV files:
#   - benchmarks_comp.csv    containing measured compression times
#   - benchmarks_decomp.csv  containing measured decompression times
#   - benchmarks_ratio.csv   containing measured compression ratios
#
# Time values are in microseconds. Ratio is output as a percentage.
#
#
# The script should be called from the directory where ExCom testing
# application is present (usually bin/src/app). The Prague Corpus files
# should be present in the same directory, by default under PragueCorpus 
# folder (this can be changed by -d argument).

########################################################################

# Default values for parameters

# Number of times to run each operation.
repeatTimes=50

# Parameter increment
increment=1

# This variable defines the directory containing tested files.
corpusDirectory="PragueCorpus"

# Write messages about progress.
print_progress=true
print_progress_files=true

# Save compression ratio as percentage.
ratio_percentage=true

# Path to ExCom testing application. Relative to current directory.
app="app"

# This controls whether the parameter will be set for decompression too
decompression_too=false

########################################################################


function usage {
cat <<EOM
Usage: ./$(basename "$0") [options] method param min max
Arguments:
  method  tested method, run ./app -m? for list of methods in ExCom
  param   parameter name, run ./app -m method -p? for list
  min     starting value of the parameter
  max     last value of the parameter (inclusive)  
Options: 
  -h      show help
  -r T    repeat each operation T times, default is: 50
  -i INC  parameter increment, default is: 1
  -a      apply parameters to both compression and decompression operations
  -s      silent mode - do NOT print any messages about progress
  -b      brief mode - do NOT print message for each file
  -d DIR  use files from DIR directory to perform benchmarks, default is: PragueCorpus
  -c APP  relative path to ExCom app, default is: app
  -n      do NOT use percentage for compression ratio (disable multiplication of ratios by 100)
EOM
}


# Options parsing code

while getopts ":hr:i:asbd:c:n" opt; do
  case $opt in
    h)
      usage
      exit 2
      ;;
    r)
      repeatTimes=${OPTARG}
      ;;
    i)
      increment=${OPTARG}
      ;;
    s)
      print_progress=false
      print_progress_files=false
      ;;
    b)
      print_progress_files=false
      ;;
    d)
      corpusDirectory=${OPTARG}
      ;;
    c)
      app=${OPTARG}
      ;;
    n)
      ratio_percentage=false
      ;;
    a)
      decompression_too=true
      ;;
    \?)
      echo "ERROR: Invalid option: -$OPTARG" >&2
      echo ""
      usage
      exit 1
      ;;
    :)
      echo "ERROR: Option -$OPTARG requires an argument." >&2
      echo ""
      usage
      exit 1
      ;;
  esac
done

shift $((OPTIND - 1))

if [ "$increment" -lt 1 ]; then
	echo "ERROR: Increment must be greater or equal to 1!" >&2
	exit 1
fi

if [ ! -d "$corpusDirectory" ]; then
	echo "ERROR: Directory with corpus files not found!" >&2
	echo "Corpus files must either be in PragueCorpus subdirectory of the current folder or in any other location provided as a -d argument." >&2
	echo ""
	usage
    exit 1
fi

if [ ! -f "$app" ]; then
    echo "ERROR: ExCom app not found!" >&2
	echo "It must either be in current directory or specified as a value of -c parameter." >&2
	echo ""
	usage
    exit 1
fi

if [ "$#" -lt 4 ]; then
	echo "ERROR: To few arguments given!" >&2
	echo ""
	usage
	exit 1
fi

if [ "$#" -ne 4 ]; then
	echo "WARNING: Unexpected number of parameters." >&2
fi

method=${1}
param="$2"
value=${3}
valueMax=${4}

if [ "$value" -gt "$valueMax" ]; then
	echo "ERROR: min can not be larger than max!" >&2
	exit 1
fi


# Benchmarks

echo -n "$param" | tee benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
for filename in "$corpusDirectory"/*; do
	echo -n ",${filename##*/}" | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
done
echo "" | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null


function cleanup {
	#remove temporary files
	rm -f tmp_file_compressed.bin tmp_file_decompressed.bin
}
trap cleanup EXIT

if [ "$print_progress_files" = true ] ; then
	shopt -s nullglob
	numFiles=("$corpusDirectory"/*)
	numFiles=${#numFiles[@]}
	numParams=$(( ($valueMax - $value) / $increment + 1 ))
	totalFiles=$(($numFiles * $numParams))
	i=1
fi

#run benchmarks for all given parameter values
while [ $value -le $valueMax ]; do
	if [ "$print_progress" = true ] ; then
		echo "Running benchmarks for $method with parameter: $param = $value"
	fi
	echo -n "$value" | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
	for filename in "$corpusDirectory"/*; do
		if [ "$print_progress_files" = true ] ; then
			echo "[$i/$totalFiles] file: $filename"
			i=$((i+1))
		fi
		echo -n "," | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
		./"$app" -r "$repeatTimes" -t -m "$method" -p"$param=$value" -i "$filename" -o file_compressed.bin 2>/dev/null | grep '^[0-9]' | awk 'NR == 1 || $3 < min {min = $3}END{print min}' | xargs echo -n | tee -a benchmarks_comp.csv >/dev/null
		if [ "$decompression_too" = true ] ; then
			./"$app" -r "$repeatTimes" -d -t -m "$method" -p"$param=$value" -i file_compressed.bin -o file_decompressed.bin 2>/dev/null | grep '^[0-9]' | awk 'NR == 1 || $3 < min {min = $3}END{print min}' | xargs echo -n | tee -a benchmarks_decomp.csv >/dev/null
		else
			./"$app" -r "$repeatTimes" -d -t -m "$method" -i file_compressed.bin -o file_decompressed.bin 2>/dev/null | grep '^[0-9]' | awk 'NR == 1 || $3 < min {min = $3}END{print min}' | xargs echo -n | tee -a benchmarks_decomp.csv >/dev/null
		fi
		
		if ! cmp -s "$filename" file_decompressed.bin; then
			echo "ERROR: The decompressed file is not identical to the original file!"
			echo -n "?" | tee -a benchmarks_ratio.csv >/dev/null
			exit 1
		fi

		origsize="$(wc -c <file_decompressed.bin)"
		compsize="$(wc -c <file_compressed.bin)"
		
		if [ "$ratio_percentage" = true ] ; then
			echo -n $(echo "scale=2; 100*$compsize/$origsize " | bc) | tee -a benchmarks_ratio.csv >/dev/null
		else
			echo -n $(echo "scale=4; $compsize/$origsize " | bc) | tee -a benchmarks_ratio.csv >/dev/null
		fi

	done
	echo "" | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
	
	value=$[$value+$increment];
done

if [ "$print_progress" = true ] ; then
	echo "Finished running benchmarks. The output was written to files:"
	echo "benchmarks_comp.csv   - compression times (in microseconds)"
	echo "benchmarks_decomp.csv - decompression times (in microseconds)"
	if [ "$ratio_percentage" = true ] ; then
		echo "benchmarks_ratio.csv  - compression ratios (as percentage)"
	else
		echo "benchmarks_ratio.csv  - compression ratios"
	fi
fi

