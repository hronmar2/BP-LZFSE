#!/bin/sh

source "common.sh"

#putline <corpus_file> <method> <tex_file> <tab_file>
puttime() {
	t=`mintime <"$d"/_1$1.$2.txt`
	xt=`separators $t`
	printf '%16s' "$xt &" >>"$3"
	printf '%16s' "$t" >>"$4"
}

#putunline <corpus_file> <method> <tex_file> <tab_file>
putuntime() {
	t=`mintime <"$d"/_1$1.$2.un.txt`
	xt=`separators $t`
	printf '%16s' "$xt &" >>"$3"
	printf '%16s' "$t" >>"$4"
}

out=time_all.txt
tex=time_all.tex
# write header
echo '\hline' >$tex
printf '%16s%16s%16s%16s%16s%16s%16s\\\\\n\\hline\n' "File &" "ACB &" "DCA &" "PPM &" "de-ACB &" "de-DCA &" "de-PPM" >>$tex
printf '%16s%16s%16s%16s%16s%16s%16s\n\n\n' "File" "ACB" "DCA" "PPM" "de-ACB" "de-DCA" "de-PPM" >$out

for f in corpus/*; do
	g=`basename $f`
	printf '%16s' "$g &" >>$tex
	printf '%16s' "$g" >>$out
	for met in acb dca ppm; do
		puttime "$g" $met "$tex" "$out"
	done
	for met in acb dca ppm; do
		putuntime "$g" $met "$tex" "$out"
	done
	printf '\\\\\n' >>$tex
	printf '\n' >>$out
done

printf '\\hline\n' >>$tex

