#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/method/arith/arith.hpp"

#define HANDLE 1
#define HANDLE_TMP 4
#define HANDLE_IO_IN 2
#define HANDLE_IO_OUT 3

// TODO update
class CompArithTestSuite: public CxxTest::TestSuite {
	CompArith *comp;
public:
	void setUp() {
		comp = new CompArith(HANDLE);
	}
	void tearDown() {
		delete comp;
	}

	void testParameters() {
		unsigned int v;
		TS_ASSERT_EQUALS(comp->setParameter(-1, NULL), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->getValue(-1, NULL), EXCOM_ERR_PARAM);
                v = 1;
                TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ARITH_STATIC, &v), EXCOM_ERR_MEMORY);
                TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ARITH_STATIC, &v), EXCOM_ERR_OK);
                TS_ASSERT_EQUALS(v, false);
                
                TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ARITH_STATIC, NULL), EXCOM_ERR_OK);
                TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ARITH_STATIC, NULL), EXCOM_ERR_MEMORY);
                TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ARITH_STATIC, &v), EXCOM_ERR_OK);
                TS_ASSERT_EQUALS(v, true);

		// a value common to all compression modules
		int u;
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_COMP_RESULT, &u), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(u, EXCOM_ERR_LATER);
	}

	void testIOInputModules() {
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		// invalid pointer to a module
		TS_ASSERT_EQUALS(comp->connectIOModule(NULL, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_MEMORY);
		// invalid connection type
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, -1), EXCOM_ERR_PARAM);
		// input module connected as an output module
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		delete reader;
	}

	void testIOOutputModules() {
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		// output module connected as an input module
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_DUPLICATE);
		delete writer;
	}

	void testCompressionAdaptive() {
		// original input
		unsigned char A[16] = {'a','l','f',' ','e','a','t','s',' ','a','l','f','a','l','f','a'};
		// compressed output
		unsigned char B[32]; //20
		// this is how the compressed output should look like
		unsigned char C[17] = {
                    0x43, 0x08, 0x49, 0x19, 0x59, 0x11, 0xf7, 0x76,
                    0x71, 0xa1, 0x3d, 0xee, 0x6a, 0xdf, 0x47, 0x32,
                    0x00
                };
		unsigned int v;

		// input and output modules
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(A, sizeof(A)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(B, sizeof(B)), EXCOM_ERR_OK);

		// running shouldn't succeed without an input I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// ...or without output I/O module
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// run the compression
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether output data matches our specimen
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v + 1, sizeof(C));
		TS_ASSERT_SAME_DATA(B, C, sizeof(C));

		delete reader;
		delete writer;
	}

	void testDecompressionAdaptive() {
		// original input
		unsigned char A[16] = {'a','l','f',' ','e','a','t','s',' ','a','l','f','a','l','f','a'};
		// this is how the compressed output should look like
		unsigned char C[17] = {
                    0x43, 0x08, 0x49, 0x19, 0x59, 0x11, 0xf7, 0x76,
                    0x71, 0xa1, 0x3d, 0xee, 0x6a, 0xdf, 0x47, 0x32,
                    0x00
                };
		// decompressed output
		unsigned char D[32];
		unsigned int v;

		// input and output modules
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(C, sizeof(C)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(D, sizeof(D)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// parameters are set by input
		TS_ASSERT_EQUALS(comp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		// run the decompression
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether output data matches our specimen
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, sizeof(A));
		TS_ASSERT_SAME_DATA(A, D, sizeof(A));

		delete reader;
		delete writer;
	}

        void testCompressionStatic() {
		// original input
		unsigned char A[16] = {'a','l','f',' ','e','a','t','s',' ','a','l','f','a','l','f','a'};
		// compressed output
		unsigned char B[32]; //20
		// this is how the compressed output should look like
		unsigned char C[29] = {
                    0xc1, 0x00, 0x00, 0x4c, 0x20, 0x02, 0xb2, 0x80,
                    0x02, 0xcc, 0x00, 0x1b, 0x60, 0x00, 0x6e, 0x60,
                    0x00, 0xba, 0x00, 0x02, 0x00, 0x00, 0x02, 0xa9,
                    0xaa, 0x98, 0xb0, 0x67, 0x68
                };
		unsigned int v;

		// input and output modules
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(A, sizeof(A)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(B, sizeof(B)), EXCOM_ERR_OK);

		// running shouldn't succeed without an input I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// ...or without output I/O module
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

                // set static
                TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ARITH_STATIC, NULL), EXCOM_ERR_OK);
                TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ARITH_STATIC, &v), EXCOM_ERR_OK);
                TS_ASSERT_EQUALS(v, true);
                
		// run the compression
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether output data matches our specimen
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v + 1, sizeof(C));
		TS_ASSERT_SAME_DATA(B, C, sizeof(C));

		delete reader;
		delete writer;
	}

	void testDecompressionStatic() {
		// original input
		unsigned char A[16] = {'a','l','f',' ','e','a','t','s',' ','a','l','f','a','l','f','a'};
		// this is how the compressed output should look like
		unsigned char C[29] = {
                    0xc1, 0x00, 0x00, 0x4c, 0x20, 0x02, 0xb2, 0x80,
                    0x02, 0xcc, 0x00, 0x1b, 0x60, 0x00, 0x6e, 0x60,
                    0x00, 0xba, 0x00, 0x02, 0x00, 0x00, 0x02, 0xa9,
                    0xaa, 0x98, 0xb0, 0x67, 0x68
                };
		// decompressed output
		unsigned char D[32];
		unsigned int v;

		// input and output modules
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(C, sizeof(C)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(D, sizeof(D)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// parameters are set by input
		TS_ASSERT_EQUALS(comp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		// run the decompression
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether output data matches our specimen
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, sizeof(A));
		TS_ASSERT_SAME_DATA(A, D, sizeof(A));

		delete reader;
		delete writer;
	}
};


