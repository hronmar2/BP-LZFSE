#!/bin/sh

resdir=`date +"%d-%m_%H-%M-%S"`_mem

mkdir $resdir

for f in korpus/*; do
	met=ppm
	g=`basename $f`.$met
	libtool --mode=execute memusage ./app -m $met -r 1 -q -i $f -o $resdir/$g >/dev/null 2>$resdir/_2$g.txt
	libtool --mode=execute memusage ./app -d -m $met -r 1 -q -i $resdir/$g -o $resdir/$g.un >/dev/null 2>$resdir/_2$g.un.txt
	met=dca
	g=`basename $f`.$met
	libtool --mode=execute memusage ./app -m $met -r 1 -q -i $f -o $resdir/$g -e $resdir/$g.exc >/dev/null 2>$resdir/_2$g.txt
	libtool --mode=execute memusage ./app -d -m $met -r 1 -q -i $resdir/$g -o $resdir/$g.un -e $resdir/$g.exc >/dev/null 2>$resdir/_2$g.un.txt
	met=acb
	g=`basename $f`.$met
	libtool --mode=execute memusage ./app -m $met -r 1 -q -i $f -o $resdir/$g -p s=10,l=8 >/dev/null 2>$resdir/_2$g.txt
	libtool --mode=execute memusage ./app -d -m $met -r 1 -q -i $resdir/$g -o $resdir/$g.un -e $resdir/$g.exc >/dev/null 2>$resdir/_2$g.un.txt
done

