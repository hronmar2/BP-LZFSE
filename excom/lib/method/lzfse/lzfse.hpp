#ifndef __METHOD_LZFSE_HPP__
#define __METHOD_LZFSE_HPP__

/*
 * File:       method/lzfse/lzfse.hpp
 * Purpose:    Declaration of the LZFSE compression method
 * Author:     Martin Hron <hronmar2@fit.cvut.cz>, 2018
 *             bachelor thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Bachelor thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2018 Martin Hron
 *
 * This file is part of LZFSE coding module linked to the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZFSE module or with the ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file lzfse.hpp
 *  \brief Declaration of the LZFSE compression method.
 *
 *  This file declares LZFSE compression module class implemented in lzfse.cpp. 
 *  The LZFSE module is a wrapper over the reference implementation of LZFSE 
 *  that is present in the lzfse subfolder. 
 *
 */

#include <excom.h>
#include "../../compmodule.hpp"

#include "lzfse_params.h"

class CompLZFSE: public CompModule {
public:
	CompLZFSE(unsigned int handle): CompModule(handle), reader(NULL), writer(NULL),\
		m_Parameters({.good_match=40, .hash_bits=14}) {}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int checkConnection();
	int run();

	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);

private:
	IOReader *reader;
	IOWriter *writer;

	//adjustable LZFSE parameters
	lzfse_encode_parameters m_Parameters;

	//initial allocated size of source buffer (for both compression and decompression)
	static const unsigned long int SRC_BUFFER_INITIAL_SIZE = (1 << 20);	//1 MiB

	//largest size of buffer when allocated size will double during reallocation
	static const unsigned long int SRC_BUFFER_REALLOC_THRESHOLD = (100 << 20);	//100 MiB

	/*! \brief Performs compression or decompression based on current operation
	 *  \return
	 *         - \c EXCOM_ERR_OK - success
	 *         - \c EXCOM_ERR_MEMORY - memory allocation failed
	 */
	int performOp();
};

#endif //ifndef __METHOD_LZFSE_HPP__
