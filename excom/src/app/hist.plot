set logscale y
unset logscale x
#set key top right
set key off
set terminal postscript eps color

set xlabel "Value"
set ylabel "Frequency"
set grid
set style data histogram

### ------
### |ctx |
### ------
set title "Histogram of context-content differences"
set xrange[-50 : 4020 ]
unset logscale y

set output "acb_hist_ctx-col.eps"
set style data boxes
set style fill solid 1.0 border -1
plot 'acb_hist_ctx.txt' index 1 using 1:2 lc rgb "#D00000"

set output "acb_hist_ctx-bw.eps"
set style fill pattern border -1
plot 'acb_hist_ctx.txt' index 1 using 1:2 lc rgb "#000000"

### ------
### |conf|
### ------
set title "Histogram of lengths of the conforming contents"
set xrange[-7 : 135 ]
set logscale y

set output "acb_hist_conf-col.eps"
set style data boxes
set style fill solid 1.0 border -1
plot 'acb_hist_conf.txt' index 1 using 1:2 lc rgb "#D00000"

set output "acb_hist_conf-bw.eps"
set style fill pattern border -1
plot 'acb_hist_conf.txt' index 1 using 1:2 lc rgb "#000000"


