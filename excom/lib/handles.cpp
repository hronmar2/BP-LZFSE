/*
 * File:       handles.cpp
 * Purpose:    Implementation of classes related to unique handles' allocation
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include <excom.h>
#include "handles.hpp"

/* Default size of the pool of free handles
 * Note that this size is somewhat hard-coded into the class (pool member
 * array, getFreeHandle method, and probably others). If you change this
 * value, you'll have to revise this class. */
#define HA_POOL_SIZE 1024

HandleAllocator hand_alloc;

HandleAllocator::HandleAllocator()
{
	num_allocated = 0;
	// allocate the dictionary
	dictionary = new struct dict_s[HA_POOL_SIZE];
	for (int i = 0; i < HA_POOL_SIZE; i++) {
		dictionary[i].type = HANDLE_INVALID;
	}
	// mark all handles and all blocks empty
	for (int i = 1; i < 33; i++) {
		pool[i] = 0xfffffffful;
	}
	// handle 0 is always invalid
	pool[0] = 0xfffffffeul;
}

HandleAllocator::~HandleAllocator()
{
	if (dictionary != NULL) delete [] dictionary;
}

static const unsigned int bit_indices[32] = {
        0,1,28,2,29,14,24,3,30,22,20,15,25,17,4,8,
        31,27,13,23,21,19,16,7,26,12,18,6,11,5,10,9
};

/* Find index of the lowest bit, that is set in the bit representation of n */
static unsigned int _lowestBit(unsigned int n)
{
        // (n&-n) ... extract the lowest bit that is set in n
        // the rest ... find base 2 logarithm of (n&-n) using a lookup table
	//              with 32-bit de Bruijn's hashing
        return bit_indices[static_cast<unsigned int>((n&-n) * 0x077CB531ul) >> 27];
}

int HandleAllocator::getFreeHandle(unsigned int *handle)
{
	if (handle == NULL) return EXCOM_ERR_HANDLE;
	if (num_allocated >= HA_POOL_SIZE - 1) return EXCOM_ERR_HANDLE;
	// find a free handle, there certainly is at least one
	unsigned int free_block, free_index;
	free_block = _lowestBit(pool[32]);
	free_index = _lowestBit(pool[free_block]);
	*handle = 32 * free_block + free_index;
	// mark the handle as allocated
	pool[free_block] &= ~(1 << free_index);
	if (pool[free_block] == 0x0ul) {
		// mark the block as full
		pool[32] &= ~(1 << free_block);
	}
	num_allocated++;
	return EXCOM_ERR_OK;
}

int HandleAllocator::registerObject(unsigned int handle, enum handle_type_e type, void *object)
{
	if (handle >= HA_POOL_SIZE || handle == 0) return EXCOM_ERR_HANDLE;
	if (dictionary[handle].type != HANDLE_INVALID) {
		// the handle is already registered
		return EXCOM_ERR_HANDLE;
	}
	dictionary[handle].type = type;
	dictionary[handle].object = object;
	return EXCOM_ERR_OK;
}

int HandleAllocator::objectFromHandle(unsigned int handle, enum handle_type_e *type, void **object)
{
	if (object == NULL || type == NULL) return EXCOM_ERR_MEMORY;
	if (handle >= HA_POOL_SIZE || handle == 0) return EXCOM_ERR_HANDLE;
	if (dictionary[handle].type == HANDLE_INVALID) return EXCOM_ERR_HANDLE;
	*type = dictionary[handle].type;
	*object = dictionary[handle].object;
	return EXCOM_ERR_OK;
}

void HandleAllocator::freeHandle(unsigned int handle)
{
	if (handle >= HA_POOL_SIZE || handle == 0) return;
	// unregister object bound to the handle, if it has been registered
	dictionary[handle].type = HANDLE_INVALID;

	unsigned int block, index;
	block = handle >> 5;
	index = handle & 0x1f;
	if (pool[block] & (1 << index)) {
		// the handle wasn't even marked as allocated
		// possibly a double-free
		return;
	}
	// mark the handle unused
	pool[block] |= (1 << index);
	if (!(pool[32] & (1 << block))) {
		// the block, where this handle belongs, was marked
		// full; mark it free again
		pool[32] |= (1 << block);
	}
	num_allocated--;
}

