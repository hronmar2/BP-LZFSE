#ifndef __METHOD_COPY_HPP__
#define __METHOD_COPY_HPP__

/*
 * File:       method/copy/copy.hpp
 * Purpose:    Declaration of a dummy compression method (only copies input data to output)
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file copy.hpp
 *  \brief Declaration of a dummy compression method which only copies input data to output.
 *
 *  This module is no real compression method. When connected to a compression chain, this
 *  module will only copy input data to output. The module is only intended for testing,
 *  debugging and measuring performance of the I/O modules.
 */

#include <excom.h>
#include "../../compmodule.hpp"

class CompCopy: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;
public:
	CompCopy(unsigned int handle): CompModule(handle), reader(NULL), writer(NULL) {}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int checkConnection();
	int run();
};

#endif //ifndef __METHOD_COPY_HPP__
