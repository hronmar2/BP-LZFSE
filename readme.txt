This file describes the structure of this CD. This CD is part of 
the 'Compression method LZFSE' bachelor’s thesis. 

The CD contains the following folders and files: 

excom - sources of the ExCom library with the LZFSE module
PragueCorpus - the directory containing files of the Prague Corpus
readme.txt - this file
scripts - directory containing scripts used for benchmarking
text - the thesis text directory, it contains the following:
	src - the directory of LaTex source codes of the thesis
	thesis.pdf - the thesis text in PDF format

