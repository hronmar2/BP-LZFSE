#ifndef __METHOD_PPM_HPP__
#define __METHOD_PPM_HPP__

/*
 * File:       method/ppm/ppm.hpp
 * Purpose:    Declaration of a wrapper class for PPM compression method
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file ppm.hpp
 *  \brief Declaration of a wrapper class for the PPM compression method
 *  (Prediction by Partial Matching). Based on PPMD implementation from
 *  p7zip.
 *
 *  \todo TODO: expand the description
 */

#include <excom.h>
#include "../../compmodule.hpp"

// version of the PPM output file
#define PPM_VERSION 0

// Default values FIXME: these should be set at configuration time (config.h)
#define PPM_MEMORY_SIZE (1 << 24) /* 16 MB */
#define PPM_MIN_MEM_SIZE (1 << 11) /* 2 KB */
#define PPM_ORDER 6
#define PPM_MIN_ORDER 2
#define PPM_MAX_ORDER 32
#define PPM_RAW false

#include "PPMDcoder.h"

class CompPPM: public CompModule {
private:
	// data input and output
	IOReader *reader;
	IOWriter *writer;

	// underlying class that does all the compressing and decompressing
	ppmd::PPMDcoder *comp;
public:
	CompPPM(unsigned int handle):
		CompModule(handle), reader(NULL), writer(NULL) {
		comp = new ppmd::PPMDcoder();
	}
	~CompPPM() {
		if (comp != NULL) delete comp;
	}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();
};

#endif //ifndef __METHOD_PPM_HPP__
