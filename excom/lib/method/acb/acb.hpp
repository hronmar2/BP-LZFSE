#ifndef __METHOD_ACB_HPP__
#define __METHOD_ACB_HPP__

/*
 * File:       method/acb/acb.hpp
 * Purpose:    Declaration of ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009-2010 Filip Simek, Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file acb.hpp
 *  \brief Declaration of ACB compression method (Associative Coder of Buyanovsky).
 *
 *  This module provides the ACB contextual compression method.
 */

#define ACB_PRINT_INFO 0
#if ACB_PRINT_INFO
	#include <ctime>
	#include <iostream>
#endif

#include <stdlib.h>
#include <string.h>

#include <excom.h>
#include "../../compmodule.hpp"

#include "TripletCoder.hpp"
#include "SuffixArray.hpp"
#include "SuffixArray2.hpp"
#include "SuffixTree.hpp"

#define ACB_VERSION 1

#define ACB_DISTANCE_BITS 5
#define ACB_MAX_DISTANCE_BITS 12

#define ACB_LENGTH_BITS 7
#define ACB_MAX_LENGTH_BITS 12

#define ACB_BUFFER_SIZE_BITS 20
#define ACB_MAX_BUFFER_SIZE_BITS 63

#define ACB_CHECK_SIZE_BITS searchSizeBits - 2

#define ACB_ALPHABET_SIZE_BITS 8
#define ACB_COMPRESS_RATE_CHECK 1 << 9

#define ACB_CODER_ARITH 0
#define ACB_CODER_HUFFMAN 1
#define ACB_CODER_DEFAULT ACB_CODER_ARITH

class CompACB: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;
	// optional I/O modules (for statistical purposes)
	IOWriter *wrctx;    // if set, the module writes abs(context-content) to this stream
	IOWriter *wrconf;   // if set, the module writes length of conforming contents to this stream
        
        /*!
         * \brief Structure used for searching in context and content
         *        \c SuffixStructure ancestors differs in type of storing
         *        contexts. For example \c SuffixArray or \c SuffixTree
         */
	SuffixStructure *search;

        /*!
         * \brief Defines how many bits are used to store distance.
         *        Number ( 1 ^\c distanceBits ) is the maximum distance between
         *        best context and best content
         */
	unsigned int distanceBits;

        /*!
         * \brief Defines how many bits are used to store content length.
         */
	unsigned int lengthBits;

        /*!
         * \brief Size of buffer to search context and content in using \c search.
         */
	unsigned int searchSizeBits;

        /*!
         * \brief Size of search buffer after compression ratio is monitored
         */
        unsigned int checkSizeBits;

        /*!
         * \brief Defines how often compression ratio is checked
         */
	unsigned int compressRateCheck;

        /*!
         * \brief Defines current size of encoded input stream
         */
        unsigned long currentEncodedSize;

        /*!
         * \brief Accumulated compress ratio degradation from first occurance of
         *        degradation.
         *
         * By using this field small compress ratio degradations can be ommited
         * until overall degradation exceeds defined maximal degradation
         */
	double compressRatioDecrease;
        
        /*!
         * \brief Compress ratio computed when \c checkCompressRatio method was last time invoked and executed
         */
	double lastCompressRatio;

        /*!
         * \brief Defines which coder is used for coding of triplets.
         */
        int coder;

        /*!
         * \brief Compresses the input
         * \return EXCOM_ERR_OK if compression was successful, other EXCOM_ERR_??? otherwise
         */
	int runCompress();

        /*!
         * \brief Decompresses the input.
         * \return EXCOM_ERR_OK if decompression was successful, other EXCOM_ERR_??? otherwise
         */
	int runDecompress();

        /*!
         * \brief Reads and inits ACB module before starting decompression.         
         * \return EXCOM_ERR_OK if header was read without any error, other EXCOM_ERR_??? otherwise.
         */
	unsigned int readHeader();

        /*!
         * \brief Checks current compress ratio
         * \param currentCheckValue - If \c currentCheckValue is lower than \c compressRateCheck
         *        execution is skipped.         
         *
         * \return true if compress ration decreased and deictionary
         *         should be cleared
         * If compress ratio degrades by \c 0.1% \c search buffer is cleared and
         *  reinitialized.
         */
	bool checkCompressRatio(long &currentCheckValue);

public:
	CompACB(unsigned int handle);
	virtual ~CompACB();

	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();
};

#endif /* __METHOD_ACB_H__ */
