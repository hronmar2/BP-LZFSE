#include <unistd.h>
#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/method/ppm/ppm.hpp"

#define HANDLE 1
#define HANDLE_TMP 4
#define HANDLE_IO_IN 2
#define HANDLE_IO_OUT 3

#define DATA_ORIG "data_comp"
#define DATA_PPM "data_ppm"
#define TMP_PPM "tmp_ppm"
#define TMP_UNPPM "tmp_unppm"

class CompPPMTestSuite: public CxxTest::TestSuite {
	CompPPM *comp;
public:
	void setUp() {
		comp = new CompPPM(HANDLE);
	}
	void tearDown() {
		delete comp;
	}

	void testParameters() {
		unsigned int v;
		TS_ASSERT_EQUALS(comp->setParameter(-1, NULL), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_MEMORY_SIZE, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_ORDER, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_RAW, NULL), EXCOM_ERR_MEMORY);
		v = PPM_MIN_MEM_SIZE - 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_MEMORY_SIZE, &v), EXCOM_ERR_VALUE);
		v = PPM_MIN_MEM_SIZE;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_MEMORY_SIZE, &v), EXCOM_ERR_OK);
		v = PPM_MIN_ORDER - 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_ORDER, &v), EXCOM_ERR_VALUE);
		v = PPM_MAX_ORDER + 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_ORDER, &v), EXCOM_ERR_VALUE);
		v = PPM_MAX_ORDER;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_ORDER, &v), EXCOM_ERR_OK);
		v = 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_RAW, &v), EXCOM_ERR_OK);
		v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_RAW, &v), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(comp->getValue(-1, NULL), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_PPM_MEMORY_SIZE, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_PPM_ORDER, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_PPM_RAW, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_PPM_MEMORY_SIZE, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, (unsigned int)PPM_MIN_MEM_SIZE);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_PPM_ORDER, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, (unsigned int)PPM_MAX_ORDER);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_PPM_RAW, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 0u);

		// a value common to all compression modules
		int u;
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_COMP_RESULT, &u), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(u, EXCOM_ERR_LATER);
	}

	void testIOInputModules() {
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		// invalid pointer to a module
		TS_ASSERT_EQUALS(comp->connectIOModule(NULL, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_MEMORY);
		// invalid connection type
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, -1), EXCOM_ERR_PARAM);
		// input module connected as an output module
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		delete reader;
	}

	void testIOOutputModules() {
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		// output module connected as an input module
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_DUPLICATE);
		delete writer;
	}

	void compareFiles(const char *a, const char *b)
	{
		long sizeA, sizeB;
		FILE *A, *B;
		unsigned char *dataA = NULL, *dataB = NULL;
		A = fopen(a, "rb");
		TS_ASSERT_DIFFERS(A, (FILE*)NULL);
		fseek(A, 0, SEEK_END);
		sizeA = ftell(A);
		fseek(A, 0, SEEK_SET);
		dataA = new unsigned char[sizeA];
		TS_ASSERT_EQUALS(fread(dataA, sizeA, 1, A), 1u);
		fclose(A);

		B = fopen(b, "rb");
		TS_ASSERT_DIFFERS(B, (FILE*)NULL);
		fseek(B, 0, SEEK_END);
		sizeB = ftell(B);
		fseek(B, 0, SEEK_SET);
		TS_ASSERT_EQUALS(sizeA, sizeB);
		if (sizeA == sizeB) {
			dataB = new unsigned char[sizeB];
			TS_ASSERT_EQUALS(fread(dataB, sizeB, 1, B), 1u);
			TS_ASSERT_SAME_DATA(dataA, dataB, sizeA);
		}
		fclose(B);

		if (dataA != NULL) delete dataA;
		if (dataB != NULL) delete dataB;
	}

	void testCompression() {
		unsigned int v;

		// input and output modules
		IOFileReader *reader = new IOFileReader(HANDLE_IO_IN);
		IOFileWriter *writer = new IOFileWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->openFile(DATA_ORIG), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile(TMP_PPM), EXCOM_ERR_OK);

		// running shouldn't succeed without an input I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// ...or without output I/O module
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// set parameters
		v = 2 << 20; //2MB
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_MEMORY_SIZE, &v), EXCOM_ERR_OK);
		v = 6;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_ORDER, &v), EXCOM_ERR_OK);
		v = 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_RAW, &v), EXCOM_ERR_OK);
		// run the compression
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether output data matches our specimen
		compareFiles(TMP_PPM, DATA_PPM);

		delete reader;
		delete writer;

		// decompression
		CompPPM *decomp = new CompPPM(HANDLE_TMP);
		reader = new IOFileReader(HANDLE_IO_IN);
		writer = new IOFileWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->openFile(TMP_PPM), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile(TMP_UNPPM), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(decomp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// set parameters
		v = 1 << 20; //1MB;
		TS_ASSERT_EQUALS(decomp->setParameter(EXCOM_PARAM_PPM_MEMORY_SIZE, &v), EXCOM_ERR_OK);
		v = 6;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_ORDER, &v), EXCOM_ERR_OK);
		v = 1;
		TS_ASSERT_EQUALS(decomp->setParameter(EXCOM_PARAM_PPM_RAW, &v), EXCOM_ERR_OK);
		// run the decompression
		TS_ASSERT_EQUALS(decomp->run(), EXCOM_ERR_OK);

		// check, whether output data matches input original
		compareFiles(TMP_UNPPM, DATA_ORIG);

		delete reader;
		delete writer;
		delete decomp;

		// delete temporary files
		TS_ASSERT_EQUALS(unlink(TMP_PPM), 0);
		TS_ASSERT_EQUALS(unlink(TMP_UNPPM), 0);
	}

	void testHeader() {
		unsigned char bin[1] = {'a'};
		unsigned char x[16];
		unsigned char bout[1];
		unsigned int v;
		unsigned int size;
		// set parameters
		v = 3;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_ORDER, &v), EXCOM_ERR_OK);
		// turn raw off
		v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_PPM_RAW, &v), EXCOM_ERR_OK);

		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(bin, 1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(x, sizeof(x)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &size), EXCOM_ERR_OK);
		TS_ASSERT_LESS_THAN_EQUALS(size, sizeof(x));

		delete reader;
		delete writer;

		CompPPM *decomp = new CompPPM(HANDLE_TMP);
		reader = new IOMemReader(HANDLE_IO_IN);
		writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(x, size), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(bout, 1), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->run(), EXCOM_ERR_OK);

		// verify parameters obtained from the header
		TS_ASSERT_EQUALS(decomp->getValue(EXCOM_VALUE_PPM_ORDER, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 3u);
		TS_ASSERT_EQUALS(decomp->getValue(EXCOM_VALUE_PPM_RAW, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 0u);

		delete reader;
		delete writer;
		delete decomp;
	}
};


