\chapter{LZ family algorithms}
\label{chap:LZ_family}

\emph{LZ family algorithms} is a family of dictionary compression algorithms named after \emph{LZ77} and \emph{LZ78}, two algorithms published by Abraham Lempel and Jacob Ziv in 1977~\cite{Ziv1977} and 1978~\cite{Ziv1978} respectively. LZ77 and LZ78 will both be described in this chapter. 

LZ77 and LZ78 form the basis for many other dictionary compression methods, for example LZSS, LZW or LZ4 methods. 
LZFSE described in this thesis is also Lempel-Ziv style compression method.

\section{LZ77}
\label{sec:LZ77}
\emph{LZ77} (also referred to as \emph{LZ1}) is a dictionary compression method designed by Abraham Lempel and Jacob Ziv and published in 1977~\cite{Ziv1977}. The main idea of this method is using the previously proccesed part of input as the dictionary~\cite[p.~176]{Salomon2007}.
It is suitable for data that are compressed once and decompressed frequently. LZ77 is commonly used as a foundation for more complex compression methods. 

\subsection{Compression}
LZ77 maintains part of input data in a structure called \emph{sliding window}. Sliding window is divided into two parts called search buffer and look-ahead buffer. \emph{Search buffer} contains part of already processed input, while \emph{look-ahead buffer} contains unencoded data yet to be compressed. 
Sliding window has a fixed size. Size of the search buffer is commonly 8\,192 bits, while the look-ahead buffer usually has about 10 to 20 bits~\cite{Stringology_LZ77}.

%figure showing search and look-ahead buffer
\begin{figure}[ht!]
\centering
\begin{tabular}{c|c|c|c}
\cline{2-3}
processed data $\leftarrow$ & search buffer & look-ahead buffer & $\leftarrow$ data to be encoded \\
\cline{2-3}
\end{tabular}
\caption{LZ77 sliding window}
\end{figure}

As the input is processed, the sliding window moves forward in the input. Compressed data from look-ahead buffer transit into the search buffer. Data from the beginning of the search buffer are shifted out and look-ahead buffer is filled with new unencoded data. 

In each step, the compression algorithm searches the whole search buffer to find the longest prefix of look-ahead buffer that is present in it. It scans the search buffer backwards until the symbol look-ahead buffer starts with is encountered. Then it counts  how many characters following this symbol match the look-ahead buffer prefix. If this match is the longest found so far, its position and length are saved. 
The position of the longest match is kept as an \emph{offset}, distance from the beginning of the look-ahead buffer.
Those steps are repeated until the beginning of the search buffer is reached. 
A triplet containing offset of the longest prefix, its length and the symbol that follows it is then written to the output. The sliding window is then moved and the whole process is repeated. See Algorithm~\ref{alg:LZ77_compression} for pseudocode of the compression process.

\begin{pseudocode}
\label{alg:LZ77_compression}
initialize moving window (divided into search and look-ahead buffer)\;
fill look-ahead buffer from input\;
\While{look-ahead buffer is not empty}{
  find longest prefix \textit{p} of look-ahead buffer by scanning the search buffer backwards\;
  \textit{offset} := distance of \textit{p} from the beginning of the look-ahead buffer\;
  \textit{length} := length of \textit{p}\;
  \textit{X} := first character after \textit{p} in look-ahead buffer\;
  output triplet (\textit{offset}, \textit{length}, \textit{X})\;
  shift (move) window by \textit{length} + 1\;
}
\caption{LZ77 compression}
\end{pseudocode}

\subsection{Compression example}
An example of LZ77 compression on input string ``possessed posy'' is shown in Figure~\ref{fig:LZ77compExample}. Size of the sliding window in this example is 8 characters, half of which is search buffer. 

As seen in the example, size of the sliding window strongly affects compression ratio. For instance, in step 7 we search for a prefix beginning with~``p''. No match is found. However string ``pos'' was encountered earlier in the input and would yield a match of length 3 if present in the search buffer. 

The example input string was 14 characters long and it was encoded into 10 triplets. Depending on the representation of triplets, this could result in negative compression in this case. 

%compression example
\begin{figure}[ht!]
\centering

\input{lz77_comp_example.tex}

\caption{Example of LZ77 compression}
\label{fig:LZ77compExample}
\end{figure}

\subsection{Decompression}
Decompression uses sliding window of the same size as compression to keep previous output. 
For each triplet, the sequence from previous output it points to is copied on the current position in the output and then the following symbol contained in the triplet is also written to output. 
See Algorithm~\ref{alg:LZ77_decompression} for simplified pseudocode of the decompression process. Decompression is much simpler and faster than compression. Therefore, LZ77 is classified as an asymmetric compression method. 

\begin{pseudocode}
\label{alg:LZ77_decompression}
\While{input is not empty}{
  read triplet (\textit{offset}, \textit{length}, \textit{X}) from input\;
  go back by \textit{offset} characters in previous output 
    and output \textit{length} characters starting on that position\;
  output \textit{X}\;
  move the sliding window by \textit{length} + 1\;
}
\caption{LZ77 decompression}
\end{pseudocode}

\subsection{Decompression example}
Figure~\ref{fig:LZ77decompExample} shows LZ77 decompression of data encoded earlier in the compression example (see Figure~\ref{fig:LZ77compExample}). Size of the sliding window is always the same as during compression. In this case it is 8 characters.

%decompression example
\begin{figure}[ht!]
\centering

\input{lz77_decomp_example.tex}

\caption{Example of LZ77 decompression}
\label{fig:LZ77decompExample}
\end{figure}

\section{LZ78}
\label{sec:LZ78}
\emph{LZ78} (sometimes referred to as \emph{LZ2}) was introduced by Abraham Lempel and Jacob Ziv in 1978~\cite{Ziv1978}. 

Unlike LZ77, which uses a fixed size sliding window (see Section~\ref{sec:LZ77}), LZ78 maintains a dictionary of previously seen \emph{phrases} (sequences of symbols). This dictionary is built as the input is processed and it may potentially contain unlimited number of phrases. When a repeated occurrence of a phrase is found during encoding, dictionary index is output instead. 

\subsection{Compression}
In each step, the dictionary of previously seen strings is searched for the longest prefix of the unprocessed part of the input. A pair $(index(w), K)$ is written to the output, where $index(w)$ is an index referring to the longest matching dictionary entry $w$ and $K$ is a symbol immediately following $w$ in the input~\cite{Shanmugasundaram2011}. Additionally, new phrase consisting of $w$ concatenated with $K$ (denoted $wK$) is inserted into the dictionary.
This process is repeated until the whole input is encoded. See Algorithm~\ref{alg:LZ78_compression} for the compression pseudocode\footnote{NIL denotes an empty string. The dictionary starts with an empty string as its only element.}. 

\begin{pseudocode}
\label{alg:LZ78_compression}
\textit{w} := NIL\;
\While{input is not empty}{
  \textit{K} := next symbol from input\;
  \If{\textit{wK} exists in the dictionary}{
    \textit{w} := \textit{wK}\;
  }\Else{
    output pair (index(\textit{w}), \textit{K})\;
    add \textit{wK} to the dictionary\;
    \textit{w} := NIL\;
  }
}
\caption{LZ78 compression~\cite{Shanmugasundaram2011}}
\end{pseudocode}

\subsection{Decompression}
During decompression, the dictionary is gradually build in a similar way as during compression. 
See Algorithm~\ref{alg:LZ78_decompression} for the pseudocode of LZ78 reconstruction algorithm. 
LZ78 also preserves an important property of LZ77 that the decompression is generally significantly faster than the compression~\cite{Shanmugasundaram2011}.

\begin{pseudocode}
\label{alg:LZ78_decompression}
\While{input is not empty}{
  read pair (\textit{i}, \textit{K}) from input\;
  \textit{w} := get phrase pointed to by \textit{i} from dictionary\;
  output \textit{wK}\;
  add \textit{wK} to the dictionary\;
}
\caption{LZ78 decompression}
\end{pseudocode}

\subsection{Dictionary}
To represent the dictionary a structure called \emph{trie} (or prefix tree) may be used. Trie is an ordered tree data structure used to store strings. All vertices with common parent begin with the same prefix. The root node represents an empty string and going from it to a certain node yields a phrase from the dictionary. 

In the beginning, dictionary contains only one phrase -- an empty string. As mentioned before, the dictionary may conceptually contain unlimited number of phrases. Because of this, some LZ78 implementations may have very high space requirements. To avoid this problem, it is possible to limit the size of dictionary by some upper bound and remove the least common phrase when this bound is reached or simply clear the dictionary and start building it from scratch. 

Several algorithms based on LZ78 exists, differing mainly in dictionary implementation and usage. 
One of the most popular modifications is LZW made by Terry Welche in 1984. In LZW the dictionary is initialized with all symbols from the input alphabet and so a match is always found. Therefore, only the index to the dictionary is output in each step~\cite{Shanmugasundaram2011}. 

