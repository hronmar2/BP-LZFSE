#ifndef __HANDLES_HPP__
#define __HANDLES_HPP__

/*
 * File:       handles.hpp
 * Purpose:    Declaration of classes related to unique handles' allocation
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file handles.hpp
 *  \brief Declaration of classes that operate on the unique handles to various modules.
 *
 *  Each I/O or compression module as well as each compression chain is referred to by
 *  a unique handle. Classes declared in this file provide management of a pool of free
 *  handles. To make handle allocation a thread-safe process, some methods must work
 *  in a critical section of mutual exclusion. Therefore the algorithm for picking new
 *  handle must be as fast as possible.
 */

//! \brief Enumerates distinct types of modules that have handles assigned to them
enum handle_type_e {
	HANDLE_INVALID = 0,
	HANDLE_IO,
	HANDLE_COMPRESSION,
	HANDLE_CHAIN
};

/*! \brief Class that manages object handles
 *
 *  This class manages handles to various types of objects of the ExCom library
 *  (\ref io_modules, \ref compression, \ref chain). This class should be a singleton,
 *  since all of the objects share the same handle-space, and therefore there is only
 *  need for one instance of this class.
 *
 *  Apart from allocating new handles and managing a list of free handles, this class
 *  also provides translation between handles and objects. An application only
 *  passes object handles through the API, but the library needs to work with objects.
 *  This class provides a method to obtain pointer to an object referenced by the handle.
 *
 *  Typically, one should at first allocate new handle using #HandleAllocator::getFreeHandle,
 *  then create an object with this handle and finally call #HandleAllocator::registerObject
 *  with the handle and pointer to the object. Then the object can be retrieved from
 *  the handle using #HandleAllocator::objectFromHandle. When the object is destroyed,
 *  its handle should be unregistered (and freed for future reuse) with #HandleAllocator::freeHandle.
 */ 
class HandleAllocator {
private:
	/*! A bitmask of handles. First 32 members have 1024 bits, i.e.
	 *  one bit for each handle (this is tightly bound to the value
	 *  of HA_POOL_SIZE macro defined in handles.cpp). The last member
	 *  of this array is used to determine, which of the first 32
	 *  blocks are occupied. A bit set to 1 means free block/handle,
	 *  a bit cleared to 0 means occupied block/handle. */
	unsigned long pool[33];
	/*! Number of handles, that have been allocated and haven't
	 *  been free'd, yet */
	unsigned int num_allocated;
	struct dict_s {
		enum handle_type_e type;
		void *object;
	} *dictionary;  //!< Dictionary for translating handle to a pointer to an object.
public:
	//! \brief Constructor, allocates a pool of free handles
	HandleAllocator();
	~HandleAllocator();

	/*! \brief Returns a free handle
	 *  \param handle Pointer to a variable to hold the resulting handle
	 *  \return 
	 *         - \c EXCOM_ERR_OK - success
	 *         - \c EXCOM_ERR_HANDLE - module handle couldn't have been allocated.
	 *           Either \c handle is an invalid pointer, or there are no free handles left.
	 */
	int getFreeHandle(unsigned int *handle);
	/*! \brief Registers an object with its handle
	 *  \param handle The handle that was previously allocated using #getFreeHandle
	 *  \param type Type of the object to be registered. See #handle_type_e
	 *  \param object Pointer to the object to register
	 *  \return
	 *         - \c EXCOM_ERR_OK - success
	 *         - \c EXCOM_ERR_HANDLE - the value of \c handle is invalid (or the
	 *           same handle has already been registered)
	 *
	 *  This function registers an object to its handle so that pointer to
	 *  that object can be retrieved from the handle using #objectFromHandle.
	 */
	int registerObject(unsigned int handle, enum handle_type_e type, void *object);
	/*! \brief Translates given handle to a pointer to the object with that handle
	 *  \param handle Handle to an object
	 *  \param type Pointer to a variable to hold type of the object
	 *  \param object Pointer to a variable to hold the pointer to the object
	 *  \return
	 *         - \c EXCOM_ERR_OK - success
	 *         - \c EXCOM_ERR_HANDLE - the value of \c handle is invalid (or the
	 *           handle hasn't beed registered using #HandleAllocator::registerObject)
	 *         - \c EXCOM_ERR_MEMORY - \c object or \c type is an invalid pointer
	 */
	int objectFromHandle(unsigned int handle, enum handle_type_e *type, void **object);
	/*! \brief Unregisters and frees given handle
	 *  \param handle A handle to be unregistered
	 *  
	 *  This function frees the given handle so that it can be reused for another
	 *  object (the handle gets back to the pool and may be returned by a subsequent
	 *  call to #HandleAllocator::getFreeHandle). If the handle has been registered
	 *  with an object using #HandleAllocator::registerObject, it is automatically
	 *  unregistered.
	 */
	void freeHandle(unsigned int handle);
};

//! single instance of the HandleAllocator class to be used in all modules where necessary
extern HandleAllocator hand_alloc;

#endif // ifndef __HANDLES_HPP__

