#!/bin/sh

source "common.sh"

#putrat <corpus_file> <method> <tex_file> <tab_file> <corp_file_index>
putrat() {
	o=`wc -c "$d"/$1.$2 | grep -o '^[0-9][0-9]*'`
	rat=`echo "scale=7; $o / ${sizes[$5]}" | bc`
	xrat=`separators $rat`
	printf '%16s' "$xrat &" >>"$3"
	printf '%16s' "$rat" >>"$4"
	if [ x"$2" = "xdca" ]; then
		exc=`wc -c "$d"/$1.$2.exc | grep -o '^[0-9][0-9]*'`
		rat=`echo "scale=7; ($o+$exc) / ${sizes[$5]}" | bc`
		xrat=`separators $rat`
		printf '%16s' "$xrat &" >>"$3"
		printf '%16s' "$rat" >>"$4"
	fi
}

get_orig_sizes
export sizes

out=ratio_all.txt
tex=ratio_all.tex
# write header
echo '\hline' >$tex
printf '%16s%16s%16s%16s%16s\\\\\n\\hline\n' "File &" "ACB &" "DCA &" "DCA + exc. &" "PPM &" >>$tex
printf '%16s%16s%16s%16s%16s\n\n\n' "File" "ACB" "DCA" "DCA + exc." "PPM" >$out

i=0
for f in corpus/*; do
	g=`basename $f`
	printf '%16s' "$g &" >>$tex
	printf '%16s' "$g" >>$out
	for met in acb dca ppm; do
		putrat "$g" $met "$tex" "$out" $i
	done
	printf '\\\\\n' >>$tex
	printf '\n' >>$out
	i=$(($i+1))
done

printf '\\hline\n' >>$tex

