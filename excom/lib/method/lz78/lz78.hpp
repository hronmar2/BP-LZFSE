#ifndef __METHOD_LZ78_HPP__
#define __METHOD_LZ78_HPP__

/*
 * File:       method/lz78/lz78.hpp
 * Purpose:    Declaration of the LZ78 compression methods
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZW compression module linked to the ExCom library.
 *
 * LZW compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZW compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZW module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lz78.hpp
 *  \brief Declaration of the LZ78 compression method
 */

#include <excom.h>
#include "../../compmodule.hpp"

#include <map>
#include <vector>
#include <string>

#define DEFAULT_DICTIONARY_SIZE 4096
#define OPERATION_COMPRESS 0
#define OPERATION_DECOMPRESS 1

class CompLZ78: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;

	int runCompress();
	int runDecompress();

	unsigned int dictionary_size;
public:
	CompLZ78(unsigned int handle): CompModule(handle), reader(NULL), writer(NULL), dictionary_size(DEFAULT_DICTIONARY_SIZE) {}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();
private:
	typedef struct {
		signed int prefixIndex; 
		signed int first;
		signed int left;
		signed int right;

		unsigned char symbol;
	} lz78_node_enc_t;

	typedef struct {
		signed int prefixIndex; 
		unsigned char symbol;
	} lz78_node_dec_t;

	typedef std::vector <lz78_node_enc_t > lz78EncDictionary;
	lz78EncDictionary encDictionary;

	typedef std::vector <lz78_node_dec_t > lz78DecDictionary;
	lz78DecDictionary decDictionary;

	std::vector<unsigned char> decodedString;

	signed int dictionaryIndex;

	int minBits;
	int maxBits;
	int currentBits;
	int nextBitsIncLimit;	

	/*! \brief Initialize the LZ78 dictionary
	 *  \param op Type of operation - encode or decode
	 */
	void initializeDictionary(int op);

	bool getCode(unsigned long);

	signed int add(lz78_node_enc_t&);

	/*! \brief Returns the number of bits required to store the given number
	 *  \param n A number
	 */
	int requiredBits(unsigned int n);

	bool searchForNode(lz78_node_enc_t&);
};

#endif //ifndef __METHOD_LZ78_HPP__
