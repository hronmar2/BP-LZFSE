#ifndef _STATIC_HUFF_NODE_H
#define	_STATIC_HUFF_NODE_H

/*
 * File:       method/shuff/SHuffNode.hpp
 * Purpose:    Declaration of the Static Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Static Huffman coding module linked to the ExCom library.
 *
 * Static Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Static Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Static Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */


/*! \file SHuffNode.h
 *  \brief Declaration of Huffman node representation.
 *
 *  This class serves for Huffman node representation and manipulation
 */

#include <string>

using namespace std;

class SHuffNode {
private:
	signed int symbol;  //!< because of -1 value
	unsigned long freq;

	SHuffNode* leftChild;
	SHuffNode* rightChild;
	SHuffNode* parent;

	unsigned int code;
	unsigned int length;
public:
	// Constructors
	SHuffNode();
	SHuffNode(signed int, unsigned long);
	SHuffNode(SHuffNode*, SHuffNode*);

	// Destructor
	~SHuffNode();

	// Getters
	signed int getSymbol();
	unsigned long getFreq();

	SHuffNode* getLeftChild();
	SHuffNode* getRightChild();
	SHuffNode* getParent();

	unsigned int getCode();
	unsigned int getLength();

	// Setters
	void setSymbol(signed int);
	void setFreq(unsigned long);
	void setLeftChild(SHuffNode *);
	void setRightChild(SHuffNode *);
	void setParent(SHuffNode *);
	void setCode(unsigned int);
	void setLength(unsigned int);

	// How to comapre the nodes when inserting them into the priority queue
	struct compare_nodes {
		bool operator()(SHuffNode* n1, SHuffNode* n2) {
			return n2->getFreq() <= n1->getFreq();
		}
	};
};

#endif	/* _STATIC_HUFF_NODE_H */

