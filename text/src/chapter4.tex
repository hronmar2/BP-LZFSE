\chapter{LZFSE}
\label{chap:LZFSE}

LZFSE is a lossless compression algorithm that was developed by Apple Inc. and later released as open-source. The algorithm is based on Lemple-Ziv dictionary compression (described in Chapter~\ref{chap:LZ_family}) and uses finite state entropy, a variant of ANS described in Chapter~\ref{chap:ANS}, for entropy coding~\cite{DeSimone2016}. 

According to its authors, LZFSE matches the compression ratio of 
zlib\footnote{zlib is a compression library that provides an abstraction over the DEFLATE compression algorithm. zlib implementation of DEFLATE allows users to choose a compression level between $0$ and $9$. Lower level means preferring speed and higher level means prioritizing compression ratio. Level $5$ provides a good balance between speed and compression ratio.} level 5, 
while being between two to three times faster for both compression and decompression and having better energy efficiency. LZFSE is a balanced compression method intended for situations when compression ratio and decompression speed are equally important~\cite{AppleDocumentation}.

A reference implementation of LZFSE in C language written by Eric Bainville was released in 2016~\cite{LZFSE_github}. This reference implementation serves as a definition of the LZFSE method, as Apple did not publish any other detailed description of it (e.g.\ pseudocodes or format description). 

This chapter describes the reference implementation of the LZFSE method. All details pertain to the version from 22 May 2017, which is the most recent version as of March 2018~\cite{LZFSE_github}. The file structure of the reference implementation and its main functions are described in Appendix~\ref{appendix_LZFSE}. 

\section{Compression}
\label{sec:lzfse_comp}

LZFSE combines LZ-style dictionary compression algorithm with finite state entropy. The dictionary algorithm serves as a \emph{frontend} and the finite state entropy serves as a \emph{backend} and is used to encode the matches and literals 
produced (emitted) by the frontend. 

LZFSE compresses the input data and generates one or more \emph{compressed blocks}. 
There are several types of blocks that may be produced by the reference implementation. Each block starts with a \emph{header} containing information about this block. Headers for different types of blocks differ not only in constitution but also in size. Regardless of block type however, the first four bytes of a header consist of special value identifying the block type (referred to as a \emph{block magic}). There is also a special value indicating the end of file. 

The blocks produced by LZFSE compression are described at the end of Section~\ref{subsec:lzfse_comp_backend}. There is also an \emph{uncompressed block} type for unencoded data. If the input is really small (the default threshold is $8$ bytes) or if LZFSE compression fails for some reason, the input is just copied unaltered and saved as an uncompressed block. 

The reference implementation also contains a heuristic that will fallback to a simpler compression algorithm called LZVN when the input is smaller than a given threshold. The reason is that on small data LZFSE does not perform as well according to the author~\cite{LZFSE_github}. Because this thesis focuses on LZFSE, this algorithm will not be covered here. LZVN compression also was not implemented as part of the LZFSE module into ExCom, as the goal was to test the performance of the LZFSE method. 

\subsection{Frontend}
\label{subsec:lzfse_comp_frontend}

The LZFSE frontend works by searching for matches in the input. 
A \emph{match} is a sequence of bytes that exactly matches other sequence encountered earlier in the input. The frontend scans the input for matches using a procedure described below. These matches are then \emph{emitted} -- sent to the backend, where they are encoded. 

The frontend produces matches in form of triplets $ (L, M, D) $, where $M$ is the length of the match, $D$ is the distance to the previous occurrence (i.e.\ offset) and $L$ is the number of literals emitted with this match. \emph{Literals} are values from the original input stream. When a match is emitted, all literals between this match and the previous one are also emitted with it. 
Emitted literals are kept in concatenated form and then encoded separately in the backend. 
For details on how the emitted matches and literals are processed in the backend, see Section~\ref{subsec:lzfse_comp_backend}.

Triplets that represent the LZFSE matches are somewhat similar to triplets produced by the LZ77 algorithm (see Section~\ref{sec:LZ77}). The difference is that while LZ77 triplets contain only one symbol following the match, in case of LZFSE the $L$ symbols (literals) before the match are emitted with it. 

Because LZFSE uses a 32-bit offset for encoding matches, if the input is large, it must be divided into smaller blocks and each of them processed separately. This is done for inputs significantly smaller than the 2~GiB upper bound, as it is better for algorithm efficiency~\cite{LZFSE_github}. 


\subsubsection{History table}
LZFSE uses a \emph{history table} and hashing to search for matches. The history table resembles a hash table with separate chaining (see Section~\ref{subsec:hash_table}) to some extent. It is an array indexed by hash values. 
On each position in the array, there is a \emph{history set} containing locations of four-byte sequences encountered earlier in the input that hash to the same value -- the index of this history set in the array. 

Apart from the position, the first four bytes at that position are also stored for each element in the history set to quickly eliminate false-positive matches caused by hash function collisions.  
By default, each history table set can hold up to four positions. When more positions of four-byte sequences with this hash are found, the oldest element from the set is removed and the new entry is added in the beginning of this set. 

For the hash function, an implementation of multiplicative hash described in Section~\ref{subsec:mult_hash} is used. This implementation also allows to change the hash length (how many bits the hash function produces), which is controlled by a parameter. The hash length also determines the size of the history table. 

\begin{figure}
\centering
\def\svgwidth{0.9\textwidth}
\input{history_table.pdf_tex}
\caption[LZFSE history table]{The history table used by LZFSE frontend to search for matches. A set on index $i$ contains positions of previously seen four-byte sequences (and their values) with hash equal to $i$. To get match candidates for a position in the input, the hash of the four-byte sequence starting on that position is computed. This value is than used as an index into the history table. }
\label{fig:LZFSE_history_table}
\end{figure}


To find match candidates for some position in the input, hash function is computed for the first four bytes on that position. The hash value is then used as an index into the history table, yielding a set of match candidate positions. Because entry for each candidate also contains the first four bytes on that position, collisions can be quickly eliminated by comparing these bytes with bytes on current position. Figure~\ref{fig:LZFSE_history_table} depicts the history table and illustrates this process. 

As already mentioned, the size of the history table depends on the hash length. If the hash function produces $14$-bit values, which is the default, the~table will contain $2^{14}=16384$ elements (sets). 

\subsubsection{Searching for matches}
%lzfse_encode_base()
Algorithm that searches for matches keeps a \emph{pending match} -- the best match found so far, which will be emitted unless a better one is found. 
Note that the matches are internally kept by the frontend in a slightly different form than how they are then emitted to the backend. Here the position where the match starts, the position where the previous occurrence starts (called \emph{source}) and the length of the match are stored. 

The matches are searched by repeating the following procedure for each position in the input. 
First, a hash value of first four bytes starting on current position is computed. An entry for this position is later added into the correct set of the history table before moving on to the next position. 

The current position may be inside some previous match. 
This can be detected by keeping track of the next unencoded literal (i.e.\ the first literal in the input that was not part of any previous match). If the current position is before this literal, it is inside a previously emitted match. 
In that case, the history table is just updated and the algorithm continues on the next position. 

The computed hash value for current position is used to get match candidates from the history table as described above. The best match is then chosen from these candidates. This is done by counting how many bytes on each of the candidate positions match the bytes following the current position. The longest match is taken. Only matches of length at least $4$ are considered~\cite{LZFSE_github}. 

The best match found is also expanded backwards if possible. While the bytes preceding the starting position of the match and the bytes before the source position are equal, the match length is incremented and both positions are moved back by one. 

If the best match found for this position (the \emph{current match}) has length greater than some given threshold (default is 40), it is immediately emitted. Otherwise, if there is no pending match, the current match will become the new pending match and the algorithm will continue on the next position. 
If there is already a pending match however, it should be emitted first and then the current match should become the new pending match. 
This may not be possible if the pending match overlaps with the current match (i.e.\ the current match starts inside the pending match). In that case, the longer of these two will be emitted. 

If no match is found for the current position, it may still be desirable to emit some literals so that the current position is not too far ahead of the next unencoded literal. If the distance is larger than some given threshold, the pending match is emitted (if there is one) along with some literals. Otherwise, the algorithm just moves on to the next position. 

This procedure is repeated until the end of input buffer is reached. 
Then the pending match and any remaining literals are also emitted and a method of backend is called to produce the final block and an end-of-file marker. 

\subsection{Backend}
\label{subsec:lzfse_comp_backend}

The backend of the LZFSE encoder uses an implementation of finite state entropy described in Section~\ref{sec:FSE}. It encodes the matches and literals emitted by the frontend. 

The frontend emits matches and literals by calling the appropriate function of the backend. The matches are emitted as $(L, M, D)$ triplets described in Section~\ref{subsec:lzfse_comp_frontend}.%, the $L$ literals are emitted as a part of the match. 
When literals need to be emitted separately (i.e.\ not as a part of match), they are emitted by creating a fake match, a match of length $0$, and emitting them along with this match. 

The encoder backend keeps buffers for emitted matches and literals. % in the current state. 
For each element of the $(L, M, D)$ triplet there is a separate buffer and there is also a buffer for emitted literals. 
Whenever a match is emitted, each of its $L$, $M$ and $D$ elements is pushed into its respective buffer and the $L$ literals emitted as a part of this match are pushed into the literals buffer. When any of these buffers is full, all stored matches and literals are encoded using finite state entropy and a compressed block is produced by the backend. 

Contents of each of the four buffers is encoded separately -- the numbers of literals ($L$), the match lengths ($M$), the offsets ($D$) and the concatenated literals are each encoded separately using finite state entropy. 
How the finite state encoder is implemented in the reference implementation is described in Section~\ref{subsubsec:lzfse_comp_fse} below. 

Before encoding, the frequencies of symbols are counted. 
These frequencies are then normalized and used to create the coding tables for finite state entropy encoders. 
The exact process is described in Section~\ref{subsubsec:lzfse_comp_fse}. 
The frequency tables are also saved into the header to be used during decoding. 

The backend internally uses two types of headers. A \emph{\texttt{v\_1} header} contains frequency tables, final encoder states and all the other information in unencoded form. However, the header is written to output in a different form called~\emph{\texttt{v\_2} header}. This type of header contains all the information from \emph{\texttt{v\_1}~header}, but the frequency tables are compressed using a form of Huffman coding. Moreover, the other fields from \emph{\texttt{v\_1} header} are ``\textit{packed}'' -- copied in compact form to shared continuous block of memory to save some space\footnote{For instance the final state of FSE encoder for $M$ elements is stored as a 16-bit integer in \emph{\texttt{v\_1}~header}. However, since it can only take values in range 0--63, such representation is inefficient. It is copied into fewer bits in \emph{\texttt{v\_2} header}.}. This header will be output in the beginning of the compressed block. 

The literals are encoded first. They are encoded bytewise but four literal bytes are encoded in each step using one FSE encoder for each. So there are four FSE encoders for literals, which rotates in encoding the literal bytes. 
The four literal FSE encoders all use the same frequency tables and they also share the encoding tables but each of them has its own state. 
The reference implementation also uses an output stream that allows to output individual bits by accumulating them and outputting whole bytes when the accumulator is full. The four literal encoders share one such output stream. 

The process described in previous paragraph requires that the number of literal bytes is divisible by four. This is ensured before the literals are encoded by adding up to three additional literals (to increase the number to nearest multiply of four). Note that even with the added literals, the original data can still be decoded unambiguously, because the number of preceding literals is kept for each match (as a $L$ in the $(L, M, D)$ triplet). 

The FSE encoders for literals have $2^8 = 256$ possible input symbols (i.e.\ all possible byte values) and they use 1024 states in the reference implementation. The encoding also uses the procedure mentioned in Section~\ref{sec:ANS}: the literals are encoded backwards starting from the last literal. Because decoding works in the opposite direction, it will start from the first. 
When all literals are encoded, the final encoder state for each of the four FSE encoders is saved into the header. 

The $L$, $M$ and $D$ elements of match triplets are encoded next. Three separate FSE encoders for each of the elements are used. 
In each step, the $L$, $M$, $D$ elements of one match triplet are encoded and the algorithm then continues with the next match. Similar output stream as was used when encoding literals is also used here to allow outputting individual bits. One such stream is shared by the three FSE encoders. 

The $L$, $M$ and $D$ values may be possibly very large, so the FSE encoders would need to have many symbols and the encoding tables would be huge. Instead, each encoder has a fixed number of possible symbols (it is $20$ for $L$ and $M$, $64$ for $D$). There is also a limit on the values $L$, $M$ and $D$ may take. If either $L$ or $M$ is larger than its maximum value, the match must be split into multiple matches and each of them encoded separately. These maximums are still considerably larger than the number of FSE symbols, for instance it is 2359 for match length~($M$), while its FSE encoder has only $20$ possible symbols. 
To map the values of $L$, $M$ and $D$ elements to symbols used by FSE, tables\footnote{These tables do not change with input data. They are hard-coded as constant static arrays in the reference implementation (see lzfse\_encode\_tables.h and lzfse\_internal.h).} are used. For each possible value, they contain its \emph{base value}. The element is encoded as its base value and the extra bits representing the difference between value of the element and its base value are also written to output. In the reference implementation the extra value bits are written before the extra state bits~(produced by FSE). 
Because lower values are more probable, they generally map to same base value and no extra bits need to be written. For each base value (i.e.\ symbol), the number of extra bits is stored in another table\footnote{This is also hard-coded in the reference implementation.} to be used during decompression. 

As in the case of literals, the match triplets are also encoded starting from the last, so that they can be decoded starting from the first. The final state for each of the FSE encoders is also saved into the header when all matches are encoded. 

%\pagebreak
\paragraph*{Compressed block structure}
\begin{flushleft}
In summary, the LZFSE compressed block (with the \emph{\texttt{v\_2} header}) has the following structure: 
\end{flushleft}

\begin{itemize}
\item It begins with a header that contains the following elements: 
\begin{itemize}
\item block magic identifying the type of this block (LZFSE compressed block)
\item number of decoded (output) bytes in block
\item number of literals\footnote{Slightly different terminology is used in the reference implementation. 
There, all the bytes between the previous and the next emitted match are referred to as one literal. 
Therefore, as noted in a comment there, this number is not the number of literals defined in this way.} (i.e.\ number of bytes taken by literals in unencoded form, will be multiple of 4)
\item size of the encoded literals (i.e.\ number of bytes taken by literals in encoded form)
\item number of $(L, M, D)$ matches
\item three bits representing internal state of literal FSE output stream %literal_bits
\item the final states of the four literal FSE encoders
\item number of bytes used to encode the $(L, M, D)$ matches
\item three bits representing internal state of matches FSE output stream %lmd_bits
\item size of the header
\item final states of the $L$, $M$ and $D$ FSE encoders
\item compressed frequency tables for FSE encoders
\end{itemize}

\item Then it contains all the compressed literals. 

\item Then it contains the compressed $(L, M, D)$ triplets representing matches. 

\end{itemize}

All fields of the header starting with number of literals are kept in compact form. All fields between number of literals and final states of $L, M, D$ encoders (inclusive) are stored into shared continuous block of memory. The frequency tables for $L, M, D$ and literal encoders are compressed using a form of static Huffman coding. 

\subsubsection{Finite state entropy encoding}
\label{subsubsec:lzfse_comp_fse}

The finite state entropy encoding as implemented in the reference implementation of LZFSE is described here (see Section~\ref{sec:FSE} for general description). 

Before the actual encoding can take place, the encoding table must be initialized. This is done based on the symbol frequencies, which are counted in advance. Using the frequencies of symbols, the states\footnote{The number of states for FSE encoders is fixed. It is hard-coded for each encoder type~($L, M, D$ and literals) in the reference implementation.} are distributed into subsets (corresponding to symbols). 
Each symbol is assigned a number of states based on its frequency. Symbols with higher frequency of occurrence will get more states but every symbol that has non-zero frequency is given at least one. For instance, if a symbol has frequency of 15\% (i.e.\ 15\% of input symbols are equal to this symbol), then approximately 15\% of possible states will be assigned to it. 

In this implementation of FSE, the encoder table contains an entry for each possible \emph{symbol}. Each entry contains a value called \emph{delta}. This value is relative increment used to obtain the next state~\cite{LZFSE_github}. When a symbol is encoded, the next state is computed by adding $delta$ (from the table entry for this symbol) to the current state. The value of $delta$ for each symbol depends on the number of states that were assigned to this symbol. The more states the symbol has the lower the $delta$. 
Thus, symbols with larger frequencies are assigned more states and so they have lower $delta$. 

The mechanism to ensure that states remain in a fixed range that was described in Chapter~\ref{chap:ANS} is also used here. When a state would be larger than the maximum state, its $k$ least significant bits are \emph{shifted} -- transferred to the output. This $k$ is computed before encoding and saved for each symbol. However, this number also depends on the current state, larger states may require to transfer more bits to the output to keep in the range. Because of this, the first state requiring more bits to be shifted (denoted $s_0$) is also computed and saved for each symbol in the table. If the current state is smaller than $s_0$, only its $k-1$ bits are shifted. Otherwise, if the state is larger or equal, its $k$ bits are shifted. Naturally, for both of these variants a different $delta$ will be used to get to the next state. Both variants are stored in the table, denoted $delta_0$ and $delta_1$ respectively.

The encoding of a symbol using FSE as it is done in the LZFSE reference implementation is summarised in the following pseudocode: 

\begin{pseudocodehere}
\label{alg:LZFSE_FSE_encode}
\Input{\textit{symbol}, current state $s$, encoding table $t$}
\Output{new state $s\prime$}
\If{$s < t[symbol].s_0$ }{
  \textit{nBits} := $t[symbol].k - 1$\;
  \textit{delta} := $t[symbol].delta_0$\;
} \Else {
  \textit{nBits} := $t[symbol].k$\;
  \textit{delta} := $t[symbol].delta_1$\;
}
output \textit{nBits} least significant bits of $s$\;
$s\prime$ := $(s >> \textit{nBits}) + \textit{delta}$\;
%\Return{$s\prime$}
\caption{FSE symbol encoding}
\end{pseudocodehere}


\section{Decompression}

The decompression is done block by block. The header for the current block is loaded first. As described in Section~\ref{sec:lzfse_comp}, the first four bytes of each block contain a special value identifying type of that block (called block magic). If the block magic is equal to the special value denoting the end of file, the decompression is finished. Otherwise, the header is decoded in a way specific to the particular block type. Information about the current block is obtained from it and saved to be used during decoding. 

If the current block is of uncompressed block type, it is decoded simply by copying the block content to the output unchanged. The decompression algorithm then moves on to the next block. 

As mentioned in the compression section, there is a fallback algorithm called LZVN, which is used for compressing small files in the reference implementation (for speed reasons). It also has its own block type and its own decompression algorithm, which is used if a block of this type is encountered. As stated before, this algorithm will not be covered here, because this work focuses on the LZFSE algorithm. 

There are two types of headers for blocks compressed by LZFSE, which were described in Section~\ref{subsec:lzfse_comp_backend}. As also mentioned there, only the \emph{\texttt{v\_2} header} (the header with compressed frequency tables) is actually used in practice. The other header type (\emph{\texttt{v\_1}}) is only used internally, but the decoder also supports blocks starting with header of that type. 

When the compressed header is encountered, it is decoded first and all the information, including frequency tables for FSE, is obtained. 
Four FSE encoders were used to encode the literals, there also was one encoder for each of the $(L, M, D)$ elements (see Section~\ref{subsec:lzfse_comp_backend}). For each of these FSE encoders, a corresponding FSE decoder is used during decoding. The final states of all encoders were saved inside the header, from which they are now read and used to initialize all the FSE decoders. As in the case of encoding, one shared decoding table is used by all four literal decoders. 
%The decoding tables are constructed from the frequency tables (obtained from block header) for each FSE decoder. 

The literals were encoded first and so they are present in the beginning of the block immediately following the header. 
Thus, all the literals are decoded first using the four FSE decoders, which rotates in decoding the literals. Decoded literals are stored in a buffer to be used later when decoding the matches. Thanks to the literals being encoded in reverse order (starting from the last), they are now decoded starting from the first\footnote{As described in Section~\ref{sec:ANS}, ANS decoding works in opposite direction than encoding.}. This also applies to the $L, M, D$ values decoded later. 
The finite state entropy decoding is described in a separate section below, see Section~\ref{subsec:lzfse_decomp_fse}. %The FSE decoder is initialized with frequencies stored in the header. 

The matches produced by the encoder in a form of $ (L, M, D) $ triplets are present next in the block. 
The matches are decoded successively in the process described below. The output is not written directly but it is rather kept in a buffer so that the previous output can be copied when decoding the matches. This buffer is referred to as the \emph{output buffer}. %and it will contain all the decoded input data when the decoding is over. 
The current position in the output buffer is kept and updated when executing the matches (as described further in the text). 

For each match, the $ L, M, D $ values are decoded first using their respective FSE decoders. As described in Section~\ref{subsec:lzfse_comp_backend}, the values were encoded as their base value and bits representing the difference between their actual value and base value were written to the output. Therefore, additional bits must be read from the input and added to the base value yielded by FSE decoding step. This is done in the FSE decoder, the number of additional bits for each symbol is known in advance\footnote{As described in the backend section of encoder, the tables used to convert values to their base values are hard-coded in the reference implementation.} and so it is saved in the decoder entries during the initialization. 

When the $L, M, D$ values of match are decoded, the match is \emph{executed}. 
Firstly the $L$ literals from the literal buffer are copied on the current position in the output buffer. The pointer to the literal buffer is then moved by $L$ positions (to point to the next literal that was not yet written to output). 
Then the $M$ bytes starting $D$ positions before the current position in the output buffer are copied on the current position in the output. 

The process of executing matches is similar to how LZ77 decodes the matches. A portion of previous output pointed to by the current match is copied on the current position. In case of LZ77, the previous output is kept in a sliding window, from which symbols are removed  and new ones added every step. In case of LZFSE, all output is accumulated in the output buffer. The values are copied within the output buffer and only the pointer to current position needs to be updated, so no costly operation (as is the sliding window management in case of LZ77) needs to be done. 

The process of decoding one match is formalized in Algorithm~\ref{alg:LZFSE_decode}, \textit{dst} is pointer to the current position in the output buffer and \textit{lit} is pointer to the literal buffer. Note that the literal buffer contains all literals, which were decoded during initialization. 
The $L, M, D$ symbols are decoded using their FSE decoder (the FSE decoding is described in Section~\ref{subsec:lzfse_decomp_fse} below).

This is repeated until all matches are decoded. If the output buffer runs out of space, the decoding is interrupted. 
When all matches are decoded, the output buffer contains the whole decoded block. 
Subsequently, the decoder state is updated and the decompression continues with the next block. 

\begin{pseudocode}
\label{alg:LZFSE_decode}
decode the next $L, M, D$ symbols (using FSE decoders)\;
copy $L$ bytes (literals) from \textit{lit} to \textit{dst}\;
\textit{dst} := \textit{dst} + $L$\;
\textit{lit} := \textit{lit} + $L$\;
copy $M$ bytes from \textit{dst} - $D$ to \textit{dst}\;
\textit{dst} := \textit{dst} + $M$\;
\caption{LZFSE match decoding step}
\end{pseudocode}


\subsection{Finite state entropy decoding}
\label{subsec:lzfse_decomp_fse}

\subsubsection{Decoding literals}
The decoder for literals is simpler than the $L, M, D$ values decoder (described further in the text). It is an implementation of the finite state entropy decoding. The decoding table is initialized before the decoding may take place. The initialization uses the same frequency table as was used for initializing the encoding table. 

The decoding table contains an entry for each state. Each entry must contain an information on how to get to the previous state\footnote{From the decoding viewpoint, this state is actually the next state, but it will be referred to as the previous state to conform to the description of asymmetric numeral systems in Chapter~3.} and the decoded symbol\footnote{This is the symbol that was encoded by transitioning to this state during encoding.}. The decoded $symbol$ is simply stored in each table entry.  

The decoder table entry also contains values called $delta$ and $k$. 
The value of $k$ is the number of bits that were transferred to output during encoding to ensure the state is lower than given maximum (see Section~\ref{subsubsec:lzfse_comp_fse}). During decoding, these $k$ bits must be read from input and added to the $delta$ value to get to the previous state. The decoding step is described in the following pseudocode:

\begin{pseudocodehere}
\label{alg:LZFSE_FSE_decode_lit}
\Input{current state $s$, decoding table $t$}
\Output{decoded symbol (literal), new state $s\prime$}
$n$ := read $t[s].k$ bits from input\;
$s\prime$ := $t[s].delta + n$\;
\Return{$t[s].symbol$}
\caption{Decoding of a literal using FSE}
\end{pseudocodehere}

%The previous state is stored in a similar way as the next state was stored in the FSE encoder table (see Section~\ref{subsubsec:lzfse_comp_fse}). The information about the previous state is kept in a form of $delta$ -- the relative increment used to get to the previous state. 

\subsubsection{Decoding $L, M, D$ values}

The $L, M, D$ values are decoded in a similar way as the literals.
The values were encoded as their base values (this was described in Section~\ref{subsec:lzfse_comp_backend}) and the difference was written to the output. 
When they are decoded, this difference must be read from input before reading the bits that are added to $delta$ to obtain the previous state.  

Each entry of the decoding table contains: $delta$ (as in the case of literals), the value base (\textit{vbase}), the number of bits that will be read from input (called \textit{total\_bits}) and a number indicating how many of these bits represent the difference from the base value (called \textit{value\_bits}). 

The following pseudocode shows how a $L$, $M$ or $D$ value is decoded\footnote{Note that in the reference implementation, all the extra bits are read together. They are then separated on state and value bits using bit operations. However, they are read separately in the pseudocode for simplicity reasons.}: 

\begin{pseudocodehere}
\label{alg:LZFSE_FSE_decode_LMD}
\Input{current state $s$, decoding table $t$}
\Output{decoded value ($L$, $M$ or $D$), new state $s\prime$}
\textit{value\_extra} := read $t[s].value\_bits$ bits from input\;
\textit{state\_extra} := read $(t[s].total\_bits - t[s].value\_bits)$ bits from input\;
$s\prime$ := $t[s].delta + state\_extra$\;
\Return{$t[s].vbase + value\_extra$}
\caption{Decoding of one $L$, $M$ or $D$ value using FSE}
\end{pseudocodehere}

