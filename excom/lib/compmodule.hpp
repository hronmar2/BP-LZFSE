#ifndef __COMPMODULE_HPP__
#define __COMPMODULE_HPP__

/*
 * File:       compmodule.hpp
 * Purpose:    Declaration of abstract ancestor of the compression modules
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file compmodule.hpp
 *  \brief Declaration of abstract ancestor of the distinct compression modules.
 *
 *  The outer API which operates on the compression modules may be found here: \ref compression.
 */

#include <excom.h>
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#ifdef PERF_MON
#include "performance.hpp"
#endif
#include "handles.hpp"
#include "iomodule.hpp"

// declared in src/libexcom/chain.hpp
class CompressionChain;

//! \brief Abstract ancestor of all the compression modules
class CompModule {
protected:
	//! operation that the module will be performing
	int operation;
	//! unique identifier of the compression module
	unsigned int module_handle;
	//! pointer to a chain object that this compression module is part of
	CompressionChain *chain;

	friend class CompressionChain;
	friend int exc_connectModules(unsigned int, unsigned int, int);

#ifdef PERF_MON
	PERF_VARIABLES
#endif

public:
	//! result of the last run() call
	int result;

public:
	/*! \brief Constructor, registers given handle
	 *  \param handle Unique handle to this compression module
	 */
	CompModule(unsigned int handle) {
		hand_alloc.registerObject(handle, HANDLE_COMPRESSION, this);
		module_handle = handle;
		chain = NULL;
		operation = EXCOM_OP_COMPRESS;
		// this signalizes, that no operation has been performed, yet
		result = EXCOM_ERR_LATER;
#ifdef PERF_MON
		PERF_INIT
#endif
	}
	//! \brief Destructor, destroys the module and unregisters its handle
	virtual ~CompModule() {
		hand_alloc.freeHandle(module_handle);
	}

	/*! \brief Connects an I/O module to this compression module.
	 *  \param module Pointer to the I/O module to be connected
	 *  \param connection_type Type of the connection that is being made
	 *         between the two modules. See #exc_connection_e for a list
	 *         of common connection types, but note that each particular
	 *         compression module may define its own types.
	 *
	 *  This is the implementation of the #exc_connectModules API call
	 *  within a particular compression module. See the module \ref chain
	 *  to learn what is meant by module connection.
	 */
	virtual int connectIOModule(IOModule *module, int connection_type) = 0;

	/*! \brief Sets mode of operation of the module.
	 *  \param operation The operation that the module should perform
	 *  \return
	 *         - \c EXCOM_ERR_OK - success
	 *         - \c EXCOM_ERR_OPERATION - invalid operation for the given
	 *              compression methdo
	 */
	virtual int setOperation(enum exc_oper_e operation) = 0;

	/*! \brief Sets a parameter of the compression module
	 *  \param parameter Symbolic constant of the parameter to be set
	 *  \param value Pointer to the actual value of the parameter.
	 *
	 *  This is the implementation of the #exc_setParameter API call
	 *  within a particular compression module.
	 */
	virtual int setParameter(int parameter, void *value) {
		// no parameters for the general compression module
		return EXCOM_ERR_PARAM;
	}

	/*! \brief Requests a value from the compression module
	 *  \param parameter Symbolic constant of the parameter to be retrieved
	 *  \param value Pointer to the memory area to store the value into
	 *
	 *  This is the implementation of the #exc_getValue API call
	 *  within a particular compression module.
	 */
	virtual int getValue(int parameter, void *value) {
		switch (parameter) {
		case EXCOM_VALUE_COMP_RESULT:
			if (value == NULL) return EXCOM_ERR_MEMORY;
			*(int*)value = result;
			return EXCOM_ERR_OK;
		default:
			return EXCOM_ERR_PARAM;
		}
	}

	/*! \brief Checks whether the module has connections to all I/O modules that it needs
	 *  \return
	 *         - \c EXCOM_ERR_OK - the module has all necessary connections
	 *         - \c EXCOM_ERR_CHAIN - at least one of the necessary I/O modules is not connected
	 *
	 *  This method checks, whether the compression module has all its necessary I/O modules
	 *  connected. A particular compression module may have slots for several I/O modules, but can
	 *  be operational with only a subset of them actually connected. If any mandatory slot
	 *  is empty, this method will return \c EXCOM_ERR_CHAIN and a subsequent call to #CompModule::run
	 *  will fail, unless all the necessary slots are filled using the #CompModule::connectIOModule function.
	 *
	 *  Optionally, this method may perform other checks or further initialization.
	 *  This is most likely the last method to be called prior to run(), so the
	 *  module may want to allocate all the buffers that it needs and that it hasn't
	 *  allocated before. It's better to do allocation code here than in run(), because
	 *  throwing std::bad_alloc exception from run() may cause the entire application to
	 *  be terminated.
	 */
	virtual int checkConnection() = 0;

	/*! \brief Runs the operation
	 *
	 *  This method provides the operation (commonly compression or decompression)
	 *  determined by the \c operation member variable with the particular compression
	 *  method. The method doesn't return until the entire operation is complete.
	 *  In order to allow running multiple operations simultaneously, the
	 *  compression chain, that owns the compression module, will call this
	 *  method in a separate thread.
	 */
	virtual int run() = 0;

#ifdef PERF_MON
	PERF_ACCESSORS
#endif
};


#endif // ifndef __COMPMODULE_HPP__

