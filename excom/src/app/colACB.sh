#!/bin/sh

source 'common.sh'

#putline <corpus_file> <value> <tex_file> <tab_file> <orig_size>
putline() {
	t=`mintime <"$d/$2_1.txt"`
	xt=`separators $t`
	o=`wc -c "$d/$2" | grep -o '^[0-9][0-9]*'`
	rat=`echo "scale=7; $o / $5" | bc`
	xrat=`separators $rat`
	printf '%12s%16s%16s\\\\\n' "$2&" "$xt &" "$xrat" >>"$3"
	printf '%18s%16s%16s\n' "$2" "$t" "$rat" >>"$4"
}

# obtain sizes of the original corpus files to be able to compute ratio
size=`wc -c corpus/asyoulik.txt | grep -o '^[0-9][0-9]*'`

out=acb_ss_asyoulik.txt.txt
tex=acb_ss_asyoulik.txt.tex
# write header
echo '\hline' >$tex
printf '\$\\log_2 s\$ &%16s%16s\\\\\n' "Time[\$\\mu\$s]&" "Ratio&" >>$tex
echo '\hline' >>$tex
printf 'Search buf. size  %16s%16s\n' "Time[us]" "Ratio" >$out
echo -e "\n" >>$out

for ss in `seq 5 14`; do
	putline asyoulik.txt $ss "$tex" "$out" $size
done

echo '\hline' >>$tex


