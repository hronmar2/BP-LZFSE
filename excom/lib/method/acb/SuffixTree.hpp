#ifndef SUFFIXTREE_H_
#define SUFFIXTREE_H_

/*
 * File:       method/acb/SuffixTree.hpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "SuffixStructure.hpp"
#include "SuffixTreeNode.hpp"

#define ST_DEBUG 0
#if ST_DEBUG || SUFFIX_STRUCTURE_PERF
	#include <ctime>
	#include <iostream>
#endif

class SuffixTree : public SuffixStructure {
private:

        /*!
         * \brief Root of the Suffix Tree
         */
	SuffixTreeNode* root;

#if ST_DEBUG
	void printTree();
	void printTree(children_map_t* nodes, unsigned int depth, bool showSiblings);
	void printMethodStatistics();
#endif

	SuffixTreeNode* searchContext(long position);

public:

	SuffixTree();
	virtual ~SuffixTree();

	void search(long position, int &distance, unsigned int &contentLength);

        void translate(long position, int distance, unsigned int contentLength, unsigned char nextCharacter);

        void updateIndex(long position, long length);

	unsigned char* clear(long position, bool needData);
};

#endif /* SUFFIXTREE_H_ */
