#ifndef __CHAIN_HPP__
#define __CHAIN_HPP__

/*
 * File:       chain.hpp
 * Purpose:    Declaration of classes related to the compression chain
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file chain.hpp
 *  \brief Declaration of classes related to the \ref chain.
 */

#include <vector>
#include <pthread.h>

#include <excom.h>
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include "iomodule.hpp"
#include "compmodule.hpp"
#ifdef PERF_MON
#include "performance.hpp"
#endif

/*! \brief Class representing one compression chain
 *
 *  The class only needs to keep lists of compression and I/O modules that are involved,
 *  not the exact graph of their interconnection, because each compression module keeps
 *  track of its own connections.
 *
 *  The chain class needs to keep the list of related I/O and compression modules in order
 *  to destroy them when the entire chain is being destroyed. Moreover, a list of compression
 *  modules is necessary when the chain is being run, because the respective method needs
 *  to start all the compression modules at the same time (to prevent deadlocking of compression
 *  chains with more than one compression module).
 */
class CompressionChain {
protected:
	unsigned int chain_handle;  //!< unique identifier of the compression chain among currently existing modules
	std::vector<CompModule*> comp_modules;  //!< list of compression modules that belong to this chain
	std::vector<IOModule*> io_modules;      //!< list of I/O modules that belong to this chain

	friend int exc_connectModules(unsigned int, unsigned int, int);

	pthread_mutex_t mutex_wait; //!< mutex used for synchronizing concurrent wait and tryWait calls

public:
	int running;                //!< tells whether this chain is currently running
	pthread_t *thread;          //!< an array of threads that are running in this chain
	struct run_thread_info {
		int finished;       //!< flag signaling whether the thread has finished (0 it hasn't, 1 it has, 2 - it's already joined)
		CompModule *comp;   //!< pointer to the particular compression module that the thread is running
	} *thread_info;             //!< array of per-thread information that the thread's main function needs to know
	unsigned int thread_num;    //!< number of threads, that have been created

#ifdef PERF_MON
	PERF_VARIABLES
#endif

public:
	/*! \brief Constructor, registers the given handle
	 *  \param handle Unique handle to this newly created compression chain
	 */
	CompressionChain(unsigned int handle);
	/*! \brief Destructor, destroys the chain with all its members, unregisters the handle
	 *  
	 *  This method iterates through the \c comp_modules and \c io_modules vectors
	 *  and destroys all objects that they contain. Private \c chain_handle is unregistered
	 *  from the global handle pool.
	 */
	~CompressionChain();

	/*! \brief Adds a compression module to the compression chain
	 *  \param module Pointer to the module to be added
	 */
	void addCompModule(CompModule *module);
	/*! \brief Adds an I/O module to the compression chain
	 *  \param module Pointer to the module to be added
	 */
	void addIOModule(IOModule *module);

	/*! \brief Runs the compression chain
	 *
	 *  This is the implementation of the #exc_run API call on a particular
	 *  compression chain.
	 */
	int run();
	/*! \brief Waits for the compression chain to finish
	 *
	 *  This is the implementation of the #exc_wait API call on a particular
	 *  compression chain.
	 */
	int wait();
	/*! \brief Checks whether the compression chain has finished and returns immediately if it hasn't
	 *
	 *  This is the implementation of the #exc_tryWait API call on a particular
	 *  compression chain.
	 */
	int tryWait();

#ifdef PERF_MON
	PERF_ACCESSORS
#endif
};

#endif // ifndef __CHAIN_HPP__


