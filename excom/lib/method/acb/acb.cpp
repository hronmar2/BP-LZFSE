/*
 * File:       method/acb/acb.hpp
 * Purpose:    Declaration of ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009-2011 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */
#include "acb.hpp"

CompACB::CompACB(unsigned int handle) :
	CompModule(handle), reader(NULL), writer(NULL),
	wrctx(NULL), wrconf(NULL) {

	lengthBits          = ACB_LENGTH_BITS;
	distanceBits        = ACB_DISTANCE_BITS;
	searchSizeBits      = ACB_BUFFER_SIZE_BITS;
        checkSizeBits       = 0;    // 0 so it may be default set by setParameter
	compressRateCheck   = ACB_COMPRESS_RATE_CHECK;
        coder               = ACB_CODER_DEFAULT;

	search = new SuffixArray2();
//     search = new SuffixTree();
//      search = new SuffixArray();
}

CompACB::~CompACB() {
	if (search != NULL) {
		delete search;
		search = NULL;
	}
}

//FIXME: this method may be common to more compression methods,
// consider implementing it (as virtual) in CompModule
int CompACB::connectIOModule(IOModule *module, int connection_type) {
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	case EXCOM_CONN_ACB_CONTEXT:
		if (wrctx != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		wrctx = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	case EXCOM_CONN_ACB_CONFORMITY:
		if (wrconf != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		wrconf = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompACB::setOperation(enum exc_oper_e _operation) {
	if (_operation == EXCOM_OP_COMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else if (_operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompACB::setParameter(int parameter, void *value) {
	unsigned int u;
	switch (parameter) {
	case EXCOM_PARAM_ACB_DISTANCE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u < 1 || u > ACB_MAX_DISTANCE_BITS) return EXCOM_ERR_VALUE;
		distanceBits = u;
		break;
	case EXCOM_PARAM_ACB_LENGTH:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u < 1 || u > ACB_MAX_LENGTH_BITS) return EXCOM_ERR_VALUE;
		lengthBits = u;
		break;
	case EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u < 13 || u > ACB_MAX_BUFFER_SIZE_BITS) return EXCOM_ERR_VALUE;
		searchSizeBits = u;
                if(checkSizeBits == 0) (checkSizeBits = u - 2);
		break;
        case EXCOM_PARAM_ACB_CHECK_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);
		if (u < 12 || u > ACB_MAX_BUFFER_SIZE_BITS) return EXCOM_ERR_VALUE;
		checkSizeBits = u;
		break;
        case EXCOM_PARAM_ACB_CODING:
		if (value != NULL) return EXCOM_ERR_MEMORY;
		coder = ACB_CODER_HUFFMAN;
		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompACB::getValue(int parameter, void *value) {
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_ACB_DISTANCE:
		u = distanceBits;
		break;
	case EXCOM_VALUE_ACB_LENGTH:
		u = lengthBits;
		break;
        case EXCOM_VALUE_ACB_SEARCH_BUFFER_SIZE:
		u = searchSizeBits;
		break;
        case EXCOM_VALUE_ACB_CHECK_SIZE:
		u = checkSizeBits;
		break;
        case EXCOM_VALUE_ACB_CODING:
		u = coder;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

int CompACB::checkConnection() {
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

/* Encode or decode */
int CompACB::run() {
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif
        // reset
        currentEncodedSize = 0;
        lastCompressRatio = 1;
        compressRatioDecrease = 0;

        // start opertation
	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}

#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}

int CompACB::runCompress() {
        // set default setting is neccessary
        if(checkSizeBits == 0) checkSizeBits = ACB_CHECK_SIZE_BITS;
        // check for lower bound
        if(checkSizeBits < 12) checkSizeBits = 12;
        // write header
    	writer->writeNBits(4, (unsigned char) ACB_VERSION);
	writer->writeNBits(4, (unsigned char) distanceBits);
	writer->writeNBits(4, (unsigned char) lengthBits);
	writer->writeNBits(6, (unsigned char) searchSizeBits);
        writer->writeNBits(6, (unsigned char) checkSizeBits);
        writer->writeNBits(1, (unsigned char) coder);

        // -1 because codes cannot encode negative values, i have to move them into positive range
	long searchRange = (1 << distanceBits) - 1;
        long negativeValueAddition = (searchRange << 1) + 1;

        // set properties to dictionary
        search->setSize(searchSizeBits, checkSizeBits);
	search->setMaxDistance(searchRange);
        search->setMaxContentLength((1 << lengthBits) - 1);

	// Init
        unsigned int bufferSize = 1 << 16; // 64 kB
        unsigned char *buffer = new unsigned char[bufferSize];
	unsigned int sizeRead = reader->readBlock(buffer, bufferSize);
        search->addData(buffer, sizeRead);
        unsigned char character = 0;

        if(sizeRead > 0) {
            character = search->getData(0);
        }

        tripletcoder::Encoder* encoder;
        if(coder == ACB_CODER_ARITH) {
            encoder = new tripletcoder::ArithmeticEncoder(writer, lengthBits, distanceBits + 1, ACB_ALPHABET_SIZE_BITS);
            encoder->encodeCharacter(character);
        } else {
            // Need to initialize adaptive huffman tree with some
            // reasonable data so it wont waste bits on character
            // which will never occur in the stream. Length
            // can be initialized to zero and distance to 1
            encoder = new tripletcoder::HuffmanEncoder(writer, lengthBits, distanceBits + 1, ACB_ALPHABET_SIZE_BITS, character);
            writer->writeNBits(ACB_ALPHABET_SIZE_BITS, character);
        }

        currentEncodedSize++;
	
#if ACB_PRINT_INFO
        long triplets = 0;
	long bitWiseCase = 0;
	long notBitWiseCase = 0;
#endif	
	long i = 0;
	long position = 0;
        long dataLength = sizeRead;
	while (currentEncodedSize < dataLength) {
                int distance = 0;
                unsigned int contentLength = 0;
#if SC_ENABLED
                int zzzLength = 0;
		unsigned int isBitWise = search->search(position, distance, contentLength, zzzLength);
#else
                search->search(position, distance, contentLength);
#endif
                unsigned int nextContent = contentLength + 1;	
                currentEncodedSize += nextContent;

		if(currentEncodedSize >= dataLength) {
			sizeRead = reader->readBlock(buffer, bufferSize);
			if(sizeRead > 0) {
                                dataLength += sizeRead;
				search->addData(buffer, sizeRead);
                                currentEncodedSize -= nextContent;
				// redo last context and content search
				continue;
			}
		    	// Last element cannot be nothing, but it has to be
                        // last char in stream
			if(contentLength > 0 && currentEncodedSize > dataLength) {
				contentLength--;				
                                nextContent--;
                                currentEncodedSize--;
			}
		}
                long nextPosition = position + nextContent;

#if SC_ENABLED
		if(isBitWise) {
			// Search in sorted contents was used
#if ACB_PRINT_INFO
			bitWiseCase++;
#endif                        
                        // 1 means, current best match is lower than current text
			// 0 means, current best match is greater than current text
			unsigned int isLower = 1;
			if(zzzLength < 0) {
				isLower = 0;
				zzzLength = -zzzLength;
			}

                        encoder->encodeLength(zzzLength);
                        // distance is used as position in sorted contents
                        encoder->encodeDistance(distance);
                        unsigned char character = search->getData(nextPosition);
                        encoder->encodeCharacter(character);
                        encoder->encodeIsLower(isLower);
		} else {
#endif
			// Normal triplet or single character is encoded
#if ACB_PRINT_INFO
			notBitWiseCase++;
#endif
                        encoder->encodeLength(contentLength);

			// Write whole triplet if - content length > 0 or singles are not enabled
			if(contentLength > 0) {
#if ACB_PRINT_INFO
				triplets++;
#endif
                                int _distance = distance;
				if(_distance < 0) {
				    _distance += negativeValueAddition;
				}
                                encoder->encodeDistance(_distance);
                        }

                        unsigned char character = search->getData(nextPosition);
                        encoder->encodeCharacter(character);
#if SC_ENABLED
		}
#endif
                // Update index - encoded length + one new character
		search->updateIndex(position, nextContent);

		position = nextPosition;

		// If dictionary is full, monitor compression ratio. If it starts
		// degrading, clear dictionary
		if(search->isCheckCompressRatio()) {
			i += nextContent;
			bool clear = checkCompressRatio(i);
                        if(clear) {
                            search->clear(position, false);
                            position = 0;
                        }
		}

#ifdef PERF_MON
		if (wrctx != NULL) {
			unsigned int dif = distance;
			if (dif < 0) {
				dif = -dif;
			}
			wrctx->writeByte((unsigned char)(dif & 0xff));
			wrctx->writeByte((unsigned char)((dif>>8) & 0xff));
			wrctx->writeByte((unsigned char)((dif>>16) & 0xff));
			wrctx->writeByte((unsigned char)((dif>>24) & 0xff));
		}
		if (wrconf != NULL) {
			wrconf->writeByte((unsigned char)(contentLength & 0xff));
			wrconf->writeByte((unsigned char)((contentLength>>8) & 0xff));
			wrconf->writeByte((unsigned char)((contentLength>>16) & 0xff));
			wrconf->writeByte((unsigned char)((contentLength>>24) & 0xff));
		}
#endif
	}

        // Write EOF in length alphabet as it is the first one to read it
        // while decoding
        encoder->flush();
	writer->eof();

        delete [] buffer;
	buffer = NULL;
        delete encoder;
        encoder = NULL;

#ifdef PERF_MON
	if (wrctx != NULL) wrctx->eof();
	if (wrconf != NULL) wrconf->eof();
#endif
#if ACB_PRINT_INFO
	printf("  Compression ratio : %.3f \n", ((double) ((writer->bitsWritten() / 8.0) / currentEncodedSize))  * 100);
	printf("  BitWise/NonBitWise: %d / %d \n", (unsigned int) bitWiseCase, (unsigned int) notBitWiseCase);
	printf("  Singles/Triplets  : %d / %d \n", (unsigned int) (notBitWiseCase - triplets), (unsigned int) triplets);
#endif
	return EXCOM_ERR_OK;
}

int CompACB::runDecompress() {
	// read header
	unsigned int error = readHeader();
	if(error != EXCOM_ERR_OK) {
		return error;
	}
	// -1 because codes cannot encode negative values, i have to move them into positive range
	long searchRange = (1 << distanceBits) - 1;
        long negativeValueAddition = (searchRange << 1) + 1;
        
        search->setSize(searchSizeBits, checkSizeBits);
        search->setMaxDistance(searchRange);
        search->setMaxContentLength((1 << lengthBits) - 1);

        int character = 0;
        tripletcoder::Decoder* decoder;
        if(coder == ACB_CODER_ARITH) {
            decoder = new tripletcoder::ArithmeticDecoder(reader, lengthBits, distanceBits + 1, ACB_ALPHABET_SIZE_BITS);
            character = decoder->decodeCharacter();
        } else {
            // Need to initialize adaptive huffman tree with some
            // reasonable data so it wont waste bits on character
            // which will never occur in the stream. Length
            // can be initialized to zero and distance to 1
            unsigned long data = 0;
            reader->readNBits(ACB_ALPHABET_SIZE_BITS, &data);
            character = data;
            decoder = new tripletcoder::HuffmanDecoder(reader, lengthBits, distanceBits + 1, ACB_ALPHABET_SIZE_BITS, character);
        }
        
	search->addData(character);
        currentEncodedSize++;

	int distance;
	int contentLength;
        long i = 0;
        long position = 0;
	while (true) {
                // read length
                contentLength = decoder->decodeLength();
		// read new data
		if(contentLength > 0) {
                        distance = decoder->decodeDistance();
			if(distance > searchRange) {
				distance -= negativeValueAddition;
			}
		}
                character = decoder->decodeCharacter();

                if(decoder->isEof()) {
                    break;
                }
                
                // add new data
		search->translate(position, distance, contentLength, character);

		// move forward
                unsigned int nextContent = contentLength + 1;
		position += nextContent;
                currentEncodedSize += nextContent;

		if(search->isCheckCompressRatio()) {
			i += nextContent;
			bool clear = checkCompressRatio(i);                        
                        if(clear) {
                            unsigned char* dataToWrite = search->clear(position, true);
                            writer->writeBlock(dataToWrite, position);
                            delete[] dataToWrite;
                            position = 0;
                        }
		}
	}

        // write rest of the data
	unsigned char* data = search->getData();
	writer->writeBlock(data, search->getDataSize());
        delete [] data;
	data = NULL;

        delete decoder;
        decoder = NULL;
        
	writer->eof();

#if ACB_PRINT_INFO
	printf("  Compression ratio : %.3f \n", ((double) ((reader->bitsRead() / 8.0) / currentEncodedSize))  * 100);
#endif
	return EXCOM_ERR_OK;
}

unsigned int CompACB::readHeader() {
	unsigned long x = 0;
        int eof = reader->readNBits(4, &x);
	if (eof == EOF) return EXCOM_ERR_INCOMPLETE;
	if (x != ACB_VERSION) return EXCOM_ERR_FORMAT;
#if ACB_PRINT_INFO
	std::cout << "  ACB Version : " << (unsigned int) x << std::endl;
#endif
	eof = reader->readNBits(4, &x);
	if (eof == EOF) return EXCOM_ERR_INCOMPLETE;
	this->distanceBits = (unsigned int) x;
#if ACB_PRINT_INFO
	std::cout << "  Maximum distance : " << distanceBits << std::endl;
#endif
	eof = reader->readNBits(4, &x);
	if (eof == EOF) return EXCOM_ERR_INCOMPLETE;
	this->lengthBits = (unsigned int) x;
#if ACB_PRINT_INFO
	std::cout << "  Maximum length : " << lengthBits << std::endl;
#endif
	eof = reader->readNBits(6, &x);
	if (eof == EOF) return EXCOM_ERR_INCOMPLETE;
	this->searchSizeBits = (unsigned int) x;
#if ACB_PRINT_INFO
	std::cout << "  Search buffer size bits : " << searchSizeBits << std::endl;
#endif
        eof = reader->readNBits(6, &x);
	if (eof == EOF) return EXCOM_ERR_INCOMPLETE;
	this->checkSizeBits = (unsigned int) x;
#if ACB_PRINT_INFO
	std::cout << "  Check buffer size bits : " << checkSizeBits << std::endl;
#endif
        eof = reader->readNBits(1, &x);
	if (eof == EOF) return EXCOM_ERR_INCOMPLETE;
        coder = x;
#if ACB_PRINT_INFO
	std::cout << "  Using arithmetic coding : " << (coder == ACB_CODER_ARITH) << std::endl;
#endif
	return EXCOM_ERR_OK;
}

inline bool CompACB::checkCompressRatio(long &currentCheckValue) {
        bool out = false;
        if(currentCheckValue > compressRateCheck) {
		currentCheckValue = 0;
                unsigned long currentCompressedSizeBits = 0;
                if(operation == EXCOM_OP_COMPRESS) {
                    currentCompressedSizeBits = writer->bitsWritten();
                } else {
                    currentCompressedSizeBits = reader->bitsRead();
                }
                if(!currentCompressedSizeBits) {
                    // cannot determine current compressed size so
                    // pretend compress ration decreased
                    return true;
                }
		double currentCompressRatio = (double) ((currentCompressedSizeBits / 8.0) / currentEncodedSize);
		double dif = lastCompressRatio - currentCompressRatio;
		compressRatioDecrease += dif;
		// if compress ratio degrades by 0.1% clear dictionary
		if(compressRatioDecrease < -0.001) {
                    out = true;
			compressRatioDecrease = 0;
		} else if(compressRatioDecrease > 0) {
			compressRatioDecrease = 0;
		}
#if SUFFIX_STRUCTURE_PERF
                printf("Compression ratio [last:current] : %.3f : %.3f\n", lastCompressRatio, currentCompressRatio);
#endif
		lastCompressRatio = currentCompressRatio;
	}
        return out;
}
