#ifndef __PPMD_SUBALLOC_H__
#define __PPMD_SUBALLOC_H__

/*
 * File:       method/ppm/PPMDSubAlloc.h
 * Purpose:    Memory management for the PPM module
 *
 * Based on PPMD implementation from p7zip, which is based
 * on Dmitry Shkarin's PPMdH
 *
 * Copyright (C) 1999-2002 Dmitry Shkarin
 * Copyright (C) 1999-2007 Igor Pavlov
 * Copyright (C) 2009 Filip Simek <simekf1@fel.cvut.cz>
 *
 * This file is part of PPM module linked to the ExCom library.
 *
 * PPM module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PPM module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with PPM module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <string.h>
#include <stdlib.h>

const unsigned int N1 = 4, N2 = 4, N3 = 4, N4 = (128 + 3 - 1 * N1 - 2 * N2 - 3 * N3) / 4;
const unsigned int UNIT_SIZE = 12, N_INDEXES = N1 + N2 + N3 + N4;

// Extra 1 * UNIT_SIZE for NULL support
// Extra 2 * UNIT_SIZE for s0 in GlueFreeBlocks()
const uint32_t kExtraSize = (UNIT_SIZE * 3);
const uint32_t kMaxMemBlockSize = 0xFFFFFFFF - kExtraSize;

struct MEM_BLK {
	uint16_t Stamp, NU;
	uint32_t Next, Prev;
	void InsertAt(uint8_t *Base, uint32_t p) {
		Prev = p;
		MEM_BLK *pp = (MEM_BLK *)(Base + p);
		Next = pp->Next;
		pp->Next = ((MEM_BLK *)(Base + Next))->Prev = (uint32_t)((uint8_t *)this - Base);
	}
	void Remove(uint8_t *Base) {
		((MEM_BLK *)(Base + Prev))->Next = Next;
		((MEM_BLK *)(Base + Next))->Prev = Prev;
	}
};


class CSubAllocator
{
	uint32_t SubAllocatorSize;
	uint8_t Indx2Units[N_INDEXES], Units2Indx[128], GlueCount;
	uint32_t FreeList[N_INDEXES];

	uint8_t *Base;
	uint8_t *HeapStart, *LoUnit, *HiUnit;
public:
	uint8_t *pText, *UnitsStart;
	CSubAllocator():
			SubAllocatorSize(0),
			GlueCount(0),
			LoUnit(0),
			HiUnit(0),
			pText(0),
			UnitsStart(0) {
		memset(Indx2Units, 0, sizeof(Indx2Units));
		memset(FreeList, 0, sizeof(FreeList));
	}
	~CSubAllocator() {
		StopSubAllocator();
	};

	void *GetPtr(uint32_t offset) const {
		return (offset == 0) ? 0 : (void *)(Base + offset);
	}
	void *GetPtrNoCheck(uint32_t offset) const {
		return (void *)(Base + offset);
	}
	uint32_t GetOffset(void *ptr) const {
		return (ptr == 0) ? 0 : (uint32_t)((uint8_t *)ptr - Base);
	}
	uint32_t GetOffsetNoCheck(void *ptr) const {
		return (uint32_t)((uint8_t *)ptr - Base);
	}
	MEM_BLK *GetBlk(uint32_t offset) const {
		return (MEM_BLK *)(Base + offset);
	}
	uint32_t *GetNode(uint32_t offset) const {
		return (uint32_t *)(Base + offset);
	}

	void InsertNode(void* p, int indx) {
		*(uint32_t *)p = FreeList[indx];
		FreeList[indx] = GetOffsetNoCheck(p);
	}

	void* RemoveNode(int indx) {
		uint32_t offset = FreeList[indx];
		uint32_t *p = GetNode(offset);
		FreeList[indx] = *p;
		return (void *)p;
	}

	unsigned int U2B(int NU) const {
		return (unsigned int)(NU) * UNIT_SIZE;
	}

	void SplitBlock(void* pv, int oldIndx, int newIndx) {
		int i, UDiff = Indx2Units[oldIndx] - Indx2Units[newIndx];
		uint8_t* p = ((uint8_t*)pv) + U2B(Indx2Units[newIndx]);
		if (Indx2Units[i = Units2Indx[UDiff-1]] != UDiff) {
			InsertNode(p, --i);
			p += U2B(i = Indx2Units[i]);
			UDiff -= i;
		}
		InsertNode(p, Units2Indx[UDiff - 1]);
	}

	uint32_t GetUsedMemory() const {
		uint32_t RetVal = SubAllocatorSize - (uint32_t)(HiUnit - LoUnit) - (uint32_t)(UnitsStart - pText);
		for (uint32_t i = 0; i < N_INDEXES; i++)
			for (uint32_t pn = FreeList[i]; pn != 0; RetVal -= (uint32_t)Indx2Units[i] * UNIT_SIZE)
				pn = *GetNode(pn);
		return (RetVal >> 2);
	}

	uint32_t GetSubAllocatorSize() const {
		return SubAllocatorSize;
	}

	void StopSubAllocator() {
		if (SubAllocatorSize != 0) {
			free(Base);
			SubAllocatorSize = 0;
			Base = 0;
		}
	}

	bool StartSubAllocator(uint32_t size) {
		if (SubAllocatorSize == size)
			return true;
		StopSubAllocator();
		if (size == 0)
			Base = 0;
		else {
			if ((Base = (uint8_t *)malloc(size + kExtraSize)) == 0)
				return false;
			HeapStart = Base + UNIT_SIZE; // we need such code to support NULL;
		}
		SubAllocatorSize = size;
		return true;
	}

	void InitSubAllocator() {
		unsigned int i, k;
		memset(FreeList, 0, sizeof(FreeList));
		HiUnit = (pText = HeapStart) + SubAllocatorSize;
		unsigned int Diff = UNIT_SIZE * (SubAllocatorSize / 8 / UNIT_SIZE * 7);
		LoUnit = UnitsStart = HiUnit - Diff;
		for (i = 0, k = 1; i < N1 ; i++, k += 1)        Indx2Units[i] = (uint8_t)k;
		for (k++; i < N1 + N2      ;i++, k += 2)      Indx2Units[i] = (uint8_t)k;
		for (k++; i < N1 + N2 + N3   ;i++, k += 3)     Indx2Units[i] = (uint8_t)k;
		for (k++; i < N1 + N2 + N3 + N4; i++, k += 4) Indx2Units[i] = (uint8_t)k;
		GlueCount = 0;
		for (k = i = 0; k < 128; k++) {
			i += (Indx2Units[i] < k + 1);
			Units2Indx[k] = (uint8_t)i;
		}
	}

	void GlueFreeBlocks() {
		uint32_t s0 = (uint32_t)(HeapStart + SubAllocatorSize - Base);

		// We need add exta MEM_BLK with Stamp=0
		GetBlk(s0)->Stamp = 0;
		s0 += UNIT_SIZE;
		MEM_BLK *ps0 = GetBlk(s0);

		uint32_t p;
		unsigned int i;
		if (LoUnit != HiUnit)
			*LoUnit = 0;
		ps0->Next = ps0->Prev = s0;

		for (i = 0; i < N_INDEXES; i++)
			while (FreeList[i] != 0) {
				MEM_BLK *pp = (MEM_BLK *)RemoveNode(i);
				pp->InsertAt(Base, s0);
				pp->Stamp = 0xFFFF;
				pp->NU = Indx2Units[i];
			}
		for (p = ps0->Next; p != s0; p = GetBlk(p)->Next) {
			for (;;) {
				MEM_BLK *pp = GetBlk(p);
				MEM_BLK *pp1 = GetBlk(p + pp->NU * UNIT_SIZE);
				if (pp1->Stamp != 0xFFFF || int(pp->NU) + pp1->NU >= 0x10000)
					break;
				pp1->Remove(Base);
				pp->NU = (uint16_t)(pp->NU + pp1->NU);
			}
		}
		while ((p = ps0->Next) != s0) {
			MEM_BLK *pp = GetBlk(p);
			pp->Remove(Base);
			int sz;
			for (sz = pp->NU; sz > 128; sz -= 128, p += 128 * UNIT_SIZE)
				InsertNode(Base + p, N_INDEXES - 1);
			if (Indx2Units[i = Units2Indx[sz-1]] != sz) {
				int k = sz - Indx2Units[--i];
				InsertNode(Base + p + (sz - k) * UNIT_SIZE, k - 1);
			}
			InsertNode(Base + p, i);
		}
	}
	void* AllocUnitsRare(int indx) {
		if (!GlueCount) {
			GlueCount = 255;
			GlueFreeBlocks();
			if (FreeList[indx] != 0)
				return RemoveNode(indx);
		}
		unsigned int i = indx;
		do {
			if (++i == N_INDEXES) {
				GlueCount--;
				i = U2B(Indx2Units[indx]);
				return (UnitsStart - pText > i) ? (UnitsStart -= i) : (NULL);
			}
		} while (FreeList[i] == 0);
		void* RetVal = RemoveNode(i);
		SplitBlock(RetVal, i, indx);
		return RetVal;
	}

	void* AllocUnits(int NU) {
		int indx = Units2Indx[NU - 1];
		if (FreeList[indx] != 0)
			return RemoveNode(indx);
		void* RetVal = LoUnit;
		LoUnit += U2B(Indx2Units[indx]);
		if (LoUnit <= HiUnit)
			return RetVal;
		LoUnit -= U2B(Indx2Units[indx]);
		return AllocUnitsRare(indx);
	}

	void* AllocContext() {
		if (HiUnit != LoUnit)
			return (HiUnit -= UNIT_SIZE);
		if (FreeList[0] != 0)
			return RemoveNode(0);
		return AllocUnitsRare(0);
	}

	void* ExpandUnits(void* oldPtr, int oldNU) {
		int i0 = Units2Indx[oldNU - 1], i1 = Units2Indx[oldNU - 1 + 1];
		if (i0 == i1)
			return oldPtr;
		void* ptr = AllocUnits(oldNU + 1);
		if (ptr) {
			memcpy(ptr, oldPtr, U2B(oldNU));
			InsertNode(oldPtr, i0);
		}
		return ptr;
	}

	void* ShrinkUnits(void* oldPtr, int oldNU, int newNU) {
		int i0 = Units2Indx[oldNU - 1], i1 = Units2Indx[newNU - 1];
		if (i0 == i1)
			return oldPtr;
		if (FreeList[i1] != 0) {
			void* ptr = RemoveNode(i1);
			memcpy(ptr, oldPtr, U2B(newNU));
			InsertNode(oldPtr, i0);
			return ptr;
		} else {
			SplitBlock(oldPtr, i0, i1);
			return oldPtr;
		}
	}

	void FreeUnits(void* ptr, int oldNU) {
		InsertNode(ptr, Units2Indx[oldNU - 1]);
	}
};

#endif // ifndef __PPMD_SUBALLOC_H__
