#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/method/acb/acb.hpp"

#define HANDLE 1
#define HANDLE_TMP 4
#define HANDLE_IO_IN 2
#define HANDLE_IO_OUT 3

// TODO update
class CompACBTestSuite: public CxxTest::TestSuite {
	CompACB *comp;
public:
	void setUp() {
		comp = new CompACB(HANDLE);
	}
	void tearDown() {
		delete comp;
	}

	void testParameters() {
		unsigned int v;
		TS_ASSERT_EQUALS(comp->setParameter(-1, NULL), EXCOM_ERR_PARAM);
		// FIXME: these two parameters should be removed and replaced by another parameter
		// which will be the size of the internal buffer (and will have nothing to do with
		// format of the output stream)
                TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_DISTANCE, NULL), EXCOM_ERR_MEMORY);
                TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_LENGTH, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE, NULL), EXCOM_ERR_MEMORY);

                v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_DISTANCE, &v), EXCOM_ERR_VALUE);
                v = 13;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_DISTANCE, &v), EXCOM_ERR_VALUE);
                v = 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_DISTANCE, &v), EXCOM_ERR_OK);
                v = 12;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_DISTANCE, &v), EXCOM_ERR_OK);
                v = 5;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_DISTANCE, &v), EXCOM_ERR_OK);
                v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_LENGTH, &v), EXCOM_ERR_VALUE);
                v = 13;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_LENGTH, &v), EXCOM_ERR_VALUE);
                v = 1;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_LENGTH, &v), EXCOM_ERR_OK);
                v = 12;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_LENGTH, &v), EXCOM_ERR_OK);
                v = 7;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_LENGTH, &v), EXCOM_ERR_OK);
		v = 0;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE, &v), EXCOM_ERR_VALUE);
                v = 64;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE, &v), EXCOM_ERR_VALUE);
                v = 20;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE, &v), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(comp->getValue(-1, NULL), EXCOM_ERR_PARAM);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ACB_DISTANCE, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ACB_LENGTH, NULL), EXCOM_ERR_MEMORY);
                TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ACB_SEARCH_BUFFER_SIZE, NULL), EXCOM_ERR_MEMORY);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ACB_SEARCH_BUFFER_SIZE, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 20u);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ACB_DISTANCE, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 5u);
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_ACB_LENGTH, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, 7u);

		// a value common to all compression modules
		int u;
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_COMP_RESULT, &u), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(u, EXCOM_ERR_LATER);
	}

	void testIOInputModules() {
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		// invalid pointer to a module
		TS_ASSERT_EQUALS(comp->connectIOModule(NULL, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_MEMORY);
		// invalid connection type
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, -1), EXCOM_ERR_PARAM);
		// input module connected as an output module
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		delete reader;
	}

	void testIOOutputModules() {
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		// output module connected as an input module
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_DUPLICATE);
		delete writer;
	}

	void testCompression() {
		// original input
		unsigned char A[16] = {'a','l','f',' ','e','a','t','s',' ','a','l','f','a','l','f','a'};
		// compressed output
		unsigned char B[32]; //20
		// this is how the compressed output should look like
		unsigned char C[24] = {
                    0x15, 0x75, 0x12, 0x30, 0x4F, 0xE9, 0xE5, 0xA7,
                    0xCC, 0x65, 0x15, 0x66, 0x2B, 0x57, 0xED, 0x39,
                    0x73, 0x0B, 0x04, 0x97, 0x99, 0x06, 0xEB, 0xE0
                };
		unsigned int v;

		// input and output modules
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(A, sizeof(A)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(B, sizeof(B)), EXCOM_ERR_OK);

		// running shouldn't succeed without an input I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// ...or without output I/O module
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// set parameters
		v = 20;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_SEARCH_BUFFER_SIZE, &v), EXCOM_ERR_OK);
		v = 5;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_DISTANCE, &v), EXCOM_ERR_OK);
		v = 7;
		TS_ASSERT_EQUALS(comp->setParameter(EXCOM_PARAM_ACB_LENGTH, &v), EXCOM_ERR_OK);
		// run the compression
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether output data matches our specimen
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &v), EXCOM_ERR_OK);
                // as it is written by bit it may not be at full byte in writer
		TS_ASSERT_EQUALS(v + 1, sizeof(C));
		TS_ASSERT_SAME_DATA(B, C, sizeof(C));

		delete reader;
		delete writer;
	}

	void testDecompression() {
		// original input
		unsigned char A[16] = {'a','l','f',' ','e','a','t','s',' ','a','l','f','a','l','f','a'};
		// this is how the compressed output should look like
		unsigned char C[24] = {
                    0x15, 0x75, 0x12, 0x30, 0x4F, 0xE9, 0xE5, 0xA7,
                    0xCC, 0x65, 0x15, 0x66, 0x2B, 0x57, 0xED, 0x39,
                    0x73, 0x0B, 0x04, 0x97, 0x99, 0x06, 0xEB, 0xE0
                };
		// decompressed output
		unsigned char D[32];
		unsigned int v;

		// input and output modules
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(C, sizeof(C)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(D, sizeof(D)), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// parameters are set by input
		TS_ASSERT_EQUALS(comp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		// run the decompression
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether output data matches our specimen
		TS_ASSERT_EQUALS(writer->getValue(EXCOM_VALUE_IO_POSITION, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, sizeof(A));
		TS_ASSERT_SAME_DATA(A, D, sizeof(A));

		delete reader;
		delete writer;
	}
};


