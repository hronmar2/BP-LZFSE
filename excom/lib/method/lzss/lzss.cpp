/*
 * File:       method/lzss/lzss.hpp
 * Purpose:    Declaration of the LZSS compression method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZSS coding module linked to the ExCom library.
 *
 * LZSS coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZSS coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZSS module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzss.hpp
 *  \brief Implementation of LZSS coding method.
 */

#include "lzss.hpp"
#include "../lz_common/LookaheadBuffer.h"
#include "../lz_common/SearchBuffer.h"
#include "../universal_codes/UniversalCodes.h"

#include <iostream>
#include <utility>
#include <cstdlib>
#include <map>
#include <list>
#include <deque>

using namespace std;

int CompLZSS::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZSS::setParameter(int parameter, void *value)
{
	unsigned int u;
	int bits = 0;
	switch (parameter) {
	case EXCOM_PARAM_LZSS_SEARCH_BUFFER_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);

		if (u < 1 || u > 65536) return EXCOM_ERR_VALUE;
		search_buffer_size = u;
		while ((u >>= 1) > 0) bits++;
		search_buffer_bits = bits;

		break;
	case EXCOM_PARAM_LZSS_LOOKAHEAD_BUFFER_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);

		if (u < 1 || u > 65536) return EXCOM_ERR_VALUE;
		lookahead_buffer_size = u;
		while ((u >>= 1) > 0) bits++;
		lookahead_buffer_bits = bits;

		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZSS::getValue(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_LZSS_SEARCH_BUFFER_SIZE:
		u = search_buffer_size;
		break;
	case EXCOM_VALUE_LZSS_LOOKAHEAD_BUFFER_SIZE:
		u = lookahead_buffer_size;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

int CompLZSS::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompLZSS::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompLZSS::runCompress() {
	unsigned char symbol;
	unsigned int index;
	unsigned int i;
	unsigned int sBufferIndex;
	unsigned int sBufferLength;
	unsigned int bestPosition;
	unsigned int bestLength;
	unsigned int bestLengthTmp;
	unsigned int maxLength;

	// Size of how many bytes was read from the file
	unsigned int size; 
	unsigned int bitsWritten;
	unsigned int minLength;

	// From the search buffer to the lookahead buffer
	bool boundsCrossed; 

	UniversalCodes* uc = new UniversalCodes();

	unsigned char* byteBuffer = new unsigned char[lookahead_buffer_size];
	deque<unsigned char> lBufferBytes;

	LookaheadBuffer* lookaheadBuffer = new LookaheadBuffer(lookahead_buffer_size);
	SearchBuffer* searchBuffer = new SearchBuffer(search_buffer_size);

	map <unsigned char, deque<unsigned int> > sBufferIndices;
	deque<unsigned int> indices;

	minLength = 3;

	// Fill some data first
	size = reader->readBlock(byteBuffer, lookahead_buffer_size);
	lBufferBytes.insert(lBufferBytes.end(), byteBuffer, byteBuffer+size);
	lookaheadBuffer->fill(lBufferBytes);

	maxLength = 15 + minLength; 
	bitsWritten = 0;
	sBufferLength = searchBuffer->getLength();
	buffer_bounds_t sBufferBounds;

	// Repeat until we have some data to encode
	while ((!lBufferBytes.empty()) or (lookaheadBuffer->getSize() > 0)) {
		indices = sBufferIndices[lookaheadBuffer->getByte(0)];
		sBufferBounds = searchBuffer->getBounds(); 	  

		bestLength = 0;
		bestPosition = 0;

		// Process all indices to find the longest match
		while (!indices.empty() && bestLength < maxLength) {
			sBufferIndex = indices.back();
			i = sBufferIndex;	
			bestLengthTmp = index = 0;	
			boundsCrossed = false;	

			symbol = searchBuffer->getByte(i);

			// Find the longest match
			while ((bestLengthTmp < maxLength) && (lookaheadBuffer->getByte(index) == symbol)) {
				if (boundsCrossed == false) {
					if (i == sBufferBounds.begin) {
						i = 0;
						boundsCrossed = true;
					} else {
						i = (i + 1) % search_buffer_size;
					}
				} else {
					i++;
				}

				symbol = (boundsCrossed == false) ? (searchBuffer->getByte(i)) : (lookaheadBuffer->getByte(i));
				bestLengthTmp++;	
				index++;

				if (index == lookaheadBuffer->getSize()) break; 	
			}	 

			if (bestLengthTmp > bestLength) {
				bestLength = bestLengthTmp;
				bestPosition = sBufferIndex;
			}	

			indices.pop_back(); 
		}

		// Map function, transform the real position in buffer to the one which should be output  
		bestPosition = ((sBufferLength + sBufferBounds.begin - bestPosition) % sBufferLength) + 1;

		if (bestLength < minLength) {
			writer->writeNBits(1, LITERAL_BIT);
			writer->writeNBits(8, lookaheadBuffer->getByte(0));

			searchBuffer->shiftData(lookaheadBuffer, sBufferIndices, 1);

			bitsWritten = (bitsWritten + 9) % 8;
		} else {
			writer->writeNBits(1, TOKEN_BIT);
			writer->writeNBits(search_buffer_bits, bestPosition);
			writer->writeNBits(lookahead_buffer_bits, bestLength-minLength);

			searchBuffer->shiftData(lookaheadBuffer, sBufferIndices, bestLength);

			bitsWritten = (bitsWritten + 17) % 8;
		}

		// Check if there are still more data in the input stream
		if (size == lookahead_buffer_size) {
			size = reader->readBlock(byteBuffer, lookahead_buffer_size);
			lBufferBytes.insert(lBufferBytes.end(), byteBuffer, byteBuffer+size);
		}

		// And read more data to a Lookahead buffer
		lookaheadBuffer->fill(lBufferBytes);
	}

	// Allign to bytes
	writer->writeNBits((8-bitsWritten) % 8, 0);
	writer->eof();

	// Clean up the data
	delete [] byteBuffer;
	//searchBuffer->~SearchBuffer();

	delete searchBuffer;
	searchBuffer = NULL;

	delete lookaheadBuffer;
	lookaheadBuffer = NULL;

	delete uc;
	uc = NULL;

	return EXCOM_ERR_OK;
}

int CompLZSS::runDecompress() {
	unsigned int minLength = 3;
	int x;

	unsigned int i; 
	unsigned int sBufferIndex;
	unsigned int symbolCount; 
	unsigned int index;
	unsigned int tmpPosition;

	unsigned long bitIndicator;
	unsigned long position;
	unsigned long length;
	unsigned long symbol;

	unsigned char* sBuffer = new unsigned char[search_buffer_size];

	list<unsigned char> bytesBuffer;

	sBufferIndex = 0;
	symbolCount = 0;
	do {
		x = reader->readNBits(1, &bitIndicator);
		if (x == EOF) break; // no more token pairs
		if (bitIndicator == 1) {
			x = reader->readNBits(8, &symbol);

			sBuffer[sBufferIndex] = symbol;
			sBufferIndex = (sBufferIndex + 1) % search_buffer_size;
			writer->writeByte(symbol);

			if (x == EOF) break;
	
			symbolCount = symbolCount+1;
		} else {
			x = reader->readNBits(search_buffer_bits, &position);
			if (x == EOF) {
				writer->eof();

				// Clean up the data
				delete [] sBuffer;
				sBuffer = NULL;

				return EXCOM_ERR_OK;
			}

			x = reader->readNBits(lookahead_buffer_bits, &length);

			length += minLength;

			tmpPosition = position;

			for (i = 0; i < length; i++) {
				index = (symbolCount - tmpPosition) % search_buffer_size;
				bytesBuffer.push_back(sBuffer[index]);
				tmpPosition = (tmpPosition == 1) ? position : (tmpPosition-1); 
			}

			for (i = 0; i < length; i++) {
				sBuffer[sBufferIndex] = bytesBuffer.front();
				sBufferIndex = (sBufferIndex + 1) % search_buffer_size;
				writer->writeByte(bytesBuffer.front());
				bytesBuffer.pop_front();
			}

			if (x == EOF) break;
			
			symbolCount = symbolCount+length;
		}

	} while (true);

	writer->eof();

	// Clean up the data
	delete [] sBuffer;
	sBuffer = NULL;

	return EXCOM_ERR_OK;
}


int CompLZSS::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}


#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}


