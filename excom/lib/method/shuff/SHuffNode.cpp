/*
 * File:       method/shuff/SHuffNode.cpp
 * Purpose:    Declaration of the Static Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Static Huffman coding module linked to the ExCom library.
 *
 * Static Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Static Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Static Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file SHuffNode.cpp
 *  \brief Concrete implementation of Huffman node representation.
 *
 *  This class serves for Huffman node representation and manipulation
 */

#include "SHuffNode.h"

// Constructors _begin_
SHuffNode::SHuffNode() {
	leftChild = rightChild = parent = NULL;
	freq = (unsigned long) 0;
	symbol = -1;
}

SHuffNode::SHuffNode(signed int s, unsigned long f) {
	leftChild = rightChild = parent = NULL;
	symbol = s;
	freq = f;
}

SHuffNode::SHuffNode(SHuffNode* node0, SHuffNode* node1) {
	symbol = -1;
	freq = node0->getFreq() + node1->getFreq();
	leftChild = node0;
	rightChild = node1;
}
// Constructors _end_

// Destrcutor
SHuffNode::~SHuffNode() {
}

// Getters _begin_
signed int SHuffNode::getSymbol() {
	return symbol;
}

unsigned int SHuffNode::getCode() {
	return code;
}

unsigned int SHuffNode::getLength() {
	return length;
}

unsigned long SHuffNode::getFreq() {
	return freq;
}

SHuffNode* SHuffNode::getLeftChild() {
	return leftChild;
}

SHuffNode* SHuffNode::getRightChild() {
	return rightChild;
}

SHuffNode* SHuffNode::getParent() {
	return parent;
}
// Getters _end_

// Setters _begin_
void SHuffNode::setCode(unsigned int c) {
	code = c;
}

void SHuffNode::setLength(unsigned int l) {
	length = l;
}

void SHuffNode::setSymbol(signed int s) {
	symbol = s;
}

void SHuffNode::setFreq(unsigned long f) {
	freq = f;
}

void SHuffNode::setLeftChild(SHuffNode* lc) {
	leftChild = lc;
}

void SHuffNode::setRightChild(SHuffNode* rc) {
	rightChild = rc;
}

void SHuffNode::setParent(SHuffNode* p) {
	parent = p;
}
// Setters _end_
