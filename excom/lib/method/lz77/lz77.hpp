#ifndef __METHOD_LZ77_HPP__
#define __METHOD_LZ77_HPP__

/*
 * File:       method/lz77/lz77.hpp
 * Purpose:    Declaration of the LZ77 compression method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZ77 coding module linked to the ExCom library.
 *
 * LZ77 coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZ77 coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZ77 module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lz77.hpp
 *  \brief Declaration of LZ77 coding method.
 *
 *  This module provides dictionary-based LZ77 compression method, which uses 
 *	the principle of a sliding window consisting of the search buferr and 
 *	the lookahead buffer. 
 */

#include <excom.h>
#include "../../compmodule.hpp"

#define DEFAULT_SEARCH_BUFFER_SIZE 4096
#define DEFAULT_SEARCH_BUFFER_BITS 12

#define DEFAULT_LOOKAHEAD_BUFFER_SIZE 16
#define DEFAULT_LOOKAHEAD_BUFFER_BITS 4

class CompLZ77: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;

	int runCompress();
	int runDecompress();

	unsigned int search_buffer_size;
	unsigned int search_buffer_bits;

	unsigned int lookahead_buffer_size;
	unsigned int lookahead_buffer_bits;
public:
	CompLZ77(unsigned int handle): CompModule(handle), reader(NULL), writer(NULL),\
		search_buffer_size(DEFAULT_SEARCH_BUFFER_SIZE), search_buffer_bits(DEFAULT_SEARCH_BUFFER_BITS),\
		lookahead_buffer_size(DEFAULT_LOOKAHEAD_BUFFER_SIZE), lookahead_buffer_bits(DEFAULT_LOOKAHEAD_BUFFER_BITS) {}
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();
};

#endif //ifndef __METHOD_LZ77_HPP__
