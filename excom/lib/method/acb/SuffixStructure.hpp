#ifndef _SUFFIXSTRUCTURE_HPP
#define	_SUFFIXSTRUCTURE_HPP

/*
 * File:       method/acb/SuffixStructure.hpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#define SUFFIX_STRUCTURE_PERF 0
#if SUFFIX_STRUCTURE_PERF
	#include <ctime>
	#include <iostream>
	#include <stdio.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#define ALPHABET_SIZE 256

/*!
 * \brief Defines how many cells are used in addition to normal buffer size.
 *        It is used so comparison can stop on BUFFER_SENTINEL_VALUE
 *        instead of checking for buffer bounds.
 */
#define STRUCTURE_SIZE_EXTENSION 3

/*!
 * \brief Sentinel value in data - character which is nowhere
 *        else except begin and end of data so comparison
 *        will stop on these characters. In other
 *        words it replaces bound checking.
 */
#define BUFFER_SENTINEL_VALUE -1

/*!
 * \brief Defines how many bytes one character in buffer occupies.
 */
#define BUFFER_CELL_SIZE 2

#define IS_FULL currentSize > maxSize

class SuffixStructure {
protected:
#if SUFFIX_STRUCTURE_PERF
	clock_t binarySearchTime;
	clock_t addDataTime;
	clock_t addDataNewCharacterTime;
	clock_t getDataTime;
	clock_t getDataPositionTime;
	clock_t getDataSizeTime;
	clock_t searchContextTime;
	clock_t searchContentTime;
	clock_t translateTime;
	clock_t updateIndexTime;
	
	unsigned int binarySearchCount;
	unsigned int addDataCount;
	unsigned int addDataNewCharacterCount;
	unsigned int getDataCount;
	unsigned int getDataPositionCount;
	unsigned int getDataSizeCount;
	unsigned int searchContextCount;
	unsigned int searchContentCount;
	unsigned int translateCount;
	unsigned int updateIndexCount;
#endif

	/*!
	 * \brief Pointer to the whole text this index is for
         *
         * It is \c uint16_t and not \c char so I can set characters before start
         * of the data and after end of the data to character which cannot occur
         *  in the data stream thus creating data hard stops and removing the
         * neccessarity to check, if I am comparing characters before or after
         * the allocated memory
	 */
	uint16_t* data;

	/*!
	* \brief Size of data - allocated data size
	*/
	unsigned long bufferSize;

	/*!
	* \brief Size of data - real data in buffer
	*/
	unsigned long dataSize;

        /*!
	* \brief Size of ddictionary - current dictionary size
	*/
        unsigned long currentSize;

        /*!
         * \brief Maximum size of dictionary
         */
	unsigned long maxSize;

        /*!
         * \brief Size of dictionary after wich compression ratio is checked
         */
        unsigned long checkRatioSize;

        /*!
         * \brief Maximum content length to compare
         */
	unsigned int maxContentLength;

        /*!
         * \brief Maximum distance of content from best context
         */
        unsigned int maxDistance;

        /*!
         * \brief Adds \c data which start at position \c content in current \c data
         *  to current \c data
         * \param content - Data to be added
         * \param size - Size of \c data to add
         */
	void addData(unsigned long content, unsigned long size);

        /*!
         * \brief Checks if current buffer for \c data is big enough to store
         *        data of \c currentSize + \c size. If not it allocates new buffer of
         *        sufficient size.
         * \param size - Size of new data to be stored
         * \return Pointer to old \c data buffer
         */
	void assertBufferSize(unsigned long size);

        /*!
         * \brief Prints method statistics
         */
	void printMethodStatistics();

public:

	SuffixStructure();
	virtual ~SuffixStructure();

        /*!
         * \brief Add one new \c newCharacter to current \c data
         * \param newCharacter - Character to be added
         */
	void addData(unsigned char character);

        /*!
         * \brief Adds \c data to current \c data
         * \param data - Data to be added
         * \param size - Size of \c data to add
         */
	void addData(unsigned char* data, unsigned long size);

        /*!
         * \brief Returns copy of currently encoded/decoded data.
         * \return Copy of currently encoded/decoded data.
         *
         * This method is returning copy of the data array, because it is stored
         * int \c short \c array and not \c char \c array.
         */
	unsigned char* getData();

        /*!
         * \brief Returns character at specified \c position.
         * \param position - Position of character.
         * \return Character at specified \c position.
         */
	unsigned char getData(long position);

        /*!
         * \brief Returns the size of \c data.
         * \return The size of \c data.
         */
	unsigned long getDataSize();

        /*!
         * \brief Sets size of data structure
         * \param sizeInBits - maximum size of structure. Actual size is 2 ^ sizeInBits
         * \param checkSizeInBits - size after which compression rate is checked. Actual size is 2 ^ checkSizeBits
         */
	virtual void setSize(unsigned int sizeInBits, unsigned int checkSizeBits);

        /*!
         * \brief Sets maximum content length
         * \param length - maximum length of content
         */
	virtual void setMaxContentLength(unsigned int length);

        /*!
         * \brief Sets maximum distance between context and content
         * \param distance - maximum distance between context and content
         */
        virtual void setMaxDistance(unsigned int distance);

        /*!
         * \brief Searches for best context and content and its length for current
         *        position in text.
         * \param position - Position in data i am searching at
         * \param distance - Distance between context and content (content - context)
         * \param contentLength - Common length ov best content and current content
         */
	virtual void search(long position, int &distance, unsigned int &contentLength) = 0;

        /*!
         * \brief Searches for best context and content and its length for current
         *        position in text.
         *        If context length is longer or equal to maximum value
         *        content is search in sorted contents. Distance is position in these contents,
         *        contentLength is length of content and zzzLength is length of common content
         *        decreased by common length of two nearest matching contents to the new one.
         * \param position - Position in data i am searching at
         * \param distance - Distance between context and content (content - context)
         * \param contentLength - Common length ov best content and current content
         * \param zzzLength - Length of common bytes with best match. If it is negative, best match is lower index, otherwise upper index
         * \return true if search was made in sorted contents, false means normal \c search
         *         was done
         */
	virtual unsigned int search(long position, int &distance, unsigned int &contentLength, int &zzzLength);

        /*!
         * \brief Decodes data read from encoded input and inserts new characters
         *        to decoded data and also updates dictionary.
         * \param position - current position in encoded/decoded data
         * \param distance - Distance between context and content (content - context)
         * \param contentLength - Common length ov best content and current content
         * \param nextCharacter - one more character read from input
         */
	virtual void translate(long position, int distance, unsigned int contentLength, unsigned char nextCharacter) = 0;

        /*!
         * \brief Updates index from \c position and inserts \c length contexts
         * \param position from which to update index
         * \param length of data to update
         */
        virtual void updateIndex(long position, long length) = 0;

        /*!
         * \brief Returns true if data structure is filled.
         *
         * \return true if structure is filled
         */
	bool isFull();

        /*!
         * \brief Returns true if data structure is filled past size where compression
         *        rate can be checked.
         * \return true if structure is filled
         */
        bool isCheckCompressRatio();

        /*!
         * \brief Clears dictionary structure and returns encoded/decoded data until
         *        \c position if needed
         * \param position - position to which clear data
         * \param needData - if true returns deleted data
         * \return Data which were encoded/decoded and were deleted by this clear.
         *         Data are returned only if \c needData is true.
         *         Memory management of returned \c Data is responsibility of the caller
         */
	virtual unsigned char* clear(long position, bool needData) = 0;
};

#endif	/* _SUFFIXSTRUCTURE_HPP */

