/*
 * File:       method/sfano/ShannonFanoNode.cpp
 * Purpose:    Declaration of Shannon-Fano statistical compression method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Shannon-Fano module linked to the ExCom library.
 *
 * Shannon-Fano module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Shannon-Fano module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Shannon-Fano module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file ShannonFanoNode.cpp
 *  \brief Concrete implementation of Shannon-Fano node representation.
 *
 *  This class serves for a Shannon-Fano node representation
 */

#include "ShannonFanoNode.h"

// Constructor for internal node
ShannonFanoNode::ShannonFanoNode() {
	leftChild = rightChild = parent = 0;
	freq = (unsigned long) 0;
	symbol = -1;
}

// Constructor for leaf node
ShannonFanoNode::ShannonFanoNode(unsigned char s, unsigned long f) {
	leftChild = rightChild = parent = 0;
	symbol = s;
	freq = f;
}

// Destructor
ShannonFanoNode::~ShannonFanoNode() {
}

// Geters _begin_
signed int ShannonFanoNode::getSymbol() {
	return symbol;
}

unsigned long ShannonFanoNode::getFreq() {
	return freq;
}

ShannonFanoNode* ShannonFanoNode::getLeftChild() {
	return leftChild;
}

ShannonFanoNode* ShannonFanoNode::getRightChild() {
	return rightChild;
}

ShannonFanoNode* ShannonFanoNode::getParent() {
	return parent;
}

string ShannonFanoNode::getCode() {
	return code;
}
// Getters _end_


// Setters _begin_
void ShannonFanoNode::setSymbol(signed int s) {
	symbol = s;
}

void ShannonFanoNode::setFreq(unsigned long f) {
	freq = f;
}

void ShannonFanoNode::setLeftChild(ShannonFanoNode* lc) {
	leftChild = lc;
}

void ShannonFanoNode::setRightChild(ShannonFanoNode* rc) {
	rightChild = rc;
}

void ShannonFanoNode::setParent(ShannonFanoNode* p) {
	parent = p;
}

void ShannonFanoNode::setCode(string c) {
	code = c;
}
// Setters _end_
