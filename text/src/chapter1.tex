\chapter{Data compression}

This chapter introduces basic concepts of data compression and defines terms used further in the text. Classification of compression methods into various categories and performance measurement of methods are also discussed in this chapter.

\section{Basic data compression concepts}

\emph{Data compression} is a process of transforming computer data into a representation requiring fewer bits while preserving information contained in the data. 
The reverse process of reconstructing the original data from this representation is called \emph{decompression}. The terms \emph{encoding} and \emph{decoding} will also be used to refer to the compression and decompression processes respectively. 

The input data (for the compression process) are referred to as the \emph{original} or \emph{unencoded} data. 
The output data are considered \emph{compressed} or \emph{encoded}.

Algorithm that takes the original data and generates a representation with smaller size (compresses data) is called \emph{compression algorithm}. Algorithm that operates on this compressed representation to reconstruct the original data or produce their approximation is called \emph{reconstruction algorithm}. Both the compression and reconstruction algorithms are referred to together by the term \emph{compression method}.

\section{Information entropy and redundancy}

Two concepts of information theory, entropy and redundancy, are introduced in this section.

Information entropy is a term introduced by Claude Shannon in 1948~\cite{Shannon1948}. 
Entropy measures the amount of information contained in the data. Intuitively, the more diverse and unpredictable the data are, the more information they contain. It is possible to view entropy as an average minimum number of bits needed to store the information.

Redundancy measures the inefficiency of information representation. It is a difference between entropy of data of given length and its maximum value. Compression methods may attempt to reduce data redundancy.

\subsection{Entropy}
Let $X$ be a random variable that takes $n$ values with probabilities $ p_1, p_2, \ldots, p_n $ respectively. Then \emph{entropy} of $X$, denoted $H(X)$, is defined to be~\cite{Shannon1948}:

\begin{displaymath}
H(X) = -\sum_{i=1}^{n} p_i \log_2 p_i
\end{displaymath}

\subsection{Redundancy}
Let $S$ be a string of $l$ characters and $ H(c_i) $ be the entropy of its $i$-th character.
Then entropy of string $S$ is $H(S) = \sum_{i=1}^{l} H(c_i) = l \cdot H_{AVG}$, where $H_{AVG}$ is average symbol entropy.~\cite{Simek2009}

Let $L(S)$ be the length of $S$ in bits. Then \emph{redundancy} of string $S$, denoted $R(S)$, is defined as
\begin{displaymath}
R(S) = L(S) - H(S)
\end{displaymath}
Lossless methods of compression (see Section~\ref{subsec:losslesscategory}) work by minimizing redundancy in the input data. 
\cite{Simek2009}

\subsection{Source coding theorem}
\label{subsec:shannon_theorem}
The \emph{source coding theorem} formulated by C. Shannon in 1948 in~\cite{Shannon1948} establishes theoretical limits of data compression.

Let $S$ be a sequence of $N$ symbols. Each of those symbols is an independent sample of a random variable $X$ with entropy $H(X)$. Let $L$ be the average number of bits required to encode the $N$ symbol sequence $S$. The source coding theorem states that the minimum $L$ satisfies~\cite{SourceCodingTheorem}: 

\begin{displaymath}
H(X) \leq L < H(X) + \frac{1}{N}
\end{displaymath}

\section{Classification of compression methods}

There exists a large variety of data compression methods intended for several different purposes. 
Some basic classifications of compression methods based on their various properties are discussed here. 

\subsection{Lossy/lossless compression}
\label{subsec:losslesscategory}
\emph{Lossless} compression methods allow the exact reconstruction of the original data, and thus no information is lost during the compression.
Lossless compression methods should be used when even a small change to the data would have serious impact. For instance, when compressing text files, a change to just a few letters may alter the meaning of the original text or render it unreadable. This is especially true for compression of source codes. 

%\begin{flushleft}
\emph{Lossy} compression methods, on the other hand, lose some information during the compression process. They achieve better compression at the expense of preventing the exact reconstruction of the data. They are generally used for applications where this is not a problem. 
An example of such application is video compression. We generally do not care that the reconstruction of a video is slightly different from the original as long as noticeable artifacts are not present. As such, video is usually compressed using lossy compression methods~\cite[p.~5]{Sayood2006}.
%\end{flushleft}

\subsection{Adaptability}
Another possible classification of compression methods is according to how they manage their model of the input data and how they modify their operations. 

A \emph{nonadaptive} compression method does not modify its operations based on the data being processed. They may be faster because there is no time spent on updating the model. However, they would generally achieve worse compression ratio. Such methods perform best on data that is all of a single given type~\cite[p.~8]{Salomon2007}.

%\begin{flushleft}
An \emph{adaptive} compression method, on the other hand, modifies its algorithms according to the input data. The model is updated dynamically as the input is processed. Unlike for semi-adaptive methods, the input is processed in single run.
%\end{flushleft}

%\begin{flushleft}
If the method adapts itself to local conditions in the input stream and changes its model as it moves through the input, it is called \emph{locally adaptive}. 
For example move-to-front algorithm uses this approach~\cite[p.~37]{Salomon2007}.
%\end{flushleft}

%\begin{flushleft}
\label{semiadaptivemodel}
\emph{Semi-adaptive} methods use a 2-pass algorithm. During the first pass, whole input data are read and a model is built. In the second pass this model is used for compression. 
As the model is built specifically for the data being compressed, they generally achieve very good compression. However, it is not possible to infer the model from the compressed data. For this reason, the model must be appended to the compressed data for reconstruction algorithm to work, thus worsening the compression ratio. Moreover, these methods may be slow as the input is read twice.
%\end{flushleft}

\subsection{Symmetrical compression}
The situation when compression algorithm is similar to the reconstruction algorithm but works in the ``opposite'' direction is called 
\emph{symmetrical compression}~\cite[p.~9]{Salomon2007}. 
In such case, complexity of the compression algorithm is generally the same as complexity of the reconstruction algorithm. Symmetrical methods are useful when compression and decompression are performed equally frequently.

%\begin{flushleft}
\emph{Asymmetrical compression} is useful when decompression is performed much more often than compression and vice versa. For instance, when a file is compressed once, then uploaded to a server and downloaded by users and decompressed frequently, it is useful to have simple and fast reconstruction algorithm at the expense of a more complex compression algorithm.
%\end{flushleft}

\subsection{Types of lossless compression}
\label{subsec:losslesscategories}

\begin{itemize}

\item \emph{Dictionary methods} use some form of \emph{dictionary}, a data structure where previously seen occurrences of symbol sequences are saved. When a sequence present in the dictionary is encountered in the input again, a reference to the dictionary is written to output instead. LZ~family algorithms belong to this category.

\item \emph{Statistical methods} work by assigning shorter codes to symbols with higher probability of occurrence and longer codes to rare symbols. They often use semi-adaptive approach (see Section~\ref{semiadaptivemodel}). During the first pass, frequencies of symbols are counted and statistical model is built. During the second pass, the input is then encoded. 
Examples of this category include Huffman coding and arithmetic coding, which are both very briefly described in Chapter~\ref{chap:ANS}. 

\end{itemize}

Apart from those two main types, other types exist such as contextual methods (for example PPM and DCA algorithms).

\section{Measures of performance}

Performance of compression methods may be expressed using various measures. One of commonly used metrics is compression ratio.

\subsection{Compression ratio}
\emph{Compression ratio} is defined to be:

\begin{displaymath}
\text{Compression ratio} \: = \: \frac{\text{size of compressed data}}{\text{size of original data}}
\end{displaymath}

The lower the compression ratio is, the bigger amount of space was saved thanks to the  compression. For instance compression ratio of 0.8 (or 80\%) means that the compressed data size is $80\%$ the original size.

If the compression ratio has value greater than $1$, in other words the size of compressed data is greater than the size of original data, then we will say \emph{negative compression} occurred.

\subsection{Data corpora}
To test and compare performance of different compression algorithms various corpora exists. \emph{Corpus} is a standardized set of files containing most common types of binary and text files to test compression performance on.

Several corpora were published, for instance Calgary Corpus~(1987) or Canterbury Corpus~(1997). In 2011, the Prague Corpus was established on Czech Technical University in Prague~\cite{PragueCorpus}.

\section{ExCom library}

ExCom~\cite{ExComWeb} is a modular compression library written in C++.
ExCom contains many compression methods and is extensible by adding new modules.
It provides support for module testing, time measurement and benchmarking.
It is described in detail in~\cite{Simek2009}.

\section{Hash function and hash table}

\subsection{Hash function}
\emph{Hash function} is a mathematical function that maps input of arbitrary length to fixed-size output. This output value is called a \emph{hash}. 

Let $U$ be a set of all possible inputs: $ U = {\bigcup\limits_{i=1}^{n} {\lbrace 0,1 \rbrace}^i} $, 
$n$ is maximum input length (theoretically $n$ may even be infinity). 
Then function %$f$:
\begin{displaymath}
f\colon U \to {\lbrace 0,1 \rbrace}^l
\end{displaymath}
is a hash function that maps any element of $U$ to output of fixed length $l$.

The hash function must be deterministic. If $k_1 = k_2$, then also ${f(k_1) = f(k_2)}$. So for given input, hash function $f$ always produces the same output value.
If $k_1 \neq k_2$, but $ f(k_1) = f(k_2)$, then we say a \emph{collision} occurred.

\begin{samepage}
Good hash function (for use in hash tables) is usually required to have the following properties:

\begin{itemize}
  \item It should distribute the output values evenly to minimize number of collisions.
  \item It should be very fast to compute.
\end{itemize}
\end{samepage}

\subsection{Multiplicative hash}
\label{subsec:mult_hash}
One possible method to obtain a good hash function is described in~\cite[Section~11.3]{Cormen2009}. 
The input value $k$ is multiplied by a constant $ A \in \left[ 0, 1 \right] $ and the fractional part of the result is then multiplied by integer $m$. The floor of this number is the hash value. 
The hash function then looks as follows: 
\begin{displaymath}
f(k) = \lfloor m(kA \bmod 1) \rfloor
\end{displaymath}
$ kA \bmod 1 = kA - \lfloor kA \rfloor $ is the fractional part of $kA$. Value of $m$ is not critical, it is usually chosen as a power of $2$.~\cite{Cormen2009}

See~\cite{Cormen2009} for implementation details of this method. This type of hash function is also used in the LZFSE reference implementation. 

\subsection{Hash table}
\label{subsec:hash_table}
\emph{Hash table} is a data structure which implements dictionary used by dictionary compression methods. It stores its elements in an array and uses hash function for indexing. When accessing an element, its hash is computed and used as an index. However, since hash function may produce collisions, two different elements may get mapped to same position in the array. 

\begin{flushleft}
Various techniques are used to resolve collisions:
\end{flushleft}

\begin{itemize}
  \item \emph{Separate chaining} -- For each position in the array (i.e.\ each possible hash value), list of elements that map to this position is kept. 
Linked list is most commonly used for this purpose. When collision occurs during insertion, element is simply added at the end of the corresponding list. When accessing elements, the list on given position must be scanned until the element being accessed is found.
  \item \emph{Open addressing} -- All elements are stored directly in the array. Insertion is done by searching for first empty position using some predefined order. When accessing an element, the array is searched in the same order until either the required element or an empty cell is encountered.
\end{itemize}

