#ifndef __METHOD_ARITH_HPP__
#define __METHOD_ARITH_HPP__

/*
 * File:       method/arith/arith.hpp
 * Purpose:    Declaration of a arithmetic compression method
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2011
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Michal Valach
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

#include <excom.h>
#include "../../compmodule.hpp"
#include "ArithmeticCoder.hpp"

#define ALPHABET_SIZE 256

class CompArith: public CompModule {
private:
	IOReader *reader;
	IOWriter *writer;

        bool staticCoding;

        int compress();

        int decompress();
public:
	CompArith(unsigned int handle): CompModule(handle),
                reader(NULL), writer(NULL), staticCoding(false){}
        int setParameter(int parameter, void *value);
        int getValue(int parameter, void *value);
	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int checkConnection();
	int run();
};

#endif //ifndef __METHOD_ARITH_HPP__
