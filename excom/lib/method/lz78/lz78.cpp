/*
 * File:       method/lz78/lz78.cpp
 * Purpose:    Implementation of the LZ78 compression methods
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZ78 compression module linked to the ExCom library.
 *
 * LZ78 compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZ78 compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZ78 module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lz78.cpp
 *  \brief Implementation of the LZ78 compression method
 */

#include "lz78.hpp"
#include "../universal_codes/UniversalCodes.h"

#include <iostream>
#include <utility>
#include <queue>
#include <algorithm>

using namespace std;

int CompLZ78::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZ78::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompLZ78::setParameter(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_PARAM_LZ78_DICTIONARY_SIZE:
		if (value == NULL) return EXCOM_ERR_MEMORY;
		u = *((unsigned int*)value);

		if (u < 1 || u > 65536) return EXCOM_ERR_VALUE;
		dictionary_size = u;

		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZ78::getValue(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_LZ78_DICTIONARY_SIZE:
		u = dictionary_size;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

int CompLZ78::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompLZ78::requiredBits(unsigned int value) {
	// '0' needs 1 bit to represent
	int bits = 1;

	// Shift to right and counts needed operations
	while ((value >>= 1) > 0) bits++;
	return bits;
}

void CompLZ78::initializeDictionary(int operation) {
	switch (operation) {
		case OPERATION_COMPRESS:
			encDictionary.clear();

			lz78_node_enc_t encNode;

			encNode.prefixIndex = -1;
			encNode.first = -1;
			encNode.right = -1;
			encNode.left = -1;
			encNode.symbol = -1;

			encDictionary.push_back(encNode); 
			break;
		case OPERATION_DECOMPRESS:
			decDictionary.clear();
	
			lz78_node_dec_t decNode;

			decNode.prefixIndex = -1;
			decNode.symbol = -1;

			decDictionary.push_back(decNode); 
			break;
	}

	// Next available index
	dictionaryIndex = 1;
}

// The code is based on an idea of Juha Nieminen
// http://warp.povusers.org/EfficientLZW/ 
signed int CompLZ78::add(lz78_node_enc_t& node) {
	if (node.prefixIndex < encDictionary.size()) {
		signed int index = encDictionary[node.prefixIndex].first;

		if (index == -1) {
			encDictionary[node.prefixIndex].first = dictionaryIndex;
		} else {
			while (true) {
				if (encDictionary[index].symbol == node.symbol) return index;

				if (encDictionary[index].symbol < node.symbol) {
					if (encDictionary[index].right == -1) {
						encDictionary[index].right = dictionaryIndex;
						break; 
					}      

					index = encDictionary[index].right;
				} else {

					if (encDictionary[index].left == -1) {
						encDictionary[index].left = dictionaryIndex;
						break; 
					}        
					index = encDictionary[index].left;

				}
			}
		}
	}

	return -1;
}

bool CompLZ78::searchForNode(lz78_node_enc_t& node) {
	signed index = add(node);

	if (index != -1) {
		// Phrase was found
		node.prefixIndex = index;

		return true;
	}

	return false;
}

bool CompLZ78::getCode(unsigned long index) {
	decodedString.clear();

	if (decDictionary[index].prefixIndex != index) {
		while (index != 0) {
			const lz78_node_dec_t& node = decDictionary[index];
			decodedString.push_back(node.symbol);
			index = node.prefixIndex;
		}
	
		return true;		

	} else {
		return false;
	}
}

int CompLZ78::runCompress() {
	bool matchFound;

	signed int currentSymbol;
	signed int prefix;	

	prefix = 0;

	lz78_node_enc_t lz78Node;

	initializeDictionary(OPERATION_COMPRESS);

	minBits = requiredBits(dictionaryIndex);
	maxBits = requiredBits(dictionary_size) - 1;
	currentBits = minBits;
	nextBitsIncLimit = (1 << minBits) - 1;

	while (currentSymbol = reader->readByte(), currentSymbol != EOF) {
		lz78Node.symbol = currentSymbol;
		lz78Node.prefixIndex = prefix;
		lz78Node.first = -1;
		lz78Node.right = -1;
		lz78Node.left = -1;

		if (searchForNode(lz78Node)) {
			prefix = lz78Node.prefixIndex;
			matchFound = true;
		} else {
			writer->writeNBits(currentBits, prefix); // First part of a pair = position
			writer->writeNBits(8, currentSymbol); // Second part of a pair = symbol

			// Insert into the dictionary
			encDictionary.push_back(lz78Node);
			dictionaryIndex++;
			prefix = 0;

			if (encDictionary.size() == nextBitsIncLimit+1) {
				if (currentBits == maxBits) {
					currentBits = minBits;
					initializeDictionary(OPERATION_COMPRESS);
					nextBitsIncLimit = (1 << minBits) - 1; 
				} else {
					currentBits++;
					nextBitsIncLimit = (1 << currentBits) - 1;
				}
			}

			matchFound = false;
		}
	}

	// We still have to output some data
	if (matchFound) {
		writer->writeNBits(currentBits, prefix); // Only the position now	
	}

	writer->eof();

	return EXCOM_ERR_OK;
}

int CompLZ78::runDecompress() {
	int x;
	int y;

	unsigned long symbol;
	unsigned long index;	

	lz78_node_dec_t lz78Node;

	initializeDictionary(OPERATION_DECOMPRESS);

	minBits = requiredBits(dictionaryIndex);
	maxBits = requiredBits(dictionary_size) - 1;
	currentBits = minBits;
	nextBitsIncLimit = (1 << minBits) - 1;

	vector<unsigned char>::iterator it;

	while (true) {

		x = reader->readNBits(currentBits, &index);
		if (x == EOF) break;

		y = reader->readNBits(8, &symbol);

		if (index != 0) {
			if ((index > dictionaryIndex) || (!getCode(index))) {
				return EXCOM_ERR_FORMAT;
			}

			reverse(decodedString.begin(), decodedString.end());
			for (it = decodedString.begin(); it < decodedString.end(); it++) {
				writer->writeByte(*it);
			}	    
		}	

		// If a codeword was full = index+symbol
		if (y != EOF) writer->writeByte(symbol);	  

		lz78Node.prefixIndex = index;
		lz78Node.symbol = symbol;

		decDictionary.push_back(lz78Node);
		dictionaryIndex++;

		if (decDictionary.size() == nextBitsIncLimit+1) {
			if (currentBits == maxBits) {
				currentBits = minBits;
				initializeDictionary(OPERATION_DECOMPRESS);
				nextBitsIncLimit = (1 << minBits) - 1; 
			} else {
				currentBits++;
				nextBitsIncLimit = (1 << currentBits) - 1;
			}
		}

		if (y == EOF) break;
	}

	writer->eof();

	return EXCOM_ERR_OK;
}


int CompLZ78::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}


#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}


