#ifndef SUFFIXARRAY2_H_
#define SUFFIXARRAY2_H_

/*
 * File:       method/acb/SuffixArray2.hpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#define SA_DEBUG 0

#include <stdlib.h>
#include "SuffixStructure.hpp"
#include "BPlusTree.hpp"
#include "SortedContent.hpp"

#if SUFFIX_STRUCTURE_PERF
	#include <ctime>
	#include <iostream>
#endif

#if SA_DEBUG
    #include <stdio.h>
    #include <iostream>
#endif

class SuffixArray2 : public SuffixStructure {
private:

        /*!
         * \brief BPlus Tree using to store contexts
         */
	BPlusTree* tree;

        /*!
         * \brief Tree used to store sorted contents which contexts are
         *        longer than \c ACB_MAX_CONTEXT_LENGTH . 
         */
	SortedContentsTree* contentsTree;

#if SA_DEBUG
        void printPosition(long position);
        void printNode(BPlusTreeLeafNode* node);
#endif
	void printMethodStatistics();

        /*!
         * \brief Searches for best context.
         * \param position - Position in data i am currently at.
         * \param contextIndex - Index of context in \c BPlusTreeLeafNode.
         * \return \c BPlusTreeLeafNode which contains subset of sorted contexts.
         *
         * Best context is found at position returned by \c BPlusTreeLeafNode->getKeys()[contextIndex] .
         */
	BPlusTreeLeafNode* searchContext(long position, unsigned int &contextIndex);

       /*!
         * \brief Searches best content for best context in \c contextNode
         * \param position - Position in data i am searching at
         * \param distance - Distance between context and content (content - context)
         * \param contentLength - Common length ov best content and current content
         * \param contextNode - BPlusTreeLeafNode which contains best matching context
         * \param contextIndex - Index of best matchin context in \c contextNode
         */
        void searchInline(long position, int& distance, unsigned int& contentLength, BPlusTreeLeafNode* contextNode, unsigned int contextIndex);
public:

	SuffixArray2();
	virtual ~SuffixArray2();

	void search(long position, int &distance, unsigned int &contentLength);
        void translate(long position, int distance, unsigned int contentLength, unsigned char nextCharacter);
	void updateIndex(long position, long length);

	unsigned char* clear(long position, bool needData);

#if SC_ENABLED
	void setMaxContentLength(unsigned int maxContentLength);
        void setMaxDistance(unsigned int maxDistance);
        void setSize(unsigned int sizeInBits, unsigned int checkSizeBits);
        unsigned int search(long position, int &distance, unsigned int &contentLength, int &zzzLength);
#endif
};

#endif /* SUFFIXARRAY2_H_ */
