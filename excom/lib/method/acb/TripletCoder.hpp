/*
 * File:       method/acb/TripletCoder.hpp
 * Purpose:    Declaration of ACB compression method (Associative Coder of Buyanovsky)
 *
 * Copyright (C) 2011 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef TRIPLETCODER_HPP
#define	TRIPLETCODER_HPP

#include <libio.h>

#include "../../iomodule.hpp"
#include "../arith/ArithmeticCoder.hpp"
#include "../dhuff/DHuffTree.h"

namespace tripletcoder {

// ===============================================================
// ========================= Encoder =============================
// ===============================================================

/*!
 * \brief Ancestor class for encoding triplets into output stream.
 *
 * Current subclasses are \c ArithmeticEncoder for ussage of adaptive arithmetic
 * coding and \c HuffmanEncoder for usage of adaptive huffman coding.
 */
class Encoder {
protected:
    ::IOWriter* writer;
public:
    Encoder(::IOWriter* writer);
    virtual ~Encoder();

    /*!
     * \brief Encode \c length to output stream \c writer.
     *
     * \param length - value to encode.
     */
    virtual void encodeLength(unsigned int length) = 0;
    
    /*!
     * \brief Encode \c distance to output stream \c writer.
     *
     * \param distance - value to encode.
     */
    virtual void encodeDistance(unsigned int distance) = 0;

    /*!
     * \brief Encode \c character to output stream \c writer.
     *
     * \param character - value to encode.
     */
    virtual void encodeCharacter(unsigned int character) = 0;

    /*!
     * \brief Encode \c isLower to output stream \c writer.
     *
     * \param isLower - value to encode.
     */
    virtual void encodeIsLower(unsigned int isLower) = 0;

    /*!
     * \brief Encode EOF using \c encodeLength and flush remaining data to
     *        output stream.
     */
    virtual void flush() = 0;
};

class ArithmeticEncoder: public Encoder {
protected:
    arithcoder::AEncoder* encoder;

    arithcoder::AlphabetInfo* lengthAlphabet;
    arithcoder::AlphabetInfo* distanceAlphabet;
    arithcoder::AlphabetInfo* characterAlphabet;
    arithcoder::AlphabetInfo* isLowerAlphabet;
    
public:
    ArithmeticEncoder(::IOWriter* writer, unsigned int length, unsigned int distance, unsigned int alphabet);
    virtual ~ArithmeticEncoder();

    void encodeLength(unsigned int length);
    void encodeDistance(unsigned int distance);
    void encodeCharacter(unsigned int character);
    void encodeIsLower(unsigned int isLower);

    void flush();
};

class HuffmanEncoder: public Encoder {
protected:
    DHuffTree* lengthTree;
    DHuffTree* distanceTree;
    DHuffTree* characterTree;

    unsigned int codeLength;
    
public:
    HuffmanEncoder(::IOWriter* writer, unsigned int length, 
            unsigned int distance, unsigned int alphabet,
            unsigned char character);
    virtual ~HuffmanEncoder();

    void encodeLength(unsigned int length);
    void encodeDistance(unsigned int distance);
    void encodeCharacter(unsigned int character);
    void encodeIsLower(unsigned int isLower);

    void flush();
};

// ===============================================================
// ========================= Decoder =============================
// ===============================================================
/*!
 * \brief Ancestor class for decoding triplets into output stream.
 *
 * Current subclasses are \c ArithmeticDecoder for ussage of adaptive arithmetic
 * coding and \c HuffmanDecoder for usage of adaptive huffman coding.
 */
class Decoder {
protected:
    ::IOReader* reader;

public:
    Decoder(::IOReader* reader);
    ~Decoder();

    /*!
     * \brief Decode \c length from input stream \c reader.
     *
     * \return Decoded length.
     */
    virtual unsigned int decodeLength() = 0;

    /*!
     * \brief Decode \c distance from input stream \c reader.
     *
     * \return Decoded distance.
     */
    virtual unsigned int decodeDistance() = 0;

    /*!
     * \brief Decode \c character from input stream \c reader.
     *
     * \return Decoded character.
     */
    virtual unsigned int decodeCharacter() = 0;

    /*!
     * \brief Decode \c isLower from input stream \c reader.
     *
     * \return Decoded isLower
     */
    virtual unsigned int decodeIsLower() = 0;

    /*!
     * \brief Checks if end of input stream \c reader was reached.
     *
     * \return True if end of stream was reached.
     */
    virtual bool isEof() = 0;
};

class ArithmeticDecoder: public Decoder {
protected:
    arithcoder::ADecoder* decoder;

    arithcoder::AlphabetInfo* lengthAlphabet;
    arithcoder::AlphabetInfo* distanceAlphabet;
    arithcoder::AlphabetInfo* characterAlphabet;
    arithcoder::AlphabetInfo* isLowerAlphabet;

    unsigned int lastLength;

public:
   ArithmeticDecoder(::IOReader* reader, unsigned int length, unsigned int distance, unsigned int alphabet);
   virtual ~ArithmeticDecoder();

   unsigned int decodeLength();
   unsigned int decodeDistance();
   unsigned int decodeCharacter();
   unsigned int decodeIsLower();

   bool isEof();
};

class HuffmanDecoder: public Decoder {
protected:
    DHuffTree* lengthTree;
    DHuffTree* distanceTree;
    DHuffTree* characterTree;

    int eof;

    unsigned int getNextCode(DHuffTree* tree);

public:
   HuffmanDecoder(::IOReader* reader, unsigned int length,
           unsigned int distance, unsigned int alphabet,
           unsigned char character);
   virtual ~HuffmanDecoder();

   unsigned int decodeLength();
   unsigned int decodeDistance();
   unsigned int decodeCharacter();
   unsigned int decodeIsLower();

   bool isEof();
};
}
#endif	/* TRIPLETCODER_HPP */

