#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/method/copy/copy.hpp"
#include "../../lib/iomodule.hpp"

#define HANDLE 1
#define HANDLE_IO_IN 2
#define HANDLE_IO_OUT 3

class CompCopyTestSuite: public CxxTest::TestSuite {
	CompCopy *comp;
public:
	void setUp() {
		comp = new CompCopy(HANDLE);
	}
	void tearDown() {
		delete comp;
	}

	void testParameters() {
		int v;
		// 'copy' compression module has no parameters and provides no values
		TS_ASSERT_EQUALS(comp->setParameter(0, NULL), EXCOM_ERR_PARAM);
		// but its superclass, CompModule, provides a value
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_COMP_RESULT, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, EXCOM_ERR_LATER);
	}

	void testIOInputModules() {
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		// invalid pointer to a module
		TS_ASSERT_EQUALS(comp->connectIOModule(NULL, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_MEMORY);
		// invalid connection type
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, -1), EXCOM_ERR_PARAM);
		// input module connected as an output module
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		delete reader;
	}

	void testIOOutputModules() {
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		// output module connected as an input module
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_DUPLICATE);
		delete writer;
	}

	void testRunning() {
		char bin[10];
		char bout[10];

		// testing data to be 'compressed' (copied)
		for (int i=0; i<10; i++) {
			bin[i] = 'A' + i;
		}

		// input and output modules
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		TS_ASSERT_EQUALS(reader->attachMemory(bin, 10), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->attachMemory(bout, 10), EXCOM_ERR_OK);

		// running shouldn't succeed without an input I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// ...or without output I/O module
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// when both input and input is connected, running should succeed
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		// check, whether copied data is the same as original
		TS_ASSERT_SAME_DATA(bin, bout, 10);

		delete reader;
		delete writer;
	}
};


