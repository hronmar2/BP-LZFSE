/*
 * File:       method/integer_compression/other.cpp
 * Purpose:    Implementation of other coding functors
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file other.cpp
 *  \brief Implementation of other coding functors
 *
 *  This classes provides encoding and decoding methods
 */

#include "method.hpp"
#include <stack>

Unary::Unary(BitBuffer& bb): Method(bb) {}
Block::Block(BitBuffer& bb): Method(bb) {}
TernaryComma::TernaryComma(BitBuffer& bb): Method(bb) {}
Rice::Rice(BitBuffer& bb, unsigned param): Method(bb), p(fast_log2(param)) {}
Golomb::Golomb(BitBuffer& bb, unsigned param): Method(bb), p(param), c(fast_log2(p)) {}

void Unary::operator() (unsigned long data) {
	if (!data) throw "Cannot encode zero!";
	while (--data)
		bb.pushBit('1');
	bb.pushBit('0');
}

unsigned long Unary::operator() () {
	unsigned long data = 1;
	bool bit;
	while (bb.getBit(bit) && bit)
		data++;
	return data;
}

unsigned long Unary::length(unsigned long data) {
	return data;
}

void Block::operator() (unsigned long data) {
	bool zero = true;
	for (int i = 24; i > 0; i -= 8) {
		unsigned char byte = (data >> i) & 0xFF;
		if (!byte && zero) continue;
		zero = false;
		bb.writer()->writeByte(byte);
	}
	bb.writer()->writeByte(data & 0xFF);
}

void TernaryComma::operator() (unsigned long data) {
	if (!data) {
		bb.pushNBits("11");
		return;
	}
	std::stack<unsigned short> stack;
	if (!--data) stack.push(0);
	while (data) {
		stack.push(data % 3);
		data /= 3;
	}
	while (!stack.empty()) {
		switch (stack.top()) {
			case 0:
				bb.pushNBits("00");
				break;
			case 1:
				bb.pushNBits("01");
				break;
			case 2:
				bb.pushNBits("10");
				break;
			default:
				throw "Unknown modulo";
		}
		stack.pop();
	}
	bb.pushNBits("11");
}

void Rice::operator() (unsigned long data) {
	unsigned long q = (data-1) >> p;
	unsigned long r = data - (q<<p)	- 1;
	Unary un(bb);
	un(q+1);
	bb.pushULong(r, p);
}

unsigned long Rice::operator() () {
	Unary un(bb);
	unsigned long q = un() - 1;
	Beta b(bb);
	unsigned long r = b(p,NULL) + 1;
	return (q<<p) + r;
}

unsigned long Rice::length(unsigned long data) {
	DummyBitBuffer dbb;
	Rice r(dbb, 65536);
	r(data);
	return dbb.bitsStored();
}

void Golomb::operator() (unsigned long data) {
	unsigned long q = (data-1) / p;
	unsigned long r = data - (q*p)	- 1;
	Unary un(bb);
	un(q+1);
	if (r < c)
		bb.pushULong(r, c);
	else {
		bb.pushBit(1);
		bb.pushULong(r-c, c);
	}
}

unsigned long Golomb::operator() () {
	Unary un(bb);
	unsigned long q = un() - 1;
	bool bit;
	Beta b(bb);
	unsigned long r;
	if (bb.peekBit(bit) && bit) {
		bb.getBit(bit);
		r = c + b(c,NULL) + 1;
	}
	else {
		r = b(c,NULL) + 1;
	}
	return (q*p) + r;
}

unsigned long Golomb::length(unsigned long data) {
	DummyBitBuffer dbb;
	Golomb g(dbb, 100000);
	g(data);
	return dbb.bitsStored();
}

