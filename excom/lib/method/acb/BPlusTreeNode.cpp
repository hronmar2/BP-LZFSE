/*
 * File:       method/acb/BPlusTreeNode.cpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "BPlusTreeNode.hpp"

// ===============================================================
// ======================= BPlusTreeNode =========================
// ===============================================================
BPlusTreeNode::BPlusTreeNode(){
	currentSize = 0;
	this->keys = new long[B_TREE_NODE_KEYS];
};

unsigned int BPlusTreeNode::_size_of_long = sizeof(long);
unsigned int BPlusTreeNode::_size_of_pointer = sizeof(void *);

unsigned int BPlusTreeNode::_b_tree_node_keys = 1 << B_TREE_NODE_KEYS_BITS;
unsigned int BPlusTreeNode::_b_tree_node_keys_size = BPlusTreeNode::_b_tree_node_keys * BPlusTreeNode::_size_of_long;
unsigned int BPlusTreeNode::_b_tree_node_pointers = _b_tree_node_keys + 1;
unsigned int BPlusTreeNode::_b_tree_split_size = _b_tree_node_keys >> 1;
unsigned int BPlusTreeNode::_b_tree_split_size_pointers = _b_tree_split_size + 1;

BPlusTreeNode::~BPlusTreeNode(){
	delete [] keys;
	keys = NULL;
};


bool BPlusTreeNode::isLeaf(){
	return false;
}

bool BPlusTreeNode::isFull(){
	return this->currentSize == B_TREE_NODE_KEYS;
}

void BPlusTreeNode::addKey(unsigned int slot, long position) {
	addKeyInline(slot, position);
}

long* BPlusTreeNode::getKeys() {
	return this->keys;
}

void BPlusTreeNode::incSize() {
	currentSize++;
}

unsigned int BPlusTreeNode::getSize() {
	return currentSize;
}

void BPlusTreeNode::setSize(unsigned int size){
	this->currentSize = size;
}

long BPlusTreeNode::getFirstKey(){
	return keys[0];
}

unsigned int BPlusTreeNode::search(uint16_t* data, long position) {
	unsigned int begin = 0;
	unsigned int end = currentSize;
	while(begin < end) {
		unsigned int middle = (begin + end) >> 1;
		uint16_t* context = data + keys[middle];
		uint16_t* current = data + position;
                uint16_t contextLength = 0;
		short ret = 0;
//		while (*current-- == *context-- && contextLength++ < ACB_MAX_CONTEXT_LENGTH){}
//		if(*current < *context) {
		while (!(ret = *current - *context) 
			&& contextLength < ACB_MAX_CONTEXT_LENGTH
			){
			contextLength++;
			context--, current--;		
		}
		if(ret <= 0) {
			end = middle;
		} else {			
			begin = middle + 1;
		}
	}
	return end;
}

inline void BPlusTreeNode::addKeyInline(unsigned int slot, long position) {
	MOVE_KEYS(keys + slot + 1, keys + slot, this->currentSize - slot);
	currentSize++;
	keys[slot] = position;
}

void BPlusTreeNode::splitKeys(unsigned int slot, long position, BPlusTreeNode* newNode) {
	COPY_KEYS(newNode->keys, this->keys + B_TREE_SPLIT_SIZE, B_TREE_SPLIT_SIZE);
	this->currentSize = B_TREE_SPLIT_SIZE;
	newNode->currentSize = B_TREE_SPLIT_SIZE;
	
	if(slot <= B_TREE_SPLIT_SIZE) {
		addKeyInline(slot, position);
	} else {
		newNode->addKeyInline(slot - B_TREE_SPLIT_SIZE, position);
	}
}

// ===============================================================
// ==================== BPlusTreeInnerNode =======================
// ===============================================================
BPlusTreeInnerNode::BPlusTreeInnerNode(){
	this->children = new BPlusTreeNode*[B_TREE_NODE_POINTERS];
}

BPlusTreeInnerNode::~BPlusTreeInnerNode(){
	for(int i = 0; i <= currentSize; i++) {
		delete children[i];
		children[i] = NULL;
	}

	delete [] children;
	children = NULL;
}

BPlusTreeNode** BPlusTreeInnerNode::getChildren() {
	return this->children;
}

// ===============================================================
// ==================== BPlusTreeLeafNode =======================
// ===============================================================
BPlusTreeLeafNode::BPlusTreeLeafNode(){
	this->leftSibling = NULL;
	this->rightSibling = NULL;
}

BPlusTreeLeafNode::~BPlusTreeLeafNode(){
}

bool BPlusTreeLeafNode::isLeaf(){
	return true;
}

BPlusTreeLeafNode* BPlusTreeLeafNode::getLeftSibling(){
	return this->leftSibling;
}

BPlusTreeLeafNode* BPlusTreeLeafNode::getRightSibling(){
	return this->rightSibling;
}

void BPlusTreeLeafNode::setRightSibling(BPlusTreeLeafNode* node){
	this->rightSibling = node;
	if(node != NULL) {
		node->leftSibling = this;
	}
}
