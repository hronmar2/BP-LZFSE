#ifndef SUFFIXTREENODE_H_
#define SUFFIXTREENODE_H_

/*
 * File:       method/acb/SuffixTreeNode.hpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include <map>
#include <vector>
#include <string.h>
#include <stdint.h>

class SuffixTreeNode;

typedef std::map<unsigned char, SuffixTreeNode* > children_map_t;
typedef std::pair<unsigned char, SuffixTreeNode* > child_entry_t;
typedef children_map_t::iterator children_iterator_t;

class SuffixTreeNode {
private:
        /*!
         * \brief Initializes this suffix
         * \param position - Position in data i am currently at
         * \param nodeStartPosition - Position at which this sub suffix starts
         * \param nodeEndPosition - Position at which this sub suffix ends
         */
	void init(long position, long nodeStartPosition, long nodeEndPosition);
public:

        /*!
         * \brief Position in data this suffix was first encountered
         */
	long position;

        /*!
         * \brief Position at which this sub suffix starts
         */
	long nodeStartPosition;

        /*!
         * \brief Position at which this sub suffix ends
         */
	long nodeEndPosition;

        /*!
         * \brief Tells, if current node has any children
         */
	bool leaf;

        /*!
         * \brief Left sibling of this node - suffix is lower than current one
         */
	SuffixTreeNode* leftSibling;

        /*!
         * \brief Right sibling of this node - suffix is greater than current one
         */
	SuffixTreeNode* rightSibling;

        /*!
         * \brief List of suffixes, which have this suffix as its parent
         */
	children_map_t* children;

        /*!
         * \brief Creates new suffix
         */
	SuffixTreeNode();

        /*!
         * \brief Creates new suffix
         * \param position - Position in data i am currently at
         * \param nodeStartPosition - Position at which this sub suffix starts
         * \param nodeEndPosition - Position at which this sub suffix ends
         */
	SuffixTreeNode(long position, long nodeStartPosition, long nodeEndPosition);
	virtual ~SuffixTreeNode();

        /*!
         * \brief Is this suffix a leaf
         * \return
         */
	bool isLeaf();

        /*!
         * \brief Adds new suffix to this suffix. The new suffix has this suffix as its parent
         * \param data - Data to compress
         * \param position - Position in data i am currently at
         * \param nodeStartPosition - Position at which this sub suffix starts
         */
	void addNode(const uint16_t* data, long position, long nodeStartPosition);

        /*!
         * \brief Branches current suffix
         * \param data - Data to compress
         * \param position - Position in data i am currently at
         * \param nodeStartPosition - Position at which this sub suffix starts
         * \param same - How many characters are common to both new created suffixes
         */
	void branchNode(const uint16_t* data, long position, long nodeStartPosition, long same);
};

#endif /* SUFFIXTREENODE_H_ */
