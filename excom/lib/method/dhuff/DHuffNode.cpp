/*
 * File:       method/dhuff/DHuffNode.cpp
 * Purpose:    Declaration of the Dymamic Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Dynamic Huffman coding module linked to the ExCom library.
 *
 * Dynamic Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dynamic Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Dynamic Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file DHuffNode.cpp
 *  \brief Concrete implementation of Huffman node representation.
 *
 *  This class serves for Huffman node representation and manipulation
 */

#include "DHuffNode.h"

using namespace std;

// Constructors _begin_
DHuffNode::DHuffNode() {
	leftChild = rightChild = 0;
        symbol = 0;
	weight = 0;
	symbolNode = false;
	code = LEFT_CHILD_CODE;
}

DHuffNode::DHuffNode(unsigned int s) {
        leftChild = rightChild = 0;
	symbol = s;
	weight = 1;
	symbolNode = true;
	code = RIGHT_CHILD_CODE; 
}
// Constructors _end_

// Destructor
DHuffNode::~DHuffNode() {
	if(leftChild != NULL) {
		delete leftChild;
		leftChild = NULL;
	}

	if(rightChild != NULL) {
		delete rightChild;
		rightChild = NULL;
	}
}

// Getters _begin_
unsigned int DHuffNode::getSymbol() {
	return symbol;
}

unsigned long DHuffNode::getWeight() {
	return weight;
}

int DHuffNode::getIndex() {
	return index;
}

DHuffNode* DHuffNode::getLeftChild() {
	return leftChild;
}

DHuffNode* DHuffNode::getRightChild() {
	return rightChild;
}

DHuffNode* DHuffNode::getParent() {
	return parent;
}

bool DHuffNode::isSymbolNode() {
	return symbolNode;
}

unsigned int DHuffNode::getCode() {
	return code;
}
// Getters _end_

// Setters _begin_
void DHuffNode::setSymbol(unsigned int s) {
	symbol = s;
}

void DHuffNode::setWeight(unsigned long w) {
	weight = w;
}

void DHuffNode::setIndex(int i) {
	index = i;
}

void DHuffNode::setLeftChild(DHuffNode *lc) {
	leftChild = lc;
}

void DHuffNode::setRightChild(DHuffNode *rc) {
	rightChild = rc;
}

void DHuffNode::setParent(DHuffNode *p) {
	parent = p;
}

void DHuffNode::setCode(int c) {
	code = c;
}
// Setters _end_

void DHuffNode::incWeight() {
	weight++;
}
