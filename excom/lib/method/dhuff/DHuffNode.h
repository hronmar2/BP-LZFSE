#ifndef _DYNAMIC_HUFF_NODE_H
#define	_DYNAMIC_HUFF_NODE_H

/*
 * File:       method/dhuff/DHuffNode.h
 * Purpose:    Declaration of the Dymamic Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Dynamic Huffman coding module linked to the ExCom library.
 *
 * Dynamic Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Dynamic Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Dynamic Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file DHuffNode.h
 *  \brief Declaration of Huffman node representation.
 *
 *  This class serves for Huffman node representation and manipulation
 */


#include <string> 

#define LEFT_CHILD_CODE 0
#define RIGHT_CHILD_CODE 1

using namespace std;

class DHuffNode {
private:
	unsigned long weight;
	unsigned int symbol;

	int index;

	bool symbolNode;

	int code;

	DHuffNode *leftChild;
	DHuffNode *rightChild;
	DHuffNode *parent;
public:
	// Constructors
	DHuffNode();
	DHuffNode(unsigned int s);

	// Destructor
	~DHuffNode();

	// Getters
	unsigned int getSymbol();
	unsigned long getWeight();

	int getIndex();

	bool isSymbolNode();

	unsigned int getCode();

	DHuffNode* getLeftChild();
	DHuffNode* getRightChild();
	DHuffNode* getParent();

	// Setters
	void setSymbol(unsigned int s);
	void setWeight(unsigned long w);
	void setIndex(int);
	void setLeftChild(DHuffNode * node);
	void setRightChild(DHuffNode * node);
	void setParent(DHuffNode * node);
	void incWeight();
	void setCode(int s);
};

#endif	/* _DYNAMIC_HUFF_NODE_H */

