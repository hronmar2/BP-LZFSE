#ifndef __METHOD_LZY_HPP__
#define __METHOD_LZY_HPP__

/*
 * File:       method/lzy/lzy.hpp
 * Purpose:    Declaration of the LZY compression methods
 * Author:     Tomas Zemancik <zemanto6@fit.cvut.cz>, 2012
 *             diploma thesis at Faculty of Information Technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2012 Tomas Zemancik
 *
 * This file is part of LZY compression module linked to the ExCom library.
 *
 * LZY compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZY compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZY module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzy.hpp
 *  \brief Declaration of the LZY compression method
 */

#include <excom.h>
#include "../../compmodule.hpp"
#include <vector>

class CompLZY : 
	public CompModule 
{
private:
	IOReader *reader;
	IOWriter *writer;

	int runCompress();
	int runDecompress();

	unsigned dict_size;
	unsigned dict_capacity;
	unsigned root_size;	
	unsigned char char_bitsize;

	int (CompLZY::* readSymbol)(unsigned long *);
	int (CompLZY::* writeSymbol)(unsigned long);

public:
	static const unsigned DEFAULT_DICT_SIZE = 4096;
	static const unsigned char DEFAULT_CHAR_BITSIZE = 8;

	CompLZY(unsigned int handle);

	int connectIOModule(IOModule *module, int connection_type);
	int setOperation(enum exc_oper_e operation);
	int setParameter(int parameter, void *value);
	int getValue(int parameter, void *value);
	int checkConnection();
	int run();

private:

	typedef struct {
		unsigned prefixIndex; 
		unsigned first;
		unsigned left;
		unsigned right;
			
		unsigned suffix;		

		unsigned symbol;
	} lzw_node_enc_t;

	typedef std::vector <lzw_node_enc_t > lzwEncDictionary;
	lzwEncDictionary encDict;

	unsigned char bitsize;
	unsigned threshold;

	std::vector<unsigned> decodedString;

	int readNBits(unsigned long *value);
	int readByte(unsigned long *value);
	int writeNBits(unsigned long value);
	int writeByte(unsigned long value);

	void initializeDictionary();

	unsigned add(lzw_node_enc_t &node, bool *was_inserted);
	unsigned find(lzw_node_enc_t &) const;

	unsigned searchNode(lzw_node_enc_t&) const;
	unsigned searchInsertNode(lzw_node_enc_t &node, bool *was_inserted);
};

inline int CompLZY::checkConnection() {
	if (reader == NULL || writer == NULL) 
		return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

inline int CompLZY::readNBits(unsigned long *value) {
	return reader->readNBits(char_bitsize, value);		
}
inline int CompLZY::readByte(unsigned long *value) {
	int result = reader->readByte();
	if (result != EOF)
		*value = result;
	return result;
}
inline int CompLZY::writeNBits(unsigned long value) {
	return writer->writeNBits(char_bitsize, value);
}
inline int CompLZY::writeByte(unsigned long value) {
	return writer->writeByte((unsigned char)value);
}

#endif //ifndef __METHOD_LZY_HPP__
