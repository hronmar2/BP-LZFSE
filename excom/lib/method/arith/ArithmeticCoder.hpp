/*
 * File:       method/arith/ArithmeticCoder.hpp
 * Purpose:    Declaration of ACB compression method (Associative Coder of Buyanovsky)
 *
 * Based on implementation from Arithmetic Encoding and Decoding Library by
 * Michael Dipperstein, http://michael.dipperstein.com/arithmetic/
 *
 * Copyright (C) 2004 Michael Dipperstein
 * Copyright (C) 2011 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#ifndef ARITHMETICCODER_HPP
#define	ARITHMETICCODER_HPP

#include "../../iomodule.hpp"
#include <math.h>

namespace arithcoder {

#define BUILD_DEBUG_OUTPUT 0

/*!
 * \brief Probability count type
 */
typedef unsigned short probability_t;

/*!
 * \brief number of bits used to compute running code values
 */
#define PRECISION           (8 * sizeof(arithcoder::probability_t))

/*!
 * \brief 2 bits less than precision. keeps lower and upper bounds from crossing.
 */
#define MAX_PROBABILITY     (1 << (PRECISION - 2))

/*!
 * \brief set bit x to 1 in probability_t.  Bit 0 is MSB.
 */
#define MASK_BIT(x) (arithcoder::probability_t)(1 << (PRECISION - (1 + (x))))

/*!
 * \brief Indices for a symbol's lower cumulative probability range
 */
#define LOWER(c)        (c)

/*!
 * \brief Indices for a symbol's upper cumulative probability range
 */
#define UPPER(c)    ((c) + 1)

/*!
 * \brief Calculates how many bits are needed to store \c size
 */

#define BIT_SIZE(size)  int(ceil(log2(size)))

/*!
 * \brief Class to store \c probabilities of \c symbols in \c alphabet
 *        of size \c size.
 */
class AlphabetInfo {
public:

    /*!
     * \brief Size of alphabet for this arithmetic code.
     *        Also EOF code of this alphabet.
     */
    unsigned int size;
    
    /*!
     * \brief Index of upper bound of this alphabet
     */
    unsigned int upper_bound;

    /*!
     * \brief Probability ranges for each symbol: [ranges[LOWER(c)], ranges[UPPER(c)]
     */
    probability_t* ranges;

    /*!
     * \brief Cumulative probability  of all ranges
     */
    probability_t cumulativeProb;

    /*!
     * \brief Mappings of symbol to index in ranges after sorting by probabilities
     */
    int* symbolToIndexMapping;

    /*!
     * \brief Mappings of index in ranges to symbol after sorting by probabilities
     */
    int* indexToSymbolMapping;

    /*!
     * \brief Creates a probability holder for arithmetic coding.
     *
     *        If used for adaptive variant, probabilities are updated
     *        automatically and sorted from lowest to highest.
     *
     *        If used for non adaptive variant caller must initialize alphabet
     *        using method
     *        SEncoder::buildProbabilityRangeList(AlphabetInfo* alphabet, ::IOReader reader)
     *
     *        But if caller wants to do it himself he must follow these steps:
     *
     *        1. Frst insert into
     *        \c ranges probabilities of each symbol with maximum value of
     *        \c cumulativeProb is lower than \c MAX_PROBABILITY. If it is
     *        greater or equal, it has to be rescaled to meet this requirement.
     *
     *        2. To create ranges instead of only count
     *        method \c symbolCountToProbabilityRanges must be called.
     *
     * \Requirement !DO NOT sort probabilities by yourself!
     *
     * \param size - Actual size of Alphabet
     */
    AlphabetInfo(unsigned int size);
    virtual ~AlphabetInfo();
};

class Coder {
protected:
    /*!
     * \brief Lower bound of current code range.
     */
    probability_t lower;

    /*!
     * \brief Upper bound of current code range.
     */
    probability_t upper;

    /*!
     * \brief Current MSBs of encode input stream.
     */
    probability_t code;

    /*!
     * \brief Current underflow bit count.
     */
    unsigned char underflowBits;

    /*!
     * \brief Value of MASK_BIT(0)
     */
    probability_t _mask_bit_0;
    
    /*!
     * \brief Value of MASK_BIT(1)
     */
    probability_t _mask_bit_1;

    /*!
     * \brief Method for initializing successors of \c Coder/
     */
    virtual void init() = 0;

    /*!
     * \brief This function is used for both encoding and decoding.  It
     *        applies the range restrictions of a new symbol to the
     *        current upper and lower range bounds of an encoded stream.
     *
     * \param alphabet - Probabilities distribution.
     * \param symbol - The symbol to be added to the current code range.
     *
     * \Effects The current upper and lower range bounds are adjusted to
     *          include the range effects of adding another symbol to the
     *          encoded stream.
     */
    void updateLowerUpper(AlphabetInfo* alphabet, int symbol);
    
public:
    Coder();
    virtual ~Coder();

    /*!
     * \brief This function is used for both encoding and decoding.  It
     *        applies the range restrictions of a new symbol to the
     *        current upper and lower range bounds of an encoded stream.
     *        If an adaptive model is being used, the probability range
     *        list will be updated after the effect of the symbol is
     *        applied.
     *
     * \param alphabet - Probabilities distribution.
     * \param symbol - The symbol to be added to the current code range.
     *
     * \Effects The current upper and lower range bounds are adjusted to
     *          include the range effects of adding another symbol to the
     *          encoded stream.  If an adaptive model is being used, the
     *          probability range list will be updated.
     */
    virtual void insert(AlphabetInfo* alphabet, int symbol) = 0;
};

class Encoder: public Coder {
protected:

    /*!
     * \brief Output stream for encoder.
     */
    ::IOWriter* writer;

    void init();

    /*!
     * \brief This function attempts to shift out as many code bits as
     *        possible, writing the shifted bits to the encoded output
     *        file.  Only bits that will be unchanged when additional
     *        symbols are encoded may be written out.
     *
     *        If the n most significant bits of the lower and upper range
     *        bounds match, they will not be changed when additional
     *        symbols are encoded, so they may be shifted out.
     *
     *        Adjustments are also made to prevent possible underflows
     *        that occur when the upper and lower ranges are so close
     *        that encoding another symbol won't change their values.
     *
     * \Effects The upper and lower code bounds are adjusted so that they
     *          only contain only bits that may be affected by the
     *          addition of a new symbol to the encoded stream.
     *
     * \return none
     */
    void writeEncodedBits();


    /*!
     * \brief This function writes out all remaining significant bits
     *         in the upper and lower ranges and the underflow bits once
     *         the last symbol has been encoded.
     * \Effects Remaining significant range bits are written to the output
     *          file.
     * \return Size of written bits to output stream.
     */
    void writeRemaining();

public:
    Encoder(::IOWriter* writer);
    virtual ~Encoder();

    /*!
     * \brief Encode one symbol to output stream using probabilities
     *        stored in \c alphabet.
     *
     * \param alphabet - Probabilities distribution.
     * \param symbol - The symbol to be written to the output stream.
     *
     * \return none
     */
    void add(AlphabetInfo* alphabet, int symbol);

    /*!
     * \brief Encode EOF symbol into output stream and flush remaining bits
     *        into output stream using probabilities stored in \c alphabet.
     *
     * \param alphabet - Probabilities distribution.
     *
     * \return none
     */
    void flush(AlphabetInfo* alphabet);
};

class Decoder: public Coder{
protected:
    /*!
     * \brief Input stream for encoder.
     */
    ::IOReader* reader;

    void init();

    /*!
     * \brief This function undoes the scaling that ApplySymbolRange
     *        performed before bits were shifted out.  The value returned
     *        is the probability of the encoded symbol.
     *
     * \return The probability of the current symbol
     */
    probability_t getUnscaledCode(AlphabetInfo* alphabet);

    /*!
     * \brief Given a probability, this function will return the symbol
     *        whose range includes that probability.  Symbol is found
     *        binary search on probability ranges.
     * \param alphabet - probabilities distribution
     * \param probability - probability of symbol.
     *
     * \return -1 for failure, otherwise encoded symbol
     */
    int getSymbolFromProbability(AlphabetInfo* alphabet, probability_t probability);

    /*!
     * \brief This function attempts to shift out as many code bits as
     *         possible, as bits are shifted out the coded input is
     *         populated with bits from the encoded file.  Only bits
     *         that will be unchanged when additional symbols are decoded
     *         may be shifted out.
     *
     *         If the n most significant bits of the lower and upper range
     *         bounds match, they will not be changed when additional
     *         symbols are decoded, so they may be shifted out.
     *
     *         Adjustments are also made to prevent possible underflows
     *         that occur when the upper and lower ranges are so close
     *         that decoding another symbol won't change their values.
     *
     * \Effects The upper and lower code bounds are adjusted so that they
     *          only contain only bits that will be affected by the
     *          addition of a new symbol.  Replacements are read from the
     *          encoded stream.
     *
     * \return Size of read bits from input stream.
     */
    void readEncodedBits();

public:
    Decoder(::IOReader* reader);
    virtual ~Decoder();

    /*!
     * \brief Decode one symbol from input stream using probabilities
     *        stored in \c alphabet.
     *
     * \param alphabet - Probabilities distribution.
     * \param symbol - The symbol to be read from the input stream.
     *
     * \return Size of read bits from input stream.
     */
    int read(AlphabetInfo* alphabet);
};

// ===============================================================
// ================= Adaptive arithmetic coder ===================
// ===============================================================
class ACoder {
public:
    ACoder();
    virtual ~ACoder();

    /*!
     * \brief This function is used to updated probability range list
     *        after the effect of the symbol is applied.
     *
     *        Before probabilities are updated, they are rearanged
     *        by placing the more common symbols near the end of the
     *        list of ranges so update will not require on average
     *        ALPHABET_SIZE / 2 updates but less.
     *
     * \param alphabet - Probabilities distribution.
     * \param symbol - The symbol to be added to the current code range.
     */
    static void updateProbabilities(AlphabetInfo* alphabet, int symbol);

};

class AEncoder: public Encoder, public ACoder {
protected :
    void insert(AlphabetInfo* alphabet, int symbol);

public:
    AEncoder(::IOWriter* writer);
    virtual ~AEncoder();
};

class ADecoder: public Decoder, public ACoder {
protected :
    void insert(AlphabetInfo* alphabet, int symbol);

public:
    ADecoder(::IOReader* reader);
    virtual ~ADecoder();
};

// ===============================================================
// ================== Static arithmetic coder ====================
// ===============================================================
class SCoder {
public:
    SCoder();
    virtual ~SCoder();

    /*!
     *\brief This routine converts the ranges array containing only
     *       symbol counts to an array containing the upper and lower
     *       probability ranges for each symbol.
     *
     *\Effects Ranges array containing symbol counts in the upper field
     *         for each symbol is converted to a list of upper and lower
     *         probability bounds for each symbol.
     */
    static void symbolCountToProbabilityRanges(AlphabetInfo* alphabet);
};

class SEncoder: public Encoder, public SCoder {
protected:
    void insert(AlphabetInfo* alphabet, int symbol);

public:
    SEncoder(::IOWriter* writer);
    virtual ~SEncoder();

    /*!
     * \brief This routine reads the input file and builds the global
     *        list of upper and lower probability ranges for each
     *        symbol.
     *
     *        Each symbol is BIT_SIZE(alphabet->size) long in bits.
     *
     * \param alphabet - Probabilities distribution.
     * \param reader - Reader to read probabilities from.
     *
     * \Effects Ranges array is made to contain probability ranges for
     *          each symbol.
     *
     * \return EXCOM_ERR_OK for success.
     */
    static int buildProbabilityRangeList(AlphabetInfo* alphabet, ::IOReader* reader);

    /*!
     * \brief This function writes each symbol contained in the encoded
     *        file as well as its rescaled number of occurrences.  A
     *        decoding algorithm may use these numbers to reconstruct
     *        the probability range list used to encode the file.
     *
     * \param alphabet - Probabilities distribution.
     * \param writer - Writer to write header to.
     *
     * \Effects Symbol values and symbol counts are written to a file.
     * \return EXCOM_ERR_OK
     */
    static int writeHeader(AlphabetInfo* alphabet, ::IOWriter* writer);
};

class SDecoder: public Decoder, public SCoder {
protected:
    void insert(AlphabetInfo* alphabet, int symbol);

public:
    SDecoder(::IOReader* reader);
    virtual ~SDecoder();
    
    /*!
     * \brief This function reads the header information stored by
     *        \c writeHeader.  The header can then be used to build a
     *        probability range list matching the list that was used to
     *        encode the file.
     *
     * \param alphabet - Probabilities distribution.
     * \param reader - Reader to read header from.
     *
     * \Effects: Probability range list is built.
     *
     * \return EXCOM_ERR_OK if it was successfull, EXCOM_ERR_INCOMPLETE otherwise.
     */
    static int readHeader(AlphabetInfo* alphabet, ::IOReader* reader);
};
}
#endif	/* ARITMETICCODER_HPP */