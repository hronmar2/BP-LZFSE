#!/bin/sh

source 'common.sh'

#putline <corpus_file> <value> <tex_file> <tab_file> <orig_size>
putline() {
	t=`mintime <"$d/m$2_1.txt"`
	xt=`separators $t`
	o=`wc -c "$d/m$2" | grep -o '^[0-9][0-9]*'`
	rat=`echo "scale=7; $o / $5" | bc`
	xrat=`separators $rat`
	exc=`wc -c "$d/m$2.exc" | grep -o '^[0-9][0-9]*'`
	excrat=`echo "scale=7; ($o+$exc) / $5" | bc`
	xexcrat=`separators $excrat`
	printf '%22s%16s%16s%16s\\\\\n' "$2&" "$xt &" "$xrat &" "$xexcrat" >>"$3"
	printf '%22s%16s%16s%16s\n' "$2" "$t" "$rat" "$excrat" >>"$4"
}

# obtain sizes of the original corpus files to be able to compute ratio
size=`wc -c corpus/kennedy.xls | grep -o '^[0-9][0-9]*'`

out=dca_max_kennedy.xls.txt
tex=dca_max_kennedy.xls.tex
# write header
echo '\hline' >$tex
printf 'Max. antiword length &%16s%16s  Ratio inc. exceptions\\\\\n' "Time[\$\\mu\$s]&" "Ratio&" >>$tex
echo '\hline' >>$tex
printf 'Max. antiword length  %16s%16s  Ratio inc. exceptions\n' "Time[us]" "Ratio" >$out
echo -e "\n" >>$out

for max in `seq 4 4 20`; do
	putline kennedy.xls $max "$tex" "$out" $size
done

echo '\hline' >>$tex


