#include <unistd.h>
#include <cxxtest/TestSuite.h>
#include <excom.h>
#include "../../lib/method/mtf/mtf.hpp"
#include "../../lib/iomodule.hpp"

#define HANDLE 1
#define HANDLE_IO_IN 2
#define HANDLE_IO_OUT 3
#define HANDLE_TMP 4

#define DATA_ORIG "data_comp"
#define TMP_MTF "tmp_mtf"
#define TMP_UNMTF "tmp_unmtf"

class CompMTFTestSuite: public CxxTest::TestSuite {
	CompMTF *comp;
public:
	void setUp() {
		comp = new CompMTF(HANDLE);
	}
	void tearDown() {
		delete comp;
	}

	void testParameters() {
		int v;
		// 'mtf' compression module has no parameters and provides no values
		TS_ASSERT_EQUALS(comp->setParameter(0, NULL), EXCOM_ERR_PARAM);
		// but its superclass, CompModule, provides a value
		TS_ASSERT_EQUALS(comp->getValue(EXCOM_VALUE_COMP_RESULT, &v), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(v, EXCOM_ERR_LATER);
	}

	void testIOInputModules() {
		IOMemReader *reader = new IOMemReader(HANDLE_IO_IN);
		// invalid pointer to a module
		TS_ASSERT_EQUALS(comp->connectIOModule(NULL, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_MEMORY);
		// invalid connection type
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, -1), EXCOM_ERR_PARAM);
		// input module connected as an output module
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_DUPLICATE);
		delete reader;
	}

	void testIOOutputModules() {
		IOMemWriter *writer = new IOMemWriter(HANDLE_IO_OUT);
		// output module connected as an input module
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_PARAM);
		// correct connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);
		// duplicate connection
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_DUPLICATE);
		delete writer;
	}

	void compareFiles(const char *a, const char *b)
	{
		long sizeA, sizeB;
		FILE *A, *B;
		unsigned char *dataA = NULL, *dataB = NULL;
		A = fopen(a, "rb");
		TS_ASSERT_DIFFERS(A, (FILE*)NULL);
		fseek(A, 0, SEEK_END);
		sizeA = ftell(A);
		fseek(A, 0, SEEK_SET);
		dataA = new unsigned char[sizeA];
		TS_ASSERT_EQUALS(fread(dataA, sizeA, 1, A), 1u);
		fclose(A);

		B = fopen(b, "rb");
		TS_ASSERT_DIFFERS(B, (FILE*)NULL);
		fseek(B, 0, SEEK_END);
		sizeB = ftell(B);
		fseek(B, 0, SEEK_SET);
		TS_ASSERT_EQUALS(sizeA, sizeB);
		if (sizeA == sizeB) {
			dataB = new unsigned char[sizeB];
			TS_ASSERT_EQUALS(fread(dataB, sizeB, 1, B), 1u);
			TS_ASSERT_SAME_DATA(dataA, dataB, sizeA);
		}
		fclose(B);

		if (dataA != NULL) delete dataA;
		if (dataB != NULL) delete dataB;
	}

	void testCompression() {
		unsigned v;

		// input and output modules
		IOFileReader *reader = new IOFileReader(HANDLE_IO_IN);
		IOFileWriter *writer = new IOFileWriter(HANDLE_IO_OUT);

		TS_ASSERT_EQUALS(reader->openFile(DATA_ORIG), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile(TMP_MTF), EXCOM_ERR_OK);

		// running shouldn't succeed without an input I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		// ...or without output I/O module...
		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_CHAIN);
		TS_ASSERT_EQUALS(comp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(comp->run(), EXCOM_ERR_OK);

		delete reader;
		delete writer;

		// decompression
		CompMTF *decomp = new CompMTF(HANDLE_TMP);
		reader = new IOFileReader(HANDLE_IO_IN);
		writer = new IOFileWriter(HANDLE_IO_OUT);

		TS_ASSERT_EQUALS(reader->openFile(TMP_MTF), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(writer->openFile(TMP_UNMTF), EXCOM_ERR_OK);

		TS_ASSERT_EQUALS(decomp->setOperation(EXCOM_OP_DECOMPRESS), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(reader, EXCOM_CONN_DATA_INPUT), EXCOM_ERR_OK);
		TS_ASSERT_EQUALS(decomp->connectIOModule(writer, EXCOM_CONN_DATA_OUTPUT), EXCOM_ERR_OK);

		// run the decompression
		TS_ASSERT_EQUALS(decomp->run(), EXCOM_ERR_OK);

		// check, whether output data matches input original
		compareFiles(TMP_UNMTF, DATA_ORIG);

		delete reader;
		delete writer;
		delete decomp;

		// delete temporary files
		TS_ASSERT_EQUALS(unlink(TMP_MTF), 0);
		TS_ASSERT_EQUALS(unlink(TMP_UNMTF), 0);
	}

};


