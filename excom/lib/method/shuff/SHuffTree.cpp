/*
 * File:       method/shuff/SHuffTree.cpp
 * Purpose:    Declaration of the Static Huffman coding method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Static Huffman coding module linked to the ExCom library.
 *
 * Static Huffman coding module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Static Huffman coding module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Static Huffman coding module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file SHuffTree.cpp
 *  \brief Concrete implementation of a static Huffman tree structure.
 *
 *  This class serves for a static Huffman tree representation and its manipulation
 */


#include <algorithm>
#include <iostream>
#include <stdlib.h>
#include <string.h>

#include "SHuffTree.h"
#include "SHuffNode.h"

SHuffTree::SHuffTree() {
	leavesCount = 0;
	universalCodes = new UniversalCodes();
	err = false;
	symbolFrequency = new unsigned long[ALPHABET_SIZE];
	memset(symbolFrequency, 0, ALPHABET_SIZE * sizeof(unsigned long));

	leaves = new SHuffNode*[ALPHABET_SIZE];
	memset(leaves, 0, ALPHABET_SIZE * sizeof(SHuffNode*));
}

SHuffTree::~SHuffTree() {	

	for (nodeIter = nodes.begin(); nodeIter != nodes.end(); nodeIter++) {
		delete *nodeIter;
		*nodeIter = NULL;				
	}

	delete universalCodes;
	universalCodes = NULL;

	delete symbolFrequency;
	symbolFrequency = NULL;

	for(int i = 0; i < ALPHABET_SIZE; i++) {
		delete leaves[i];
		leaves[i] = NULL;
	}
	delete leaves;
	leaves = NULL;
}

void SHuffTree::addNode(SHuffNode* node) {
	nodeQueue.push(node);
	leaves[node->getSymbol()] = node;
	leavesCount++;
}

bool SHuffTree::buildTree() {
	SHuffNode* internalNode = NULL;
	SHuffNode* shn = NULL;

	unsigned int symbols = 0;
	// Add all symbols with their frequencies to a queue
	for(int i = 0; i < ALPHABET_SIZE; i++) {
		unsigned long frequency = symbolFrequency[i];
		if(frequency > 0) {
			shn = new SHuffNode((unsigned char) i, frequency);
			addNode(shn);	
			symbols++;
		}
	}

	if(symbols == 0) {
		return false;
	}

	// If only one node is available, attach it to the root
	if (nodeQueue.size() == 1) {
		SHuffNode* node = nodeQueue.top();
		nodeQueue.pop();

		internalNode = new SHuffNode();

		internalNode->setLeftChild(node);
		node->setParent(internalNode);

		nodeQueue.push(internalNode);
	} else {
		// Repeat the same until only root remains
		while (nodeQueue.size() > 1) {
			SHuffNode* node0 = nodeQueue.top();
			nodeQueue.pop();

			SHuffNode* node1 = nodeQueue.top();
			nodeQueue.pop();

			internalNode = new SHuffNode(node0, node1);
			nodes.push_back(internalNode);
	
			node0->setParent(internalNode);
			node1->setParent(internalNode);

			nodeQueue.push(internalNode);
		} 
	}

	root = nodeQueue.top(); 

	setSymbolCodes();
	
	return true;
}

// Assign codes for faster retrieval
void SHuffTree::setSymbolCodes() {
	for(int i = 0; i < ALPHABET_SIZE; i++) {
		SHuffNode* node = leaves[i];	
		if(node != NULL) {
			unsigned int length = 0;
			unsigned int code = getCode(node, length);
			node->setCode(code);
			node->setLength(length);
		}
	}
}

string SHuffTree::traversePreorder(SHuffNode* node) {
	string code = "";

	// Internal node
	if ((node->getLeftChild() != 0) || (node->getRightChild() != 0)) {
		code += INTERNAL_NODE_CODE; 
		if (node->getLeftChild() != 0) code += traversePreorder(node->getLeftChild());     
		if (node->getRightChild() != 0) code += traversePreorder(node->getRightChild());
	// Leaf
	} else {
		code = code + LEAF_NODE_CODE + universalCodes->getBeta(node->getSymbol(), BITS_PER_SYMBOL);
	}

	return code;
}

void SHuffTree::addSymbol(unsigned char s) {
	symbolFrequency[s] = symbolFrequency[s] + 1;
}

// Traverse the tree to obtain the code for a given node
unsigned int SHuffTree::getCode(SHuffNode* node, unsigned int &length) {
	length = 0;
	unsigned int bit = 0;
	unsigned int code = 0;
	while(node != root) {
		if (node->getParent()->getLeftChild() == node) {
			bit = 0;
		} else {
			bit = 1;
		}
		code = code + (bit << length);
		node = node->getParent();
		length++;
	}
	return code;
}

// Reconstruct the Huffman tree from a bit stream
void SHuffTree::reconstructTree(string bits) {
	int x;
	int index;

	SHuffNode* actualNode;
	SHuffNode* newNode;

	if (bits.size() != 0) {
		if (bits[0] == '0') {
			actualNode = new SHuffNode;
			root = currentNode = actualNode;
			index = 1;

			nodes.push_back(actualNode);

			while (index < bits.size()) {
				if (bits[index] == '0') {
					newNode = new SHuffNode;
					newNode->setParent(actualNode);

					nodes.push_back(newNode);

					if (actualNode->getLeftChild() == 0) {
						actualNode->setLeftChild(newNode);
						actualNode = newNode;
					} else if (actualNode->getRightChild() == 0) {
						actualNode->setRightChild(newNode);
						actualNode = newNode;
					// Traverse up in the tree
					} else {
						x = 0;  

						while ((actualNode->getLeftChild() != 0) && (actualNode->getRightChild() != 0)) {
							if (actualNode->getParent() != 0) {
								actualNode = actualNode->getParent();	
								x++;
							} else {
								err = true;
								return;
							}
						}
					}

					index++;
				} else {
					newNode = new SHuffNode;
					newNode->setParent(actualNode);
					newNode->setSymbol((unsigned char) universalCodes->binToDec(bits.substr(index+1, 8)));

					addNode(newNode);

					if (actualNode->getLeftChild() == 0) {
						actualNode->setLeftChild(newNode);
					} else if (actualNode->getRightChild() == 0) {
						actualNode->setRightChild(newNode);
						x = 0;

						while ((actualNode->getLeftChild() != 0) && (actualNode->getRightChild() != 0) && (actualNode != root)) {
							if (actualNode->getParent() != 0) {
								actualNode = actualNode->getParent();	
								x++;
							} else {
								err = true;
								return;
							}
						}
					} 

					index = index + 8 + 1;
				}
			}
		}
	}
}

string SHuffTree::getTreeAsString() {
	return universalCodes->getBeta(leavesCount-1, 8)
	  +traversePreorder(root);
}

unsigned int SHuffTree::getSymbolCode(unsigned char s, unsigned int &length) {
	SHuffNode* node = leaves[s];
	length = node->getLength();
	return node->getCode();
}

bool SHuffTree::isLeaf(int i) {
	if ((currentNode->getRightChild() != 0) && (currentNode->getLeftChild() != 0)) {
		currentNode = (i == 1) ? currentNode->getRightChild() : currentNode->getLeftChild();
		return (currentNode->getSymbol() != -1);
	} else {
		err = true;
		return true;
	}
}

bool SHuffTree::isErr() {
	return err;
}

unsigned char SHuffTree::getActualNodeSymbol() {
	return currentNode->getSymbol();
}

void SHuffTree::traverseUp() {
	currentNode = root;
}
