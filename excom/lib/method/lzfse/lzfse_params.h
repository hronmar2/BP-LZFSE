#ifndef LZFSE_PARAMS_H
#define LZFSE_PARAMS_H

/*
 * File:       method/lzfse/lzfse_params.h
 * Purpose:    Declaration of parameters for the LZFSE compression method
 * Author:     Martin Hron <hronmar2@fit.cvut.cz>, 2018
 *             bachelor thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Bachelor thesis leader: Ing. Jan Baier
 *
 * Copyright (C) 2018 Martin Hron
 *
 * This file is part of LZFSE coding module linked to the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZFSE module or with the ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file lzfse_params.h
 *  \brief Declaration of parameters for the LZFSE compression method.
 *
 *  Structure(s) declared in this file replaces preprocessor constants defined in the lzfse/lzfse_tunables.h
 *  and allows passing parameters to the LZFSE method.
 */

/*! \brief Parameters for the LZFSE compression.
 *
 *  This structure defines parameters for the LZFSE compression replacing preprocessor constants from tunables.h.
 *  Pass this structure to lzfse_encode_buffer function to set compression parameters.
 */
typedef struct {
    //! Match length in bytes to cause immediate emission. 
    /*! This parameter replaces the LZFSE_ENCODE_GOOD_MATCH preprocessor constant. */
    unsigned int good_match;

    //! Number of bits for hash function to produce. 
    /*! This parameter replaces the LZFSE_ENCODE_HASH_BITS preprocessor constant. 
     *  Its value should be in the range [10, 16]. 
     */
    unsigned int hash_bits;
} lzfse_encode_parameters;

#endif /* LZFSE_PARAMS_H */

