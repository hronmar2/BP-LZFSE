#ifndef _SHANNON_FANO_TREE_H
#define	_SHANNON_FANO_TREE_H

/*
 * File:       method/sfano/ShannonFanoTree.h
 * Purpose:    Declaration of Shannon-Fano statistical compression method
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of Shannon-Fano module linked to the ExCom library.
 *
 * Shannon-Fano module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Shannon-Fano module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Shannon-Fano module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file ShannonFanoTree.h
 *  \brief Declaration of Shannon-Fano tree representation.
 *
 *  This purpose of this class is to provide structure to easily handle with
 *  Shannon-Fano tree, i.e., to insert new symbols, obtain their binary codes,
 *  and traverse the whole tree. 
 */

#include "ShannonFanoNode.h"
#include "../universal_codes/UniversalCodes.h"

#include <map>
#include <vector>

#define INTERNAL_NODE_CODE "0"
#define LEAF_NODE_CODE "1"
#define BITS_PER_SYMBOL 8

using namespace std;

class ShannonFanoTree {
private:
	vector<ShannonFanoNode*> nodes;
	vector<ShannonFanoNode*>::iterator nodeIter;	

	map<unsigned char, ShannonFanoNode*> leaves;  //!< Only for the leaf nodes
	map<unsigned char, ShannonFanoNode*>::iterator iterNodes;

	map<unsigned char, unsigned long> symbolFrequency;
	map<unsigned char, unsigned long>::iterator iterSymbols;

	vector<ShannonFanoNode*> treeNodes;  //!< To store all nodes

	unsigned long leavesCount;	
	unsigned long nodesCount;	

	bool err;

	ShannonFanoNode* root;
	ShannonFanoNode* currentNode;
	ShannonFanoNode* newNode;
	ShannonFanoNode* actualNode;

	UniversalCodes* universalCodes;

	/*! \brief Inserts an internal node to the tree
	 *  \param node Pointer to a Shannon-Fano node
	 */
	void addNode(ShannonFanoNode* node); 
	/*! \brief Inserts a leaf to the tree
	 *  \param node Pointer to a Shannon-Fano node
	 */
	void addSymbolNode(ShannonFanoNode* node);
	/*! \brief Creates recursively a Shannon-Fano tree
	 *  \param node Pointer to a node
	 *  \param x Left index of range where to split nodes
	 *  \param y Right index of range where to split nodes
	 */
	void createTree(ShannonFanoNode* node, int x, int y);
	/*! \brief Assigns the codes to all input symbols
	 * 
	 *  The codes are obtained by a tree traversal
	 */
	void setSymbolCodes();
	/*! \brief Sort all leaves using a built-in sorting mechanism
	 */
	void sortNodes();

	/*! \brief Finds the node where to split the actual set
	 *  \param x Left index of range where to split nodes
	 *  \param y Right index of range where to split nodes
	 *  \return The split point = index of node
	 *
	 *  This function determines a node which is just in the middle of both parts
	 *  with a view to be both part nearly the same in a sense of sum of their
	 *  frequencies
	 */
	int splitNodes(int x, int y);	
	/*! \brief Counts a sum of frequencies in a given range of nodes
	 *  \param x Left index of range where to split nodes
	 *  \param y Right index of range where to split nodes
	 *  \return The sum of frequencies
	 */
	long getFreqSum(int x, int y);

	/*! \brief Returns a binary code for a particular node
	 *  \param node Pointer to a node
	 *  \return The binary codeword as a string
	 */
	string getCode(ShannonFanoNode* node);
	/*! \brief Traverses the whole Shannon-Fano tree
	 *  \param node Pointer to node where to split the tree
	 *  \return The Shannon-Fano tree as a string
	 *
	 *  This function traverses the tree in preorder, i.e., starts at the root
	 *  and continues to visit all nodes and thus to save it in a binary form,
	 *  which would be easy to reconstruct it again
	 */
	string traversePreorder(ShannonFanoNode* node); 	
public:
	// Constructor
	ShannonFanoTree();

	// Destructor
	~ShannonFanoTree();

	/*! \brief Adds a symbol to the tree
	 *  \param s A symbol to be added
	 *
	 *  This function checks whether the symbol was previously encountered
	 *  if yes, its frequency is increased, otherwise the symbol is added	 
	 */
	void addSymbol(unsigned char s);
	/*! \brief Sets the root as a current node
	 *
	 *  This function is called when a leaf during the traversal is reached
	 *  when the decoder reads a bit by bit with a view to find the match
	 *  for a codeword. In other words, it is some kind of reset 
	 */
	void traverseUp();
	/*! \brief Reconstructs the Shannon-Fano tree
	 *  \param s A tree in binary form as a string
	 *
	 *  This function reconstructs a tree to obtain the symbol codes
	 *  however the frequencies of occurrence are not retrieved due
	 *  to saving more space	 
	 */
	bool reconstructTree(string s);

	/*! \brief Checks if the current node is a leaf
	 *
	 *  This function is called when a leaf during the traversal is reached
	 *  when the decoder reads a bit by bit with a view to find the match
	 *  for a codeword. In other words, it is some kind of reset 
	 */
	bool isLeaf(int);
	/*! \brief Generates a tree from the input symbols
	 */
	bool buildTree();
	/*! \brief Returns true if some error occurs
	 */
	bool isErr();

	/*! \brief Returns a code for the current node in a tree
	 */
	unsigned char getActualNodeSymbol();

	/*! \brief A wrapper function for a tree traversal
	 *
	 *  Preorder traversal, 0=internal node, 1=leaf + its code
	 */
	string getTreeAsString();
	/*! \brief Returns a code of a node for a given symbol
	 *  \param s A symbol
	 */
	string getSymbolCode(unsigned char s);
};

#endif	/* _SHANNON_FANO_TREE_H */
