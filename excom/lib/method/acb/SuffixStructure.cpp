/*
 * File:       method/acb/SuffixStructure.cpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "SuffixStructure.hpp"

SuffixStructure::SuffixStructure(){
	data = NULL;
	dataSize = 0;
	bufferSize = 0;
	maxSize = 0;
        currentSize = 0;
        checkRatioSize = 0;
	maxContentLength = 0;
};

SuffixStructure::~SuffixStructure(){
#if SUFFIX_STRUCTURE_PERF
	printMethodStatistics();
#endif
        if(data != NULL) {
            data -= 2;
            delete [] data;
            data = NULL;
        }
};

unsigned int SuffixStructure::search(long position, int &distance, unsigned int &contentLength, int &zzzLength) {
	search(position, distance, contentLength);
	return 0;
}

void SuffixStructure::addData(unsigned char character) {
#if SUFFIX_STRUCTURE_PERF
	addDataNewCharacterCount++;
	clock_t method_begin = clock ();
#endif
        // +1 new character +2 begin and end byte
	if(this->bufferSize < this->dataSize + 1 + STRUCTURE_SIZE_EXTENSION) {
		addData(&character, 1);
	} else {                
		data[this->dataSize] = character;
		this->dataSize++;
                data[this->dataSize] |= BUFFER_SENTINEL_VALUE;
	}
#if SUFFIX_STRUCTURE_PERF
	addDataNewCharacterTime += (clock() - method_begin);
#endif
}

void SuffixStructure::addData(unsigned char* data, unsigned long size) {
#if SUFFIX_STRUCTURE_PERF
	addDataCount++;
	clock_t method_begin = clock ();
#endif
	assertBufferSize(size);

	long from = 0;
	long newSize = this->dataSize + size;
	for(long to = this->dataSize; to < newSize; to++) {
		this->data[to] = (uint16_t) data[from];
		from++;
	}

	this->dataSize += size;
        // Set data hard stop after last added character
        this->data[this->dataSize] |= BUFFER_SENTINEL_VALUE;
        
#if SUFFIX_STRUCTURE_PERF
	addDataTime += (clock() - method_begin);
#endif
}

void SuffixStructure::addData(unsigned long content, unsigned long size) {
#if SUFFIX_STRUCTURE_PERF
	addDataCount++;
	clock_t method_begin = clock ();
#endif
	assertBufferSize(size);

        this->data[this->dataSize] &= 0xFF;

        // memcpy cannot be used as it does not copy bytes
        // it does not everwrite data on overlapping buffers
        // memcpy(this->data + this->dataSize, this->data + content, size * BUFFER_CELL_SIZE);

        for(int i = 0; i < size; i++) {
            this->data[this->dataSize + i] = this->data[content + i];
        }

        this->dataSize += size;
        // Set data hard stop after last added character
        this->data[this->dataSize] |= BUFFER_SENTINEL_VALUE;
#if SUFFIX_STRUCTURE_PERF
	addDataTime += (clock() - method_begin);
#endif
}

unsigned char* SuffixStructure::getData() {
#if SUFFIX_STRUCTURE_PERF
	getDataCount++;
#endif
	unsigned char* out = new unsigned char[this->dataSize];
	for(long i = 0; i < this->dataSize; i++) {
		out[i] = (unsigned char) this->data[i];
	}
	return out;
}

unsigned char SuffixStructure::getData(long position) {
#if SUFFIX_STRUCTURE_PERF
	getDataPositionCount++;
	clock_t method_begin = clock ();
	unsigned char out = (unsigned char) this->data[position];
	getDataPositionTime += (clock() - method_begin);
	return out;
#else
	return (unsigned char) this->data[position];
#endif
}

unsigned long SuffixStructure::getDataSize() {
#if SUFFIX_STRUCTURE_PERF
	getDataSizeCount++;
#endif
    return this->dataSize;
}

void SuffixStructure::setSize(unsigned int sizeInBits, unsigned int checkSizeBits) {
	this->maxSize = 1 << sizeInBits;
        this->checkRatioSize = 1 << checkSizeBits;
}

void SuffixStructure::setMaxContentLength(unsigned int length) {
	this->maxContentLength = length;
}

void SuffixStructure::setMaxDistance(unsigned int distance) {
    this->maxDistance = distance;
}

inline void SuffixStructure::assertBufferSize(unsigned long size) {
	unsigned long newDataSize = this->dataSize + size;	
	if(this->bufferSize < newDataSize + STRUCTURE_SIZE_EXTENSION) {
		if(this->bufferSize == 0) {
			this->bufferSize = 1 << 10;
			this->bufferSize += STRUCTURE_SIZE_EXTENSION;
		}
		this->bufferSize -= STRUCTURE_SIZE_EXTENSION;
		do {
			this->bufferSize = this->bufferSize << 1;
		} while(newDataSize >= this->bufferSize);
		this->bufferSize += STRUCTURE_SIZE_EXTENSION;
		uint16_t* buffer = new uint16_t[this->bufferSize];
		memset(buffer, 0, this->bufferSize * BUFFER_CELL_SIZE);
		// First and last "bytes" are empty for faster searches
		buffer += STRUCTURE_SIZE_EXTENSION - 1;
		if(this->data != NULL) {
			memcpy(buffer, this->data, this->dataSize * BUFFER_CELL_SIZE);
                        this->data -= 2;
                        delete [] this->data;
		}
		this->data = buffer;

                // Setting data hard stops
                this->data[-1] |= BUFFER_SENTINEL_VALUE;
                this->data[-2] |= BUFFER_SENTINEL_VALUE;
                this->data[this->bufferSize - STRUCTURE_SIZE_EXTENSION] |= BUFFER_SENTINEL_VALUE;
	}
}


bool SuffixStructure::isFull() {
	return IS_FULL;
}

bool SuffixStructure::isCheckCompressRatio() {
        return currentSize > checkRatioSize;
}

unsigned char* SuffixStructure::clear(long position, bool needData) {
        currentSize = 0;
        unsigned char* out = NULL;
        if(needData) {
            out = new unsigned char[position];
            for(long i = 0; i < position; i++) {
		out[i] = (unsigned char) this->data[i];
            }
        }

        // move unencoded data to position 0
        // may use memcpy as arrays do not overlap
        memcpy(this->data, this->data + position,
                (this->dataSize - position + 1) * BUFFER_CELL_SIZE);
        this->dataSize -= position;
        return out;
}

void SuffixStructure::printMethodStatistics() {
#if SUFFIX_STRUCTURE_PERF
	printf("  Method invocation times : total time [s], count [-]\n");
	printf("    addData				: %.3f s, %d\n",(double) addDataTime / CLOCKS_PER_SEC, addDataCount);
	printf("    addDataNewCharacter			: %.3f s, %d\n",(double) addDataNewCharacterTime / CLOCKS_PER_SEC, addDataNewCharacterCount);
	printf("    binarySearch			: %.3f s, %d\n",(double) binarySearchTime / CLOCKS_PER_SEC, binarySearchCount);
	printf("    getData				: %.3f s, %d\n",(double) getDataTime / CLOCKS_PER_SEC, getDataCount);
	printf("    getDataPosition			: %.3f s, %d\n",(double) getDataPositionTime / CLOCKS_PER_SEC, getDataPositionCount);
	printf("    getDataSize				: %.3f s, %d\n",(double) getDataSizeTime / CLOCKS_PER_SEC, getDataSizeCount);
	printf("    searchContent			: %.3f s, %d\n",(double) searchContentTime / CLOCKS_PER_SEC, searchContentCount);
	printf("    searchContext			: %.3f s, %d\n",(double) searchContextTime / CLOCKS_PER_SEC, searchContextCount);
	printf("    translate				: %.3f s, %d\n",(double) translateTime / CLOCKS_PER_SEC, translateCount);
	printf("    updateIndex				: %.3f s, %d\n",(double) updateIndexTime / CLOCKS_PER_SEC, updateIndexCount);
#endif
}
