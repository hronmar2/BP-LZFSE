/*
 * File:       method/integer_compression/fibonacci.cpp
 * Purpose:    Implementation of the Fibonacci code
 * Author:     Jan Baier <baierjan@fit.cvut.cz>, 2011
 *             diploma thesis at Faculty of Information technology,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2011 Jan Baier
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file fibonacci.cpp
 *  \brief Implementation of the Fibonacci code
 *
 */

#include "method.hpp"

Fibonacci::Fibonacci(BitBuffer& bb, unsigned param): Method(bb),order(param) {}

void Fibonacci::operator() (unsigned long data) {
	unsigned long fib_1 = 1;
	unsigned long fib_2 = 1;
	unsigned long fib_3 = fib_1 + fib_2;

	if (fibs.empty()) fibs.push_back(fib_2);

	while (fibs.back() <= data) {
		if (fibs.back() < fib_3) fibs.push_back(fib_3);
		fib_1 = fib_2;
		fib_2 = fib_3;
		fib_3 = fib_1 + fib_2;
	}

	std::string buffer = "1";
	bool first = true;
	std::vector<unsigned long>::reverse_iterator iter;
	for (iter = fibs.rbegin(); iter < fibs.rend(); iter++) {
		if (first && data < *iter) continue;
		if (data >= *iter) {
			buffer = "1" + buffer;
			data -= *iter;
			first = false;
		}
		else
			buffer = "0" + buffer;
	}
	bb.pushBits(buffer.c_str());
}

unsigned long Fibonacci::operator() () {
	unsigned long value = 0;

	unsigned long fib_1 = 1;
	unsigned long fib_2 = 1;
	unsigned long fib_3 = fib_1 + fib_2;
	if (fibs.empty()) fibs.push_back(fib_2);

	std::string buffer;
	unsigned long length = 0;
	bool bit;

	while (bb.getBit(bit)) {
		if (fibs.back() < fib_3) fibs.push_back(fib_3);
		fib_1 = fib_2;
		fib_2 = fib_3;
		fib_3 = fib_1 + fib_2;

		buffer += bit?'1':'0';
		length++;
		if (bit && length >= 2 && buffer[length-2] == '1') break;
	}

	for (unsigned long i = 0; i < length-1; i++)
		if (buffer[i] == '1') value += fibs[i];

	return value;
}

unsigned long Fibonacci::length(unsigned long data) {
	DummyBitBuffer dbb;
	Fibonacci fib(dbb, 2);
	fib(data);
	return dbb.bitsStored();
}

std::vector<unsigned long> Fibonacci::fibs;
