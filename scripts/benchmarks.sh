#!/bin/bash

# This script will run compression and decompression of given methods
# and measure the time taken by these operations and resulting 
# compression ratio. Each operation is run multiple times (this can be 
# set by argument, default is 50) and the minimum from measured times
# is taken for each method.
#
# This script will produce three CSV files:
#   - benchmarks_comp.csv    containing measured compression times
#   - benchmarks_decomp.csv  containing measured decompression times
#   - benchmarks_ratio.csv   containing measured compression ratios
#
# Time values are in microseconds. Ratio is output as a percentage.
#
#
# The script should be called from the directory where ExCom testing
# application is present (usually bin/src/app). The Prague Corpus files
# should be present in the same directory, by default under PragueCorpus 
# folder (this can be changed by -d argument).


########################################################################

# Default values for parameters

# List of methods. To change which methods will be tested,
# either modify this array or use -l option and give them as arguments.
# For list of methods implemented in ExCom, run: ./app -m?
methods=("lzfse" "lz77" "lz78" "lzap" "lzmw" "lzw" "lzy" "copy" "arith" "dhuff" "ppm" "rle_n" "sfano" "shuff")

# Number of times to run each operation.
repeatTimes=50

# This variable defines the directory containing tested files.
corpusDirectory="PragueCorpus"

# If this variable is set to true, the benchmarks will stop whenever
# any file produced by compressing and decompressing a file from corpus
# is not identical to the original (the method did not work correctly).
exit_on_failure=true

# Write messages about progress.
print_progress=true
print_progress_files=true

# Save compression ratio as percentage.
ratio_percentage=true

# Path to ExCom testing application. Relative to current directory.
app="app"

########################################################################


function usage {
cat <<EOM
Usage: ./$(basename "$0") [options] [-l method...]
Options: 
  -h      show help
  -r T    repeat each operation T times, default is: 50
  -f      do NOT exit on failure (when decompressed and original file differs)
  -s      silent mode - do NOT print any messages about progress
  -b      brief mode - do NOT print message for each file
  -d DIR  use files from DIR directory to perform benchmarks, default is: PragueCorpus
  -c APP  relative path to ExCom app, default is: app
  -n      do NOT use percentage for compression ratio (disable multiplication of ratios by 100)
  -l      list of methods will be given as arguments, for example:
          ./$(basename "$0") -l lzfse lz78 lzw
EOM
}


# Options parsing code

methods_list=false
while getopts ":hr:fsbd:c:nl" opt; do
  case $opt in
    h)
      usage
      exit 2
      ;;
    r)
      repeatTimes=${OPTARG}
      ;;
    f)
      exit_on_failure=false
      ;;
    s)
      print_progress=false
      print_progress_files=false
      ;;
    b)
      print_progress_files=false
      ;;
    d)
      corpusDirectory=${OPTARG}
      ;;
    c)
      app=${OPTARG}
      ;;
    n)
      ratio_percentage=false
      ;;
    l)
      methods_list=true
      ;;
    \?)
      echo "ERROR: Invalid option: -$OPTARG" >&2
      echo ""
      usage
      exit 1
      ;;
    :)
      echo "ERROR: Option -$OPTARG requires an argument." >&2
      echo ""
      usage
      exit 1
      ;;
  esac
done

shift $((OPTIND - 1))

if [ "$methods_list" = true ] ; then
	if [ "$#" -eq 0 ]; then
		echo "ERROR: -l option set but no methods given." >&2
		echo ""
		usage
		exit 1
	fi
	methods=( "$@" )
else
	if [ "$#" -ne 0 ]; then
		echo "WARNING: Unexpected number of parameters." >&2
	fi
fi

if [ ! -d "$corpusDirectory" ]; then
	echo "ERROR: Directory with corpus files not found!" >&2
	echo "Corpus files must either be in PragueCorpus subdirectory of the current folder or in any other location provided as a -d argument." >&2
	echo ""
	usage
    exit 1
fi

if [ ! -f "$app" ]; then
    echo "ERROR: ExCom app not found!" >&2
	echo "It must either be in current directory or specified as a value of -c parameter." >&2
	echo ""
	usage
    exit 1
fi


# Benchmarks

echo -n "method" | tee benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
for filename in "$corpusDirectory"/*; do
	echo -n ",${filename##*/}" | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
done
echo "" | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null


function cleanup {
	#remove temporary files
	rm -f tmp_file_compressed.bin tmp_file_decompressed.bin
}
trap cleanup EXIT

if [ "$print_progress_files" = true ] ; then
	shopt -s nullglob
	numFiles=("$corpusDirectory"/*)
	numFiles=${#numFiles[@]}
	numMethods=${#methods[@]}
	totalFiles=$(($numFiles * $numMethods))
	i=1
fi

#run benchmarks for all methods given in 'methods' array
for method in "${methods[@]}"; do
	if [ "$print_progress" = true ] ; then
		echo "Running benchmarks for $method method."
	fi
	echo -n "$method" | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
	for filename in "$corpusDirectory"/*; do
		if [ "$print_progress_files" = true ] ; then
			echo "[$i/$totalFiles] file: $filename"
			i=$((i+1))
		fi
		echo -n "," | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
		./"$app" -r "$repeatTimes" -t -m "$method" -i "$filename" -o tmp_file_compressed.bin 2>/dev/null | grep '^[0-9]' | awk 'NR == 1 || $3 < min {min = $3}END{print min}' | xargs echo -n | tee -a benchmarks_comp.csv >/dev/null
		./"$app" -r "$repeatTimes" -d -t -m "$method" -i tmp_file_compressed.bin -o tmp_file_decompressed.bin 2>/dev/null | grep '^[0-9]' | awk 'NR == 1 || $3 < min {min = $3}END{print min}' | xargs echo -n | tee -a benchmarks_decomp.csv >/dev/null

		if ! cmp -s "$filename" tmp_file_decompressed.bin; then
			echo "$method: The decompressed file is not identical to the original file ($(basename "$filename"))!" >&2
			echo -n "?" | tee -a benchmarks_ratio.csv >/dev/null
			if [ "$exit_on_failure" = true ] ; then
				exit 1
			fi
			continue
		fi

		origsize="$(wc -c <tmp_file_decompressed.bin)"
		compsize="$(wc -c <tmp_file_compressed.bin)"
		
		if [ "$ratio_percentage" = true ] ; then
			echo -n $(echo "scale=2; 100*$compsize/$origsize " | bc) | tee -a benchmarks_ratio.csv >/dev/null
		else
			echo -n $(echo "scale=4; $compsize/$origsize " | bc) | tee -a benchmarks_ratio.csv >/dev/null
		fi

	done
	echo "" | tee -a benchmarks_comp.csv benchmarks_decomp.csv benchmarks_ratio.csv >/dev/null
done

if [ "$print_progress" = true ] ; then
	echo "Finished running benchmarks. The output was written to files:"
	echo "benchmarks_comp.csv   - compression times (in microseconds)"
	echo "benchmarks_decomp.csv - decompression times (in microseconds)"
	if [ "$ratio_percentage" = true ] ; then
		echo "benchmarks_ratio.csv  - compression ratios (as percentage)"
	else
		echo "benchmarks_ratio.csv  - compression ratios"
	fi
fi

