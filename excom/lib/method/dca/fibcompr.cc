#include "fibcompr.h"

unsigned int fibArray[FIBSIZE];
unsigned int fibIndex[FIBINDEXSIZE];
//double fibKoef;

void fibInit()
{
	int a = 1;
	int b = 1;
	for (int i = 0; i < FIBSIZE / 2; i++) {
		fibArray[2*i] = a;
		fibArray[2*i+1] = b;
		a = a + b;
		b = a + b;
	}
	int ind = 2;
	for (unsigned int i = 0; i < FIBINDEXSIZE; i++) {
		if (i + 1 >= fibArray[ind])
			ind++;
		fibIndex[i] = ind - 1;
	}
}

unsigned int fibDecompress(unsigned int num, unsigned int size)
{
	unsigned int* fa = &fibArray[size-1];
	unsigned int res = 0;
	if (size > FIBSIZE)
		return 0;
	for (int i = size - 1; i > 0; i--, fa--) {
		num = num >> 1;
		if (num & 1)
			res += *fa;
	}
	res--; // zero shift
	return res;
}

unsigned int fibCompress(unsigned int num, unsigned int* size)
{
	int pos = 0;
	num++; // we need to shift to code also "0"

	if (num < FIBINDEXSIZE)
		pos = fibIndex[num-1];
	else {
//#if 1
		int lo = 0;
		int hi = FIBSIZE - 1;
		while (lo <= hi) {
			pos = (hi + lo) / 2;
			if (fibArray[pos] == num) {
				hi = lo = pos - 1;
				break;
			} else if (fibArray[pos] > num) {
				hi = pos - 1;
			} else {
				lo = pos + 1;
			}
		}
		if (lo == FIBSIZE)
			return 0;
		if (fibArray[pos] > num)
			pos--;
/*#else
	        pos = lrint(floor(log(num)/log(fibKoef)));
#endif*/
	}

	unsigned int* fa = &fibArray[pos];
	*size = pos + 1;
	unsigned int bor = 1 << pos;
	unsigned int out = bor;
	for (int i = pos; i > 0; i--) {
		if (*fa <= num) {
			num -= *fa;
			out = (out >> 1) | bor;
		} else {
			out = out >> 1;
		}
		fa--;
	}
	return out;
}

