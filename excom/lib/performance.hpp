#ifndef __PERFORMANCE_HPP__
#define __PERFORMANCE_HPP__

/*
 * File:       performance.hpp
 * Purpose:    Declaration of variables and methods required for performance measurement
 * Author:     Filip Simek <simekf1@fel.cvut.cz>, 2009
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2009 Filip Simek
 *
 * This file is part of the ExCom library.
 *
 * ExCom library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ExCom library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ExCom library. If not, see <http://www.gnu.org/licenses/>.
 */

/*! \file performance.hpp
 *  \brief Declaration of variables and methods that are required for performance measurement
 *
 *  When performance measurement is enabled during compilation of the ExCom library (i.e.
 *  macro PERF_MON is defined), compression modules and compression chains have some extra
 *  member variables and methods for performance monitoring. This file declares such variables
 *  and methods in one place, so that it's easier to change these if the measurement platform changes.
 */

#include <time.h>

#include <excom.h>

/*! Member variables that are added to classes which should provide performance measurement */
#define PERF_VARIABLES \
	struct timespec ts_start, ts_end;

/*! Methods that are added to classes which provide performance measurement to access their private member variables */
#define PERF_ACCESSORS \
	int getPerformanceData(\
		unsigned long *time\
	) {\
		if (time == NULL) return EXCOM_ERR_MEMORY;\
		/* time is in microseconds */\
		*time = (ts_end.tv_sec * 1000000 + ts_end.tv_nsec / 1000) -\
			(ts_start.tv_sec * 1000000 + ts_start.tv_nsec / 1000);\
		return EXCOM_ERR_OK;\
	}

#define PERF_INIT \
	ts_start.tv_sec = 0;\
	ts_start.tv_nsec = 0;\
	ts_end.tv_sec = 0;\
	ts_end.tv_nsec = 0;

#define PERF_START \
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &this->ts_start);
	
#define PERF_STOP \
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &this->ts_end);


#endif // ifndef __PERFORMANCE_HPP__

