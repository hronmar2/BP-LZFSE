/*
 * File:       method/lzw/lzw.cpp
 * Purpose:    Implementation of the LZW compression methods
 * Author:     Jakub Reznicek <reznijak@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Jakub Reznicek
 *
 * This file is part of LZW compression module linked to the ExCom library.
 *
 * LZW compression method is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LZW compression method is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with LZW module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

/*! \file lzw.hpp
 *  \brief Implementation of the LZW compression method
 */

#include "lzw.hpp"
#include "../universal_codes/UniversalCodes.h"

#include <iostream>
#include <utility>
#include <queue>
#include <algorithm>

using namespace std;

CompLZW::CompLZW(unsigned int handle) : 
	CompModule(handle), 
	reader(NULL), 
	writer(NULL), 
	dict_capacity(DEFAULT_DICT_SIZE)
{
	char_bitsize = DEFAULT_CHAR_BITSIZE;
	readSymbol = &CompLZW::readByte;
	writeSymbol = &CompLZW::writeByte;	
	root_size = 1 << DEFAULT_CHAR_BITSIZE;
	decodedString.reserve(512);
}

int CompLZW::connectIOModule(IOModule *module, int connection_type)
{
	int res;
	switch (connection_type) {
	case EXCOM_CONN_DATA_INPUT:
		if (reader != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_INPUT);
		if (res != EXCOM_ERR_OK) return res;
		reader = (IOReader*) ioGeneralToParticular(module, IO_VAR_READER);
		break;
	case EXCOM_CONN_DATA_OUTPUT:
		if (writer != NULL) return EXCOM_ERR_DUPLICATE;
		res = ioCheckConnection(module, __EXC_CONN_OUTPUT);
		if (res != EXCOM_ERR_OK) return res;
		writer = (IOWriter*) ioGeneralToParticular(module, IO_VAR_WRITER);
		break;
	default:
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZW::setOperation(enum exc_oper_e _operation)
{
	if (_operation == EXCOM_OP_COMPRESS || \
	    _operation == EXCOM_OP_DECOMPRESS) {
		operation = _operation;
		return EXCOM_ERR_OK;
	} else {
		return EXCOM_ERR_OPERATION;
	}
}

int CompLZW::setParameter(int parameter, void *value)
{
	if (value == NULL) 
		return EXCOM_ERR_MEMORY;

	unsigned int u = *((unsigned int*)value);

	switch (parameter) {
	case EXCOM_PARAM_LZW_DICTIONARY_SIZE:
		if (u < (1 << char_bitsize) || u == ~0u) 
			return EXCOM_ERR_VALUE;
		dict_capacity = u;
		break;
	case EXCOM_PARAM_LZW_CHAR_BITSIZE:
		if (u == 0 || u > 31 || dict_capacity < (1 << u))
			return EXCOM_ERR_VALUE;
		if ((char_bitsize = (unsigned char)u) == 8) {
			readSymbol = &CompLZW::readByte;
			writeSymbol = &CompLZW::writeByte;
		}	
		else {
			readSymbol = &CompLZW::readNBits;
			writeSymbol = &CompLZW::writeNBits;
		}
		root_size = 1 << char_bitsize;
		break;
	default:
		// invalid parameter
		return EXCOM_ERR_PARAM;
	}
	return EXCOM_ERR_OK;
}

int CompLZW::getValue(int parameter, void *value)
{
	unsigned int u;
	switch (parameter) {
	case EXCOM_VALUE_LZW_DICTIONARY_SIZE:
		u = dict_capacity;
		break;
	case EXCOM_VALUE_LZW_CHAR_BITSIZE:
		u = char_bitsize;
		break;
	default:
		// delegate to superclass for common parameters
		return CompModule::getValue(parameter, value);
	}
	if (value == NULL) return EXCOM_ERR_MEMORY;
	*((unsigned int*)value) = u;
	return EXCOM_ERR_OK;
}

int CompLZW::checkConnection()
{
	if (reader == NULL || writer == NULL) return EXCOM_ERR_CHAIN;
	return EXCOM_ERR_OK;
}

int CompLZW::requiredBits(unsigned int value) {
  int bits = 1;
  while ((value >>= 1) > 0) bits++;
  return bits; // '0' needs 1 bit to represent
}

void CompLZW::initializeDictionary(int operation) {
	switch (operation) {
		case OPERATION_COMPRESS:
			encDict.clear();
			encDict.reserve(dict_capacity);

			lzw_node_enc_t lzwEncNode;
			lzwEncNode.first = ~0u;
			lzwEncNode.left = ~0u;
			lzwEncNode.right = ~0u;
			lzwEncNode.prefixIndex = ~0u;

			while (encDict.size() < root_size) {
				lzwEncNode.symbol = (unsigned char)encDict.size();
				encDict.push_back(lzwEncNode);
			}

			break;
		case OPERATION_DECOMPRESS:
			decDict.clear();
			decDict.reserve(dict_capacity);

			lzw_node_dec_t lzwDecNode;
			lzwDecNode.prefixIndex = ~0u;

			while (decDict.size() < root_size) {
				lzwDecNode.symbol = (unsigned char)decDict.size();
				decDict.push_back(lzwDecNode);
			}

			break;
	}
	bitsize = char_bitsize;
	threshold = 1 << char_bitsize;
}

// The code is based on an idea of Juha Nieminen
// http://warp.povusers.org/EfficientLZW/ 
unsigned CompLZW::add(lzw_node_enc_t &node) {
	unsigned index = encDict[node.prefixIndex].first;

	if (index == ~0u) {
		encDict[node.prefixIndex].first = encDict.size();
	} else {
		while (true) {
			if (encDict[index].symbol == node.symbol) 
				return index;

			if (encDict[index].symbol < node.symbol) {
				if (encDict[index].right == ~0u) {
					encDict[index].right = encDict.size();
					break; 
				}
				index = encDict[index].right;
			} else {
				if (encDict[index].left == ~0u) {
					encDict[index].left = encDict.size();
					break; 
				}        
				index = encDict[index].left;		
			}
		}
	}

	return ~0u;
}

unsigned CompLZW::find(lzw_node_enc_t &node) {
	unsigned index = encDict[node.prefixIndex].first;

	if (index != ~0u) {
		while (true) {
			if (encDict[index].symbol == node.symbol) 
				return index;

			if (encDict[index].symbol < node.symbol) {
				if (encDict[index].right == ~0u) {
					break; 
				}
				index = encDict[index].right;
			} else {
				if (encDict[index].left == ~0u) {
					break; 
				}        
				index = encDict[index].left;		
			}
		}
	}

	return ~0u;
}

bool CompLZW::searchForNode(lzw_node_enc_t& node) {
	unsigned index;
	if (encDict.size() == dict_capacity) 
		index = find(node);
	else index = add(node);

	if (index != ~0) {
		// Phrase was found
		node.prefixIndex = index;

		return true;
	}

	return false;
}

bool CompLZW::getCode(unsigned index) {
	decodedString.clear();

	if ((decDict[index].prefixIndex != index) 
		&& (decDict[index].prefixIndex < decDict.size() || decDict[index].prefixIndex == ~0u)) {
		while (index != ~0u) {
			const lzw_node_dec_t& node = decDict[index];
			decodedString.push_back(node.symbol);
			index = node.prefixIndex;
		}
		return true;		
	} else {
		return false;
	}
}

int CompLZW::runCompress() {
	bool matchFound;

	unsigned long previousSymbol;
	unsigned long currentSymbol;

	lzw_node_enc_t lzwNode;

	initializeDictionary(OPERATION_COMPRESS);

	if ((this->*readSymbol)(&previousSymbol) != EOF) {
		while ((this->*readSymbol)(&currentSymbol) != EOF) {
			lzwNode.symbol = (unsigned)currentSymbol;
			lzwNode.prefixIndex = (unsigned)previousSymbol;
			lzwNode.first = ~0u;
			lzwNode.right = ~0u;
			lzwNode.left = ~0u;

			if (searchForNode(lzwNode)) { // Match found
				previousSymbol = lzwNode.prefixIndex;
			} else {
				writer->writeNBits(bitsize, previousSymbol);

				if (encDict.size() == dict_capacity) {
					initializeDictionary(OPERATION_COMPRESS);
				}
				else {
					if (encDict.size() == threshold) {
						++bitsize;
						threshold <<= 1;
					}
	
					encDict.push_back(lzwNode);	
				}

				// Move current symbol into the previous one
				previousSymbol = currentSymbol;	    	    
			}
		}

		writer->writeNBits(bitsize, previousSymbol);
	}

	writer->eof();

	return EXCOM_ERR_OK;
}

int CompLZW::runDecompress() {
	int i;

	unsigned long input;
	unsigned newCode;
	unsigned oldCode;

	bool matchFound;

	lzw_node_dec_t lzwNode;

	vector<unsigned>::const_reverse_iterator it;

	initializeDictionary(OPERATION_DECOMPRESS);

	if (reader->readNBits(bitsize, &input) != EOF) {
		oldCode = (unsigned)input;
		writer->writeByte(oldCode);
		if (decDict.size() != dict_capacity) {
			++bitsize;
			threshold <<= 1;
		}
		while (reader->readNBits(bitsize, &input) != EOF) {
			newCode = (unsigned)input;
			if (newCode < decDict.size()) { // If the code exists in dictionary
				if (!getCode(newCode)) {
					return EXCOM_ERR_FORMAT;
				}
				matchFound = true;
			} else {
				if (!getCode(oldCode)) {
					return EXCOM_ERR_FORMAT;
				}
				matchFound = false;
			}

			for (it = decodedString.rbegin(); it != decodedString.rend(); it++) {
				(this->*writeSymbol)(*it);
			}	    
			if (!matchFound) {
				(this->*writeSymbol)(decodedString.back());
			}	
	
			if (decDict.size() == dict_capacity) {
				initializeDictionary(OPERATION_DECOMPRESS);
				if (decDict.size() != dict_capacity) {
					++bitsize;
					threshold <<= 1;
				}
			}
			else {
				lzwNode.symbol = decodedString.back();
				lzwNode.prefixIndex = oldCode;
				
				decDict.push_back(lzwNode);
				
				if (decDict.size() == dict_capacity) {
					bitsize = char_bitsize;
				}
				else if (decDict.size() >= threshold) {
					++bitsize;
					threshold <<= 1;
				}				
			}

			oldCode = newCode;
		}
	}

	writer->eof();

	return EXCOM_ERR_OK;
}


int CompLZW::run()
{
	int res;
	res = checkConnection();
	if (res != EXCOM_ERR_OK) return EXCOM_ERR_CHAIN;

#ifdef PERF_MON
	PERF_START
#endif

	if (operation == EXCOM_OP_COMPRESS) {
		res = runCompress();
	} else {
		res = runDecompress();
	}


#ifdef PERF_MON
	PERF_STOP
#endif

	return res;
}
