/*
 * File:       method/acb/SuffixArray.cpp
 * Purpose:    Data structure for ACB compression method (Associative Coder of Buyanovsky)
 * Author:     Michal Valach <valacmic@fel.cvut.cz>, 2010
 *             diploma thesis at Faculty of Electrical Engineering,
 *             Czech Technical University in Prague
 * Diploma thesis leader: doc. Ing. Jan Holub, Ph.D.
 *
 * Copyright (C) 2010 Michal Valach
 *
 * This file is part of ACB module linked to the ExCom library.
 *
 * ACB module is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ACB module is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with ACB module or with the ExCom library. If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "SuffixArray.hpp"

SuffixArray::SuffixArray(){
    index = new dictionary_t();
    for(int i = 0; i < ALPHABET_SIZE + 1;  i++) {
	    dictionary_by_symbol_t* empty = new dictionary_by_symbol_t(0);
    	    empty->reserve(DEFAULT_INDEX_CAPACITY);
	    index->push_back(empty);
    }
    // insert empty context/content into index
    (*index)[0]->push_back(-1);
};

SuffixArray::~SuffixArray(){
    for (dictionary_iterator_t it = index->begin(); it < index->end(); ++it) {
        (*it)->clear();
        delete (*it);
        (*it) = NULL;
    }

    index->clear();
    delete index;
    index = NULL;
};

/*
 * Returns index of context. Index represents position, where new index should come to.
 * So if it returns 0, it should be first, if it returns 1, it should be second etc.
 * If you are using this method to search best fitting context, you have to
 * decrement result by 1 only if result is greater than 1.
 */
long SuffixArray::searchContext(dictionary_by_symbol_t* &subIndex, long  position) {
#if SUFFIX_STRUCTURE_PERF
	binarySearchCount++;
	clock_t method_begin = clock ();
#endif
    long  end = subIndex->size();
    long  middle;
    long  begin = 0;
    long  currentBufferIndex = position - 1; // -1 for ignore last character
    uint16_t* currentDataPosition = data + currentBufferIndex;
    while (begin < end) {
        middle = (begin + end) >> 1;
        // I do not have to compare with last character, since I already know
        // it is the same one for the whole data

        // data im matching to - already compressed datas
        long  positionInContext = (*subIndex)[middle] - 1; // -1 for ignore last character
        // data im trying to match - to be compressed data
        
        uint16_t* context = data + positionInContext;
	uint16_t* current = currentDataPosition;

	int16_t ret = 0;
	// Check how many characters are the same
	while (!(ret = *current - *context)) {
		context--;
		current--;
	}
        if (ret < 0) {
            end = middle;
        } else {
            begin = middle + 1;
        }
    }
#if SUFFIX_STRUCTURE_PERF
	binarySearchTime += (clock() - method_begin);
#endif
    return end;
}

void SuffixArray::search(long position, int &distance, unsigned int &contentLength) {
#if SUFFIX_STRUCTURE_PERF
	searchContextCount++;
	clock_t method_begin = clock ();
#endif
    unsigned char startingCharacter = data[position];
    unsigned int pos = ((unsigned int) startingCharacter) + 1;
    long contextIndex = searchContext((*index)[pos], position);
    if(contextIndex > 0){
        contextIndex--;
    }

#if SUFFIX_STRUCTURE_PERF
	searchContextTime += (clock() - method_begin);
	searchContentCount++;
	method_begin = clock ();
#endif
        
    // To search only in content and not last char of context
    position++;
    distance = 0;
    contentLength = 0;
    uint16_t* currentDataPosition = data + position;

    // search for content lower than context
    dictionary_by_symbol_t* subIndex = (*index)[pos];
    long subIndexPosition = contextIndex;    
    unsigned int currentDistance = 0;
    while (currentDistance <= maxDistance && pos > 0) {
        if (subIndexPosition < 0) {
            pos--;
            subIndex = (*index)[pos];
            subIndexPosition = subIndex->size() - 1;
            if(subIndexPosition <= 0) {
		// may loop through not yet inicialized buffer
                continue;
            }
        }        
        long length = 0;
        long contentStart = (*subIndex)[subIndexPosition] + 1;
        uint16_t* content = data + contentStart;
	uint16_t* current = currentDataPosition;

	// Check how many characters are the same
	while (*content++ == *current++
		&& (contentStart < position)
		&& (length < maxContentLength) // max length is defined, because storing data using DHuff has maximum number upper bound
		) {
		length++;
		contentStart++;
	}
        // If there are more characters identical, change best found
        if (length > contentLength) {
	        contentLength = length;
	        distance = currentDistance;
        }
        subIndexPosition--;
	currentDistance++;
    }
    distance = -distance;

    // search for content greater than context
    pos = ((unsigned int) startingCharacter) + 1;
    subIndex = (*index)[pos];
    subIndexPosition = contextIndex + 1;
    currentDistance = 1;    
    int maxPos = index->size() - 1;
    unsigned long subIndexSize = subIndex->size();
    while (currentDistance <= maxDistance && pos < maxPos) {
        if (subIndexPosition == subIndexSize) {
            pos++;
            subIndex = (*index)[pos];
            subIndexPosition = 0;
	    subIndexSize = subIndex->size();
            if(subIndexSize == 0) {
		// may loop through not yet inicialized buffer
                continue;
            }
        }        
        long length = 0;
        long contentStart = (*subIndex)[subIndexPosition] + 1;
        uint16_t* content = data + contentStart;
	uint16_t* current = currentDataPosition;

	// Check how many characters are the same
	while (*content++ == *current++
		&& (contentStart < position)
		&& (length < maxContentLength) // max length is defined, because storing data using DHuff has maximum number upper bound
		) {
		length++;
		contentStart++;
	}
        if (length > contentLength) {
	        contentLength = length;
	        distance = currentDistance;
        }
        subIndexPosition++;
	currentDistance++;
    }
#if SUFFIX_STRUCTURE_PERF
	searchContentTime += (clock() - method_begin);
#endif
}

void SuffixArray::translate(long position, int distance, unsigned int contentLength, unsigned char nextCharacter) {
#if SUFFIX_STRUCTURE_PERF
	translateCount++;
	clock_t method_begin = clock();
#endif
	// If content length is 0, there is no need to search in context, nor content
	// since I am only adding one new character
	if(contentLength > 0) {
		unsigned char startingCharacter = data[position];
    		unsigned int pos = ((unsigned int) startingCharacter) + 1;
    		long contextIndex = searchContext((*index)[pos], position);
		if(contextIndex > 0){
			contextIndex--;
		}

		long contentIndex = contextIndex + distance;
		dictionary_by_symbol_t* subIndex = (*index)[pos];
		long subIndexSize = subIndex->size();
		// Search left from context
		while(contentIndex < 0 || subIndexSize == 0) {
			pos--;
			subIndex = (*index)[pos];
			subIndexSize = subIndex->size();
			contentIndex += subIndexSize;
		}
		// Search right from context
		while(contentIndex >= subIndexSize || subIndexSize == 0) {			
			pos++;
			contentIndex -= subIndexSize;
			subIndex = (*index)[pos];
			subIndexSize = subIndex->size();
		}
		long contentPosition = (*subIndex)[contentIndex];
		addData(contentPosition + 1 , contentLength);
	}
	addData(nextCharacter);
        updateIndex(position, contentLength + 1);
#if SUFFIX_STRUCTURE_PERF
	translateTime += (clock() - method_begin);
#endif
}

unsigned char* SuffixArray::clear(long position, bool needData)  { 
    for (dictionary_iterator_t it = index->begin(); it < index->end(); ++it) {
        (*it)->clear();
    }
    // insert empty context/content into index
    (*index)[0]->push_back(-1);

    return SuffixStructure::clear(position, needData);
}

void SuffixArray::updateIndex(long position, long length) {
#if SUFFIX_STRUCTURE_PERF
	updateIndexCount++;
	clock_t method_begin = clock ();
#endif
    if(IS_FULL) return;
    currentSize += length;
    for (long i = position; i < position + length; i++) {
        unsigned char character = data[i];
        dictionary_by_symbol_t* subIndex = (*index)[(unsigned int) character + 1];

	long positionInSubIndex = searchContext(subIndex, i);

        subIndex->insert(subIndex->begin() + positionInSubIndex, i);
//	printIndex(i);
    }    
#if SUFFIX_STRUCTURE_PERF
	updateIndexTime += (clock() - method_begin);
#endif
}

#if SA_DEBUG
void SuffixArray::printIndex(long  position){
    std::cout << "Suffix array : " << position << std::endl;
    for (int i = 0; i < position + 2; i++) {
        std::cout << (char) data[i];
    }
    std::cout << "|";
    for (int i = position + 2; i <= dataSize; i++) {
        std::cout << (char) data[i];
    }
    std::cout << std::endl;


    if(position > 5){
        for (int i = 0; i < position - 6; i++) {
            std::cout << " ";
        }
    }
    std::cout << " context|content" << std::endl;
    long count = 0;
    for (int i = 0; i < index->size(); i++) {
        for (int j = 0; j < (*index)[i]->size(); j++) {
           
            for (int k = 0; k <= position - (*(*index)[i])[j]; k++) {
                std::cout << " ";
            }
            for (int k = 0; k <= (*(*index)[i])[j]; k++) {
                std::cout << (char) data[k];
            }
            std::cout << "|";
            for (int k = (*(*index)[i])[j] + 1; k <= position + 1; k++) {
                std::cout << (char) data[k];
            }
            for (int k = 0; k <= (*(*index)[i])[j]; k++) {
                std::cout << " ";
            }
            std::cout << " : " << count++ << " : " << (*(*index)[i])[j] << std::endl;
        }
    }
    std::cout << " ----------------------------- " << std::endl;
}
#endif
